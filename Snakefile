modelname = "v4.1.2"
steadystate = True

path_data = "data"  # no trailing slash

# dt thickness correction
layermodel_increase_thickness = True
layermodel_dt_models = ["dt_V0.27"]  # list of previous models from which dt results are saved. All are used to correct thickness
if not layermodel_increase_thickness:
    layermodel_dt_models = []
min_dt = 10. # days
min_thick = 1.0 # m
min_thick_dt = 3.0 # m
step_thick_dt = 1.0 # m
rivdrn_as_river = False # add 'rivdrn' inffactor as Rivers (stage=bot) or Drainage

visualize = False
upload = True
LHM_MDL = list(range(1,9))
kd_l = list(range(1,9))
c_l = list(range(1,8))
ghb_l = list(range(1,9))
nlayers_LHM = list(range(1,9))
nlayers_LHM34 = list(range(1,8))
nlayers_LHMfs = list(range(1,40))
nlayers_conc_old = list(range(1,32))
RIVMONTHS = [f"{y}{m:02d}01" for m in range(1,13) for y in range(1980,2019)] + [ "20190101","20190201"]
Randflux = list(range(1,9))
twolay = list(range(1,3))


rule layermodel:
    input:
        path_coastline_vector = f"{path_data}/1-external/coastline/nl_imergis_kustlijn_2018.shp",
        path_bathymetry = f"{path_data}/1-external/offshore/bathymetry.tif",
        path_aquifer_bot = expand(path_data+"/1-external/layermodel/bot/MDL_BOT_L{number}.IDF", number=LHM_MDL),
        path_aquifer_top = expand(path_data+"/1-external/layermodel/top/MDL_TOP_L{number}.IDF", number=LHM_MDL),
        path_kd = expand(path_data+"/1-external/kd/MDL_KD_l{number}.IDF", number=kd_l), 
        path_c = expand(path_data+"/1-external/c/MDL_C_l{number}.IDF", number=c_l),
        path_ibound = expand(path_data+"/1-external/bnd/ibound_l{number}.IDF", number=nlayers_LHM),
        path_ghb = f"{path_data}/1-external/ghb_corr/ghb_head_l1.idf",
        path_aq_top_lhm34 = expand(path_data+"/1-external/LHM3.4-zoet/top/top_l{number}.IDF", number=nlayers_LHM34),
        path_aq_bot_lhm34 = expand(path_data+"/1-external/LHM3.4-zoet/bot/bot_l{number}.IDF", number=nlayers_LHM34),
        path_randflux = expand(path_data+"/1-external/wel/Randflux_L{number}.ipf", number=ghb_l),
        #paths_dt = expand(path_data+"/2-interim/{dt_model}.nc", dt_model=layermodel_dt_models),
        paths_dt = f"{path_data}/2-interim/dt_V0.27.nc",
        path_script = "src/1-prepare/layermodel.py",  
    params:
        increase_thickness=layermodel_increase_thickness,
        min_dt=min_dt,
        min_thick=min_thick, 
        min_thick_dt=min_thick_dt,
        step_thick_dt=step_thick_dt,
    output:
        path_topbot_orig_out = f"{path_data}/2-interim/top_bot_orig.nc",
        path_conductivity_out = f"{path_data}/2-interim/conductivity.nc",
        path_layermodel_out = f"{path_data}/2-interim/layermodel.nc",
        path_topbot_out = f"{path_data}/2-interim/top_bot.nc",
    script:
        "src/1-prepare/layermodel.py"

rule prior_adaptations:
    input:
        path_topbot_orig = f"{path_data}/2-interim/top_bot_orig.nc",
        path_shd = expand(f"{path_data}/1-external/shd/HEAD_STEADY-STATE_l{{number}}.idf", number = LHM_MDL),
        path_ghb_cond = f"{path_data}/1-external/ghb_corr/ghb_cond_l1.idf",
        path_ghb_head = f"{path_data}/1-external/ghb_corr/ghb_head_l1.idf",
        path_ghb_dens = f"{path_data}/1-external/ghb_corr/ghb_dens_l1.idf",
        path_riv_H_cond = expand(f"{path_data}/1-external/riv/hoofdwater/COND_HL_L{{number}}.idf",number =twolay),
        path_riv_H_head = f"{path_data}/1-external/riv/hoofdwater/steady-state/MEAN_PEILH_2011_2018.IDF",
        path_riv_H_bot = expand(f"{path_data}/1-external/riv/hoofdwater/steady-state/BOTH_J_L{{number}}.idf", number = twolay),
        path_riv_H_dens = f"{path_data}/1-external/riv/hoofdwater/riv_h_dens_L1.idf",
        path_riv_P_cond = f"{path_data}/1-external/riv/regionaal/cond_p_l0.IDF",
        path_riv_P_head = f"{path_data}/1-external/riv/regionaal/steady-state/PEIL_P1J_250.IDF",
        path_riv_P_bot = f"{path_data}/1-external/riv/regionaal/steady-state/BODH_P1J_250.IDF",
        path_riv_S_cond = f"{path_data}/1-external/riv/regionaal/cond_s_l0.IDF",
        path_riv_S_head = f"{path_data}/1-external/riv/regionaal/steady-state/PEIL_S1J_250.IDF",
        path_riv_S_bot = f"{path_data}/1-external/riv/regionaal/steady-state/BODH_S1J_250.IDF",
        path_riv_T_cond = f"{path_data}/1-external/riv/regionaal/cond_t_l0.IDF",
        path_riv_T_head = f"{path_data}/1-external/riv/regionaal/steady-state/PEIL_T1J_250.IDF",
        path_riv_T_bot = f"{path_data}/1-external/riv/regionaal/steady-state/BODH_T1J_250.IDF",
        path_drn_B_lvl = f"{path_data}/1-external/drn/BODH_B_250.IDF",
        path_drn_B_cond = f"{path_data}/1-external/drn/COND_B_250.IDF",
        path_drn_mvgrep_lvl =  f"{path_data}/1-external/drn/BODH_BRP2012_MVGREP_250.IDF",
        path_drn_mvgrep_cond = f"{path_data}/1-external/drn/COND_BRP2012_MVGREP_250.IDF",
        path_drn_SOF_lvl = f"{path_data}/1-external/drn/BODH_SOF_250.IDF",
        path_drn_SOF_cond  = f"{path_data}/1-external/drn/COND_SOF_250.IDF",
        path_well_nhi = f"{path_data}/1-external/wel/nhi/wel_nhi_all.ipf",
        path_script = "src/1-prepare/prior_adaptations.py",
    params:
        path_external = f"{path_data}/1-external",
        path_interim = f"{path_data}/2-interim",
    output:
        f"{path_data}/2-interim/ghb_ss_orig.nc",
        #"data/2-interim/ghb_ss_orig_corr.nc",
        f"{path_data}/2-interim/riv_h_ss_orig.nc",
        f"{path_data}/2-interim/riv_h_ss_orig_corr.nc",
        f"{path_data}/2-interim/riv_p_ss_orig.nc",
        f"{path_data}/2-interim/riv_p_ss_orig_corr.nc",
        f"{path_data}/2-interim/riv_s_ss_orig.nc",
        f"{path_data}/2-interim/riv_t_ss_orig.nc",
        f"{path_data}/2-interim/drn_b_orig.nc",
        f"{path_data}/2-interim/drn_mvgrep_orig.nc",
        f"{path_data}/2-interim/drn_sof_orig.nc",  
        #"data/2-interim/drn_b_orig_corr.nc",
        #"data/2-interim/drn_mvgrep_orig_corr.nc",
        #"data/2-interim/drn_sof_orig_corr.nc",
        f"{path_data}/2-interim/shd_corr.nc",
        f"{path_data}/1-external/wel/wel_nhi_all.ipf",
    script:
        "src/1-prepare/prior_adaptations.py"

rule bnd:
    input:
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_script = "src/1-prepare/bnd.py",        
    output:
        path_bnd_out = f"{path_data}/2-interim/bnd.nc"
    script:
        "src/1-prepare/bnd.py"

rule template:
    input:
        path_ibound = f"{path_data}/2-interim/bnd.nc",
        path_script = "src/1-prepare/template.py",
    output:
        path_tmpl_out = f"{path_data}/2-interim/template.nc",
        path_tmpl2d_out = f"{path_data}/2-interim/template_2d.nc",
    script:
        "src/1-prepare/template.py"
        
rule is_water:
    input:
        path_geotop_water = f"{path_data}/1-external/water/GEOTOP_watervlak.tif",
        path_coastline = f"{path_data}/1-external/water/nl_imergis_kustlijn_2018/nl_imergis_kustlijn_2018.shp",
        path_template = f"{path_data}/2-interim/template.nc",
        path_template_2d = f"{path_data}/2-interim/template_2d.nc",
        path_script = "src/1-prepare/is_water.py",        
    output:
        path_water_out = f"{path_data}/2-interim/water_masks.nc"
    script:
        "src/1-prepare/is_water.py"

rule sconc:
    input:
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_sconc = f"{path_data}/1-external/3D-chloride/3dchloride_newversion.nc",
        path_water = f"{path_data}/2-interim/water_masks.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_water_systems = f"{path_data}/1-external/2D-oppwater-chloride/surfwater_boundary.shp",
        path_cl_kriging = f"{path_data}/1-external/2D-oppwater-chloride/clconc_oppw.tif",
        path_cl_delft3d = f"{path_data}/1-external/2D-oppwater-chloride/chloride_gL_mean_2012.tif",
        path_template_2d = f"{path_data}/2-interim/template_2d.nc",        
        path_script = "src/1-prepare/sconc.py",        
    output:
        path_sconc_out = f"{path_data}/2-interim/starting_concentration.nc",
    script:
        "src/1-prepare/sconc.py"

rule correct_salinity:
    input:
        path_cl = f"{path_data}/2-interim/starting_concentration.nc",
        path_conductivity = f"{path_data}/2-interim/conductivity.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_script = "src/1-prepare/correct_3dsalinity.py",   
    output:
        path_sconc_out = f"{path_data}/2-interim/concentration_corrected.nc",
    script:
        "src/1-prepare/correct_3dsalinity.py"

#rule recharge:
#    input:
#        path_Ebs = f"{path_data}/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_msw_Ebs.nc",
#        path_qinf = f"{path_data}/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_msw_qinf.nc",
#        path_Tact = f"{path_data}/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_msw_Tact.nc",
#        path_bdgqmodf = f"{path_data}/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_bdgqmodf.nc",
#        path_cell_area = f"{path_data}/1-external/metaswap/run_NHI4.1.1_2009_2018/msw_Ebs/area_L1.idf",
#        path_water = f"{path_data}/2-interim/water_masks.nc",
#        path_template_2d = f"{path_data}/2-interim/template_2d.nc",
#        path_bnd = f"{path_data}/2-interim/bnd.nc",
#        path_bnd_lhm34 = f"{path_data}/1-external/bnd/lhm34/ibound_l1.idf",
#        path_script = "src/1-prepare/recharge.py",     
#    output:
#        path_rch_out = f"{path_data}/2-interim/recharge.nc",
#    threads: workflow.cores
#    resources:
#        mem_mb=100
#    script:
#        "src/1-prepare/recharge.py"

rule recharge:
    input:
       path_bnd = f"{path_data}/2-interim/bnd.nc",
       path_rchlhm_md = f"{path_data}/1-external/rch/groundwater_recharge_average_2011_2018_md.idf",
       path_script = "src/1-prepare/recharge_from_rate.py",     
    params:
        rch_rate_max = 1e5, # no maximum rate
    output:
        path_rch_out = f"{path_data}/2-interim/recharge.nc",
    script:
        "src/1-prepare/recharge_from_rate.py"

rule anihfb:
    input:
        path_template = f"{path_data}/2-interim/template.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_ani_factor = expand(f"{path_data}/1-external/ani/fct_laag{{number}}.idf", number=[2,3,4,5]),
        path_ani_angle = expand(f"{path_data}/1-external/ani/hk_laag{{number}}.idf", number=[2,3,4,5]),
        path_hfb = expand(f"data/1-external/hfb/hfb_l{{number}}.gen", number=[2,3,4,5,6,7]),
        path_hfb_orig = f"{path_data}/1-external/hfb/0_LHM42_L8_KD_C_SIMTYPE_6.HFB7",
        path_script = "src/1-prepare/anihfb.py",
    params:
        do_hfb_check = False,
    output:
        path_ani_out = f"{path_data}/2-interim/ani.nc",
        path_hfb_out = f"{path_data}/2-interim/hfb.hfb6",
    script:
        "src/1-prepare/anihfb.py"
        
rule ghb:
    input:
        path_template_2d = f"{path_data}/2-interim/template_2d.nc",
        path_template = f"{path_data}/2-interim/template.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_conc_corr = f"{path_data}/2-interim/concentration_corrected.nc",
        path_start_conc = f"{path_data}/2-interim/starting_concentration.nc",
        path_ghb_ss_corr = f"{path_data}/2-interim/ghb_ss_orig.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_script = "src/1-prepare/ghb.py",
    output:
        path_ghb_out = f"{path_data}/2-interim/ghb.nc",
    script:
        "src/1-prepare/ghb.py"

rule river:
    input:
        path_template = f"{path_data}/2-interim/template.nc",
        path_template_2d = f"{path_data}/2-interim/template_2d.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_conc_2d = f"{path_data}/2-interim/starting_concentration.nc",
        path_GHB_dens_LHM = f"{path_data}/1-external/ghb/ghb_dens_l1.idf",
        path_riv_h = f"{path_data}/2-interim/riv_h_ss_orig_corr.nc",
        path_riv_p = f"{path_data}/2-interim/riv_p_ss_orig_corr.nc",
        path_riv_s = f"{path_data}/2-interim/riv_s_ss_orig.nc",
        path_riv_t = f"{path_data}/2-interim/riv_t_ss_orig.nc",
        path_riv_h1_inff = f"{path_data}/1-external/riv/hoofdwater/infmz_h_250_l1.idf",
        path_riv_h2_inff = f"{path_data}/1-external/riv/hoofdwater/infmz_h_250_l2.idf",
        path_riv_p_inff = f"{path_data}/1-external/riv/regionaal/INFMZ_P1_250.IDF",
        path_riv_s_inff = f"{path_data}/1-external/riv/regionaal/INFMZ_S1_250.IDF",
        path_riv_t_inff = f"{path_data}/1-external/riv/regionaal/INFMZ_T1_250.IDF",
        path_ghb = f"{path_data}/2-interim/ghb.nc",
        path_script = "src/1-prepare/river.py",
    params:
        rivdrn_as_river = rivdrn_as_river,
        path_interim = f"{path_data}/2-interim",
    threads: workflow.cores
    resources:
        mem_mb=500
    output:
        f"{path_data}/2-interim/river_h.nc",
        f"{path_data}/2-interim/river_p.nc",
        f"{path_data}/2-interim/river_s.nc",
        f"{path_data}/2-interim/river_h_drn.nc",
        f"{path_data}/2-interim/river_p_drn.nc",
        f"{path_data}/2-interim/river_s_drn.nc",
        f"{path_data}/2-interim/river_t.nc",
    script:
        "src/1-prepare/river.py"

rule drainage:
    input:
        path_template_2d = f"{path_data}/2-interim/template_2d.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_ahn = f"{path_data}/1-external/AHN/AHN_F250.IDF",
        path_water = f"{path_data}/2-interim/water_masks.nc",
        path_ghb = f"{path_data}/2-interim/ghb.nc",
        path_drn_b = f"{path_data}/2-interim/drn_b_orig.nc",
        path_drn_mvgrep = f"{path_data}/2-interim/drn_mvgrep_orig.nc",
        path_drn_sof = f"{path_data}/2-interim/drn_sof_orig.nc",
        path_script = "src/1-prepare/drainage.py",
    output:
        path_drn_b_out = f"{path_data}/2-interim/drainage_b.nc",
        path_drn_mvgrep_out = f"{path_data}/2-interim/drainage_mvgrep.nc",
        path_drn_sof_out = f"{path_data}/2-interim/drainage_sof.nc",
    script:
        "src/1-prepare/drainage.py"

rule boils:
    input:
        path_template_2d = f"{path_data}/2-interim/template_2d.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_wellen_w = f"{path_data}/1-external/riv/wellen/peilw_wel.idf",
        path_wellen_z = f"{path_data}/1-external/riv/wellen/peilz_wel.idf",
        path_wellen_cond = f"{path_data}/1-external/riv/wellen/cond_wel.idf",
        path_script = "src/1-prepare/boils.py",
    output:
        path_boils_out = f"{path_data}/2-interim/boils.nc"
    script:
        "src/1-prepare/boils.py"

rule wel:
    input:
        path_topbot_lhmfresh = f"{path_data}/2-interim/top_bot_orig.nc",
        path_conductivity = f"{path_data}/2-interim/conductivity.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_wel_prioradapt = f"{path_data}/1-external/wel/wel_nhi_all.ipf",
        path_wel = f"{path_data}/1-external/wel",
        path_script = "src/1-prepare/wel.py",
    params:
        path_interim = f"{path_data}/2-interim",
    output:
        f"{path_data}/2-interim/wells_not_corrected.csv",    
        f"{path_data}/2-interim/wells_corrected.csv",
        f"{path_data}/2-interim/wel.nc",
    script:
        "src/1-prepare/wel.py"


rule basbtn:
    input:
        path_template = f"{path_data}/2-interim/template.nc",
        #f"{path_data}/2-interim/layermodel_corr.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_bnd = f"{path_data}/2-interim/bnd.nc",
        path_conc_corr = f"{path_data}/2-interim/concentration_corrected.nc",
        path_bnd_lhm = expand(path_data+"/1-external/bnd/ibound_l{number}.IDF", number = LHM_MDL),
        path_shd_lhm = f"{path_data}/2-interim/shd_corr.nc",
        path_script = "src/1-prepare/basbtn.py",        
    output:
        path_bnd_out = f"{path_data}/2-interim/bnd_corr.nc",
        path_bas_out = f"{path_data}/2-interim/bas.nc",
        path_btn_out = f"{path_data}/2-interim/btn.nc",
    script:
        "src/1-prepare/basbtn.py"

rule lpf:
    input:
        path_template = f"{path_data}/2-interim/template.nc",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_conductivity = f"{path_data}/2-interim/conductivity.nc",
        path_bnd_corr = f"{path_data}/2-interim/bnd_corr.nc",
        path_script = "src/1-prepare/lpf.py",        
    output:
        path_lpf_out = f"{path_data}/2-interim/lpf.nc"
    script:
        "src/1-prepare/lpf.py"

rule adaptations:
    input: 
        path_conductivity = f"{path_data}/2-interim/lpf.nc",
        path_kv_changes = f"{path_data}/1-external/adaptations/lpf/K_VERTICAL_L10_CHANGE.IDF",
        path_ibound = f"{path_data}/2-interim/bas.nc",
        path_drn_sof = f"{path_data}/2-interim/drainage_sof.nc",
        path_drn_mvgrp = f"{path_data}/2-interim/drainage_mvgrep.nc",
        path_drn_b = f"{path_data}/2-interim/drainage_b.nc",
        path_drn_rivp = f"{path_data}/2-interim/river_p_drn.nc",
        path_drn_rivs =f"{path_data}/2-interim/river_s_drn.nc",
        path_riv_p = f"{path_data}/2-interim/river_p.nc",
        path_rch = f"{path_data}/2-interim/recharge.nc",
        path_rch_change = f"{path_data}/1-external/adaptations/rch/RATE_CHANGE.IDF",
        path_drn_sof_cond_l3 = f"{path_data}/1-external/adaptations/drn_sof/CONDUCTANCE_L3_CHANGE.IDF",
        path_drn_sof_cond_l6 = f"{path_data}/1-external/adaptations/drn_sof/CONDUCTANCE_L6_CHANGE.IDF",
        path_drn_sof_cond_l9 = f"{path_data}/1-external/adaptations/drn_sof/CONDUCTANCE_L9_CHANGE.IDF",
        path_drn_mvgrp_cond = f"{path_data}/1-external/adaptations/drn_mvgrp/CONDUCTANCE_L9_CHANGE.IDF",
        path_drn_mvgrp_elev = f"{path_data}/1-external/adaptations/drn_mvgrp/ELEVATION_L9_CHANGE.IDF",
        path_riv_drnp_cond_l3 = f"{path_data}/1-external/adaptations/riv_p_drn/CONDUCTANCE_L3_CHANGE.IDF",
        path_riv_drnp_cond_l9 = f"{path_data}/1-external/adaptations/riv_p_drn/CONDUCTANCE_L9_CHANGE.IDF",
        path_riv_drns_cond_l9 = f"{path_data}/1-external/adaptations/riv_s_drn/CONDUCTANCE_L9_CHANGE.IDF",
        path_riv_p_cond_l4 = f"{path_data}/1-external/adaptations/riv_p/CONDUCTANCE_L4_CHANGE.IDF",
        path_riv_p_cond_l9 = f"{path_data}/1-external/adaptations/riv_p/CONDUCTANCE_L9_CHANGE.IDF",
        path_script = "src/1-prepare/adaptations.py",
    params:
        path_data = path_data
    output:
        path_lpf_adapt = f"{path_data}/2-interim/adaptations/lpf.nc",
        path_rch_adapt = f"{path_data}/2-interim/adaptations/recharge.nc",
        path_drnsof_adapt = f"{path_data}/2-interim/adaptations/drainage_sof.nc",
        path_drnmvgrep_adapt = f"{path_data}/2-interim/adaptations/drainage_mvgrep.nc",
        path_rivpdrn_adapt = f"{path_data}/2-interim/adaptations/river_p_drn.nc",
        path_rivsdrn_adapt = f"{path_data}/2-interim/adaptations/river_s_drn.nc",
        path_rivp_adapt = f"{path_data}/2-interim/adaptations/river_p.nc",
    script:
        "src/1-prepare/adaptations.py"

rmm_times = [f"20160705_{t:02d}00" for t in range(12,24)]+["20160706_0000"]
rule prepare_scenario_kpslr:
    input:
        path_ghb = f"{path_data}/2-interim/ghb.nc",
        path_openclose = f"{path_data}/1-external/kpslr/open_close.shp",
        path_rivpoints = f"{path_data}/1-external/kpslr/ObservationPointsCorr.shp",
        paths_rivpointsslr = expand(path_data+"/1-external/kpslr/Water level mean increase at {k} m SLR.csv", k=[1, 2, 3]),
        paths_rmm_csv = expand(path_data+"/1-external/kpslr/salinity_RMM/saliniteit_bodemlaag_plus{k}m_{t}.xyz", k=[0, 4], t=rmm_times),
        paths_subsidence = expand(path_data+"/1-external/kpslr/subsidence/bodemdaling_incl_ogzw_2020_{k}.tif", k=[2050,2100]),
        script="src/1-prepare/prepare_kpslr.py",
    params:
        startyear = 2000,
    output:
        path_outrivh = f"{path_data}/2-interim/rivh_kpslr.nc",
        path_outconc =f"{path_data}/2-interim/conc_rmm_kpslr.nc",
        path_outsubs = f"{path_data}/2-interim/subsidence.nc",
    script:
        "src/1-prepare/prepare_kpslr.py"

rule scenario_kpslr:
    input:
        path_slrcurve = f"{path_data}/1-external/kpslr/ZSScurve_vos.xlsx",
        path_layermodel = f"{path_data}/2-interim/layermodel.nc",
        path_ghb = f"{path_data}/2-interim/ghb.nc",
        path_rivh = f"{path_data}/2-interim/river_h.nc",
        path_rivh_drn = f"{path_data}/2-interim/river_h_drn.nc",
        path_rivhslr = f"{path_data}/2-interim/rivh_kpslr.nc", # ==> from prepare_kpslr
        path_concslr = f"{path_data}/2-interim/conc_rmm_kpslr.nc", # ==> from prepare_kpslr
        path_subsidence = f"{path_data}/2-interim/subsidence.nc", # ==> from prepare_kpslr
        path_rivp = f"{path_data}/2-interim/adaptations/river_p.nc",
        path_rivp_drn = f"{path_data}/2-interim/adaptations/river_p_drn.nc",
        path_rivs = f"{path_data}/2-interim/river_s.nc",
        path_rivs_drn = f"{path_data}/2-interim/adaptations/river_s_drn.nc",
        path_rivt = f"{path_data}/2-interim/river_t.nc",
        path_drn_b = f"{path_data}/2-interim/drainage_b.nc",
        path_drn_mvgrep = f"{path_data}/2-interim/adaptations/drainage_mvgrep.nc",
        path_drn_sof = f"{path_data}/2-interim/adaptations/drainage_sof.nc",
        path_drn_boils = f"{path_data}/2-interim/boils.nc",
        script="src/1-prepare/scenario_kpslr.py",
    params:
        path_interim = f"{path_data}/2-interim",
        startyear = 2000,
        max_slr = 3.,
        slr_horizons = [0.5, 1., 2., 3.],
        freq = "5AS",
    threads: workflow.cores
    output:
        directory(f"{path_data}/2-interim/ghb_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_h_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_h_drn_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_p_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_p_drn_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_s_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_s_drn_kpslr.zarr"),
        directory(f"{path_data}/2-interim/river_t_kpslr.zarr"),
        directory(f"{path_data}/2-interim/drainage_b_kpslr.zarr"),
        directory(f"{path_data}/2-interim/drainage_mvgrep_kpslr.zarr"),
        directory(f"{path_data}/2-interim/drainage_sof_kpslr.zarr"),
        directory(f"{path_data}/2-interim/boils_kpslr.zarr"),
    script:
        "src/1-prepare/scenario_kpslr.py"

rule build_model:
    input:
        path_bas = f"{path_data}/2-interim/bas.nc",
        path_lpf = f"{path_data}/2-interim/adaptations/lpf.nc",
        path_btn = f"{path_data}/2-interim/btn.nc",
        path_ani = f"{path_data}/2-interim/ani.nc",
        path_hfb = f"{path_data}/2-interim/hfb.hfb6",
        path_rch = f"{path_data}/2-interim/adaptations/recharge.nc",
        path_drn_b = f"{path_data}/2-interim/drainage_b.nc",
        path_drn_mvgrep = f"{path_data}/2-interim/adaptations/drainage_mvgrep.nc",
        path_drn_sof = f"{path_data}/2-interim/adaptations/drainage_sof.nc",
        path_boils = f"{path_data}/2-interim/boils.nc",
        path_riv_h = f"{path_data}/2-interim/river_h.nc",
        path_riv_p = f"{path_data}/2-interim/adaptations/river_p.nc",
        path_riv_s = f"{path_data}/2-interim/river_s.nc",
        path_riv_t = f"{path_data}/2-interim/river_t.nc",
        path_riv_h_drn = f"{path_data}/2-interim/river_h_drn.nc",
        path_riv_p_drn = f"{path_data}/2-interim/adaptations/river_p_drn.nc",
        path_riv_s_drn = f"{path_data}/2-interim/adaptations/river_s_drn.nc",
        path_ghb = f"{path_data}/2-interim/ghb.nc",
        path_wel = f"{path_data}/2-interim/wel.nc",
        path_script = "src/2-build/build_model.py",
    params:
        modelname=modelname,
        steadystate=steadystate,
        visualize=visualize,
        rivdrn_as_river = rivdrn_as_river,
        resultdir_is_workdir=False,
        path_data=path_data,
    output:
        f"{path_data}/3-input/{modelname}/{modelname}.run",
    script:
        "src/2-build/build_model.py"

rule build_model_kpslr:
    input:
        path_bas = f"{path_data}/2-interim/bas.nc",
        path_lpf = f"{path_data}/2-interim/adaptations/lpf.nc",
        path_btn = f"{path_data}/2-interim/btn.nc",
        path_rch = f"{path_data}/2-interim/adaptations/recharge.nc",
        path_drn_b = f"{path_data}/2-interim/drainage_b_kpslr.zarr",
        path_drn_mvgrep = f"{path_data}/2-interim/drainage_mvgrep_kpslr.zarr",
        path_drn_sof = f"{path_data}/2-interim/drainage_sof_kpslr.zarr",
        path_boils = f"{path_data}/2-interim/boils_kpslr.zarr",
        path_riv_h = f"{path_data}/2-interim/river_h_kpslr.zarr",
        path_riv_p = f"{path_data}/2-interim/river_p_kpslr.zarr",
        path_riv_s = f"{path_data}/2-interim/river_s_kpslr.zarr",
        path_riv_t = f"{path_data}/2-interim/river_t_kpslr.zarr",
        path_riv_h_drn = f"{path_data}/2-interim/river_h_drn_kpslr.zarr",
        path_riv_p_drn = f"{path_data}/2-interim/river_p_drn_kpslr.zarr",
        path_riv_s_drn = f"{path_data}/2-interim/river_s_drn_kpslr.zarr",
        path_ghb = f"{path_data}/2-interim/ghb_kpslr.zarr",
        path_wel = f"{path_data}/2-interim/wel.nc",
        path_script = "src/2-build/build_model_kpslr.py",
    params:
        modelname=modelname,
        steadystate=steadystate,
        visualize=visualize,
        rivdrn_as_river = rivdrn_as_river,
        start="2000-01-01",
        end="2278-01-01",
        timestep=0,
        path_data=path_data,
    output:
        f"{path_data}/3-input/{modelname}/{modelname}.run",
        f"{path_data}/4-output/{modelname}/{modelname}.run",
    script:
        "src/2-build/build_model_kpslr.py"
"""
rule build_model_scenario:
    input:
        f"{path_data}/2-interim/bas.nc",
        f"{path_data}/2-interim/lpf.nc",
        f"{path_data}/2-interim/btn.nc",
        f"{path_data}/2-interim/recharge.nc",
        f"{path_data}/2-interim/drainage_b.nc",
        f"{path_data}/2-interim/drainage_mvgrep.nc",
        f"{path_data}/2-interim/drainage_sof_corr.nc",
        f"{path_data}/2-interim/drainage_wadden.nc",
        f"{path_data}/2-interim/boils.nc",
        f"{path_data}/2-interim/river_h.nc",
        f"{path_data}/2-interim/river_p_corr.nc",
        f"{path_data}/2-interim/river_s_corr.nc",
        f"{path_data}/2-interim/river_t.nc",
        f"{path_data}/2-interim/ghb_2085.nc",
        f"{path_data}/2-interim/wel.nc",
        "src/2-build/build_model.py",
    params:
        modelname=modelname,
        steadystate=steadystate,
        visualize=visualize,
    output:
        f"{path_data}/3-input/{modelname}_scenario.run"
    script:
        "src/2-build/build_model_scenario.py"
"""
rule prepare_run:
    input:
        f"{path_data}/3-input/{modelname}.run",
        "src/3-model/prepare_run.py",
    params:
        modelname=modelname,
        upload=upload,
    output:
        f"{path_data}/4-output/{modelname}.run",
        f"{path_data}/4-output/metadata.txt",
        f"{modelname}_modelinput.zip",
    script:
        "src/3-model/prepare_run.py"
         
#rule run_model:
#    input:
#        f"{path_data}/3-input/%s/%s.run" % (modelname,modelname),
#    params:
#        modelname=modelname,
#    output:
#        directory(f"{path_data}/4-output/%s/dcdt" % modelname),
#    script:
#        "src/3-model/run-model.py"

rule run_lhm_fresh:
    input:
        path_rch = f"{path_data}/2-interim/recharge.nc",
        path_script = "src/-model/run_lhm_fresh.py",
        path_lhmfresh = f"{path_data}/1-external",
        script = "src/3-model/run_lhm_fresh.py"
    params:
        modelname = modelname,
    output:
        f"{path_data}/4-output/lhmfresh_{modelname}/lhmfresh_{modelname}.run"
    script:
        "src/3-model/run_lhm_fresh.py"


#rule compare_lhm_fresh:
#    input:
#        f"{path_data}/4-output/lhmfresh_{modelname}/head/head_steady-state_l1.idf"
#        script = "src/-model/run_lhm_fresh.py",
#    params:
#        modelname = modelname,
#        path_lhmfresh = f"{path_data}/4-output/lhmfresh_{modelname}"
#    output:
#        f"{path_data}/5-visualization/head_comparison_{modelname}/LHMv4_min_LHMzzv{modelname}_l1.png"


rule calculate_dt:
    input:
        path_top_bot = f"{path_data}/2-interim/top_bot.nc",
        path_bdgfff = expand(path_data+f"/4-output/{modelname}/bdgfff/bdgfff_200001010001_l{{number}}.idf", number=nlayers_LHMfs),
        path_bdgfrf = expand(path_data+f"/4-output/{modelname}/bdgfrf/bdgfrf_200001010001_l{{number}}.idf", number=nlayers_LHMfs),
        path_bdgflf = expand(path_data+f"/4-output/{modelname}/bdgflf/bdgflf_200001010001_l{{number}}.idf", number=nlayers_LHMfs),
        script = "src/4-analyze/calculate_dt.py"
    params:
        modelname=modelname,
        min_dt=min_dt,
    output:
        path_result = f"{path_data}/2-interim/dt_{modelname}.nc"
    script:
        "src/4-analyze/calculate_dt.py"


