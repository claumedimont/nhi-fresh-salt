import imod
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
from pathlib import Path
import pyvista as pv

# filenames
fn_3dclclippedtmp = "data/5-visualization/v5/3dchloride_for_crosssections.nc"
fn_3dclpyvista = "data/5-visualization/v5/3dchloride_for_pyvista.nc"
fn_toppyvista = "data/5-visualization/v5/top_for_pyvista.nc"
fn_legend = "data/1-external/legends/chloride.leg"
fn_shapenl = "data/1-external/provincie/provincie.shp"
outputdir = Path(f"./reports/figures/3dimage")
outputdir.mkdir(exist_ok=True, parents=True)

############ 3D Chloride #############
try:
    cl = xr.open_dataarray(fn_3dclpyvista)
    top = xr.open_dataarray(fn_toppyvista)
except IOError:
    cl = xr.open_dataarray(fn_3dclclippedtmp)  # , chunks={"layer": 1})["3d-chloride"]
    toplay = imod.select.upper_active_layer(cl.sel(percentile="p50"), False)
    top = cl.top.where(cl.layer == toplay).max(dim="layer")

    # to DataArray suited for imod 3d visualize
    dims = ("percentile", "z", "y", "x")
    coords = {
        "percentile": cl.percentile.values,
        "z": cl.z.values,
        "y": cl.y.values.astype(float),
        "x": cl.x.values.astype(float),
        "dz": ("z", cl.dz.values),
    }
    cl = xr.DataArray(cl.data, coords, dims, name="3d-chloride")
    cl = cl.sortby("z", ascending=True)
    cl = cl.sortby("x", ascending=False)
    cl = cl.sortby("y", ascending=False)

    cl.to_netcdf(fn_3dclpyvista)
    top.to_netcdf(fn_toppyvista)


##### NL border for overlay #####
gdf = gpd.read_file(fn_shapenl)
nlpoly = gdf.geometry
lines = []
for poly in nlpoly:
    if isinstance(poly, Polygon):
        poly = [poly]
    for p_ in poly:
        lines.append(
            imod.visualize.line_3d(p_, z=(top + 5.0) * 100.0)
        )  # 100m, * 100 vertical exagg

########### Create 3d grid ############
p25 = cl.sel(percentile="p25")
p75 = cl.sel(percentile="p75")

# normalize to classes
colors, levels = imod.visualize.read_imod_legend(fn_legend)
cmap, norm = imod.visualize.common._cmapnorm_from_colorslevels(colors, levels)

p25n = xr.apply_ufunc(norm, p25, dask="parallelized", output_dtypes=[np.int16])
p25n = p25n.where(~p25.isnull())
p75n = xr.apply_ufunc(norm, p75, dask="parallelized", output_dtypes=[np.int16])
p75n = p75n.where(~p75.isnull())
p2575range = p75n - p25n

############################################################
#  TRANSECTS
# show only at every 25 k
xshow = p2575range.x.where((p2575range.x - 125) % 25000 == 0).dropna(dim="x")
yshow = p2575range.y.where((p2575range.y - 125) % 25000 == 0).dropna(dim="y")
p2575rangec = p2575range.where((p2575range.x.isin(xshow)) | (p2575range.y.isin(yshow)))
p2575rangec = p2575rangec.compute()

annotations={v-0.5:str(v) for v in range(len(levels)+1)}
annotations[8.5] = ""

grid = imod.visualize.grid_3d(p2575rangec, vertical_exaggeration=100.0)

plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(
    grid,
    cmap="viridis",
    stitle=f"NHI fresh-salt p25-p75 range (n of classes)",
    clim=[-1, 8],
    n_colors=9,
    annotations=annotations,
    scalar_bar_args={"n_labels": 0},
    ambient=0.3,
    # scalar_bar_args={"title": "Legend"},
    # show_scalar_bar=True,
)
for line in lines:
    plotter.add_mesh(line, color="#ffffff", line_width=1)
cpos = [
    (8729.64440177528, 68741.71982973302, 395127.5673167352),
    (121757.02454128035, 452178.5276657496, -37341.64306128947),
    (0.048531026096998955, 0.7410392336819702, 0.669705602223851),
]
plotter.ren_win.OffScreenRenderingOn()
plotter.enable_anti_aliasing()
plotter.set_position((8729.64440177528, 68741.71982973302, 395127.5673167352))
plotter.set_focus((121757.02454128035, 452178.5276657496, -37341.64306128947))
plotter.set_viewup((0.048531026096998955, 0.7410392336819702, 0.669705602223851))

cpos = plotter.show(
    cpos=cpos,
    window_size=[2048, 1536],
    screenshot=(outputdir / f"3dchloride_transects_p25p75range.png").as_posix(),
)
