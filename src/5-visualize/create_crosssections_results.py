import imod
import numpy as np
import os
import pandas as pd
import geopandas as gpd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
import configparser
from pathlib import Path
from shapely.geometry import LineString, Point
from shapely.ops import nearest_points
import sys

version = "V0.29kpslr_tr_2177"

sys.path.append(os.path.dirname(__file__))  # add script directory to python path

# filenames
fn_transects = "data/1-external/cross_section_locations/combined.shp"
#fn_transects = "data/1-external/cross_section_locations/additional.shp"
fn_cl = f"data/4-output/{version}/conc/conc*"
fn_topbot = "data/2-interim/layermodel.nc"
fn_conductivity = "data/2-interim/conductivity.nc"
fn_legend = "data/1-external/legends/chloride_gL.leg"
outputdir = Path(f"./reports/figures/{version}")
overwrite = True

############ 3D Chloride #############
cl = imod.idf.open(fn_cl)#_subdomains
layermodel = xr.open_dataset(fn_topbot)
conductivity = xr.open_dataset(fn_conductivity)
is_aquitard = (conductivity["kh"] < 1.).astype(int)

########### Create cross sections ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)

# read transects from shapefile
df_transects = gpd.read_file(fn_transects)

outputdir.mkdir(exist_ok=True, parents=True)

for name, transect in zip(df_transects.Name, df_transects.geometry):
    t = cl.time.values[-1]
    if overwrite or not (outputdir / f"crosssection_{name.replace(' ','')}_{pd.Timestamp(t):%Y%m%d}.png").exists():
        print(name)

        nrows = 1#len(cl.time)
        #fig, axes = plt.subplots(nrows=nrows, squeeze=False, figsize=(6.4, 3.2 * nrows))
        #fig.suptitle(f"Transect {name}")

        cmap = None
        norm = None
        top = imod.select.cross_section_linestring(layermodel["top"], transect)
        bot = imod.select.cross_section_linestring(layermodel["bot"], transect)
        aquitards = imod.select.cross_section_linestring(is_aquitard, transect)
        aquitards = aquitards.assign_coords(top=top)
        aquitards = aquitards.assign_coords(bottom=bot)
        aquitards.coords["ds"] = np.abs(aquitards.coords["ds"])
        
        # run for each percentile
        for i, t in enumerate(cl.time.values):  # ["p50"]:#
            i=0
            ### select cross section
            da = imod.select.cross_section_linestring(cl.sel(time=t), transect)
            da = da.assign_coords(top=top)
            da = da.assign_coords(bottom=bot)

            da.coords["ds"] = np.abs(da.coords["ds"])
            # print(da)
            
            fig, ax = plt.subplots(figsize=(6.4, 3.2))
            fig.suptitle(f"Transect {name}")
            
            fig, ax = imod.visualize.cross_section(
                    da,
                    colors=colors,
                    levels=levels,
                    layers=False,
                    aquitards=aquitards,
                    kwargs_colorbar={"whiten_triangles":False},
                    return_cmap_norm=False,
                    fig=fig,
                    ax=ax,
                )
            ax.set_ylim(bottom=-350.0, top=25.0)
            ax.set_title(f"Tijdstap {pd.Timestamp(t):%Y%m%d}")

            plt.tight_layout(rect=(0, 0, 1, 0.95))
            plt.savefig(
                outputdir / f"crosssection_{name.replace(' ','')}_{pd.Timestamp(t):%Y%m%d}.png",
                dpi=300,
                bbox_inches="tight",
            )  # save w/o measurements
            plt.close()
    else:
        print(f"Crosssection {name} exists, skipping")
