import imod
import xarray as xr

conc = imod.idf.open_subdomains(r"c:/projects/nhi_fresh_salt_v1/nhi-fresh-salt/data/4-output/V0.1.2_50yr_conc/conc/conc_*_l*_*.idf")
like = xr.open_dataarray(r"c:/projects/nhi_fresh_salt_v1/nhi-fresh-salt/data/2-interim/template.nc")
layermodel = xr.open_dataset(r"c:/projects/nhi_fresh_salt_v1/nhi-fresh-salt/data/2-interim/layermodel.nc")
layermodel = layermodel.reindex_like(conc)

top = layermodel["top"]
bottom = layermodel['bot']

conc = conc.assign_coords(top=(("layer", "y", "x"), top))
conc = conc.assign_coords(bottom=(("layer", "y", "x"), bottom))

# Take the interface at 0.5 g/L
interface = conc["layer"].where(conc > 0.5).max("layer") #0.5

# Setup animation
animation = imod.visualize.GridAnimation3D(interface)
animation.plotter.camera_position = [
    (339703.47861839295, 104600.49368067965, 366347.4868680465),
    (140500.0, 469250.0, 615.0),
    (-0.19754921982378854, 0.6382992694744456, 0.74400829856622)
]
animation.peek()
animation.write("interface.mp4")

# open plotter
import pyvista as pv
p = pv.BackgroundPlotter()
p.add_mesh(animation.mesh)

# to get the camera position
p.camera_position
