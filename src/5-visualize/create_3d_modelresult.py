import imod
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
from pathlib import Path


# filenames
fn_transects = r"data\1-external\cross_section_locations\combined.shp"
fn_cl = r"data\4-output\V0.1.17_tr_merged\conc\conc*"
fn_like = "data/2-interim/template.nc"
fn_topbot = "data/2-interim/layermodel.nc"
fn_conductivity = "data/2-interim/conductivity.nc"
fn_legend = "data/1-external/legends/chloride_gL.leg"
outputdir = Path(f"./reports/figures/v0.1.17")


############ 3D Chloride #############
cl = imod.idf.open(fn_cl)
layermodel = xr.open_dataset(fn_topbot)
cl = cl.reindex_like(layermodel)
cl = cl.assign_coords(top=(("layer", "y", "x"), layermodel["top"].values))
cl = cl.assign_coords(bottom=(("layer", "y", "x"), layermodel["bot"].values))

z = 0.5*layermodel["top"] + 0.5*layermodel["bot"]
z = imod.prepare.fill(z)
cl = cl.assign_coords(z=(("layer","y","x"), z.values))
cl = cl.swap_dims({"layer": "z"})
cl = cl.assign_coords(dz=("z", layermodel["dz"].values))

conductivity = xr.open_dataset(fn_conductivity)
is_aquitard = (conductivity["kh"] < 1.).astype(int)

########### Create cross sections ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)
cmap = matplotlib.colors.ListedColormap(colors[1:-1])
cmap.set_under(colors[0])  # this is the color for values smaller than raster.min()
cmap.set_over(colors[-1])  # this is the color for values larger than raster.max()
norm = matplotlib.colors.BoundaryNorm(levels, cmap.N)

# normalize cl result
cln = cl.copy()
cln.values = norm(cl.values)
cln = cln.where(~cl.isnull()

# delete two slices
# 515000 - 525000, 415000 - 425000
clnc = cln.where(
    (cln.y > 535000) | (cln.y < 405000) | ((cln.y > 435000) & (cln.y < 505000))
)


imod.visualize.StaticGridAnimation3D(clnc, plotter_kwargs={"cmap":cmap})