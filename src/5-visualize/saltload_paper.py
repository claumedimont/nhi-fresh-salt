import imod
import xarray as xr
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter
import seaborn as sns
import rioxarray
from pathlib import Path

# filenames
path_landen = "data/1-external/shapes/Landen.shp"
path_prov = "data/1-external/shapes/provincie.shp"
path_zarrs = Path("p:/11206798-014-kpzss-verzilting/LHMzz-41_runs/landelijke_runs_zarr")
path_slr = "p:/11206798-014-kpzss-verzilting/LHMzz-41_runs/data/2-interim/slrcurve.nc"
path_output = Path(f"./reports/figures/paper")
path_output.mkdir(exist_ok=True)

slr = xr.open_dataarray(path_slr)
nederland = gpd.read_file(path_prov)
#nederland  = landen[landen["CNTRYABB"] != "NL"]
def clip(da, clipda=nederland):
    da = da.rio.write_crs(28992)
    da = da.rio.clip(nederland.geometry, nederland.crs, drop=False, invert=False)
    return da


# calculate salt load - slr and autonomous
flf = xr.open_zarr(path_zarrs / "kpslr/bdgflf.zarr")["bdgflf"].sel(layer=10, drop=True)
conc = xr.open_zarr(path_zarrs / "kpslr/conc.zarr")["conc"].sel(layer=11, drop=True)
saltload = flf * conc
saltload = saltload.where(saltload > 0, 0)
# indexing one year back
time = xr.cftime_range("2000-01-01","2277-01-01",freq="YS")
saltload.coords["time"] = time
saltload = clip(saltload)

flfaut = xr.open_zarr(path_zarrs / "autsubs/bdgflf.zarr")["bdgflf"].sel(layer=10, drop=True)
concaut = xr.open_zarr(path_zarrs / "autsubs/conc.zarr")["conc"].sel(layer=11, drop=True)
slaut = flfaut * concaut
slaut = slaut.where(slaut > 0, 0)
slaut.coords["time"] = time
slaut = clip(slaut)

# plot saltload increase versus SLR
slincr_slr = saltload - slaut
slincr_slr = slincr_slr.reindex(time=slr.time)

df_slr = slr.to_series()
df_slincr = slincr_slr.sum(dim=["x","y"]).to_series()
df_slincr.name = "SLincr_aut"
df_sl = saltload.reindex(time=slr.time).sum(dim=["x","y"]).to_series()
df_sl.name = "saltload_slr"

sl_t0 = float(saltload.isel(time=0).sum(dim=["x","y"]).compute())
df = pd.concat((df_slr,df_slincr),axis=1) #df_sl,
df.index = df.index.year.astype(int)

df["sl_rel"] = df["SLincr_aut"] / sl_t0 * 100.
df["sl_rel"] = df["sl_rel"].where(df["sl_rel"] > 0, 0)

sns.set(style="whitegrid", font="Arial")
sns.set_context("paper")

def years(dt):
    'The two args are the value and tick position'
    return f"{dt.year}"
formatter = FuncFormatter(years)

f,ax1 = plt.subplots(1,1, figsize=(5,3))
ax2 = ax1.twinx()
df["SLR"].plot(ax=ax1,ylabel="Sea-level rise (m)",ylim=[0,4.],xlim=[2000,2280])
#df["SLincr_aut"].plot(ax=ax2,label="sea-level rise", xlabel="year",ylabel="salt load increase",ylim=[0,2e7],zorder=5)
#df["SLincr_aut"].plot(ax=ax2,label="salt load increase",xlabel="year",ylabel="salt load increase",ylim=[0,2e7],zorder=5)
df["sl_rel"].plot(ax=ax2,label="sea-level rise",ylim=[0,320],xlim=[2000,2280])
df["sl_rel"].plot(ax=ax2,label="salt load increase",xlabel="year",ylabel="Salt load increase (%)",ylim=[0,320],xlim=[2000,2280])
ax2.grid(None)
plt.xlabel("Year")
plt.legend()
plt.tight_layout()
plt.savefig(path_output / "saltload_increase.png", dpi=300, bbox_inches="tight")
plt.savefig(path_output / "saltload_increase.svg", dpi=300, bbox_inches="tight")
plt.close()

#### 2 - effect maps for QGIS

# store tifs of head increase and saltload increase

head = xr.open_zarr(path_zarrs / "kpslr/head.zarr")["head"].sel(layer=11, drop=True).isel(time=-1)
headaut = xr.open_zarr(path_zarrs / "autsubs/head.zarr")["head"].sel(layer=11, drop=True).isel(time=-1)
headdiff = head - headaut
headdiff = clip(headdiff)
imod.rasterio.write(path_output / "headdiff_slr_3m.tif", -headdiff)

slincr = slincr_slr.isel(time=-1)
slincr = clip(slincr)
slincr = slincr * 365. / 6.25 / 1000  # from kg/cell/day to tonnes/ha/yr
imod.rasterio.write(path_output / "saltloaddiff_slr_3m.tif", slincr)