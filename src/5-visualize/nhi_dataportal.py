# -*- coding: utf-8 -*-
"""
Creates maps as input for the NHI Dataportaal

Input:
- data/2-interim/layermodel.nc
- data/1-external/3D-chloride/v5/Chloride_25_50_75.nc
- data/2-interim/bnd.nc

Output:
- data/5-visualization/dataportal/3dchloride_groundwater_gL.nc
- data/5-visualization/dataportal/2dchloride_surfacewater_gL.tif
- data/5-visualization/dataportal/3dchloride_depthfreshbrack_mMSL.tif

"""
import imod
import os
import xarray as xr
import pandas as pd
import numpy as np
import geopandas as gpd
from rasterio.crs import CRS
from pathlib import Path
from scipy.ndimage import percentile_filter, uniform_filter, label

crs = CRS.from_epsg(28992)  # Amersfoort RD New

def cl_include_topbot(cl):
    """Function to transform 3d Kriging result to Dataarray suited
    for further processing"""
    cl = cl.rename(
        {
            "x-coordinates": "x",
            "y-coordinates": "y",
            "z-coordinates": "z",
            "Chloride dimension": "percentile",
        }
    )
    cl = cl.sortby("z", ascending=False)  # flip z
    z = cl.z.to_series().astype(float)
    z = z.reset_index(drop=True)
    top = z.rolling(2).mean()
    bot = top.shift(-1)
    top.iloc[0] = z.iloc[0] + (z.iloc[0] - bot.iloc[0])
    bot.iloc[-1] = z.iloc[-1] - (top.iloc[-1] - z.iloc[-1])
    dz = top - bot
    z = 0.5 * top + 0.5 * bot  # recalculate z due to differences in dz

    cl.coords["z"] = range(1, len(z) + 1)
    cl.coords["percentile"] = ["p25", "p50", "p75"]
    cl = cl.rename({"z": "layer"})
    cl = cl.transpose("percentile", "layer", "y", "x")

    # to DataArray suited for idf save
    dims = ("percentile", "layer", "y", "x")
    coords = {
        "percentile": cl.percentile.values,
        "layer": cl.layer.values,
        "y": cl.y.values.astype(float),
        "x": cl.x.values.astype(float),
        "z": ("layer", z),
        "dz": ("layer", dz),
        "top": ("layer", top),
        "bottom": ("layer", bot),
    }
    cl2 = xr.DataArray(cl.values, coords, dims, name="3d-chloride")
    cl2 = cl2.sortby("y", ascending=False)  # flip y

    # return dataset
    return cl2

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

version = "v2.3.0"
# Paths
path_layermodel = "data/2-interim/layermodel.nc"
path_3dkrig = "data/1-external/3D-chloride/v5/Chloride_normlog_25_50_75.nc"
path_sconc = "data/2-interim/starting_concentration.nc"
path_clresult = f"data/4-output/{version}/conc/conc*.idf"
path_boundaries = f"reports/figures/grensvlakken_{version}/grensvlakken.nc"
path_bnd = "data/2-interim/bnd.nc"
path_template_2d = "data/2-interim/template_2d.nc"
path_prov = "data/1-external/shapes/provincie.shp"
path_dataportal = Path("data/5-visualization/dataportal")

path_dataportal.mkdir(exist_ok=True, parents=True)

# Open LHMzz layermodel
layermodel = xr.open_dataset(path_layermodel)
new_top = layermodel["top"]
new_bot = layermodel["bot"]
bnd = xr.open_dataset(path_bnd)["ibound"]

# Open 3D chloride result and transform to include top/bot info
conc3d = xr.open_dataarray(path_3dkrig) / 1000.  # to g/L
conc3d = cl_include_topbot(conc3d)
# cut off above surface level 
toplay = imod.select.upper_active_layer(new_top,False)
top_lm = new_top.where(new_top.layer == toplay).min(dim="layer")
conc3d = conc3d.where(~top_lm.isnull()).where(conc3d.bottom <= top_lm)

# For dataportal: cut off below GHB and assign attrs
conc3d.attrs["crs"] = "EPSG:28992"
conc3d.attrs["parameter"] = "groundwater chloride concentration"
conc3d.attrs["unit"] = "g/L"
conc3d_dp = conc3d.where(conc3d.top > new_bot.min(dim="layer"))
conc3d_dp.to_netcdf(path_dataportal / "3dchloride_groundwater_gL.nc") # save intermediate result for dataportaal

# Load 2d surfacewater concentrations
conc2d = xr.open_dataset(path_sconc)["sconc_2d"]

# Save result for dataportaal (in g/L)
# Set CRS and clip to ibound
conc2d_dp = conc2d.where(bnd.sel(layer=11))
conc2d_dp.attrs["crs"] = crs
conc2d_dp.attrs["parameter"] = "median surface water chloride concentration"
conc2d_dp.attrs["unit"] = "g/L"
imod.rasterio.write(path_dataportal / "2dchloride_surfacewater_gL.tif", conc2d_dp)


provincie = gpd.read_file(path_prov)
prov_da = imod.prepare.rasterize(
    provincie, bnd.isel(layer=0)
)

# Create grid of depth of fresh/brackish interface
exc, _ = imod.evaluate.interpolate_value_boundaries(conc3d.sel(percentile="p50"), conc3d.z, 1.)
depthfreshbrack = exc.max(dim="boundary")
depthfreshbrack.attrs["crs"] = crs
depthfreshbrack.attrs["parameter"] = "boundary fresh - brackish groundwater"
depthfreshbrack.attrs["unit"] = "m MSL"
imod.rasterio.write(path_dataportal / "3dchloride_depthfreshbrack_mMSL.tif", depthfreshbrack)

# Create grid of depth of fresh/brackish interface - 0.15 g/L
exc, _ = imod.evaluate.interpolate_value_boundaries(conc3d.sel(percentile="p50"), conc3d.z, .15)
depthdwcriterion = exc.max(dim="boundary")
depthdwcriterion.attrs["crs"] = crs
depthdwcriterion.attrs["parameter"] = "boundary fresh - brackish groundwater"
depthdwcriterion.attrs["unit"] = "m MSL"
imod.rasterio.write(path_dataportal / "3dchloride_depthdwcriterion_mMSL.tif", depthdwcriterion)

# concentration below cover layer
sconc3d = xr.open_dataset(path_sconc)["sconc"]
sconc3d_dp = sconc3d.sel(layer=11).where(bnd.sel(layer=11))
sconc3d_dp.attrs["crs"] = crs
sconc3d_dp.attrs["parameter"] = "groundwater chloride concentration below Holocene cover layer"
sconc3d_dp.attrs["unit"] = "g/L"
imod.rasterio.write(path_dataportal / "3dchloride_groundwater_coverlayer_gL.tif", sconc3d_dp)

# Create grid of depth of fresh/brackish interface from model results
all_gv = xr.open_dataarray(path_boundaries)
gv = all_gv.sel(threshold=1.0, time=pd.Timestamp(2020, 1, 1))
gv.attrs["crs"] = crs
imod.rasterio.write(path_dataportal / "grensvlak_1gL.tif", gv)
# fill nodata with deep value
gv = gv.fillna(-200)#combine_first(prov_da*-200)

# filter outcome, apply a focal median filter
filtered = percentile_filter((gv.fillna(1e20)).values, percentile=50, size=(13,12))
gv_filtered = gv.copy(data=filtered)
gv_filtered = gv_filtered.where(gv_filtered < 1e20)#.where(prov_da==1)
gv_filtered.attrs["crs"] = crs
gv_filtered.attrs["parameter"] = "boundary fresh - brackish groundwater"
gv_filtered.attrs["unit"] = "m MSL"
imod.rasterio.write(path_dataportal / "3dchloride_depthfreshbrack_mMSL.tif", gv_filtered.where(prov_da==1))


def isolated_cells(da, cutoff=1e2):
    """ Function that return areas that are 
    smaller than the cutoff (in number of cells)
    """
    active_cells = ~da.isnull()
    labels, _ = label(active_cells, structure=np.ones((3,3)))
    unique, counts = np.unique(labels, return_counts=True)
    isolated = xr.full_like(da, False, dtype=bool)
    if (counts < cutoff).any():  # less than cutoff: unconnected region
        isolated = unique[counts < cutoff]
        isolated = np.isin(labels, isolated)
        isolated = da.copy(data=isolated)  # set bnd to zero for isolated regions
    return isolated
sel = gv_filtered.where((gv_filtered >= -190))# & (prov_da==1))
isolated = isolated_cells(sel, cutoff=3e2)
#isolated.attrs["crs"] = crs
#imod.rasterio.write(path_dataportal / "isolated.tif", isolated.astype(float))
gv_filtered2 = gv_filtered.fillna(-200).where(~isolated, -200).where(prov_da==1)
imod.rasterio.write(path_dataportal / "3dchloride_depthfreshbrack_mMSL_filtered.tif", gv_filtered2)


## classify and polygonize
class_bounds = [-1e9,-200,-100,-75,-50,-40,-30,-20,-10,-5,1e9]
class_names = {i+1:f"{j:.1f} - {k:.1f} m NAP" for i,(j,k) in enumerate(zip(class_bounds[0:-1],class_bounds[1:]))}
class_names[1.] = f"dieper dan {class_bounds[1]:.1f} m NAP"
class_names[10.] = f"hoger dan {class_bounds[-2]:.1f} m NAP"
sort_idx = np.argsort(class_bounds)
gv_classed = gv_filtered2.copy(data=np.digitize(gv_filtered2, class_bounds, right=True))
gv_classed = gv_classed.where(gv_classed < 11)
gv_poly = imod.prepare.polygonize(gv_classed)
gv_poly.crs = crs
gv_poly = gv_poly.loc[~gv_poly.value.isnull()]
gv_poly["Depth"] = gv_poly["value"].map(class_names)
gv_poly.to_file(path_dataportal / "3dchloride_depthfreshbrack_mMSL_filtered.shp")