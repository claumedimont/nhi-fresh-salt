# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 09:41:35 2019

@author: mulde_ts
"""
import pathlib
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np    
import pandas as pd
import imod
import numpy as np
import numba
from numba import jit
import xarray as  xr
import zarr
import os
from matplotlib.colors import ListedColormap
from matplotlib import cm
# Colormap turbo
turbo_colormap_data = [[0.18995,0.07176,0.23217],[0.19483,0.08339,0.26149],[0.19956,0.09498,0.29024],[0.20415,0.10652,0.31844],[0.20860,0.11802,0.34607],[0.21291,0.12947,0.37314],[0.21708,0.14087,0.39964],[0.22111,0.15223,0.42558],[0.22500,0.16354,0.45096],[0.22875,0.17481,0.47578],[0.23236,0.18603,0.50004],[0.23582,0.19720,0.52373],[0.23915,0.20833,0.54686],[0.24234,0.21941,0.56942],[0.24539,0.23044,0.59142],[0.24830,0.24143,0.61286],[0.25107,0.25237,0.63374],[0.25369,0.26327,0.65406],[0.25618,0.27412,0.67381],[0.25853,0.28492,0.69300],[0.26074,0.29568,0.71162],[0.26280,0.30639,0.72968],[0.26473,0.31706,0.74718],[0.26652,0.32768,0.76412],[0.26816,0.33825,0.78050],[0.26967,0.34878,0.79631],[0.27103,0.35926,0.81156],[0.27226,0.36970,0.82624],[0.27334,0.38008,0.84037],[0.27429,0.39043,0.85393],[0.27509,0.40072,0.86692],[0.27576,0.41097,0.87936],[0.27628,0.42118,0.89123],[0.27667,0.43134,0.90254],[0.27691,0.44145,0.91328],[0.27701,0.45152,0.92347],[0.27698,0.46153,0.93309],[0.27680,0.47151,0.94214],[0.27648,0.48144,0.95064],[0.27603,0.49132,0.95857],[0.27543,0.50115,0.96594],[0.27469,0.51094,0.97275],[0.27381,0.52069,0.97899],[0.27273,0.53040,0.98461],[0.27106,0.54015,0.98930],[0.26878,0.54995,0.99303],[0.26592,0.55979,0.99583],[0.26252,0.56967,0.99773],[0.25862,0.57958,0.99876],[0.25425,0.58950,0.99896],[0.24946,0.59943,0.99835],[0.24427,0.60937,0.99697],[0.23874,0.61931,0.99485],[0.23288,0.62923,0.99202],[0.22676,0.63913,0.98851],[0.22039,0.64901,0.98436],[0.21382,0.65886,0.97959],[0.20708,0.66866,0.97423],[0.20021,0.67842,0.96833],[0.19326,0.68812,0.96190],[0.18625,0.69775,0.95498],[0.17923,0.70732,0.94761],[0.17223,0.71680,0.93981],[0.16529,0.72620,0.93161],[0.15844,0.73551,0.92305],[0.15173,0.74472,0.91416],[0.14519,0.75381,0.90496],[0.13886,0.76279,0.89550],[0.13278,0.77165,0.88580],[0.12698,0.78037,0.87590],[0.12151,0.78896,0.86581],[0.11639,0.79740,0.85559],[0.11167,0.80569,0.84525],[0.10738,0.81381,0.83484],[0.10357,0.82177,0.82437],[0.10026,0.82955,0.81389],[0.09750,0.83714,0.80342],[0.09532,0.84455,0.79299],[0.09377,0.85175,0.78264],[0.09287,0.85875,0.77240],[0.09267,0.86554,0.76230],[0.09320,0.87211,0.75237],[0.09451,0.87844,0.74265],[0.09662,0.88454,0.73316],[0.09958,0.89040,0.72393],[0.10342,0.89600,0.71500],[0.10815,0.90142,0.70599],[0.11374,0.90673,0.69651],[0.12014,0.91193,0.68660],[0.12733,0.91701,0.67627],[0.13526,0.92197,0.66556],[0.14391,0.92680,0.65448],[0.15323,0.93151,0.64308],[0.16319,0.93609,0.63137],[0.17377,0.94053,0.61938],[0.18491,0.94484,0.60713],[0.19659,0.94901,0.59466],[0.20877,0.95304,0.58199],[0.22142,0.95692,0.56914],[0.23449,0.96065,0.55614],[0.24797,0.96423,0.54303],[0.26180,0.96765,0.52981],[0.27597,0.97092,0.51653],[0.29042,0.97403,0.50321],[0.30513,0.97697,0.48987],[0.32006,0.97974,0.47654],[0.33517,0.98234,0.46325],[0.35043,0.98477,0.45002],[0.36581,0.98702,0.43688],[0.38127,0.98909,0.42386],[0.39678,0.99098,0.41098],[0.41229,0.99268,0.39826],[0.42778,0.99419,0.38575],[0.44321,0.99551,0.37345],[0.45854,0.99663,0.36140],[0.47375,0.99755,0.34963],[0.48879,0.99828,0.33816],[0.50362,0.99879,0.32701],[0.51822,0.99910,0.31622],[0.53255,0.99919,0.30581],[0.54658,0.99907,0.29581],[0.56026,0.99873,0.28623],[0.57357,0.99817,0.27712],[0.58646,0.99739,0.26849],[0.59891,0.99638,0.26038],[0.61088,0.99514,0.25280],[0.62233,0.99366,0.24579],[0.63323,0.99195,0.23937],[0.64362,0.98999,0.23356],[0.65394,0.98775,0.22835],[0.66428,0.98524,0.22370],[0.67462,0.98246,0.21960],[0.68494,0.97941,0.21602],[0.69525,0.97610,0.21294],[0.70553,0.97255,0.21032],[0.71577,0.96875,0.20815],[0.72596,0.96470,0.20640],[0.73610,0.96043,0.20504],[0.74617,0.95593,0.20406],[0.75617,0.95121,0.20343],[0.76608,0.94627,0.20311],[0.77591,0.94113,0.20310],[0.78563,0.93579,0.20336],[0.79524,0.93025,0.20386],[0.80473,0.92452,0.20459],[0.81410,0.91861,0.20552],[0.82333,0.91253,0.20663],[0.83241,0.90627,0.20788],[0.84133,0.89986,0.20926],[0.85010,0.89328,0.21074],[0.85868,0.88655,0.21230],[0.86709,0.87968,0.21391],[0.87530,0.87267,0.21555],[0.88331,0.86553,0.21719],[0.89112,0.85826,0.21880],[0.89870,0.85087,0.22038],[0.90605,0.84337,0.22188],[0.91317,0.83576,0.22328],[0.92004,0.82806,0.22456],[0.92666,0.82025,0.22570],[0.93301,0.81236,0.22667],[0.93909,0.80439,0.22744],[0.94489,0.79634,0.22800],[0.95039,0.78823,0.22831],[0.95560,0.78005,0.22836],[0.96049,0.77181,0.22811],[0.96507,0.76352,0.22754],[0.96931,0.75519,0.22663],[0.97323,0.74682,0.22536],[0.97679,0.73842,0.22369],[0.98000,0.73000,0.22161],[0.98289,0.72140,0.21918],[0.98549,0.71250,0.21650],[0.98781,0.70330,0.21358],[0.98986,0.69382,0.21043],[0.99163,0.68408,0.20706],[0.99314,0.67408,0.20348],[0.99438,0.66386,0.19971],[0.99535,0.65341,0.19577],[0.99607,0.64277,0.19165],[0.99654,0.63193,0.18738],[0.99675,0.62093,0.18297],[0.99672,0.60977,0.17842],[0.99644,0.59846,0.17376],[0.99593,0.58703,0.16899],[0.99517,0.57549,0.16412],[0.99419,0.56386,0.15918],[0.99297,0.55214,0.15417],[0.99153,0.54036,0.14910],[0.98987,0.52854,0.14398],[0.98799,0.51667,0.13883],[0.98590,0.50479,0.13367],[0.98360,0.49291,0.12849],[0.98108,0.48104,0.12332],[0.97837,0.46920,0.11817],[0.97545,0.45740,0.11305],[0.97234,0.44565,0.10797],[0.96904,0.43399,0.10294],[0.96555,0.42241,0.09798],[0.96187,0.41093,0.09310],[0.95801,0.39958,0.08831],[0.95398,0.38836,0.08362],[0.94977,0.37729,0.07905],[0.94538,0.36638,0.07461],[0.94084,0.35566,0.07031],[0.93612,0.34513,0.06616],[0.93125,0.33482,0.06218],[0.92623,0.32473,0.05837],[0.92105,0.31489,0.05475],[0.91572,0.30530,0.05134],[0.91024,0.29599,0.04814],[0.90463,0.28696,0.04516],[0.89888,0.27824,0.04243],[0.89298,0.26981,0.03993],[0.88691,0.26152,0.03753],[0.88066,0.25334,0.03521],[0.87422,0.24526,0.03297],[0.86760,0.23730,0.03082],[0.86079,0.22945,0.02875],[0.85380,0.22170,0.02677],[0.84662,0.21407,0.02487],[0.83926,0.20654,0.02305],[0.83172,0.19912,0.02131],[0.82399,0.19182,0.01966],[0.81608,0.18462,0.01809],[0.80799,0.17753,0.01660],[0.79971,0.17055,0.01520],[0.79125,0.16368,0.01387],[0.78260,0.15693,0.01264],[0.77377,0.15028,0.01148],[0.76476,0.14374,0.01041],[0.75556,0.13731,0.00942],[0.74617,0.13098,0.00851],[0.73661,0.12477,0.00769],[0.72686,0.11867,0.00695],[0.71692,0.11268,0.00629],[0.70680,0.10680,0.00571],[0.69650,0.10102,0.00522],[0.68602,0.09536,0.00481],[0.67535,0.08980,0.00449],[0.66449,0.08436,0.00424],[0.65345,0.07902,0.00408],[0.64223,0.07380,0.00401],[0.63082,0.06868,0.00401],[0.61923,0.06367,0.00410],[0.60746,0.05878,0.00427],[0.59550,0.05399,0.00453],[0.58336,0.04931,0.00486],[0.57103,0.04474,0.00529],[0.55852,0.04028,0.00579],[0.54583,0.03593,0.00638],[0.53295,0.03169,0.00705],[0.51989,0.02756,0.00780],[0.50664,0.02354,0.00863],[0.49321,0.01963,0.00955],[0.47960,0.01583,0.01055]]
Jet_colormap_data = cm.get_cmap('jet', 255)

def interpolate(colormap, x):
  x = max(0.0, min(1.0, x))
  a = int(x*255.0)
  b = min(255, a + 1)
  f = x*255.0 - a
  return [colormap[a][0] + (colormap[b][0] - colormap[a][0]) * f,
          colormap[a][1] + (colormap[b][1] - colormap[a][1]) * f,
          colormap[a][2] + (colormap[b][2] - colormap[a][2]) * f]

def interpolate_or_clip(colormap, x):
  if   x < 0.0: return [0.0, 0.0, 0.0]
  elif x > 1.0: return [1.0, 1.0, 1.0]
  else: return interpolate(colormap, x)


os.chdir(r'c:\Users\mulde_ts\projects\nhi-fresh-salt')


nrow = 1300
ncol = 1200
nlay = 40
nodata = -9999.0
path_top_bot = r'data/2-interim/top_bot.nc'
lagenmodel = xr.open_dataset(path_top_bot)
new_top = lagenmodel['top']
new_bot = lagenmodel["bot"]
zmin = new_bot[-1]
zmax = new_top[0]
dz = lagenmodel["dz"]
import geopandas as gpd



z =  zmin + (dz.isel(layer=slice(None, None, -1)).cumsum("layer")) - 0.5 * dz
z = z.sortby('layer')
#imod.idf.save('depth',z)


head_NHI_ZZ = imod.idf.open_subdomains(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output_004\head\head*.idf')

head_NHI_ZZ = head_NHI_ZZ.squeeze('time', drop = True)
#head_NHI_ZZ = head_NHI_ZZ.drop(['dx','dy','dz','z'])

conc_TVD  =  imod.idf.open_subdomains(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-parralel_tvd_strikt\conc\conc*.IDF')
conc_fd = imod.idf.open_subdomains(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-parralel_fd_strikt\conc\conc*')
start_conc = imod.idf.open(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\3-input\NHI_zz_002\btn\starting_concentration*')

conc = conc_fd

conc['dz'] = dz
dens = 1.316 * conc + 1000.
dens = dens.squeeze('time', drop = True )
dens['z'] = z
head_f = ((dens/1000.) * head_NHI_ZZ) - (((dens - 1000)/1000) * dens["z"])



porosity = 0.3
freshwater = conc < 1.0
#conc = conc.assign_coords(dz=("layer", dz))
volume = freshwater * np.abs(conc["dz"]) * np.abs(conc["dx"]) * np.abs(conc["dy"])*porosity
vol_million_m3 = volume/1000000

timeseries = volume.sum(dim=["y", "x", "layer"])

timeseries = vol_million_m3.sum(dim=["y", "x", "layer"])

plt.figure(figsize=(16,9))
timeseries.plot()#.sel(time=slice('2000-12-31', '2033-12-31')).plot()
plt.xlabel("Time", fontsize = 15)
plt.title('Fresh water volume development TVD solver',fontsize = 17)
plt.ylabel(" Million m3", fontsize= 15)

plt.savefig(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-parralel_tvd_strikt\figures\fresh_water_development_TVD.png', dpi=300)

#get head for first active layer

#
#def get_upper_active_layer(da, is_ibound=True, include_constant_head=False):
#    """
#    Function to get the upper active layer from ibound xr.DataArray
#
#    Parameters:
#    da : 3d xr.DataArray
#    is_ibound: da is interpreted as ibound, with values 0 inactive, 1, active, -1 chd
#                if False: upper_active_layer is interpreted as upper layer that has data
#    include_constant_head : also incluse chd cells? bool, default False
#
#    Returns:
#    2d xr.DataArray of layernumber of upper active model layer
#    """
#    if is_ibound:
#        # check if indeed ibound: convertible to int
#        if not da.astype(int).equals(da):
#            raise ValueError(
#                "Passed DataArray is no ibound, while is_bound was set to True"
#            )
#        # include constant head cells (?)
#        if include_constant_head:
#            is_active = da.fillna(0) != 0  # must be filled for argmax
#        else:
#            is_active = da.fillna(0) > 0
#    else:
#        is_active = ~da.isnull()
#
#    # get layer of upper active cell
#    da = is_active.layer.isel(layer=is_active.argmax(dim="layer"))
#    da = da.drop("layer")
#
#    # skip where no active cells
#    return da.where(is_active.sum(dim="layer") > 0)
#
#
#bnd = imod.idf.open(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\3-input\NHI_zz_003\bas\ibound_l*.idf')
#da = get_upper_active_layer(bnd)
#head = head_f
#test = head.layer == da
#phreatic = head.where(test)
#phreatic = phreatic.transpose("layer","y", "x")
#phreatic = phreatic.min(dim = 'layer').compute()
#
##phreatic = phreatic.squeeze('time', drop= True)
#imod.idf.save('phreatic_004_freshhead.idf',phreatic)
#
#gdf = gpd.open()
#imod.visualize()
#overlays =[{"gdf": gdf, "color": "none", "edgecolor": "black"}] 
#
#head_LHM = imod.idf.open(r'n:\Deltabox\Postbox\Delsman, Joost\vanJanneke\LHM340_modflow\results\TEST01\head\head_steady-state_l*.idf')
#head_LHM = head_LHM.squeeze(['time'], drop =True)
#
#head_LHM.sel(layer = 1).plot.imshow(cmap ='terrain', vmin = -7.0, vmax = 35.0)
#head_LHM.sel(layer = 1).plot.imshow(cmap ='nipy_spectral', vmin = -7.0, vmax = 50.0)
#
#head_diff = head_LHM - phreatic
#head_diff.plot.imshow
#
#
#levels = [-6.0,-3., -1.,-.5, 0, 1, 3., 5.,15., 30.0]
##levels = [-18.0, -16.0,-14.0, -12.0,-10.0, -8.0,-6.0, -4.0,-2.0, 0.0, 2.0]
#colors = ['#a50026',
#          '#d73027',
#          '#f46d43',
#          '#fdae61',
#          '#fee090',         
#          '#ffffbf',
#          '#e0f3f8',
#          '#abd9e9', 
#          '#74add1',
#          '#4575b4',
#          '#313695'
#]  
##colors = ['#67001f','#b2182b','#d6604d','#f4a582','#fddbc7','#f7f7f7','#d1e5f0','#92c5de','#4393c3','#2166ac','#053061']  
#colors = colors[::-1]          
#head_LHM.plot(levels = levels, colors=colors, cbar_kwargs={'ticks':levels, 'spacing':'proportional'})


#cross_sections

TR_results = xr.open_zarr(r"c:\Users\mulde_ts\projects\nhi-fresh-salt\data\4-output\nhi_zz_002\results.zarr.zip")


path_top_bot = r'data/2-interim/top_bot_min3m.nc'
lagenmodel = xr.open_dataset(path_top_bot)
new_top = lagenmodel['top']
new_bot = lagenmodel["bot"]
new_thickness = lagenmodel['dz']



conc = TR_results['conc']
cross_Joost = gpd.read_file(r'c:\Users\mulde_ts\projects\nhi-fresh-salt\data\1-external\cross_section_locations\transect.shp').geometry[0]
cross_Zeeland = gpd.read_file(r'c:\Users\mulde_ts\projects\nhi-fresh-salt\data\1-external\cross_section_locations\Zeeland_line.shp').geometry[0]
cross_Delfland = gpd.read_file(r'c:\Users\mulde_ts\projects\nhi-fresh-salt\data\1-external\cross_section_locations\Delfland.shp').geometry[0]
cross_Alkmaar = gpd.read_file(r'c:\Users\mulde_ts\projects\nhi-fresh-salt\data\1-external\cross_section_locations\Alkmaar.shp').geometry[0]
cross_Harlingen = gpd.read_file(r'c:\Users\mulde_ts\projects\nhi-fresh-salt\data\1-external\cross_section_locations\Harlingen.shp').geometry[0]
#select cross section you want to make
linestring = cross_Joost
cross_section = imod.select.cross_section_linestring(conc, linestring)
tops_cross_section =  imod.select.cross_section_linestring(new_top, linestring)
bots_cross_section = imod.select.cross_section_linestring(new_bot, linestring)
cross_section = cross_section.assign_coords(top=tops_cross_section)
cross_section = cross_section.assign_coords(bottom=bots_cross_section)

levels_conc = [0.0,.5, 1., 2., 4., 6., 8., 10., 12., 16., 18.]
#colors = ['#a50026','#d73027','#f46d43','#fdae61','#fee090','#e0f3f8','#abd9e9','#74add1','#4575b4','#313695']
#colors = ['#313695', '#416AAF', '#6399C7', '#90C3DD', '#BDE2EE', '#E5F5EF', '#FFFEBE', '#FEE597', '#FDBF71', '#F88C51', '#EA5739', '#CE2827']
ncolors = len(levels_conc) + 1
pick_colors = np.linspace(0, 255, ncolors).astype(np.int)
colors = np.array(turbo_colormap_data)[pick_colors]
colors_jet = np.array(Jet_colormap_data(pick_colors))
colors[1] = [0.  , 0.22440945, 1.]
#colors = colors[::-1]
ds = cross_section
x = ds['x']
y = dz['y']

ds.attrs["units"] = "Chloride (g/L)"
is_aquifer  = xr.open_dataset(r'data/2-interim/is_aquifer_3m.nc')['__xarray_dataarray_variable__']
is_aq_cross = imod.select.cross_section_linestring(is_aquifer, linestring)
#z_cross  = imod.select.points_values(z, x = )
import os 
os.chdir(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-parralel_fd_strikt\figures\cross_sections')    
yr = 0
i = 1
for i in range (0,10,1):
    print (i)       
    conc = ds.isel(time=i)
    #conc= conc.where(conc>-0.01)
    conc= conc.where(~(conc < 0.0), other=0.01)
#    conc = xr.full_like(like, conc)
    #yr = ds['time'].isel(time=i)
    #yr = str(yr.values)

    ##Cross section S-N trough DOW
#    conc.plot.imshow(figsize=(30,10),levels = levels_conc, colors = colors,origin = 'upper')
    conc = conc.isel(s=slice(None, -1))
    #imod.visualize.cross_section(conc, layers=False, levels = levels_conc, colors=colors, cbar_kwargs={'ticks':levels_conc, 'spacing':'proportional'}, vmin=0.0, vmax=16.0)
    fig, ax = imod.visualize.cross_section(conc, colors=colors_jet, levels=levels_conc, kwargs_colorbar={"spacing": "uniform"})
    fig, ax = imod.visualize.cross_section(is_aq_cross, levels=[0, 1], colors='none',hatches=['---']) 
#    plt.contourf(is_aq_cross, levels=[0, 1], colors='none', hatches=['---'], origin = 'upper')
    #plt.streamplot(coords["y"], coords["z"], vy.sel(x=43000, method = 'nearest'), vz.sel(x=43000, method = 'nearest'), arrowsize=1, density=3)
    yr = int(ds['time'].isel(time=i).dt.year)
    fig.suptitle(f"Chlorideverdeling {yr}")
    ax.set_xlabel("Afstand (m)")
    ax.set_ylabel("Hoogte (m NAP)")
    plt.savefig(f"Chloride_distribution_Harlingen_{yr}_JET.png", dpi=300, bbox_inches="tight")
    plt.close()


#plt.contourf(coords_CS['y'], coords_CS['z'], check_sand_3d.sel(x=x, method = 'nearest'), ,ylim=(-60,0))
for i in range (0,10,1):
    print (i)       
    conc = ds.isel(time=i)
    #conc= conc.where(conc>-0.01)
    conc= conc.where(~(conc < 0.0), other=0.01)
#    conc = xr.full_like(like, conc)
    #yr = ds['time'].isel(time=i)
    #yr = str(yr.values)
    # hatches
    hatch_top = is_aq_cross["top"].where(is_aq_cross == 0.0).dropna("layer", how="all")
    hatch_bot = is_aq_cross["bottom"].where(is_aq_cross == 0.0).dropna("layer", how="all")
    ##Cross section S-N trough DOW
#    conc.plot.imshow(figsize=(30,10),levels = levels_conc, colors = colors,origin = 'upper')
    conc = conc.isel(s=slice(None, -1))
    fig, ax = imod.visualize.cross_section(conc, colors=colors_jet, levels=levels_conc, kwargs_colorbar={"spacing": "uniform"})
    for layer in hatch_top["layer"]:
        ax.fill_between(
            hatch_top.s.values,
            hatch_bot.sel(layer=layer).values,
            hatch_top.sel(layer=layer).values,
            linestyle="--",
            facecolor="b",
            edgecolor="none",
        )
#    plt.contourf(is_aq_cross, levels=[0, 1], colors='none', hatches=['---'], origin = 'upper')
    #plt.streamplot(coords["y"], coords["z"], vy.sel(x=43000, method = 'nearest'), vz.sel(x=43000, method = 'nearest'), arrowsize=1, density=3)
    yr = int(ds['time'].isel(time=i).dt.year)
    fig.suptitle(f"Chlorideverdeling {yr}")
    ax.set_xlabel("Afstand (m)")
    ax.set_ylabel("Hoogte (m NAP)")
    plt.savefig(f"Chloride_distribution_Zandvoort_{yr}_JET.png", dpi=300, bbox_inches="tight")
    plt.close()
    


    

#budgets



##modelo V062
ds = imod.tec.read(r"e:\projects\dow\ref_model_150_yrs\results_ref150_rch_cal\conc\CONCVELO.TEC", 'vz')
######
##VELOCITY VECTORS
######

#Intervals (following TEC ONES)
levels_vz=[-0.05,-0.006,-0.0055,-0.005,-0.0045,-0.004,-0.0035,-0.003,-0.0025,
           -0.002, -0.0015,-0.001,-0.0009,-0.0008,-0.0007,-0.0006,-0.0005,
           -0.0004,-0.0003,-0.0002, -0.0001, 0, 0.0001, 0.0002, 0.0003, 0.0004,
           0.0005, 0.0006, 0.0007,0.0008,0.0009, 0.001, 0.0015, 0.002, 0.0025, 0.003,
           0.0035, 0.004, 0.0045, 0.005, 0.0055, 0.006, 0.05]


yr = 0
for i in range (0,300,30):
    print (i)
    vz = ds["vz"].isel(time = i)
    vz= vz.where(vz>-990.0)
    vz = xr.full_like(like, -vz)
    yr = ds['time'].isel(time=i)
    yr = str(yr.values)
    

    #vz.isel(column = 109).plot(figsize=(20,10),yincrease=False, levels=[-0.006,-0.005, -0.004, -0.003, -0.002, -0.001, 0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006], cmap = "RdYlBu")
    
    ##Cross section S-N trough DOW
    vz.sel(x = 43000., method = 'nearest').plot(figsize=(30,10),levels = levels_vz, cmap = "RdYlBu")
    plt.title("Vz_S_N_CENTRE_%s"%(yr))
    plt.savefig("Vz_S_N_CENTRE_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(x = 38000., method = 'nearest').plot(figsize=(30,10),levels = levels_vz, cmap = "RdYlBu")
    plt.title("Vz_S_N_WEST_%s"%(yr))
    plt.savefig("Vz_S_N_WEST_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(x = 48000., method = 'nearest').plot(figsize=(30,10),levels = levels_vz, cmap = "RdYlBu")
    plt.title("Vz_S_N_EST_%s"%(yr))
    plt.savefig("Vz_S_N_EST_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(y = 372000., method = 'nearest').plot(figsize=(30,10),levels = levels_vz, cmap = "RdYlBu")
    plt.title("Vz_W_E_NORTH_%s"%(yr))
    plt.savefig("Vz_W_E_NORTH_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(y = 367000., method = 'nearest').plot(figsize=(30,10),levels = levels_vz, cmap = "RdYlBu")
    plt.title("Vz_W_E_SOUTH_%s"%(yr))
    plt.savefig("Vz_W_E_SOUTH_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(z = 0., method = 'nearest').plot(figsize=(20,18.4),levels = levels_vz, cmap = "RdYlBu")
    plt.plot([43000,43000],[ymin, ymax], 'k--')
    plt.plot([38000,38000],[ymin, ymax], 'k--')
    plt.plot([48000,48000],[ymin, ymax], 'k--')
    plt.plot([xmin, xmax],[372000, 372000], 'k--')
    plt.plot([xmin, xmax],[367000, 367000], 'k--')
    plt.title("Vz Z = 0_%s"%(yr))
    #plt.grid('True')
    plt.savefig("Vz_Z_00_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(z = -10., method = 'nearest').plot(figsize=(20,18.4),levels = levels_vz, cmap = "RdYlBu")
    plt.plot([43000,43000],[ymin, ymax], 'k--')
    plt.plot([38000,38000],[ymin, ymax], 'k--')
    plt.plot([48000,48000],[ymin, ymax], 'k--')
    plt.plot([xmin, xmax],[372000, 372000], 'k--')
    plt.plot([xmin, xmax],[367000, 367000], 'k--')
    plt.title("Vz Z = -10_%s"%(yr))
    #plt.grid('True')
    plt.savefig("Vz__Z-10_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(z = -20., method = 'nearest').plot(figsize=(20,18.4),levels = levels_vz, cmap = "RdYlBu")
    plt.plot([43000,43000],[ymin, ymax], 'k--')
    plt.plot([38000,38000],[ymin, ymax], 'k--')
    plt.plot([48000,48000],[ymin, ymax], 'k--')
    plt.plot([xmin, xmax],[372000, 372000], 'k--')
    plt.plot([xmin, xmax],[367000, 367000], 'k--')
    plt.title("Z = -20_%s"%(yr))
    plt.savefig("Vz_Z-20_V061_w_s_%syr.PNG"%(yr))
    
    vz.sel(z = -30., method = 'nearest').plot(figsize=(20,18.4),levels = levels_vz, cmap = "RdYlBu")
    plt.plot([43000,43000],[ymin, ymax], 'k--')
    plt.plot([38000,38000],[ymin, ymax], 'k--')
    plt.plot([48000,48000],[ymin, ymax], 'k--')
    plt.plot([xmin, xmax],[372000, 372000], 'k--')
    plt.plot([xmin, xmax],[367000, 367000], 'k--')
    plt.title("Z = -30_%s"%(yr))
    plt.savefig("Vz_Z-30_V061_w_s_%syr.PNG"%(yr))

