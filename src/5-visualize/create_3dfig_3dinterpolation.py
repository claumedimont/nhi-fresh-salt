import imod
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
from pathlib import Path
import pyvista as pv

# filenames
fn_3dclclippedtmp = "data/5-visualization/v5/3dchloride_for_crosssections.nc"
fn_3dclpyvista = "data/5-visualization/v5/3dchloride_for_pyvista.nc"
fn_toppyvista = "data/5-visualization/v5/top_for_pyvista.nc"
fn_legend = "data/1-external/legends/chloride.leg"
fn_shapenl = "data/1-external/provincie/provincie.shp"
outputdir = Path(f"./reports/figures/3dimage")
outputdir.mkdir(exist_ok=True, parents=True)

############ 3D Chloride #############
try:
    cl = xr.open_dataarray(fn_3dclpyvista)
    top = xr.open_dataarray(fn_toppyvista)
except IOError:
    cl = xr.open_dataarray(fn_3dclclippedtmp)  # , chunks={"layer": 1})["3d-chloride"]
    toplay = imod.select.upper_active_layer(cl.sel(percentile="p50"), False)
    top = cl.top.where(cl.layer == toplay).max(dim="layer")

    # to DataArray suited for imod 3d visualize
    dims = ("percentile", "z", "y", "x")
    coords = {
        "percentile": cl.percentile.values,
        "z": cl.z.values,
        "y": cl.y.values.astype(float),
        "x": cl.x.values.astype(float),
        "dz": ("z", cl.dz.values),
    }
    cl = xr.DataArray(cl.data, coords, dims, name="3d-chloride")
    cl = cl.sortby("z", ascending=True)
    cl = cl.sortby("x", ascending=False)
    cl = cl.sortby("y", ascending=False)

    cl.to_netcdf(fn_3dclpyvista)
    top.to_netcdf(fn_toppyvista)


##### NL border for overlay #####
gdf = gpd.read_file(fn_shapenl)
nlpoly = gdf.geometry
lines = []
for poly in nlpoly:
    if isinstance(poly, Polygon):
        poly = [poly]
    for p_ in poly:
        lines.append(
            imod.visualize.line_3d(p_, z=(top + 5.0) * 100.0)
        )  # 100m, * 100 vertical exagg

########### Create 3d grid ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)
annotations = {i - 1: f"{j/1000.:0.2f}" for i, j in enumerate([0] + levels)}
annotations[len(levels)] = "(g/L)"
cmap, norm = imod.visualize.cross_sections._cmapnorm_from_colorslevels(colors, levels)


for p in cl.percentile.values:
    print(p)
    p50 = cl.sel(percentile=p)

    # normalize to classes
    p50n = xr.apply_ufunc(norm, p50, dask="parallelized", output_dtypes=[np.int16])
    p50n = p50n.where(~p50.isnull())

    ############################################################
    #  SLICED
    # delete two slices
    # 515000 - 525000, 415000 - 425000
    #p50nc = p50n.where(
    #    (p50n.y > 535000) | (p50n.y < 405000) | ((p50n.y > 435000) & (p50n.y < 505000))
    #)
    slices = [415000, 475000, 535000, 595000]
    b = p50n.y > 0
    for slice_ in slices:
        b = b & ~((p50n.y > (slice_ - 20000)) & (p50n.y < slice_))
    p50nc = p50n.where(b)
    p50nc = p50nc.compute()

    grid = imod.visualize.grid_3d(p50nc, vertical_exaggeration=100.0)

    # grid.plot(cmap=cmap)
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(
        grid,
        cmap=colors,
        stitle=f"NHI fresh-salt {p} groundwater Cl concentration",
        clim=[-1, 8],
        annotations=annotations,
        scalar_bar_args={"n_labels": 0},
        ambient=0.1,
        # scalar_bar_args={"title": "Legend"},
        # show_scalar_bar=True,
    )
    for line in lines:
        plotter.add_mesh(line, color="#ffffff", line_width=1)
    # cpos = [
    #    (-48525.08168213184, 92434.18174194878, 366608.6240665396),
    #    (140750.0, 462500.0, -23750.0),
    #    (0.2917349089321565, 0.6195128387517468, 0.7287623656117945),
    # ]
    cpos = [
        (8729.64440177528, 68741.71982973302, 395127.5673167352),
        (121757.02454128035, 452178.5276657496, -37341.64306128947),
        (0.048531026096998955, 0.7410392336819702, 0.669705602223851),
    ]
    cpos = plotter.show(
        cpos=cpos,
        window_size=[2048, 1536],
        screenshot=(outputdir / f"3dchloride_sliced_{p}.png").as_posix(),
    )
    # auto_close=False, interactive=False,
    # plotter.screenshot((outputdir / "3dchloride_sliced.png").as_posix())
    # plotter.close()

    ############################################################
    #  TRANSECTS
    # show only at every 25 k
    xshow = p50.x.where((p50.x - 125) % 25000 == 0).dropna(dim="x")
    yshow = p50.y.where((p50.y - 125) % 25000 == 0).dropna(dim="y")
    p50nc = p50n.where((p50n.x.isin(xshow)) | (p50n.y.isin(yshow)))
    p50nc = p50nc.compute()

    grid = imod.visualize.grid_3d(p50nc, vertical_exaggeration=100.0)

    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(
        grid,
        cmap=colors,
        stitle=f"NHI fresh-salt {p} groundwater Cl concentration",
        clim=[-1, 8],
        annotations=annotations,
        scalar_bar_args={"n_labels": 0},
        ambient=0.3,
        # scalar_bar_args={"title": "Legend"},
        # show_scalar_bar=True,
    )
    for line in lines:
        plotter.add_mesh(line, color="#ffffff", line_width=1)
    cpos = [
        (8729.64440177528, 68741.71982973302, 395127.5673167352),
        (121757.02454128035, 452178.5276657496, -37341.64306128947),
        (0.048531026096998955, 0.7410392336819702, 0.669705602223851),
    ]
    plotter.ren_win.OffScreenRenderingOn()
    plotter.enable_anti_aliasing()
    plotter.set_position((8729.64440177528, 68741.71982973302, 395127.5673167352))
    plotter.set_focus((121757.02454128035, 452178.5276657496, -37341.64306128947))
    plotter.set_viewup((0.048531026096998955, 0.7410392336819702, 0.669705602223851))

    cpos = plotter.show(
        cpos=cpos,
        window_size=[2048, 1536],
        screenshot=outputdir / f"3dchloride_transects_{p}.png",
    )
    # auto_close=False, interactive=False,
    # plotter.screenshot((outputdir / "3dchloride_transects.png").as_posix())
    # plotter.close(off_screen=True)
