import imod
import numpy as np
import os
import pandas as pd
import geopandas as gpd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
import configparser
from pathlib import Path
from shapely.geometry import LineString, Point
from shapely.ops import nearest_points
import sys

sys.path.append(os.path.dirname(__file__))  # add script directory to python path

# filenames
fn_transects = "data/1-external/cross_section_locations/combined.shp"
#fn_transects = "data/1-external/cross_section_locations/additional.shp"
fn_cl = f"data/2-interim/starting_concentration.nc"
fn_clcorr = f"data/2-interim/concentration_corrected.nc"
fn_topbot = "data/2-interim/layermodel.nc"
fn_conductivity = "data/2-interim/conductivity.nc"
fn_legend = "data/1-external/legends/chloride_gL.leg"
outputdir = Path(f"./reports/figures/salinity_correction")
overwrite = False#True

############ 3D Chloride #############
cl = xr.open_dataarray(fn_clcorr)
clorg = xr.open_dataset(fn_cl)["sconc"]
layermodel = xr.open_dataset(fn_topbot)
conductivity = xr.open_dataset(fn_conductivity)
is_aquitard = (conductivity["kh"] < 1.).astype(int)

cl = cl.assign_coords(top=layermodel["top"], bottom=layermodel["bot"])
clorg = clorg.assign_coords(top=layermodel["top"], bottom=layermodel["bot"])

########### Create cross sections ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)

# read transects from shapefile
df_transects = gpd.read_file(fn_transects)

outputdir.mkdir(exist_ok=True, parents=True)

for name, transect in zip(df_transects.Name, df_transects.geometry):
    if overwrite or not (outputdir / f"crosssection_{name.replace(' ','')}.png").exists():
        print(name)
        #break
        nrows = 2#len(cl.time)
        
        cmap = None
        norm = None
        top = imod.select.cross_section_linestring(layermodel["top"], transect)
        bot = imod.select.cross_section_linestring(layermodel["bot"], transect)
        aquitards = imod.select.cross_section_linestring(is_aquitard, transect)
        aquitards = aquitards.assign_coords(top=top)
        aquitards = aquitards.assign_coords(bottom=bot)
        aquitards.coords["ds"] = np.abs(aquitards.coords["ds"])
        
        #for i, t in enumerate(cl.time.values):  # ["p50"]:#
        i=0
        ### select cross section
        da = imod.select.cross_section_linestring(cl, transect)
        daorg = imod.select.cross_section_linestring(clorg, transect)
        #da = da.assign_coords(top=top)
        #da = da.assign_coords(bottom=bot)

        da.coords["ds"] = np.abs(da.coords["ds"])
        daorg.coords["ds"] = np.abs(daorg.coords["ds"])
        # print(da)
        
        fig, (ax1, ax2) = plt.subplots(nrows=nrows, squeeze=True, figsize=(6.4, 3.2 * nrows))
        fig.suptitle(f"Transect {name}")
        #df.to_csv("test.csv")
        _ = imod.visualize.cross_section(
                da,
                colors=colors,
                levels=levels,
                layers=False,
                aquitards=aquitards,
                kwargs_colorbar={"whiten_triangles":False},
                return_cmap_norm=False,
                fig=fig,
                ax=ax1,
            )
        ax1.set_ylim(bottom=-350.0, top=25.0)
        ax1.set_title(f"Corrected")

        _ = imod.visualize.cross_section(
                daorg,
                colors=colors,
                levels=levels,
                layers=False,
                aquitards=aquitards,
                kwargs_colorbar={"whiten_triangles":False},
                return_cmap_norm=False,
                fig=fig,
                ax=ax2,
            )
        ax2.set_ylim(bottom=-350.0, top=25.0)
        ax2.set_title(f"Original")

        plt.tight_layout(rect=(0, 0, 1, 0.95))
        plt.savefig(
            outputdir / f"crosssection_{name.replace(' ','')}.png",
            dpi=300,
            bbox_inches="tight",
        )  # save w/o measurements
        plt.close()
    else:
        print(f"Crosssection {name} exists, skipping")
