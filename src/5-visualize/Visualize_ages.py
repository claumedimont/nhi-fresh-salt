# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 21:36:26 2020

@author: janssen_gs
"""

import imod
import xarray as xr
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt

top = imod.idf.open(r"data\3-input\bas\top_l*.idf")
bottom = imod.idf.open(r"data\3-input\bas\bottom_l*.idf")
ahn = imod.idf.open(r"data\1-external\AHN\AHN_F250.IDF")
age = imod.idf.open(r"data\4-output\Ages\Age_v0\Age*.idf")

_, xmin, xmax, _, ymin, ymax = imod.util.spatial_reference(age)
top = top.sel(y=slice(ymax, ymin), x=slice(xmin, xmax))
bottom = bottom.sel(y=slice(ymax, ymin), x=slice(xmin, xmax))
ahn = ahn.sel(y=slice(ymax, ymin), x=slice(xmin, xmax))

age = age.assign_coords(top=(("layer", "y", "x"), top))
age = age.assign_coords(bottom=(("layer", "y", "x"), bottom))

dst_top = xr.ones_like(age.sel(layer=1)) * (ahn.data - 9.9)
dst_top = dst_top.expand_dims("layer")
dst_bot = xr.ones_like(age.sel(layer=1)) * (ahn.data - 10.1)
dst_bot = dst_bot.expand_dims("layer")
layer_regridder = imod.prepare.LayerRegridder(method="mean")
dst = layer_regridder.regrid(age, age["top"], age["bottom"], dst_top, dst_bot)


agesel = age.where((age["top"] > (ahn-10.))&(age["bottom"] <= (ahn-10.)))
agesel = agesel.min(dim="layer")

#Plot ages
provincies = gpd.read_file(r'data\1-external\gis\B1_Provinciale_indeling_van_NederlandPolygon.shp')
waterlopen = gpd.read_file(r'data\1-external\gis\newwalo.shp')
legfile = r"data\1-external\gis\Ages.leg"
mask = imod.idf.open(r'data\3-input\mask.IDF')

overlays = [{"gdf": provincies,"color":"none", "edgecolor": "black","linewidth":0.5},{"gdf": waterlopen,"color":"blue", "edgecolor": "blue","linewidth":0.3}]
legend = imod.visualize.spatial.read_imod_legend(legfile)
for i in [5., 10., 15., 20., 25., 30., 35., 40., 45., 50.]:
    # dst_top = xr.ones_like(age.sel(layer=1)) * (ahn - i + 0.01)
    # dst_top = dst_top.expand_dims("layer")
    # dst_bot = xr.ones_like(age.sel(layer=1)) * (ahn - i - 0.01)
    # dst_bot = dst_bot.expand_dims("layer")
    # layer_regridder = imod.prepare.LayerRegridder(method="mean")
    # dst = layer_regridder.regrid(age, age["top"], age["bottom"], dst_top, dst_bot)

    dst = age.where((age["top"] > (ahn-i))&(age["bottom"] <= (ahn-i)))
    idf = dst.min(dim="layer")

    #print("Plotten figuur " + str(i) + "...")
    # idf = dst.isel(layer = 0) #da_heads_change.isel(layer = i)
    idf = idf*mask  ## Limburg eraf knippen
    
    idf.attrs["units"] = "Yr"
    fig, ax, cbar = imod.visualize.spatial.plot_map(idf,legend[0],legend[1],overlays,figsize=(10,10), return_cbar = True)
    title = "Groundwater age " + str(int(i)) + " m.b.s.s."
    ax.set_title(title,fontsize=24, fontweight=2.0)
    cbar.set_label(label = idf.attrs["units"], fontsize=20,weight='bold')
    ax.axis('off')
    #fig = ax.figure
    fig.tight_layout()
    fig.savefig(f"data/4-output/Ages/Age_v0/{title}.png", dpi=150)
    plt.close()


dst.isel(layer=0).plot.imshow(levels=np.arange(0.0, 100, 5.))
#grid = imod.visualize.grid_3d(age)
#grid.plot()