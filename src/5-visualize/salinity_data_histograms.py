import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import FuncFormatter
import seaborn as sns
import configparser
from pathlib import Path
from sqlalchemy import create_engine, func

# filenames
# fn_transects = "data/1-external/cross_section_locations/additional.shp"
fn_cfg = "src/1-prepare/chloride_database/credentials_zoetzout_remote.cfg"
path_output = Path(f"./reports/figures/histograms")
path_output.mkdir(exist_ok=True)

#fallback optie: lees punten uit shapefiles
fn_analyses = "c:/Users/delsman/OneDrive - Stichting Deltares/Documents/2020/LHM zoetzout/GIS/xyzv_analyses_gw_aggregate2.shp"
fn_geophysics = "c:/Users/delsman/OneDrive - Stichting Deltares/Documents/2020/LHM zoetzout/GIS/xyzv_geophysics.shp"



############ Database connection #############
cf = configparser.ConfigParser()
cf.read(fn_cfg)
cf = dict(cf["Postgis"])
connstr = f'postgres://{cf["user"]}:{cf["pwd"]}@{cf["host"]}:5432/{cf["dbname"]}'
engine = create_engine(connstr, echo=True)

markers = {
    "ANA": "o",
    "BL": "p",
    "ECPT": "D",
    "VES": "d",
    "AEM": "v",
    "FHEM": "v",
    "TEC": "h",
    "SF": "H",
    "SOFT_SEA": "^",
    "SOFT_DEEP": "<",
    "SOFT_OTH": "^",
    "SOFT_FRESHBRACK": ">",
    "SOFT_BRACKSALT": ">",
    "SOFT_TRANSGR": "*",
    "SOFT_CONNATE": "X",
}
labels = {
    "ANA": "Chemical analysis",
    "BL": "Bore log",
    "ECPT": "ECPT",
    "VES": "VES",
    "AEM": "Airborne EM",
    "FHEM": "Freshem",
    "TEC": "ECPT",    # to minimize categories
    "SF": "Bore log",  # to minimize categories
    "SOFT_SEA": "soft - zee",
    "SOFT_DEEP": "soft - diep",
    "SOFT_OTH": "soft - anders",
    "SOFT_FRESHBRACK": "soft - zoet-brak",
    "SOFT_BRACKSALT": "soft - brak-zout",
    "SOFT_TRANSGR": "soft - transgressie",
    "SOFT_CONNATE": "soft - connaat",
}

### get measurement points from database and plot
# - chloride analyses
sql = f"SELECT * FROM xyzv_analyses_gw_aggregate"
df_ana = gpd.read_postgis(sql, engine, "geometrypoint")

df_ana = gpd.read_file(fn_analyses)


# - geophysics
sql = f"SELECT * FROM xyzv_geophysics"
df_gp = gpd.read_postgis(sql, engine, "geometrypoint")

df_gp = gpd.read_file(fn_geophysics)

# merge into one big dataframe
df_ana["typecode"] = "ANA"
df_ana["datetime"] = df_ana["end_time"]   # for analyses: take last value and last time
df_ana["value"] = df_ana["latest_val"]

df = pd.concat((df_ana, df_gp), axis=0, ignore_index=True)
df["Measurement type"] = df["typecode"].map(labels)
#df["year"] = df["datetime"].dt.year
df["year"] = df["datetime"].apply(lambda x:int(x[:4]))

def thousands(x, pos):
    'The two args are the value and tick position'
    return '%1.1fk' % (x * 1e-3)
formatter = FuncFormatter(thousands)

sns.set(style="whitegrid", font="Arial")
sns.set_context("paper")
# create a color palette (we only have three different colors for the three different tests T1...T3)
pal = "deep"#sns.color_palette()#"pastel"#sns.color_palette(n_colors=3)

# plot stacked histograms for value, year and depth
f = plt.figure(figsize=(10,8))
for i, (col, lab) in enumerate(zip(["value","year","z"],["Concentration (g/L Cl-)", "Measurement year", "Z-coordinate (m MSL)"])):
    ax = f.add_subplot(2,2,i+1)
    sns.histplot(data=df, ax=ax, stat="count", multiple="stack",
                x=col, kde=False, bins=30, #log_scale=log,
                palette=pal, hue="Measurement type",
                element="bars", legend=(i==0))
    ax.set_xlabel(lab)
    if i%2 == 0:
        ax.set_ylabel("Count")
    ax.yaxis.set_major_formatter(formatter)
    if col == "value":
        xlim = ax.get_xlim()
        ax.set_xlim(-0.5, 20.)
f.text(0.01, 0.98, "a)", va="top", fontsize="x-large")
f.text(0.51, 0.98, "b)", va="top", fontsize="x-large")
f.text(0.01, 0.48, "c)", va="top", fontsize="x-large")
f.text(0.51, 0.48, "d)", va="top", fontsize="x-large")
plt.tight_layout()
plt.savefig(path_output / f"histograms.png", bbox_inches="tight", dpi=300)
plt.savefig(path_output / f"histograms.svg", bbox_inches="tight", dpi=300)
plt.close()

## faceted







"""





# plot stacked histograms (measurement types stacked, columns varying)
##### FIRST: VALUE
# for value: display a broken y-axis
# from: https://gist.github.com/pfandzelter/0ae861f0dee1fb4fd1d11344e3f85c9e

ylim  = [150_000, 220_000]
ylim2 = [0, 30_000]
ylimratio = (ylim[1]-ylim[0])/(ylim2[1]-ylim2[0]+ylim[1]-ylim[0])
ylim2ratio = (ylim2[1]-ylim2[0])/(ylim2[1]-ylim2[0]+ylim[1]-ylim[0])
gs = gridspec.GridSpec(2, 1, height_ratios=[ylimratio, ylim2ratio])
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax1.set_ylim(ylim)
ax2.set_ylim(ylim2)


f, (ax1, ax2) = plt.subplots(ncols=1, nrows=2,
                             sharex=True, figsize=(7,5))
ax1 = sns.histplot(data=df, ax=ax1, stat="count", multiple="stack",
             x="value", kde=False,
             palette=pal, hue="Measurement type",
             element="bars", legend=True)

# we basically do the same thing again for the second plot
ax2 = sns.histplot(data=df, ax=ax2, stat="count", multiple="stack",
             x="value", kde=False,
             palette=pal, hue="Measurement type",
             element="bars", legend=False)

# here is the fun part: setting the limits for the individual y axis
ax1.set_ylim(150_000, 220_000)
ax2.set_ylim(0, 30_000)
ax2.set_xlim(0, 19.)
# the upper part does not need its own x axis as it shares one with the lower part
#ax1.get_xaxis().set_visible(False)

# by default, each part will get its own "Latency in ms" label, but we want to set a common for the whole figure
# first, remove the y label for both subplots
ax1.set_ylabel("")
ax2.set_ylabel("")
ax1.yaxis.set_major_formatter(formatter)
ax2.yaxis.set_major_formatter(formatter)

# then, set a new label on the plot (basically just a piece of text) and move it to where it makes sense (requires trial and error)
f.text(0.05, 0.55, "Count", va="center", rotation="vertical")

# by default, seaborn also gives each subplot its own legend, which makes no sense at all
# soe remove both default legends first
#ax1.get_legend().remove()
#ax2.get_legend().remove()
# then create a new legend and put it to the side of the figure (also requires trial and error)
#ax2.legend(loc=(1.025, 0.5), title="Measurement type")

# let's put some ticks on the top of the upper part and bottom of the lower part for style
#ax1.spines["bottom"].set_visible(False)
#ax2.spines["top"].set_visible(False)
#ax1.xaxis.tick_top()
ax2.xaxis.tick_bottom()
ax1.grid(visible=True, which="both")
ax2.grid(visible=True, which="both")

# finally, adjust everything a bit to make it prettier (this just moves everything, best to try and iterate)
f.subplots_adjust(left=0.15, hspace=0.05, wspace=0.05)  #left=0.15, right=0.85, bottom=0.15, top=0.85, 

d = .01  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
kwargs = dict(transform=ax1.transAxes, color="k", clip_on=False, zorder=10)
ax1.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
ax1.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
plt.show()
gc.collect()




"""