import imod
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
from pathlib import Path
import pyvista as pv
import warnings
warnings.filterwarnings("ignore")
np.seterr(all="ignore")

version = "v2.3.0_scenario1"

# filenames
fn_conc = f"data/4-output/{version}/conc/conc*.idf"
fn_flf =  f"data/4-output/{version}/bdgflf/bdgflf*.idf"
fn_drn =  f"data/4-output/{version}/bdgdrn/bdgdrn*.idf"  # for boils
#fn_layermodel = f"data/2-interim/layermodel_corr.nc"
fn_legend = "data/1-external/legends/chloride_gL.leg"
fn_shapenl = "data/1-external/provincie/provincie.shp"
outputdir = Path(f"data/5-visualization/{version}/saltload")
outputdir.mkdir(exist_ok=True, parents=True)

cl = imod.idf.open(fn_conc)
flf = imod.idf.open(fn_flf)
drn = imod.idf.open(fn_drn)
#layermodel = xr.open_dataset(fn_layermodel)
#layermodel = layermodel.reindex_like(cl.isel(time=0))

diffuse = flf.sel(layer=10) * cl.sel(layer=11)
boils = (drn.sel(layer=[11,12,13,14,15]) * cl.sel(layer=[11,12,13,14,15])).sum(dim="layer")
saltload = diffuse + boils

imod.idf.save(outputdir / "saltload", saltload)
