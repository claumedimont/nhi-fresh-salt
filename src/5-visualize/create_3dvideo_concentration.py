import imod
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
from pathlib import Path
import pyvista as pv
import warnings
warnings.filterwarnings("ignore")
np.seterr(all="ignore")

version = "v2.3.0_scenario1"

# filenames
fn_conc = f"data/4-output/{version}/conc/conc*.idf"
fn_layermodel = f"data/2-interim/layermodel_corr.nc"
fn_legend = "data/1-external/legends/chloride_gL.leg"
fn_shapenl = "data/1-external/provincie/provincie.shp"
outputdir = Path(f"./reports/videos/{version}")
outputdir.mkdir(exist_ok=True, parents=True)

############ 3D Chloride #############
cl = imod.idf.open(fn_conc)
layermodel = xr.open_dataset(fn_layermodel)
layermodel = layermodel.reindex_like(cl.isel(time=0))

# to DataArray suited for imod 3d visualize
cl = cl.assign_coords(top=(("layer", "y", "x"), layermodel["top"]))
cl = cl.assign_coords(bottom=(("layer", "y", "x"), layermodel["bot"]))


##### NL border for overlay #####
toplay = imod.select.upper_active_layer(layermodel["ibound"])
top = layermodel["top"].where(layermodel["top"].layer == toplay).max(dim="layer")
gdf = gpd.read_file(fn_shapenl)
nlpoly = gdf.geometry
lines = []
for poly in nlpoly:
    if isinstance(poly, Polygon):
        poly = [poly]
    for p_ in poly:
        lines.append(
            imod.visualize.line_3d(p_, z=(top + 5.0) * 100.0)
        )  # 100m, * 100 vertical exagg

########### Create 3d grid ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)
annotations = {i - 1: f"{j:0.2f}" for i, j in enumerate([0] + levels)}
annotations[len(levels)] = "(g/L)"
cmap, norm = imod.visualize.common._cmapnorm_from_colorslevels(colors, levels)


# normalize to classes
cln = xr.apply_ufunc(norm, cl, dask="parallelized", output_dtypes=[np.int16])
cln = cln.where(~cl.isnull())


############################################################
#  SLICED
# delete two slices
# 515000 - 525000, 415000 - 425000
#clnc = cln.where(
#    (cln.y > 535000) | (cln.y < 405000) | ((cln.y > 435000) & (cln.y < 505000))
#)
# delete three slices
# 515000 - 525000, 415000 - 425000
slices = [415000, 475000, 535000, 595000]
b = cln.y > 0
for slice_ in slices:
    b = b & ~((cln.y > (slice_ - 20000)) & (cln.y < slice_))
clnc = cln.where(b)
clnc = clnc.compute()


mesh_kwargs = {
    "cmap": colors,
    "stitle": "Groundwater Cl concentration - {timestamp:%Y}",
    "clim": [-1, 8],
    "annotations": annotations,
    "scalar_bar_args": {"n_labels": 0},
    "ambient": 0.1,
}

anim = imod.visualize.GridAnimation3D(
    clnc,
    vertical_exaggeration=100.0,
    mesh_kwargs=mesh_kwargs,
    plotter_kwargs={"window_size": [1680, 1024], "off_screen": True},
)
for line in lines:
    anim.plotter.add_mesh(line, color="#ffffff", line_width=1)
anim.plotter.set_position((8729.64440177528, 68741.71982973302, 395127.5673167352))
anim.plotter.set_focus((121757.02454128035, 452178.5276657496, -37341.64306128947))
anim.plotter.set_viewup((0.048531026096998955, 0.7410392336819702, 0.669705602223851))
anim.write(outputdir / "concentration_sliced.mp4", framerate=2)

"""
############################################################
#  TRANSECTS
# show only at every 25 k
xshow = cl.x.where((cl.x - 125) % 25000 == 0).dropna(dim="x")
yshow = cl.y.where((cl.y - 125) % 25000 == 0).dropna(dim="y")
clnc = cln.where((cln.x.isin(xshow)) | (cln.y.isin(yshow)))
#clnc = clnc.compute()

mesh_kwargs = {
    "cmap": colors,
    "stitle": "Groundwater Cl concentration - {timestamp:%Y}",
    "clim": [-1, 8],
    "annotations": annotations,
    "scalar_bar_args": {"n_labels": 0},
    "ambient": 0.3,
}

anim = imod.visualize.GridAnimation3D(
    clnc,
    vertical_exaggeration=100.0,
    mesh_kwargs=mesh_kwargs,
    plotter_kwargs={"window_size": [1680, 1024], "off_screen": True},
)
for line in lines:
    anim.plotter.add_mesh(line, color="#ffffff", line_width=1)
anim.plotter.set_position((8729.64440177528, 68741.71982973302, 395127.5673167352))
anim.plotter.set_focus((121757.02454128035, 452178.5276657496, -37341.64306128947))
anim.plotter.set_viewup((0.048531026096998955, 0.7410392336819702, 0.669705602223851))
anim.write(outputdir / "concentration_transects.mp4", framerate=2)
"""