import imod
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
import configparser
from pathlib import Path
from sqlalchemy import create_engine, func
from shapely.geometry import LineString, Point
from shapely.ops import nearest_points

version = "v5"

# filenames
# fn_transects = "data/1-external/cross_section_locations/additional.shp"
fn_transects = "data/1-external/cross_section_locations/combined.shp"
fn_cfg = "src/1-prepare/chloride_database/credentials_zoetzout_remote.cfg"
fn_3dcl = f"data/1-external/3D-chloride/{version}/Chloride_normlog_25_50_75.nc"
fn_3dcltmp = f"data/5-visualization/{version}/3dchloride_inctopbot.nc"
fn_3dclclippedtmp = f"data/5-visualization/{version}/3dchloride_for_crosssections.nc"
fn_topbot = "data/2-interim/layermodel.nc"
fn_conductivity = "data/2-interim/conductivity.nc"
fn_legend = "data/1-external/legends/chloride.leg"
outputdir = Path(f"./reports/figures/{version}")
overwrite = False
plot_softdata = True
buffer = 500  # buffer around transects to retrieve measurement data


############ Database connection #############
cf = configparser.ConfigParser()
cf.read(fn_cfg)
cf = dict(cf["Postgis"])
connstr = f'postgres://{cf["user"]}:{cf["pwd"]}@{cf["host"]}:5432/{cf["dbname"]}'
engine = create_engine(connstr, echo=True)


############ 3D Chloride #############
try:
    cl2 = xr.open_dataarray(fn_3dclclippedtmp)
except IOError:
    cl = xr.open_dataarray(fn_3dcl)
    cl = cl.rename(
        {
            "x-coordinates": "x",
            "y-coordinates": "y",
            "z-coordinates": "z",
            "Chloride dimension": "percentile",
        }
    )
    cl = cl.sortby("z", ascending=False)  # flip z
    z = cl.z.to_series().astype(float)
    z = z.reset_index(drop=True)
    top = z.rolling(2).mean()
    bot = top.shift(-1)
    top.iloc[0] = z.iloc[0] + (z.iloc[0] - bot.iloc[0])
    bot.iloc[-1] = z.iloc[-1] - (top.iloc[-1] - z.iloc[-1])
    dz = top - bot
    z = 0.5 * top + 0.5 * bot

    cl.coords["z"] = range(1, len(z) + 1)
    cl.coords["percentile"] = ["p25", "p50", "p75"]
    cl = cl.rename({"z": "layer"})
    cl = cl.transpose("percentile", "layer", "y", "x")

    # to DataArray suited for idf save
    dims = ("percentile", "layer", "y", "x")
    coords = {
        "percentile": cl.percentile.values,
        "layer": cl.layer.values,
        "y": cl.y.values.astype(float),
        "x": cl.x.values.astype(float),
        "z": ("layer", z),
        "dz": ("layer", dz),
        "top": ("layer", top),
        "bottom": ("layer", bot),
    }
    cl2 = xr.DataArray(cl.values, coords, dims, name="3d-chloride")
    cl2 = cl2.sortby("y", ascending=False)  # flip y

    # save temporary dataset
    cl2.to_netcdf(fn_3dcltmp)

    ############ Clip to layer model ############
    topbot = xr.open_dataset(fn_topbot)
    top = topbot["top"]
    bot = topbot["bot"]
    # top of upper active layer
    top = top.where(
        top.layer == imod.select.upper_active_layer(top, is_ibound=False)
    ).max(dim="layer")
    # for bot: flip layers first
    bot = bot.sortby("layer", ascending=False)
    bot = bot.where(
        bot.layer == imod.select.upper_active_layer(bot, is_ibound=False)
    ).max(dim="layer")

    # set cl2 to nodata if outside bounds
    cl2 = cl2.where(~top.isnull())
    cl2 = cl2.where((cl2.bottom < top) & (cl2.top > bot))
    cl2.to_netcdf(fn_3dclclippedtmp)

########### Create cross sections ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)
markers = {
    "ANA": "o",
    "BL": "p",
    "ECPT": "D",
    "VES": "d",
    "AEM": "v",
    "FHEM": "v",
    "TEC": "h",
    "SF": "H",
    "SOFT_SEA": "^",
    "SOFT_DEEP": "<",
    "SOFT_OTH": "^",
    "SOFT_FRESHBRACK": ">",
    "SOFT_BRACKSALT": ">",
    "SOFT_TRANSGR": "*",
    "SOFT_CONNATE": "X",
}
labels = {
    "ANA": "chemische analyse",
    "BL": "boorgatmeting",
    "ECPT": "ECPT",
    "VES": "VES",
    "AEM": "AEM",
    "FHEM": "Freshem",
    "TEC": "prikstok",
    "SF": "SlimFlex",
    "SOFT_SEA": "soft - zee",
    "SOFT_DEEP": "soft - diep",
    "SOFT_OTH": "soft - anders",
    "SOFT_FRESHBRACK": "soft - zoet-brak",
    "SOFT_BRACKSALT": "soft - brak-zout",
    "SOFT_TRANSGR": "soft - transgressie",
    "SOFT_CONNATE": "soft - connaat",
}

# conductivity
layermodel = xr.open_dataset(fn_topbot)
conductivity = xr.open_dataset(fn_conductivity)
is_aquitard = (conductivity["kh"] < 1.0).astype(int)

# read transects from shapefile
df_transects = gpd.read_file(fn_transects)

outputdir.mkdir(exist_ok=True)

for name, transect in zip(df_transects.Name, df_transects.geometry):
    if (
        overwrite
        or not (outputdir / f"cross_section_3d_{name.replace(' ','')}.png").exists()
    ):
        print(name)
        
        nrows = len(cl2.percentile)
        fig, axes = plt.subplots(nrows=nrows, squeeze=False, figsize=(6.4, 3.2 * nrows))
        fig.suptitle(f"Transect {name}")

        cmap, norm = imod.visualize.common._cmapnorm_from_colorslevels(colors, levels)
        top = imod.select.cross_section_linestring(layermodel["top"], transect)
        bot = imod.select.cross_section_linestring(layermodel["bot"], transect)
        aquitards = imod.select.cross_section_linestring(is_aquitard, transect)
        aquitards = aquitards.assign_coords(top=top)
        aquitards = aquitards.assign_coords(bottom=bot)
        aquitards.coords["ds"] = np.abs(aquitards.coords["ds"])
        plot_attrs = {
            "s": 50,
            "cmap": cmap,
            "norm": norm,
            "linewidths": .5,
            "linestyles":":",
            "edgecolors": "k",
        }

        # run for each percentile
        for i, pct in enumerate(cl2.percentile.values):  # ["p50"]:#

            ### select cross section
            da = imod.select.cross_section_linestring(cl2.sel(percentile=pct), transect)
            da.coords["ds"] = np.abs(da.coords["ds"])
            fig, axes[i, 0] = imod.visualize.cross_section(
                da,
                colors=colors,
                levels=levels,
                layers=False,
                aquitards=aquitards,
                kwargs_colorbar={"whiten_triangles": False},
                return_cmap_norm=False,
                fig=fig,
                ax=axes[i, 0],
            )
            axes[i, 0].set_ylim(bottom=-350.0, top=25.0)
            axes[i, 0].set_title(f"Percentiel {pct}")

        plt.tight_layout(rect=(0, 0, 1, 0.95))
        plt.savefig(
            outputdir / f"cross_section_3d_{name.replace(' ','')}_nomeas.png",
            dpi=300,
            bbox_inches="tight",
        )  # save w/o measurements

        # run for each percentile
        for i, pct in enumerate(cl2.percentile.values):  # ["p50"]:)

            ### get measurement points from database and plot
            # - chloride analyses
            sql = f"SELECT * FROM xyzv_analyses_gw_aggregate WHERE ST_DWithin( ST_GeomFromText('{str(transect)}', 28992), geometrypoint, {buffer} )"
            df = gpd.read_postgis(sql, engine, "geometrypoint")
            if len(df):
                df["s_transect"] = df["geometrypoint"].apply(
                    lambda x: transect.project(x)
                )  # map points on transect
                axes[i, 0].scatter(
                    df["s_transect"],
                    df["z"],
                    c=df["median_value"] * 1000.0,
                    marker=markers["ANA"],
                    label=labels["ANA"],
                    **plot_attrs
                )

            # - geophysics
            sql = f"SELECT * FROM xyzv_geophysics WHERE ST_DWithin( ST_GeomFromText('{str(transect)}', 28992), geometrypoint, {buffer} )"
            df = gpd.read_postgis(sql, engine, "geometrypoint")
            df["s_transect"] = df["geometrypoint"].apply(
                lambda x: transect.project(x)
            )  # map points on transect
            for t in df.typecode.unique():
                dfsel = df.loc[df.typecode == t]
                if len(dfsel):
                    axes[i, 0].scatter(
                        dfsel["s_transect"],
                        dfsel["z"],
                        c=dfsel["value"] * 1000.0,
                        marker=markers[t],
                        label=labels[t],
                        **plot_attrs
                    )

            # - freshem
            sql = f"SELECT * FROM xyzv_freshem_p50 WHERE ST_DWithin( ST_GeomFromText('{str(transect)}', 28992), geometrypoint, {buffer} )"
            df = gpd.read_postgis(sql, engine, "geometrypoint")
            if len(df):
                df["s_transect"] = df["geometrypoint"].apply(
                    lambda x: transect.project(x)
                )  # map points on transect
                axes[i, 0].scatter(
                    df["s_transect"],
                    df["z"],
                    c=df[f"cl_p50"] * 1000.0,
                    marker=markers["FHEM"],
                    label=labels["FHEM"],
                    **plot_attrs
                )

            if plot_softdata:
                # - soft data
                sql = f"SELECT * FROM xyzv_softdata WHERE ST_DWithin( ST_GeomFromText('{str(transect)}', 28992), geometrypoint, {buffer*5} )"
                df = gpd.read_postgis(sql, engine, "geometrypoint")
                if len(df):
                    df["s_transect"] = df["geometrypoint"].apply(
                        lambda x: transect.project(x)
                    )  # map points on transect
                    for t in df.typecode.unique():
                        dfsel = df.loc[df.typecode == t]
                        if len(dfsel):
                            axes[i, 0].scatter(
                                dfsel["s_transect"],
                                dfsel["z"],
                                c=dfsel["value"] * 1000.0,
                                marker=markers[t],
                                label=labels[t],
                                **plot_attrs
                            )

        axes[-1, 0].legend(fontsize="small", loc="lower right")
        plt.tight_layout(rect=(0, 0, 1, 0.95))
        plt.savefig(
            outputdir / f"cross_section_3d_{name.replace(' ','')}.png",
            dpi=300,
            bbox_inches="tight",
        )
        plt.close()
    else:
        print(f"Crosssection {name} exists, skipping")
