# -*- coding: utf-8 -*-
"""
<Description of script, for instance:>
This script creates input for GHB package, by following
the following steps:
1. 
2. 
3. 

Input:
<which files are input to this script?>


Output:
<output file (netcdf)>
"""
# all imports at the top, no 'starred imports' (from numpy import *)
import numpy as np
import os
import xarray as xr
import pandas as pd
import imod
from pathlib import Path

# Function 1
# -> write a docstring: what is the function supposed to do?
# -> use a function at least whenever you duplicate code
# -> functions are also a great way to make your main code more
#    readable and testable

# Function 2


# Main script
# comment on what each step is doing (the steps outlined in the description above), 
# and when a statement is not self-evident.
# E.g. useful comment:
# something = something / 1000   # input data in units m, to mm
# Not so useful:
# something = something / 1000   # divided by 1000


# Write output