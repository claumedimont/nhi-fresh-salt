# -*- coding: utf-8 -*-
"""
This script creates the iMOD-WQ model, by following
the following steps:
1. Open created packages from saved netcdf files
2. Build the model with the imod model builder using the opened input
3. Write the model (with imod default values, constants and input parameters)
4. Visualize the model input in 2d maps


Input:
- data/2-interim/conductivity.nc
- data/2-interim/template.nc
- data/2-interim/top_bot.nc
- data/2-interim/bnd.nc
- data/2-interim/drain.nc
- data/2-interim/river.nc
- data/2-interim/ghb.nc
- data/2-interim/starting_concentration.nc
- data/2-interim/recharge.nc
- data/2-interim/wel/negative_well*.ipf

Output:
- iMOD-WQ model input + runfile
"""
import numpy as np
import os
import xarray as xr
import imod
import datetime
import cftime
import pandas as pd
from pandas import date_range
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

try:
    modelname = snakemake.params.modelname
except NameError:
    modelname = 'LHM4.1_v05'


cache = None

print(f"Building kpslr model {modelname}")
path_bas = snakemake.input.path_bas    
path_lpf = snakemake.input.path_lpf    
path_btn = snakemake.input.path_btn    
path_rch = snakemake.input.path_rch    
path_drn_b = snakemake.input.path_drn_b     
path_drn_mvgrep = snakemake.input.path_drn_mvgrep    
path_drn_sof = snakemake.input.path_drn_sof     
path_boils = snakemake.input.path_boils    
path_riv_h = snakemake.input.path_riv_h    
path_riv_p = snakemake.input.path_riv_p    
path_riv_s = snakemake.input.path_riv_s    
path_riv_t = snakemake.input.path_riv_t    
path_ghb = snakemake.input.path_ghb     
path_wel = snakemake.input.path_wel     
path_riv_h_drn = snakemake.input.path_riv_h_drn
path_riv_p_drn = snakemake.input.path_riv_p_drn
path_riv_s_drn = snakemake.input.path_riv_s_drn
rivdrn_as_river = snakemake.params.rivdrn_as_river
steadystate = snakemake.params.steadystate
timestep = snakemake.params.timestep
if steadystate and timestep == -1:
    start = imod.wq.timeutil.to_datetime(snakemake.params.end, use_cftime=True)
else:
    start = imod.wq.timeutil.to_datetime(snakemake.params.start, use_cftime=True)
endss = start + datetime.timedelta(hours=1)
end100y = imod.wq.timeutil.to_datetime(snakemake.params.end, use_cftime=True)
path_data = Path(snakemake.params.path_data)

def _to_cftimeindex(tidx):
    if tidx.values.dtype == "datetime64[ns]":#"<M8[ns]":
        return xr.CFTimeIndex([cftime.DatetimeProlepticGregorian(*pd.Timestamp(t).timetuple()[:6]) for t in tidx.values])
    else:
        return xr.CFTimeIndex([cftime.DatetimeProlepticGregorian(*t.timetuple()[:6]) for t in tidx.values])

# Build the model from cache
m = imod.wq.SeawatModel(modelname, check=None)
m["bas"] = imod.wq.BasicFlow.from_file(path_bas, chunks={"layer":1})#, cache) #"data/2-interim/bas.nc"
m["lpf"] = imod.wq.LayerPropertyFlow.from_file(path_lpf, chunks={"layer":1})#, cache) #"data/2-interim/lpf.nc"
m["btn"] = imod.wq.BasicTransport.from_file(path_btn, chunks={"layer":1})#, cache)  #"data/2-interim/btn.nc"
m["adv"] = imod.wq.AdvectionTVD(courant=1.0)
m["dsp"] = imod.wq.Dispersion(longitudinal=1.0, diffusion_coefficient=0.0000864)
m["vdf"] = imod.wq.VariableDensityFlow(density_concentration_slope=1.316)

# Boundary conditions
m["rch"] = imod.wq.RechargeHighestActive.from_file(path_rch)#, cache) #"data/2-interim/recharge.nc"
m["wel"] = imod.wq.Well.from_file(path_wel)#, cache)  #"data/2-interim/wel.nc"

# NOT NECESSARY ANYMORE -> fixed in imod-python to allow efficient last timestep selection: first open as ds
m["drn_b"] = imod.wq.Drainage.from_file(path_drn_b)#, chunks={"layer":1,"time":1})#, cache) #"data/2-interim/drainage_b.nc"
m["drn_sof"] = imod.wq.Drainage.from_file(path_drn_sof)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/drainage_sof.nc"
m["drn_drainage_mvgrep"] = imod.wq.Drainage.from_file(path_drn_mvgrep)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/drainage_mvgrep.nc"
m["drn_boils"] = imod.wq.Drainage.from_file(path_boils)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/boils.nc"

m["riv_h"] = imod.wq.River.from_file(path_riv_h)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/river_h.nc"
m["riv_p"] = imod.wq.River.from_file(path_riv_p)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/river_p.nc"
m["riv_s"] = imod.wq.River.from_file(path_riv_s)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/river_s.nc"
m["riv_t"] = imod.wq.River.from_file(path_riv_t)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/river_t.nc"
if rivdrn_as_river:
    m["riv_h_drn"] = imod.wq.River.from_file(path_riv_h_drn)#, chunks={"layer":1,"time":1})#, cache) #"data/2-interim/river_h_drn.nc"
    m["riv_p_drn"] = imod.wq.River.from_file(path_riv_p_drn)#, chunks={"layer":1,"time":1})#, cache) #"data/2-interim/river_p_drn.nc"
    m["riv_s_drn"] = imod.wq.River.from_file(path_riv_s_drn)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/river_s_drn.nc"
else:
    m["riv_h_drn"] = imod.wq.Drainage.from_file(path_riv_h_drn)#, chunks={"layer":1,"time":1})#, cache) #"data/2-interim/river_h_drn.nc"
    m["riv_p_drn"] = imod.wq.Drainage.from_file(path_riv_p_drn)#, chunks={"layer":1,"time":1})#, cache) #"data/2-interim/river_p_drn.nc"
    m["riv_s_drn"] = imod.wq.Drainage.from_file(path_riv_s_drn)#, chunks={"layer":1,"time":1})#, cache)  #"data/2-interim/river_s_drn.nc"

m["ghb"] = imod.wq.GeneralHeadBoundary.from_file(path_ghb)#, chunks={"time":1})#, cache)  #"data/2-interim/ghb.nc"

# enforce cftime as time axis
print("Set CFTime for all packages")
for pkg in m.keys():
    if "time" in m[pkg].dims:
        m[pkg].coords["time"] = _to_cftimeindex(m[pkg].time)

# drop time to check last timestep
if steadystate:
    for pkg in m.keys():
        if "time" in m[pkg].dims:
            m[pkg] = m[pkg].isel(time=timestep, drop=True)

# Solvers and output
m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
    max_iter=150,
    inner_iter=100,
    hclose=0.0001,
    rclose=2000.0,
    relax=0.98,
    partition="rcb",
)
m["pkst"] = imod.wq.ParallelKrylovTransportSolver(
    max_iter=150, inner_iter=50, cclose=1.0e-6, partition="rcb"
)
m["oc"] = imod.wq.OutputControl(
    save_head_idf=True, save_concentration_idf=True, save_budget_idf=True
)

# all cftime
start = imod.wq.timeutil.to_datetime(start, use_cftime=True)
endss = imod.wq.timeutil.to_datetime(endss, use_cftime=True)
end100y = imod.wq.timeutil.to_datetime(end100y, use_cftime=True)

if steadystate:  # steady-state
    m["wel"].save_budget = True
    m["lpf"].save_budget = True
    m["drn_b"].save_budget = True
    m["drn_sof"].save_budget = True
    m["drn_drainage_mvgrep"].save_budget = True
    m["drn_boils"].save_budget = True
    m["riv_h"].save_budget = True
    m["riv_p"].save_budget = True
    m["riv_s"].save_budget = True
    m["riv_t"].save_budget = True
    m["ghb"].save_budget = True 
    m.time_discretization([start, endss])
else: # transient 100y
    m["lpf"].save_budget = True
    m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
        max_iter=1000,
        inner_iter=500,
        hclose=0.001,
        rclose=10000.0,
        relax=0.98,
        partition="rcb",
    )
    daterange = xr.cftime_range(start, end100y, freq="1AS", calendar="proleptic_gregorian")
    m.time_discretization(daterange.values)

m["time_discretization"]["transient"] = False
print("Writing model")
m.write(path_data / f"3-input/{modelname}", result_dir=path_data / f"4-output/{modelname}", resultdir_is_workdir=True)

if snakemake.params.visualize:
    print("visualizing")
    m.visualize(f"reports/figures_{modelname}")


