# -*- coding: utf-8 -*-
"""
This script creates the iMOD-WQ model, by following
the following steps:
1. Open created packages from saved netcdf files
2. Build the model with the imod model builder using the opened input
3. Write the model (with imod default values, constants and input parameters)
4. Visualize the model input in 2d maps


Input:
- data/2-interim/conductivity.nc
- data/2-interim/template.nc
- data/2-interim/top_bot.nc
- data/2-interim/bnd.nc
- data/2-interim/drain.nc
- data/2-interim/river.nc
- data/2-interim/ghb.nc
- data/2-interim/starting_concentration.nc
- data/2-interim/recharge.nc
- data/2-interim/wel/negative_well*.ipf

Output:
- iMOD-WQ model input + runfile
"""
import numpy as np
import os
import imod
from joblib import Memory

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

cache = ".cachedir"
#cache = Memory(location, verbose=0)

print(f"Building model {snakemake.params[0]}")

# Build the model from cache
m = imod.wq.SeawatModel(snakemake.params[0])
#m = imod.wq.SeawatModel('V0.1.9')
m["bas"] = imod.wq.BasicFlow.from_file("data/2-interim/bas.nc", cache)
m["lpf"] = imod.wq.LayerPropertyFlow.from_file("data/2-interim/lpf.nc", cache)
m["btn"] = imod.wq.BasicTransport.from_file("data/2-interim/btn.nc", cache)
m["adv"] = imod.wq.AdvectionTVD(courant=1.0)
m["dsp"] = imod.wq.Dispersion(longitudinal=1.0, diffusion_coefficient=0.0000864)
m["vdf"] = imod.wq.VariableDensityFlow(density_concentration_slope=1.316)

# Boundary conditions
m["rch"] = imod.wq.RechargeHighestActive.from_file("data/2-interim/recharge.nc", cache)
m["drn_b"] = imod.wq.Drainage.from_file("data/2-interim/drainage_b.nc", cache)
m["drn_sof"] = imod.wq.Drainage.from_file("data/2-interim/drainage_sof_corr.nc", cache)
m["drn_drainage_mvgrep"] = imod.wq.Drainage.from_file("data/2-interim/drainage_mvgrep.nc", cache)
m["drn_boils"] = imod.wq.Drainage.from_file("data/2-interim/boils.nc", cache)
m["riv_h"] = imod.wq.River.from_file("data/2-interim/river_h.nc", cache)
m["riv_p"] = imod.wq.River.from_file("data/2-interim/river_p_corr.nc", cache)
m["riv_s"] = imod.wq.River.from_file("data/2-interim/river_s.nc", cache)
m["riv_t"] = imod.wq.River.from_file("data/2-interim/river_t.nc", cache)
m["ghb"] = imod.wq.GeneralHeadBoundary.from_file("data/2-interim/ghb_2085.nc", cache)
m["wel"] = imod.wq.Well.from_file("data/2-interim/wel.nc", cache)

# Solvers and output
m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
    max_iter=150,
    inner_iter=100,
    hclose=0.001,
    rclose=500.0,
    relax=0.98,
    partition="rcb",
)
m["pkst"] = imod.wq.ParallelKrylovTransportSolver(
    max_iter=150, inner_iter=50, cclose=1.0e-6, partition="rcb"
)
m["oc"] = imod.wq.OutputControl(
    save_head_idf=True, save_concentration_idf=True, save_budget_idf=True
)

start = np.datetime64("2000-01-01T00:00")
end = np.datetime64("2100-12-31T00:01")

starttime = "2000-01-01"
endtime = "2100-12-31"

m.time_discretization(["2000-01-01", "2100-12-31"])

m["time_discretization"]["transient"] = False
print("Writing model")
m.write("data/3-input", result_dir="data/4-output", resultdir_is_workdir=True)

if snakemake.params[1]:
    print("visualizing")
    m.visualize(f"reports/figures_{snakemake.params[0]}")


