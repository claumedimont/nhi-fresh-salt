import xarray as xr
import os
import imod
import matplotlib.pyplot as plt
import matplotlib.colorbar
import pandas as pd
import geopandas as gpd
from pathlib import Path
import rioxarray
# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# LHMzz version
version = 'heads_novdf' #snakemake.params.modelname
path_lhmfresh ="data/4-output/lhmfresh_V0.13/" #Path(snakemake.params.path_lhmfresh)

# open paths
basepath = Path(".")
path_layermodel = basepath / "data/2-interim/layermodel.nc"
path_head = basepath / f"data/4-output/{version}/head/head*.idf"
path_lhm = path_lhmfresh + f"head/head_steady-state_l*.idf"
path_landen = basepath / "data/1-external/shapes/Landen.shp"
path_prov = basepath / "data/1-external/shapes/provincie.shp"
path_resleg = basepath / "data/1-external/legends/residuals.leg"
path_conc = basepath / f"data/4-output/{version}/conc/conc*.idf"
#path_concleg = basepath / r"data\1-external\legends\chloride_gL.leg"
result_path = basepath / f"data/5-visualization/head_comparison_{version}"
result_path.mkdir(exist_ok=True,parents=True)
cl_time = pd.Timestamp(2020,1,1)

# load
layermodel = xr.open_dataset(path_layermodel)
#head = imod.idf.open_subdomains(path_head).squeeze('time', drop=True)
head = imod.idf.open(path_head)#.squeeze('time', drop=True)
head = head.sel(time=cl_time, method="nearest").drop("time")

lhm = imod.idf.open(path_lhm).squeeze("time", drop=True)
landen = gpd.read_file(path_landen)
nederland  = landen[landen["CNTRYABB"] != "NL"]

provincie = gpd.read_file(path_prov)

if "_vdf" in version:  # change head to freshwater head
    conc = imod.idf.open(path_conc).squeeze("time", drop=True)

    # Conversion freshwater head
    top = layermodel["top"]
    bot = layermodel["bot"]
    z = (top + bot) / 2  # z midpoint cell

    dens_slope = 1.316  # g/L chloride
    water_dens = 1000 + conc * dens_slope  # g/L chloride
    freshwaterhead = imod.evaluate.convert_pointwaterhead_freshwaterhead(head, water_dens, z)
    head = freshwaterhead


# For phreatic layer
upper_layer = imod.select.upper_active_layer(
    head.load(), is_ibound=False
)  # fresh_head.load()
phreatic = head.where(head.layer == upper_layer)
phreatic = phreatic.min(dim="layer")

diff = lhm.sel(layer=1) - phreatic
diff = diff.rio.write_crs(28992)
diff = diff.rio.clip(nederland.geometry, nederland.crs, drop=False, invert=True)
imod.idf.write(result_path / f"LHMv4_min_LHMzzv{version}_phreatic.idf", diff)
imod.rasterio.write(result_path / f"LHMv4_min_LHMzzv{version}_phreatic.tif", diff)

phreatic = phreatic.rio.write_crs(28992)
phreatic = phreatic.rio.clip(nederland.geometry, nederland.crs, drop=False, invert=True)
imod.idf.write(result_path / f"head_LHMzzv{version}.idf", phreatic)
imod.rasterio.write(result_path / f"head_LHMzzv{version}.tif", phreatic)

res_leg = imod.visualize.read_imod_legend(path_resleg)
#conc_leg = imod.visualize.read_imod_legend(path_concleg)
res_leg[0].reverse()

#overlays = [{"gdf": landen_no_NL, "color": "white", "edgecolor": "white"}, {"gdf": provincie, "color":"None", "edgecolor": "k"}]
overlays = [ {"gdf": provincie, "color":"None", "edgecolor": "k"}]
fig, ax = imod.visualize.plot_map(
    diff, levels=res_leg[1], colors=res_leg[0], figsize=(10, 10), overlays=overlays, kwargs_colorbar={"label":"Verschil grondwaterstand (m)"}
)
_ = ax.set_xticklabels([])
_ = ax.set_yticklabels([])
ax.set_title(f"Laag 1")
fig.savefig(result_path / f"LHMv4_min_LHMzzv{version}_l1.png", bbox_inches="tight")

# For other layers

layers = {
    "3": "2",  #'1': '1',
    "5": "3",
    "7": "4",
    "9": "5",
    "11": "6",
    "13": "7",
    "15": "8",
}  # Key: LHM_id, Value: LHM_layer

for k, v in layers.items():
    upper_layer = imod.select.upper_active_layer(
        (layermodel["lhmfresh_layer"] == int(k)).load(), is_ibound=True)
    head_layer = head.where(head.layer == upper_layer)
    head_layer = head_layer.min(dim='layer')

    imod.idf.write(result_path / f"L{v}_LHMzzv{version}.idf", head_layer)  # gebruik ik verder niet, dus niet erg om niet te knippen
    diff = lhm.sel(layer=int(v)) - head_layer
    diff = diff.rio.write_crs(28992)
    diff = diff.rio.clip(nederland.geometry, nederland.crs, drop=False, invert=True)
    imod.idf.write(result_path / f"LHMv4_min_LHMzzv{version}_l{v}.idf", diff)
    imod.rasterio.write(result_path / f"LHMv4_min_LHMzzv{version}_l{v}.tif", diff)

    fig, ax = imod.visualize.plot_map(
        diff, levels=res_leg[1], colors=res_leg[0], figsize=(10, 10), overlays=overlays, kwargs_colorbar={"label":"Verschil grondwaterstand (m)"}
    )
    _ = ax.set_xticklabels([])
    _ = ax.set_yticklabels([])

    ax.set_title(f"Laag {v}")
    fig.savefig(result_path / f"LHMv4_min_LHMzzv{version}_l{v}.png", bbox_inches="tight")
    plt.close()

    """
    # also plot chloride at these depths
    fig, ax = imod.visualize.plot_map(
        conc.where(conc.layer==upper_layer).min(dim="layer"), levels=conc_leg[1], colors=conc_leg[0], figsize=(10, 10), overlays=overlays
    )

    ax.set_title(f"Chloride LHMzz {version} LHM laag {v}")
    fig.savefig(result_path / f"Chloride_LHMzzv{version}_l{v}.png")
    plt.close()
    """
