import imod
import xarray as xr
import geopandas as gpd
from pathlib import Path

fn_polderK = "data/1-external/zoutvrachten/schermer_polderK.shp"
pth_saltload = "data/5-visualization/v2.3.0/saltload/*.idf"

saltload = imod.idf.open_dataset(pth_saltload)
saltload = xr.Dataset(saltload)
sl2020 = saltload.sel(time="2020-01-01")

shp_polderK = gpd.read_file(fn_polderK)
polderK = imod.prepare.rasterize(shp_polderK, like=sl2020["saltload"])

sl_polderK = sl2020.where(polderK==1).sum().compute()  #  kg/j
sl_polderK /= 1000. # ton/jr

for prm in ["diffuse","boils","saltload"]:
    print(f"{round(float(sl_polderK[prm]),3)} ton/jr")