"""
Plausibiliteitscriteria kwelflux / zoutvracht

Hierbij worden de wellen ook meegenomen:
    Wellen zitten - als enige drain - in lagen 11 - 15. Alle andere drains in 1 - 9.
"""

import imod
import os
import numpy as np
import xarray as xr
import geopandas as gpd
import pandas as pd
from pathlib import Path

# Input:
#os.chdir(r'c:\temp\NHI')
scen = "v2.3.0"#'NHI-ZZ-2.3.0'
time = pd.Timestamp(2020,1,1)
polder1 = r'data\1-external\toetsing\haarlemmermeer.shp'
polder2 = r'data\1-external\toetsing\Noordplas.shp'
polder3 = r'data\1-external\toetsing\peilvak_9_19.shp'
polder4 = r'data\1-external\toetsing\schermer.shp'
polder5 = r'data\1-external\toetsing\wieringermeer.shp'
balansgebieden = [polder1, polder2, polder3, polder4,polder5]
outputdir = Path(f"reports/toetsing_{scen}")
outputdir.mkdir(exist_ok=True,parents=True)

# Read output data (fluxes, concentrations)
conc = imod.idf.open(f"data/4-output/{scen}/conc/conc*.idf").sel(time=time, method="nearest")
#conc = imod.idf.open(r"data\4-output_tr\conc\conc*.idf").isel(time=-1)
rch = imod.idf.open_subdomains(f"data/4-output/{scen}/bdgrch/bdgrch*.idf").sel(time=time, method="nearest")
drn = imod.idf.open(f"data/4-output/{scen}/bdgdrn/bdgdrn*.idf").sel(time=time, method="nearest")
riv = imod.idf.open_subdomains(f"data/4-output/{scen}/bdgriv/bdgriv*.idf").sel(time=time, method="nearest")
lower = imod.idf.open(f"data/4-output/{scen}/bdgflf/bdgflf*.idf").sel(time=time, method="nearest")
right = imod.idf.open_subdomains(f"data/4-output/{scen}/bdgfrf/bdgfrf*.idf").sel(time=time, method="nearest")
front = imod.idf.open_subdomains(f"data/4-output/{scen}/bdgfff/bdgfff*.idf").sel(time=time, method="nearest")

# Export results in table-format (plausbiliteit)
ow = 'oppervlaktewater'; zv = 'zoutvracht'
df = pd.DataFrame(columns=[0, 1, 2, 3, 4], 
                  index=['oppervlak (m2)','flux-horizontal (m3/d)','flux-horizontal (mm/d)',
                         'flux-kwel (m3/d)','flux-kwel (mm/d)','flux-recharge (m3/d)',
                         'flux-recharge (mm/d)', 'flux-drn (m3/d)','flux-drn (%)',
                         'flux-wellen (m3/d)','flux-wellen (mm/d)','flux-wellen (%)',
                         'flux-riv (m3/d)','flux-riv (%)','flux-oppervlaktewater (m3/d)',
                         'waterbalans-wellen (m3/d)','zoutvracht-kwel (ton/jaar)',
                         'zoutvracht-drn (ton/jaar)','zoutvracht-drn (%)',
                         'zoutvracht-wellen (ton/jaar)','zoutvracht-wellen (%)',
                         'zoutvracht-riv (ton/jaar)','zoutvracht-riv (%)',
                         'zoutvracht-oppervlaktewater (ton/jaar)'])

# Read waterbalansgebieden (i.e. polders), zie https://imod.xyz/api/evaluate.html
for i in range(len(balansgebieden)):
    gdf = gpd.read_file(balansgebieden[i])
    areaname = os.path.basename(balansgebieden[i])[:-4]; print(areaname)
    df = df.rename(columns={i: areaname})
    zone2D = imod.prepare.rasterize(gdf, like=lower.isel(layer=0))
    zonearea = np.int(np.abs(np.sum(zone2D) * zone2D.dx * zone2D.dy))
    df.loc['oppervlak (m2)'][areaname] = zonearea
    zone = xr.full_like(conc, zone2D)
    zone = zone.where(zone['layer'] <= 10)
    
    # Get and write horizontal and vertical (=kwel) flow in/out zone
    hflow = imod.evaluate.facebudget(budgetzone=zone, front=front, right=right, netflow=True)
    hflow_sum = np.float(hflow.sum())
    hflow_sum_mmd = 1000 * (hflow_sum / zonearea)
    df.loc['flux-horizontal (m3/d)'][areaname] = hflow_sum
    df.loc['flux-horizontal (mm/d)'][areaname] = hflow_sum_mmd
    
    flowfront, flowlower, flowright = imod.evaluate.facebudget(
            budgetzone=zone, front=front, lower=lower, right=right, netflow=False)
    flowlower_sum = np.float(flowlower.sum())
    flowlower_sum_mmd = 1000 * (flowlower_sum / zonearea)
    df.loc['flux-kwel (m3/d)'][areaname] = flowlower_sum
    df.loc['flux-kwel (mm/d)'][areaname] = flowlower_sum_mmd
    
    # Get and write flux rch, drn, wellen, rivers in/out zone
    flux_rch_zone = zone2D * rch
    flux_rch_sum = np.float(flux_rch_zone.sum())    
    flux_drn_zone = zone2D * drn
    flux_drn_dkl = np.float(flux_drn_zone.sel(layer=slice(1, 9)).sum())
    flux_drn_wellen = np.float(flux_drn_zone.sel(layer=slice(11, 15)).sum())
    flux_riv_zone = zone2D * riv
    flux_riv_sum = np.float(flux_riv_zone.sel(layer=slice(1, 39)).sum())
    flux_oppervlaktewater = flux_drn_dkl + flux_drn_wellen + flux_riv_sum
    nettowaterbalans = flux_rch_sum + flowlower_sum + flux_drn_dkl + flux_riv_sum
    df.loc['flux-recharge (m3/d)'][areaname] = flux_rch_sum
    df.loc['flux-recharge (mm/d)'][areaname] = 1000 * (flux_rch_sum / zonearea)
    df.loc['flux-drn (m3/d)'][areaname] = flux_drn_dkl
    df.loc['flux-wellen (m3/d)'][areaname] = flux_drn_wellen
    df.loc['flux-wellen (mm/d)'][areaname] = 1000 * (flux_drn_wellen / zonearea)
    df.loc['flux-riv (m3/d)'][areaname] = flux_riv_sum
    df.loc[f'flux-{ow} (m3/d)'][areaname] = flux_oppervlaktewater
    df.loc['waterbalans-wellen (m3/d)'][areaname] = nettowaterbalans

    # Get and write mass in/out zone (kwel, drains, rivers, wellen)
    mass_kwel_zone = conc * flowlower
    mass_kwel_sum = np.float(mass_kwel_zone.sum())
    mass_drn_zone = zone2D * conc * drn
    mass_drn_dkl = np.float(mass_drn_zone.sel(layer=slice(1, 9)).sum())
    mass_drn_wellen = np.float(mass_drn_zone.sel(layer=slice(11, 15)).sum())
    mass_riv_zone = zone2D * conc * riv
    mass_riv_sum = np.float(mass_riv_zone.sel(layer=slice(1, 39)).sum())
    mass_totaal = mass_drn_dkl + mass_drn_wellen + mass_riv_sum

    df.loc['zoutvracht-kwel (ton/jaar)'][areaname] = mass_kwel_sum *365.25/1000
    df.loc['zoutvracht-drn (ton/jaar)'][areaname] = mass_drn_dkl *365.25/1000
    df.loc['zoutvracht-wellen (ton/jaar)'][areaname] = mass_drn_wellen *365.25/1000
    df.loc['zoutvracht-riv (ton/jaar)'][areaname] = mass_riv_sum *365.25/1000
    df.loc[f'zoutvracht-{ow} (ton/jaar)'][areaname] = mass_totaal *365.25/1000

df.loc['flux-drn (%)'] = df.loc['flux-drn (m3/d)'] / df.loc[f'flux-{ow} (m3/d)']
df.loc['flux-wellen (%)'] = df.loc['flux-wellen (m3/d)'] / df.loc[f'flux-{ow} (m3/d)']
df.loc['flux-riv (%)'] = df.loc['flux-riv (m3/d)'] / df.loc[f'flux-{ow} (m3/d)']
df.loc[f'{zv}-drn (%)'] = df.loc[f'{zv}-drn (ton/jaar)'] / df.loc[f'{zv}-{ow} (ton/jaar)']
df.loc[f'{zv}-wellen (%)'] = df.loc[f'{zv}-wellen (ton/jaar)'] / df.loc[f'{zv}-{ow} (ton/jaar)']
df.loc[f'{zv}-riv (%)'] = df.loc[f'{zv}-riv (ton/jaar)'] / df.loc[f'{zv}-{ow} (ton/jaar)']

df.loc['conc-kwel (g/l)'] = df.loc[f'{zv}-kwel (ton/jaar)'] / 365.25*1000 / df.loc['flux-kwel (m3/d)']
df.loc['conc-drn (g/l)'] = df.loc[f'{zv}-drn (ton/jaar)'] / 365.25*1000 / df.loc['flux-drn (m3/d)']
# Wieringermeer heeft geen wellen, geeft foutmelding
conc_wellen_sel = df.loc[f'{zv}-wellen (ton/jaar)'][:-1] / 365.25*1000 / df.loc['flux-wellen (m3/d)'][:-1]
df.loc['conc-wellen (g/l)'] = list(conc_wellen_sel) + [0]
df.loc['conc-riv (g/l)'] = df.loc[f'{zv}-riv (ton/jaar)'] / 365.25*1000 / df.loc['flux-riv (m3/d)']
df.loc[f'conc-{ow} (g/l)'] = df.loc[f'{zv}-{ow} (ton/jaar)'] / 365.25*1000 / df.loc[f'flux-{ow} (m3/d)']

df.to_csv(outputdir / f'{scen}-toetsing-kwelflux-{zv}.csv')
print(f'Succesvol geschreven naar {outputdir}!')