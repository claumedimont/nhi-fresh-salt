"""
Analyse (klimaat)scenario (sc) NHI-ZZ t.o.v. autonome ontwikkeling (ao) 2000 - 2100
Needed: latest imod-python from GIT (0.9.1.dev), with latest boundaries.py (imod/evaluate)
For: version 2 NHI-ZZ (2000 - 2100; autonome ontwikkeling, klimaatscenario)
"""
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import imod
import os

# Predefined plot windows
totaal_nl = {'name':'totaal','xmin': 10000, 'xmax': 280000, 'ymin': 350000, 'ymax': 625000}
noord_nl = {'name':'noord','xmin': 150000, 'xmax': 280000, 'ymin': 520000, 'ymax': 620000}
west_nl = {'name':'west','xmin': 80000, 'xmax': 200000, 'ymin': 470000, 'ymax': 565000} 
zuid_nl = {'name':'zuid','xmin': 10000, 'xmax': 160000, 'ymin': 355000, 'ymax': 475000} 
wellen_nl = {'name':'west','xmin': 70000, 'xmax': 160000, 'ymin': 435000, 'ymax': 530000}

# Input:
os.chdir('c:/temp/NHI')
scen = 'NHI-ZZ-2.3.0-SC'
top_NHIzz = 'data/3-input/bas/top*.idf'
bot_NHIzz = 'data/3-input/bas/bottom_l*.idf'
con_ao_NHIzz = 'data/4-output_ao/conc/*.idf'
con_sc_NHIzz = 'data/4-output_sc/conc/*.idf'
saltload_ao = 'data/5-visualization/NHI-ZZ-2.3.0/saltload'          # output saltload.py
saltload_sc = 'data/5-visualization/NHI-ZZ-2.3.0-SC/saltload'       # output saltload.py
rch_ao = imod.idf.open('data/3-input/rch/rate_1980_2010.idf')
rch_wh = imod.idf.open('data/3-input/rch/rate_2070_2100.idf')
shp1 = 'data/1-external/provincie/2018-Imergis_provinciegrenzen_kustlijn.shp'
shp2 = 'data/1-external/coastline/nl_imergis_kustlijn_2018.shp'
show = totaal_nl    # Pick one of the prefined plot windows (see above)

# Read concentration (merge conc first, or use imod.idf.open_subdomains)
print('Reading concentrations...')
ds = xr.Dataset()
con_ao = imod.idf.open(con_ao_NHIzz)
con_sc = imod.idf.open(con_sc_NHIzz)
con_ao['time'] = con_sc['time']     # Times differ, make equal
ds['conc-ao'] = con_ao; ds['conc-sc'] = con_sc

# Read boils, saltload, diffuse 2000 - 2100
for item in ['boils','diffuse','saltload']:
    aoidfpath = os.path.join(f'{saltload_ao}/{item}*.idf')
    ds[f'{item}-ao'] = imod.idf.open(aoidfpath)
    scidfpath = os.path.join(f'{saltload_sc}/{item}*.idf')
    ds[f'{item}-sc'] = imod.idf.open(scidfpath)

# Get z and dz of dataarray; TOP's and BOT's of NHI-ZZ
print('Get z and dz of datarray...')
top = imod.idf.open(top_NHIzz)
if top.ndim == 2:
    top['layer'] = 1
bots = imod.idf.open(bot_NHIzz)
dz1 = top - bots[0]; z1 = (top + bots[0]) * 0.5
dz2 = abs(bots.diff('layer'))
z2 = (bots[:-1].drop('layer') + bots[1:].drop('layer')) * 0.5
z2['layer'] = np.arange(2,40,1); dz2['layer'] = np.arange(2,40,1)
z = xr.concat([z1, z2], dim='layer'); dz = xr.concat([dz1, dz2], dim='layer')
ds["z"] = z.transpose('layer', 'y', 'x'); 
ds["dz"] = dz.transpose('layer', 'y', 'x')

# Change in fresh / brackish / salt groundwater volume (graph: in time)
print('Change in fresh / brackish / salt groundwater volume...')
volume = np.abs(ds["dz"]) * np.abs(float(ds["dx"])) * np.abs(float(ds["dy"])) * 0.3
conc_fillna_ref = ds['conc-ao'].fillna(-32.768)
conc_fillna_sc1 = ds['conc-sc'].fillna(-32.768)
fresh_ref = np.logical_and(conc_fillna_ref >= 0., conc_fillna_ref < 1.) * volume
fresh_sc1 = np.logical_and(conc_fillna_sc1 >= 0., conc_fillna_sc1 < 1.) * volume
ds["groei in volume zoet grondwater"] = (fresh_sc1 - fresh_ref) / 5
ds["absolute verandering in het volume zoet grondwater"] = np.abs((fresh_sc1 - fresh_ref) / 5)
ds["totale groei in het volume zoet grondwater"] = fresh_sc1[1:] - fresh_ref[0]
brack_ref = np.logical_and(conc_fillna_ref >= 1., conc_fillna_ref < 8.) * volume
brack_sc1 = np.logical_and(conc_fillna_sc1 >= 1., conc_fillna_sc1 < 8.) * volume
ds["groei in volume brak grondwater"] = (brack_sc1 - brack_ref) / 5
ds["absolute verandering in het volume brak grondwater"] = np.abs((brack_sc1 - brack_ref) / 5)
ds["totale groei in het volume brak grondwater"] = brack_sc1[1:] - brack_ref[0]
saline_ref = (conc_fillna_ref >= 8.) * volume
saline_sc1 = (conc_fillna_sc1 >= 8.) * volume
ds["groei in volume zout grondwater"] = (saline_sc1 - saline_ref) / 5
ds["absolute verandering in het volume zout grondwater"] = np.abs((saline_sc1 - saline_ref) / 5)
ds["totale groei in het volume zout grondwater"] = saline_sc1[1:] - saline_ref[0]
dsv = ds.sum("layer").sum("x").sum("y").drop("conc-ao").drop("conc-sc")
df = dsv.to_dataframe()
if not os.path.exists(f"data/5-visualization/{scen}/analysis"):
    os.makedirs(f"data/5-visualization/{scen}/analysis")
df.to_csv(f"data/5-visualization/{scen}/analysis/changesalinity.csv")

# Plot graph with (total) change in in fresh / brackisch / saline gw volume
dfv = df[1:].drop(['dy', 'dx', 'z', 'dz'], axis=1)
dfv = dfv / 1e9; dfv['tijd'] = np.linspace(2005,2100,20)
for vol in ["groei in volume zoet grondwater", 
            "absolute verandering in het volume zoet grondwater",
            "totale groei in het volume zoet grondwater", 
            "groei in volume brak grondwater",
            "absolute verandering in het volume brak grondwater", 
            "totale groei in het volume brak grondwater",
            "groei in volume zout grondwater",
            "absolute verandering in het volume zout grondwater",
            "totale groei in het volume zout grondwater"]:
    fig = dfv.plot.scatter(x='tijd',y=vol).get_figure()
    if "total" in vol:
        plt.ylabel(f'Volume in miljard m$^{3}$')
    else:
        plt.ylabel(f'Volume in miljard m$^{3}$ jr$^{-1}$')
    fig.suptitle(vol)
    fig.align_xlabels()
    fig.tight_layout(rect=(0, 0, 1, 0.95))
    fig.savefig(f"data/5-visualization/{scen}/analysis/{vol}.png", dpi=200)
    plt.close()
print(f"See csv and figures in data/5-visualization/{scen}/analysis")

# Plot growth rate of fresh groundwater (as a map)
map_shp1 = gpd.read_file(shp1)
freshgw2d = ds["groei in volume zoet grondwater"].sum(dim='layer')
freshgw2d = freshgw2d / abs(freshgw2d.dx * freshgw2d.dy * 0.3)
print("Plotting growth of fresh gw in m per yr...")
for time in ds["groei in volume zoet grondwater"].time[1:].values:
    year = pd.to_datetime(time).strftime("%Y"); print(f"...plotting: {year}")
    fig, ax = plt.subplots(subplot_kw={'xticks': [], 'yticks': []})
    bounds = [-5,-1,-0.5,-0.1,0,0.1,0.5,1,5]
    s = freshgw2d.sel(time=time).drop('dx').drop('dy')
    s.name = "groei in het volume zoet grondwater in m per jaar"
    img = s.plot(ax=ax, cmap='RdBu', levels=bounds)
    img.colorbar.set_ticklabels(bounds)
    map_shp1.boundary.plot(ax=ax, color='darkgrey', linewidth=0.5)
    ax.set_xlim([show['xmin'], show['xmax']]); 
    ax.set_ylim([show['ymin'], show['ymax']])
    plt.xlabel(''); plt.ylabel('')
    fig.tight_layout()
    fig.savefig(f"data/5-visualization/{scen}/analysis/{s.name} {year}.png", dpi=300)
    fig.clf(); plt.close()
    
# Plot change in boils, saltload, diffuse
map_shp2 = gpd.read_file(shp2)
vertaling = {'boils':'zoutvracht via wellen', 'diffuse':'zoutvracht via diffuse kwel', 
             'saltload':'totale zoutvracht'}
for fluxtype in ['boils','diffuse','saltload']:
    print(f"Plotting change in {fluxtype} in kg per day...")
    if fluxtype == 'boils':
        plotextent = wellen_nl
    else:
        plotextent = show
    dloadtype = ds[f'{fluxtype}-sc'] - ds[f'{fluxtype}-ao']
    for time in ds['time'][1:].values:
        year = pd.to_datetime(time).strftime("%Y"); print(f"...plotting: {year}")
        fig, ax = plt.subplots(subplot_kw={'xticks': [], 'yticks': []})
        bounds = [-50,-10,-5,-1,0,1,5,10,50]
        s = dloadtype.sel(time=time).drop('dx').drop('dy')
        s.name = f"verandering in {vertaling[fluxtype]} in kg per dag"
        img = s.plot(ax=ax, cmap='RdBu_r', levels=bounds)
        img.colorbar.set_ticklabels(bounds)
        map_shp1.boundary.plot(ax=ax, color='darkgrey', linewidth=0.5)
        map_shp2.plot(ax=ax, color='lightgrey')
        ax.set_xlim([plotextent['xmin'], plotextent['xmax']]); 
        ax.set_ylim([plotextent['ymin'], plotextent['ymax']])
        plt.xlabel(''); plt.ylabel('')
        fig.tight_layout()
        fig.savefig(f"data/5-visualization/{scen}/analysis/{s.name} {year}.png", dpi=300)
        fig.clf(); plt.close()

# Plot change in recharge
print(f"Plotting change in groundwater recharge in mm per day...")
drch = (rch_wh - rch_ao) * 1000
fig, ax = plt.subplots(subplot_kw={'xticks': [], 'yticks': []})
bounds = [-1,-0.5,-0.2,-0.1,0,0.1,0.2,0.5,1]
s = drch.drop('dx').drop('dy')
s.name = "verandering in grondwateraanvulling door klimaatscenario wh (mm per dag)"
img = s.plot(ax=ax, cmap='RdBu', levels=bounds)
img.colorbar.set_ticklabels(bounds)
map_shp1.boundary.plot(ax=ax, color='darkgrey', linewidth=0.5)
map_shp2.plot(ax=ax, color='lightgrey')
ax.set_xlim([plotextent['xmin'], plotextent['xmax']]); 
ax.set_ylim([plotextent['ymin'], plotextent['ymax']])
plt.xlabel(''); plt.ylabel('')
fig.tight_layout()
fig.savefig(f"data/5-visualization/{scen}/analysis/{s.name}.png", dpi=300)
fig.clf(); plt.close()

print(f'Succesfull, see files in data/5-visualization/{scen}/analysis!')