import xarray as xr
import os
import imod
import matplotlib.pyplot as plt
import matplotlib.colorbar
import geopandas as gpd
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# LHMzz version
version = "0.1.17"

# open paths
basepath = Path(".")
path_layermodel = basepath / "data/2-interim/layermodel.nc"
path_conductivity = basepath / "data/2-interim/conductivity.nc"
path_head = basepath / f"data/4-output/V{version}_tr_merged/head/head*.idf"
result_path = basepath / f"data/4-output/V{version}_tr_merged/bdgflf/bdgflf"
result_path.mkdir(exist_ok=True)

# load
layermodel = xr.open_dataset(path_layermodel)
conductivity = xr.open_dataset(path_conductivity)

# head = imod.idf.open_subdomains(path_head).squeeze('time', drop=True)
head = imod.idf.open(path_head)

c = 0.5 * layermodel["dz"] / conductivity["kv"]  # m / m/d = d
c = c + c.shift(layer=-1)

flf = (head.shift(layer=-1) - head) * 62500 / c  # m3/d, positive upward

imod.idf.save(result_path, flf)