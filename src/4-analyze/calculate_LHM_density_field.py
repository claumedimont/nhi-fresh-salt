# -*- coding: utf-8 -*-
"""
Script to calculate LHM 8-layer density field from LHM fresh salt result
Averages LHM fresh-salt modeled concentration in 2020 over LHM 8-layermodel
"""
import pandas as pd
import xarray as xr
import imod
from pathlib import Path

run = "v2.3.0"
path_cl = f"data/4-output/{run}/conc/conc*.idf"
path_laymodel = "data/2-interim/layermodel_corr.nc"
#path_laymodel_LHM = "data/2-interim/top_bot_orig.nc"
path_laymodel_LHM = Path("data/1-external/LHM41_lagenmodel")
path_result = Path(f"data/4-output/{run}/LHM41_densityfield")
cl_time = pd.Timestamp(2020,1,1)

path_result.mkdir(exist_ok=True,parents=True)

cl = imod.idf.open(path_cl)
lay = xr.open_dataset(path_laymodel)

#top_bot_LHM = xr.open_dataset(path_laymodel_LHM)
#top_LHM = top_bot_LHM["top"]
#bot_LHM = top_bot_LHM["bot"]

#read LHM41 layermodel  #TEMPORARY
top_LHM = imod.idf.open(path_laymodel_LHM / "top/MDL_TOP*")
bot_LHM = imod.idf.open(path_laymodel_LHM / "bot/MDL_BOT*")

# adjust the LHM layermodel to include aquitards
bot_LHM = bot_LHM.load()
bot_LHM.loc[{"layer":1}] = top_LHM.sel(layer=2)  # use top of lower layer as bottom, to include aquitards
#top_LHM = imod.prepare.fill(top_LHM) # NO LONGER NECESSARY WITH NEW EXTENDED LAYERMODEL
#bot_LHM = imod.prepare.fill(bot_LHM)

# alternative approach: exclude aquitards
#bot_LHM2 = xr.concat((top_LHM.isel(layer=slice(1,None)),top_bot_LHM["bot"].isel(layer=-1)),dim=top_LHM.layer)

# get cl closest to given cl_time
cl = cl.sel(time=cl_time, method="nearest").drop("time")

# reindex cl to entire LHM extent (1200x1300)
cl = cl.reindex_like(lay["top"])

# assert everything > 0, < 20
cl = cl.where(cl > 0, 0)
cl = cl.where(cl < 20, 20)

# regrid result to LHM 8-layer model, use simple average concentration
regridder = imod.prepare.LayerRegridder(method="mean")
cl_lhm = regridder.regrid(cl, lay["top"], lay["bot"], top_LHM, bot_LHM)
cl_lhm_filled = imod.prepare.fill(cl_lhm)
#cl_lhm2 = regridder.regrid(cl, lay["top"], lay["bot"], top_LHM, bot_LHM2)

# assert everything > 0, < 20
assert float(((cl_lhm_filled < 0)|(cl_lhm_filled > 20)).sum().compute()) == 0

# save result
imod.idf.save(path_result / "chloride_gL",cl_lhm_filled)
