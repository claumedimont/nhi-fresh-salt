# -*- coding: utf-8 -*-
"""
Script to calculate LHM concentration input from LHM fresh salt result
LHM fresh needs concentration at aquifer - cover layer boundary, and the
concentration of boils.
aquifer - cover layer boundary: LHM concentration of layer 11
concentration of boils: Q-average concentration of layers 11 - 15. Fill with
  average concentration where no boils, if boil locations are changed

in mg/l? -> boils mg/L, transol g/L
"""
import numpy as np
import pandas as pd
import xarray as xr
import imod
from pathlib import Path

run = "v2.3.0"
path_template = "data/2-interim/template.nc"
path_layermodel = "data/2-interim/layermodel.nc"
path_cl = f"data/4-output/{run}/conc/conc*.idf"
path_drn = f"data/4-output/{run}/bdgdrn/bdgdrn*.idf"
path_result = Path(f"data/4-output/{run}/LHM_concentrationinput")
cl_time = pd.Timestamp(2020,1,1)

path_result.mkdir(exist_ok=True,parents=True)

# read 
template = xr.open_dataarray(path_template)
layermodel = xr.open_dataset(path_layermodel)
laytop = imod.select.upper_active_layer(layermodel["top"], False)
top = layermodel["top"].where(layermodel.layer==laytop).max(dim="layer")
bot = layermodel["bot"]
cl = imod.idf.open(path_cl)

# get cl closest to given cl_time
cl = cl.sel(time=cl_time, method="nearest", drop=True)

# reindex cl to entire LHM extent (1200x1300)
cl = cl.reindex_like(template)

# assert everything > 0, < 20
cl = cl.where(cl > 0, 0)
cl = cl.where(cl < 20, 20)

# concentration beneath_coverlayer: use layer 11
# except where this is too deep (> 10 m) below the surface
# cl_covlayer = cl.sel(layer=11, drop=True)
lay = bot.layer[np.argmin(bot.fillna(10) > top-10, axis=0)]
cllay = lay.where(lay < 11, 11)
# cl_covlayer = cl.sel(layer=cllay, drop=True)
cl_covlayer = cl.where(cl.layer==cllay).max(dim="layer") # faster

# drn Q
drn = imod.idf.open(path_drn)
drn = drn.sel(time=cl_time, method="nearest", drop=True)
drn = drn.reindex_like(template)
# boils in layers 11 - 15
drn = drn.sel(layer=slice(11,15))
# Q-weighted average, fill with simple average where no boils
cl_boils = (cl * drn).sum(dim="layer") / drn.sum(dim="layer")
cl_avgaq1 = cl.sel(layer=slice(11,15)).mean(dim="layer")
cl_boils = cl_boils.combine_first(cl_avgaq1)

# to mg/l and save
imod.idf.save(path_result / "cl_coverlayer_gL.idf",cl_covlayer)  # transol uses g/L
imod.idf.save(path_result / "cl_boils_mgL.idf",cl_boils * 1000.) # LHM modflow expects mg/L
