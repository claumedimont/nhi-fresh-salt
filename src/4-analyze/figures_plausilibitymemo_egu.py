import imod
import numpy as np
import xarray as xr
import geopandas as gpd
from pathlib import Path
import warnings
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore")
np.seterr(all="ignore")

versions = {
    "autonoom": "v2.3.0",
    "Klimaat + ZSS": "v2.3.0_scenario1",
    #"Klimaat + ZSS": "schouwen_scenario1",
    "ZSS": "v2.3.0_scenario2",
}

# filenames
#fn_conc = f"data/4-output/{version}/conc/conc*.idf"
fn_head = "data/4-output/{}/head/head*.idf"
fn_flf = "data/4-output/{}/bdgflf/bdgflf*_l10.idf"
#fn_drn = f"data/4-output/{version}/bdgdrn/bdgdrn*.idf"  # for boils
fn_saltload = "data/5-visualization/{}/saltload/saltload*.idf"
fn_layermodel = "data/2-interim/layermodel_corr.nc"
fn_conductivity = "data/2-interim/conductivity.nc"
fn_rch2000 = r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\LHM zoetzout\Plausibiliteitsonderzoek\RCH_SC1\rate_1980_2010.idf"
fn_rch2100 = r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\LHM zoetzout\Plausibiliteitsonderzoek\RCH_SC1\rate_2070_2100.idf"

fn_legend = "data/1-external/legends/chloride_gL.leg"
fn_shapenl = "data/1-external/provincie/provincie.shp"
fn_masknl = "c:/GISDATA/nl_mask.shp"

path_output = Path("data/5-visualization/analysis_plausiblity_egu")
path_output.mkdir(exist_ok=True, parents=True)
path_fig = Path("reports/figures/analysis_plausiblity_egu")
path_fig.mkdir(exist_ok=True, parents=True)

prov = gpd.read_file(fn_shapenl)
mask = gpd.read_file(fn_masknl)
overlays = [{"gdf":mask, "color":"white", "edgecolors":"None"}, {"gdf":prov, "lw":0.5, "color":"None", "edgecolors":"k"}]
#extent_schouwen = [13500, 80000, 357500, 430000]


# first: head
head_aut = imod.idf.open(fn_head.format("v2.3.0"))

colors = "coolwarm_r"
levels = [-1, -.5, -.25, -.1, -.05, -.01]
levels += [-l for l in levels[::-1]]
transl = {9:"freatisch",11:"wvp1"}
for name in ["ZSS"]:#["Klimaat + ZSS","ZSS"]:
    scen = versions[name]
    fn = fn_head.format(scen)
    print(fn)
    head_scen = imod.idf.open(fn)
    headdiff = head_scen - head_aut

    for l in [9, 11]:
        for y in [2020, 2100]:
            da = headdiff.sel(layer=l, time=f"{y}0101")
            imod.idf.write(path_output / f"headdiff_{name}_{y}_l{l}.idf", da)
            fig, ax = imod.visualize.plot_map(da, colors, levels, overlays, kwargs_colorbar={"whiten_triangles": False, "label": "Head difference (m)"})
            _ = ax.xaxis.set_ticklabels([])
            _ = ax.yaxis.set_ticklabels([])
            #_ = ax.set_title(f"{name} t.o.v. autonoom - {transl[l]} - {y}")
            plt.savefig(path_fig / f"headdiff_nl_{scen}_{y}_l{l}.png", dpi=300, bbox_inches="tight")
            
            # alleen schouwen
            #_ = ax.set_xlim(extent_schouwen[:2])
            #_ = ax.set_ylim(extent_schouwen[2:])
            #plt.savefig(path_fig / f"headdiff_sd_{scen}_{y}_l{l}.png", dpi=300, bbox_inches="tight")
            plt.close()

# saltload
sl_aut = imod.idf.open(fn_saltload.format("v2.3.0"))

colors = "coolwarm"
levels = [-10., -2., -1., -.5, -.1]
levels += [-l for l in levels[::-1]]
for name in ["ZSS"]:#["Klimaat + ZSS","ZSS"]:  #"Klimaat + ZSS",
    scen = versions[name]
    fn = fn_saltload.format(scen)
    print(fn)
    sl_scen = imod.idf.open(fn)
    fnh = fn_head.format(scen)
    head_scen = imod.idf.open(fnh)

    sldiff = (sl_scen - sl_aut).where(head_scen.isel(time=0,layer=11).notnull()) / 6.25

    for y in [2020, 2100]:
        da = sldiff.sel(time=f"{y}0101")
        imod.idf.write(path_output / f"saltloaddiff_{name}_{y}.idf", da)
        fig, ax = imod.visualize.plot_map(da, colors, levels, overlays, kwargs_colorbar={"whiten_triangles": False, "label": "Salt load difference (kg Cl/ha/d)"})
        _ = ax.xaxis.set_ticklabels([])
        _ = ax.yaxis.set_ticklabels([])
        #_ = ax.set_title(f"Zoutvracht {name} t.o.v. autonoom - {y}")
        plt.savefig(path_fig / f"saltloaddiff_nl_{scen}_{y}.png", dpi=300, bbox_inches="tight")
        
        # alleen schouwen
        #_ = ax.set_xlim(extent_schouwen[:2])
        #_ = ax.set_ylim(extent_schouwen[2:])
        #plt.savefig(path_fig / f"saltloaddiff_sd_{scen}_{y}.png", dpi=300, bbox_inches="tight")
        plt.close()

"""
# kwel
flf_aut = imod.idf.open(fn_flf.format("v2.3.0"))

colors = "coolwarm_r"
levels = [-0.5, -0.2, -0.1, -0.01, -0.001]
levels += [-l for l in levels[::-1]]
for name in ["Klimaat + ZSS","ZSS"]:  #"Klimaat + ZSS",
    scen = versions[name]
    fn = fn_flf.format(scen)
    print(fn)
    flf_scen = imod.idf.open(fn)
    fnh = fn_head.format(scen)
    head_scen = imod.idf.open(fnh)

    flfdiff = (flf_scen - flf_aut).where(head_scen.isel(time=0,layer=11).notnull()) / 62.5  # m3/d -> mm/d

    for y in [2020, 2100]:
        da = flfdiff.sel(time=f"{y}0101").squeeze(drop=True)
        imod.idf.write(path_output / f"seepdiff_{name}_{y}.idf", da)
        fig, ax = imod.visualize.plot_map(da, colors, levels, overlays, kwargs_colorbar={"whiten_triangles": False, "label": "Verschil kwel/inf autonoom (mm/d)"})
        _ = ax.xaxis.set_ticklabels([])
        _ = ax.yaxis.set_ticklabels([])
        _ = ax.set_title(f"Kwel {name} t.o.v. autonoom - {y}")
        plt.savefig(path_fig / f"seepdiff_nl_{scen}_{y}.png", dpi=300, bbox_inches="tight")
        
        # alleen schouwen
        _ = ax.set_xlim(extent_schouwen[:2])
        _ = ax.set_ylim(extent_schouwen[2:])
        plt.savefig(path_fig / f"seepdiff_sd_{scen}_{y}.png", dpi=300, bbox_inches="tight")
        plt.close()



# recharge
rch2100 = imod.idf.open(fn_rch2100, pattern="{name}")
rch2000 = imod.idf.open(fn_rch2000, pattern="{name}")
rchdiffmmd = (rch2100 - rch2000) * 1000. # in m/d


colors = "coolwarm_r"
levels = [-1., -0.5, -0.2, -0.1]
levels += [0] + [-l for l in levels[::-1]]

da = rchdiffmmd
y = 2100
scen = "klimaat"
imod.idf.write(path_output / f"rchdiff_{y}.idf", da)
fig, ax = imod.visualize.plot_map(da, colors, levels, overlays, kwargs_colorbar={"whiten_triangles": False, "label": "Verschil gw aanvulling autonoom (mm/d)"})
_ = ax.xaxis.set_ticklabels([])
_ = ax.yaxis.set_ticklabels([])
_ = ax.set_title(f"Grondwateraanvulling Klimaat t.o.v. autonoom - {y}")
plt.savefig(path_fig / f"rchdiff_nl_{scen}_{y}.png", dpi=300, bbox_inches="tight")

# alleen schouwen
_ = ax.set_xlim(extent_schouwen[:2])
_ = ax.set_ylim(extent_schouwen[2:])
plt.savefig(path_fig / f"rchdiff_sd_{scen}_{y}.png", dpi=300, bbox_inches="tight")
plt.close()
"""
