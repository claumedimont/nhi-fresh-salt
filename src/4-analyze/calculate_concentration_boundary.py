# -*- coding: utf-8 -*-
"""
Script to calculate 2D concentration boundary from 3D result
"""
import numpy as np
import xarray as xr
import imod
import pandas as pd
import geopandas as gpd
from pathlib import Path

run = "v2.3.0"
# path_cl = f"data/3-input/btn/starting_concentration*"
path_cl = fr"data\4-output/{run}/conc/conc*.idf"
path_laymodel = f"data/2-interim/layermodel_corr.nc"
path_ahn = "data/1-external/LHM34_lagenmodel/AHN_F250.IDF"
path_result = Path(f"reports/figures/grensvlakken_{run}")
path_landen = "data/1-external/shapes/Landen.shp"
path_prov = "data/1-external/shapes/provincie.shp"

path_result.mkdir(exist_ok=True, parents=True)

cl = imod.idf.open(path_cl)
lay = xr.open_dataset(path_laymodel)
ahn = imod.idf.open(path_ahn)

if "z" not in lay:
    lay["z"] = 0.5 * (lay["top"] + lay["bot"])
if "dz" not in lay:
    lay["dz"] = lay["top"] - lay["bot"]
z = lay["z"]
z = z.assign_coords({"dz": (("layer", "y", "x"), lay["dz"])})


def top_grensvlak_time(cl, z, thresh):
    gv_top = []
    for t in cl.coords["time"].values:
        exc, fallb = imod.evaluate.interpolate_value_boundaries(
            cl.sel(time=t), z, thresh
        )
        grensvlak = exc.sel(boundary=0)
        gv_top.append(grensvlak)

    return xr.concat(gv_top, dim="time")


thresholds = [0.15, 1.0, 3.0, 5.0]
try:
    all_gv = xr.open_dataarray(path_result / "grensvlakken.nc")
    if not np.all(all_gv.threshold.data == thresholds):
        raise IOError
except IOError:
    all_gv = []
    for thresh in thresholds:
        print(thresh)

        gv = top_grensvlak_time(cl, z, thresh)

        # gv = all_gv.sel(threshold=thresh)

        # imod.idf.save(f"gv{thresh}gL", gv)
        all_gv.append(gv)
        imod.idf.write(
            path_result / f"gv{thresh}gL_2100-2020.idf",
            gv.isel(time=-1) - gv.isel(time=3),
        )

    all_gv = xr.concat(all_gv, pd.Index(thresholds, name="threshold"))
    all_gv.to_netcdf(path_result / "grensvlakken.nc")
"""
for thresh in thresholds:
    print(thresh)

    gv = all_gv.sel(threshold=thresh)

    # imod.idf.save(f"gv{thresh}gL", gv)
    gv21002005 = gv.isel(time=-1) - gv.isel(time=3)
    gv21002020 = (gv.isel(time=-1) - gv.isel(time=3)) / 80.0
    gv20202005 = (gv.isel(time=3) - gv.isel(time=0)) / 15.0
    imod.idf.write(path_result / f"gv{thresh}gL_2100-2020_my.idf", gv21002020)
    imod.idf.write(path_result / f"gv{thresh}gL_2020-2005_my.idf", gv20202005)
    fact = gv20202005 / gv21002020
    fact = fact.where(
        ((gv20202005 > 0) & (gv21002020 > 0)) | ((gv20202005 < 0) & (gv21002020 < 0))
    )
    fact = fact.where(np.abs(gv21002005) > 10.0)
    imod.idf.write(path_result / f"gv{thresh}gL_fact.idf", fact)
    imod.rasterio.write(path_result / f"gv{thresh}gL_fact.tif", fact)
"""

# Figures
landen = gpd.read_file(path_landen)
nl_da = imod.prepare.rasterize(
    landen[landen["CNTRYABB"] == "NL"], cl.isel(layer=0, time=0)
)
landen_no_NL = landen[landen["CNTRYABB"] != "NL"]
provincie = gpd.read_file(path_prov)

levels = list(range(-200, 0, 24))
levels = [-2,-5,-10,-15,-20,-30,-40,-50,-75,-100,-125,-150,-175,-200]
overlays = [
    {"gdf": landen_no_NL, "color": "white", "edgecolor": "white"},
    {"gdf": provincie, "color": "None", "edgecolor": "k"},
]
for thresh in thresholds:
    gv_ = all_gv.sel(threshold=thresh, time=pd.Timestamp(2020, 1, 1))
    gv_ = gv_.where(nl_da == 1)
    
    # create polygon of nodata-area for later hashing
    gv_at_ghb = (gv_.isnull() & (nl_da == 1)).astype(float)
    dx, xmin, xmax, dy, ymin, ymax = imod.util.spatial_reference(gv_at_ghb)
    gv_at_ghb_poly = imod.prepare.polygonize(gv_at_ghb)
    gv_at_ghb_poly = gv_at_ghb_poly.reset_index()
    # exclude total area polygon
    gv_at_ghb_poly_ = gv_at_ghb_poly.loc[
        gv_at_ghb_poly.geometry.area != (xmax - xmin) * (ymax - ymin)
    ]
    symdiff = gpd.overlay(
        gv_at_ghb_poly_.loc[gv_at_ghb_poly_.value == 1],
        gv_at_ghb_poly_.loc[gv_at_ghb_poly_.value == 0],
        how="difference",
    )

    gv_at_ghb_overlay = [
        {
            "gdf": symdiff,
            "color": "None",
            "edgecolor": "k",
            "hatch": "/",
            "label": "grensvlak onder GHB",
        }
    ]

    fig, ax = imod.visualize.plot_map(
        gv_-ahn,
        levels=levels,
        colors="viridis",
        figsize=(10, 10),
        overlays=overlays + gv_at_ghb_overlay,
        kwargs_colorbar={"label": "Ligging grensvlak (m NAP)"},
    )
    _ = ax.set_xticklabels([])
    _ = ax.set_yticklabels([])
    
    # ax.set_title(f"Verschil kwelflux LHM 4.0 - LHMzz {version} laag 1")
    fig.savefig(path_result / f"grensvlak_{run}_{thresh}.png", bbox_inches="tight")
