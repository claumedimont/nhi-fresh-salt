import imod
import numpy as np
import xarray as xr
from pathlib import Path
import warnings
warnings.filterwarnings("ignore")
np.seterr(all="ignore")

version = "schouwen_scenario1"

# filenames
fn_conc = f"data/4-output/{version}/conc/conc*.idf"
fn_head =  f"data/4-output/{version}/head/head*.idf"
fn_flf =  f"data/4-output/{version}/bdgflf/bdgflf*.idf"
fn_drn =  f"data/4-output/{version}/bdgdrn/bdgdrn*.idf"  # for boils
fn_layermodel = "data/2-interim/layermodel_corr.nc"
fn_conductivity = "data/2-interim/conductivity.nc"

fn_legend = "data/1-external/legends/chloride_gL.leg"
fn_shapenl = "data/1-external/provincie/provincie.shp"
outputdir = Path(f"data/5-visualization/{version}/saltload")
outputdir.mkdir(exist_ok=True, parents=True)

cl = imod.idf.open(fn_conc)
drn = imod.idf.open(fn_drn)

try:
    flf = imod.idf.open(fn_flf)
except FileNotFoundError:
    layermodel = xr.open_dataset(fn_layermodel)
    layermodel = layermodel.reindex_like(cl.isel(time=0))
    conductivity = xr.open_dataset(fn_conductivity)
    conductivity = conductivity.reindex_like(cl.isel(time=0))
    head = imod.idf.open(fn_head)
    c = 0.5 * layermodel["dz"] / conductivity["kv"]  # m / m/d = d
    c = c + c.shift(layer=-1)
    flf = (head.shift(layer=-1) - head) * 62500 / c  # m3/d, positive upward

diffuse = flf.sel(layer=10) * cl.sel(layer=11)
diffuse = diffuse.where(diffuse > 0, 0)
boils = -(drn.sel(layer=[11,12,13,14,15]) * cl.sel(layer=[11,12,13,14,15])).sum(dim="layer")
saltload = diffuse + boils

imod.idf.save(outputdir / "saltload", saltload)
imod.idf.save(outputdir / "diffuse", diffuse)
imod.idf.save(outputdir / "boils", boils)
