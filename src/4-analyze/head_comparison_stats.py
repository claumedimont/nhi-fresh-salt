import xarray as xr
import os
import imod
import pandas as pd
from pathlib import Path
# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# LHMzz version
version = 'heads_novdf' #snakemake.params.modelname
path_lhmfresh ="data/4-output/lhmfresh_V0.13/" #Path(snakemake.params.path_lhmfresh)

# open paths
basepath = Path(".")
result_path = basepath / f"data/5-visualization/head_comparison_{version}"

da = imod.idf.open(result_path / f"LHMv4_min_LHMzzv{version}_l*.idf")
da1 = imod.idf.open(result_path / f"LHMv4_min_LHMzzv{version}_phreatic.idf") 
da1 = da1.assign_coords(layer=1)

da = xr.concat((da1,da), dim="layer")
df = da.transpose().to_dataframe() * -1. # for some reason diffs are lhm - lhmzz
grpd = df.reset_index()[["layer",f"lhmv4_min_lhmzzv{version}_phreatic"]].groupby("layer")
df_desc = grpd.describe()
df_desc.columns = df_desc.columns.droplevel(0)
df_desc["10%"] = grpd.quantile(0.1)
df_desc["90%"] = grpd.quantile(0.9)
tot_desc = df["lhmv4_min_lhmzzvheads_novdf_phreatic"].describe()
tot_desc["10%"] = df["lhmv4_min_lhmzzvheads_novdf_phreatic"].quantile(.1)
tot_desc["90%"] = df["lhmv4_min_lhmzzvheads_novdf_phreatic"].quantile(.9)
tot_desc.name = "all"
desc = pd.concat((df_desc,pd.DataFrame(tot_desc).T),axis=0)
desc["IQR"] = desc["75%"] - desc["25%"]
desc["p10p90range"] = desc["90%"] - desc["10%"]
desc.to_csv(result_path / "stats_difference.csv")


##### boxplot
import matplotlib.pyplot as plt
import seaborn as sns
dfbox = df.reset_index()[["layer","lhmv4_min_lhmzzvheads_novdf_phreatic"]]
sns.boxplot(data=dfbox, x="layer",y="lhmv4_min_lhmzzvheads_novdf_phreatic", showfliers=False, boxprops={"facecolor": "w"}, medianprops={"color": "green"})
plt.ylabel("Verschil stijghoogte (m)")
plt.xlabel("LHM modellaag")
plt.grid()
plt.savefig(result_path / "boxplot_difference.png", dpi=300, bbox_inches="tight")
plt.close()
