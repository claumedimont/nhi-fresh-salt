# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 14:43:30 2020

@author: mulde_ts
"""

import imod
import pathlib
import xarray as xr
import os
import pandas as pd

os.chdir(os.path.join(os.path.dirname(__file__),"..",".."))

path_bdgfrf = snakemake.input.path_bdgfrf
path_bdgfff = snakemake.input.path_bdgfff
path_bdgflf = snakemake.input.path_bdgflf
modelname = snakemake.params.modelname
min_dt = snakemake.params.min_dt
path_result = snakemake.output.path_result
path_top_bot = snakemake.input.path_top_bot
# Interim paths
top_bot = xr.open_dataset(path_top_bot)
bdgfrf = imod.idf.open(path_bdgfrf)#.squeeze("time", drop=True)
bdgfrf['dy'] = top_bot.dy.values
bdgfff = imod.idf.open(path_bdgfff)#.squeeze("time", drop=True)
bdgfff['dx'] = top_bot.dx.values
bdgflf = imod.idf.open(path_bdgflf)#.squeeze("time", drop=True)
bdgflf['dx'] = top_bot.dx.values
bdgflf['dy'] = top_bot.dy.values


#Right Face (BDGFRF), the Front Face (BDGFFF) and/or the Lower Face (BDGFLF) 
#imod.evaluate.stability_constraint_advection(front, lower, right, top_bot, porosity=0.3, R=1.0)

def get_dt_from_previous_run(bdgfrf, bdgfff, bdgflf, top_bot):
    """ calculate maximum stepsize from previous run, results stored at path
    assumes existence of {path}/bdgfrf/bdgfrf*.idf, {path}/bdgfff/bdgfff*.idf,
    {path}/bdgflf/bdgflf*.idf, 
    {top_bot} is a dataset with top and bot defined
    """
    stability_adv = imod.evaluate.stability_constraint_advection(
        bdgfff, bdgflf, bdgfrf, top_bot, porosity=0.3
    )
    # return da of maximum stepsize
    return stability_adv[0]

dt = get_dt_from_previous_run(bdgfrf, bdgfff, bdgflf, top_bot).load()
dt.to_netcdf(path_result)

# print summary:
ncells = int((dt < min_dt).sum(dim=dt.dims))
print(f"Calculated dt, saved to f{path_result}")
print(f"{ncells} have a dt < {min_dt} and should be corrected")

