"""compare two 3-input directories and report differences"""
from pathlib import Path
import imod
import numpy as np

versions = ["V0.24","V0.27"]
write_diff = True
logfile = f"whathaschanged_{versions[0]}-{versions[1]}.log"
path1 = Path(f"data/3-input/{versions[0]}")
path2 = Path(f"data/3-input/{versions[1]}")

def _print(s):
    with open(logfile, "a") as f:
        f.write(s+"\n")
    print(s)

for pkgdir1 in path1.glob("*"):
    if pkgdir1.is_dir():
        pkgname = str(pkgdir1.relative_to(path1))
        _print(f"Comparing package {pkgname}")
        pkgdir2 = path2 / pkgname
        
        if not pkgdir2.exists():
            _print(f"Package {pkgname} is present for {versions[0]}, but not for {versions[1]}")
            continue

        # load both
        try:
            if "wel" in pkgname:
                pkg1 = imod.ipf.read(pkgdir1 / "*.ipf").to_xarray()
                pkg2 = imod.ipf.read(pkgdir2 / "*.ipf").to_xarray()
            else:  
                pkg1 = imod.idf.open_dataset(pkgdir1 / "*.idf")
                pkg2 = imod.idf.open_dataset(pkgdir2 / "*.idf")

            # compare
            for k in pkg1.keys():
                da1 = pkg1[k]
                da2 = pkg2[k]
                if da1.equals(da2):
                    _print(f"{pkgname} - {k} equal for {versions[0]} and {versions[1]}")
                else:
                    with imod.util.ignore_warnings():
                        diff = da2.fillna(0) - da1.fillna(0)
                        diff = diff.where(diff != 0)
                        meandiff = float(diff.mean().compute())
                        maxdiff = float(np.abs(diff).max().compute())
                        ncells = int(diff.notnull().sum().compute())
                    if ncells == 0 or maxdiff < 1e-3:
                        _print(f"{pkgname} - {k} equal for {versions[0]} and {versions[1]}")
                    else:
                        _print(f"WARNING: {pkgname} - {k} NOT equal for {versions[0]} and {versions[1]}, {ncells} total different cells, mean difference {meandiff:.1e}")
                        if write_diff:
                            fn = f"diff_{pkgname}_{k}_{versions[0]}_{versions[1]}.nc"
                            _print(f"Differences stored as {fn}")
                            diff.to_netcdf(fn)
                            if "riv" in pkgname or "drn" in pkgname:
                                imod.idf.save(pkgname,diff)
        except FileNotFoundError:
            pass
