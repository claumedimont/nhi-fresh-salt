import xarray as xr
import os
import imod
import matplotlib.pyplot as plt
import matplotlib.colorbar
import geopandas as gpd
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# LHMzz version
version = "0.1.17"

# open paths
basepath = Path(".")
path_layermodel = basepath / "data/2-interim/layermodel.nc"
path_flf = basepath / fr"data\4-output\V{version}_tr_merged\bdgflf\bdgflf_2005010100000*.idf"
path_lhm = basepath / r"data\1-external\LHM4.0_steady_state_v3\bdgflf\bdgflf*.idf"
path_resleg = basepath / r"data\1-external\legends\residuals.leg"
result_path = basepath / r"data\5-visualization\bdgflf_comparison"
result_path.mkdir(exist_ok=True)

# load
layermodel = xr.open_dataset(path_layermodel)
# head = imod.idf.open_subdomains(path_head).squeeze('time', drop=True)
flf = imod.idf.open(path_flf).squeeze('time', drop=True)
lhm = imod.idf.open(path_lhm).squeeze("time", drop=True)

res_leg = imod.visualize.read_imod_legend(path_resleg)
# For other layers
layers = {
    2: 1,
    4: 2,
    6: 3,
    8: 4,
    10: 5,
    12: 6,
    14: 7
}  # Key: LHM_id, Value: LHM_layer

for k, v in layers.items():
    upper_layer = imod.select.upper_active_layer(
        (layermodel["lhmfresh_layer"] == k).load(), is_ibound=True)
    flf_zz = flf.where(flf.layer == upper_layer)
    flf_zz = flf_zz.min(dim='layer')

    flf_lhm = lhm.sel(layer=v)

    diff = flf_lhm - flf_zz
    diff_mm = diff / 62500 * 1000.  # m3/d to mm/d
    imod.idf.write(result_path / f"LHMv4_min_LHMzzv{version}_l{v}.idf", diff_mm)

    fig, ax = imod.visualize.plot_map(
        diff_mm, levels=res_leg[1], colors=res_leg[0], figsize=(10, 10)
    )

    ax.set_title(f"Verschil FLF LHM 4.0 - LHMzz {version} laag {v}")
    fig.savefig(result_path / f"LHMv4_min_LHMzzv{version}_l{v}.png")
    plt.close()
