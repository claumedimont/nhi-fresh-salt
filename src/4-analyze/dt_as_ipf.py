import os
import pathlib
import imod
import xarray as xr

os.chdir(os.path.join(os.path.dirname(__file__),"..",".."))

modelversion = "V0.28_speed1"

# Interim paths
path_template = "data/2-interim/template.nc"
path_template_2d = "data/2-interim/template_2d.nc"
path_conductivity = "data/2-interim/conductivity.nc"
path_layermodel = "data/2-interim/layermodel.nc"
path_well = "data/2-interim/wel.nc"
dt_run_path = f"data/4-output/{modelversion}"
dt_top_bot_path = "data/2-interim/top_bot.nc"
path_bdgfrf = f"data/4-output/{modelversion}/bdgfrf/*"
path_bdgflf = f"data/4-output/{modelversion}/bdgflf/*"
path_bdgfff = f"data/4-output/{modelversion}/bdgfff/*"
path_top_bot = "data/2-interim/top_bot.nc"

#path_bdgfrf = snakemake.input.path_bdgfrf
#path_bdgfff = snakemake.input.path_bdgfff
#path_bdgflf = snakemake.input.path_bdgflf
#path_layermodel = snakemake.input.path_layermodel
#modelname = snakemake.params.modelname
#min_dt = snakemake.params.min_dt
#path_result = snakemake.output.path_result

top_bot = xr.open_dataset(path_top_bot)
bdgfrf = imod.idf.open(path_bdgfrf).squeeze("time", drop=True)
bdgfrf['dy'] = top_bot.dy.values
bdgfff = imod.idf.open(path_bdgfff).squeeze("time", drop=True)
bdgfff['dx'] = top_bot.dx.values
bdgflf = imod.idf.open(path_bdgflf).squeeze("time", drop=True)
bdgflf['dx'] = top_bot.dx.values
bdgflf['dy'] = top_bot.dy.values
layermodel = xr.open_dataset(path_layermodel)
path_result = f"data/4-output/{modelversion}/dt"

#Right Face (BDGFRF), the Front Face (BDGFFF) and/or the Lower Face (BDGFLF) 
#imod.evaluate.stability_constraint_advection(front, lower, right, top_bot, porosity=0.3, R=1.0)

def get_dt_from_previous_run(bdgfrf, bdgfff, bdgflf, top_bot):
    """ calculate maximum stepsize from previous run, results stored at path
    assumes existence of {path}/bdgfrf/bdgfrf*.idf, {path}/bdgfff/bdgfff*.idf,
    {path}/bdgflf/bdgflf*.idf, 
    {top_bot} is a dataset with top and bot defined
    """
    stability_adv = imod.evaluate.stability_constraint_advection(
        bdgfff, bdgflf, bdgfrf, top_bot, porosity=0.3
    )
    # return da of maximum stepsize
    return stability_adv[0]

dt = get_dt_from_previous_run(bdgfrf, bdgfff, bdgflf, top_bot).load()
dt.to_netcdf(path_result)

like = xr.open_dataset(path_template)["template"]
kh = xr.open_dataset(path_conductivity)['kh']
kv = xr.open_dataset(path_conductivity)['kv']
layermodel = xr.open_dataset(path_layermodel)
dz = layermodel['dz']
#wells= xr.open_dataset(path_well)
#wells = wells.to_dataframe()

dt_min = 10

dt = dt.where(dt<dt_min)

# print summary:
ncells = (dt < dt_min).count(dim=dt.dims)
print(f"Calculated dt, saved to f{path_result}")
print(f"{ncells} have a dt < {dt_min} and should be corrected")
kh = kh.where(dt)
kv = kv.where(dt)
dz = dz.where(dt)


bdgfrf = bdgfrf.reindex_like(like)
bdgfrf = bdgfrf.where(dt)
bdgfff = bdgfff.reindex_like(like)
bdgfff = bdgfff.where(dt)
bdgflf = bdgflf.reindex_like(like)
bdgflf = bdgflf.where(dt)


dt = dt.rename('dt')
dt = dt.to_dataframe().dropna()
kh = kh.to_dataframe().dropna()
kv = kv.to_dataframe().dropna()
dz = dz.to_dataframe().dropna()
bdgfff = bdgfff.rename('bdgfff')
bdgfff = bdgfff.to_dataframe().dropna()
bdgflf = bdgflf.rename('bdgflf')
bdgflf = bdgflf.to_dataframe().dropna()
bdgfrf = bdgfrf.rename('bdgfrf')
bdgfrf = bdgfrf.to_dataframe().dropna()

        

problem_cells = dt
problem_cells['kh'] = kh.kh
problem_cells['kv'] = kv.kv
problem_cells['dz'] = dz.dz
problem_cells['bdgfff'] = bdgfff.bdgfff
problem_cells['bdgflf'] = bdgflf.bdgflf
problem_cells['bdgfrf'] = bdgfrf.bdgfrf
problem_cells = problem_cells.reset_index(level=['layer', 'y', 'x'])
problem_cells = problem_cells.rename(columns={"x": "X","y": "Y" })

problem_cells['ID']  =problem_cells.index

problem_cells = problem_cells.reindex(columns = [ 'X', 'Y','layer', 'dx', 'dy', 'dt', 'kh', 'kv', 'dz', 'bdgfff',
       'bdgflf', 'bdgfrf', 'ID'])
    
#problem_cells = problem_cells.reindex(columns = [ 'X', 'Y','layer', 'dx', 'dy', 'dt', 'ID'])
problem_cells = problem_cells.sort_values(by=['dt'])
imod.ipf.write(f"data/4-output/{modelversion}/problem_cells_V28_speed6.ipf", problem_cells)


top = imod.idf.open(r"data/3-input/V0.28/bas/top/top_l*.idf")
bot = imod.idf.open(r"data/3-input/V0.28/bas/bot/bottom_l*")
top_bot = xr.Dataset()
top_bot["top"] = top
top_bot["bot"] = bot


def stability_constraint_advection(front, lower, right, top_bot, porosity=0.3, R=1.0):
    r"""
    Computes advection stability constraint as applied in MT3D for adaptive
    timestepping (Zheng & Wang, 1999 p54):

    .. math:: \Delta t \leq \frac{R}{\frac{\left | v_{x} \right |}{\Delta x}+\frac{\left | v_{y} \right |}{\Delta y}+\frac{\left | v_{z} \right |}{\Delta z}}

    This function can be used to select
    which cells necessitate a small timestap, thereby slowing down calculations.

    Front, lower, and right arguments refer to iMOD face flow budgets, in cubic
    meters per day. In terms of flow direction these are defined as:

    * ``front``: positive with ``y`` (negative with row index)
    * ``lower``: positive with ``layer`` (positive with layer index)
    * ``right``: negative with ``x`` (negative with column index)

    Returns the minimum timestep that is required to satisfy this constraint.
    The resulting dt xr.DataArray is the minimum timestep over all three directions,
    dt_xyz is an xr.Dataset containing minimum timesteps for the three directions
    separately.

    Parameters
    ----------
    front: xr.DataArray of floats, optional
        Dimensions must be exactly ``("layer", "y", "x")``.
    lower: xr.DataArray of floats, optional
        Dimensions must be exactly ``("layer", "y", "x")``.
    right: xr.DataArray of floats, optional
        Dimensions must be exactly ``("layer", "y", "x")``.
    top_bot: xr.Dataset of floats, containing 'top', 'bot' and optionally
        'dz' of layers.
        Dimensions must be exactly ``("layer", "y", "x")``.
    porosity: float or xr.DataArray of floats, optional (default 0.3)
        If xr.DataArray, dimensions must be exactly ``("layer", "y", "x")``.
    R: Retardation factor, optional (default)
        Only when sorption is a factor.

    Returns
    -------
    dt: xr.DataArray of floats
    dt_xyz: xr.Dataset of floats
    """

    # top_bot reselect to bdg bounds
    top_bot = top_bot.sel(x=right.x, y=right.y, layer=right.layer)

    # Compute flow velocities
    qs_x, qs_y, qs_z = imod.evaluate.flow_velocity(
        front, lower, right, top_bot, porosity
    )

    if "dz" not in top_bot:
        top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    # dz between layers is 0.5*dz_up + 0.5*dz_down
    dz_m = top_bot.dz.rolling(layer=2, min_periods=2).mean()
    dz_m = dz_m.shift(layer=-1)

    # assert all dz positive - Issue #140
    if not np.all(dz_m.values[~np.isnan(dz_m.values)] >= 0):
        raise ValueError("All dz values should be positive")

    # absolute velocities (m/d)
    abs_v_x = np.abs(qs_x / porosity)
    abs_v_y = np.abs(qs_y / porosity)
    abs_v_z = np.abs(qs_z / porosity)

    # dt of constituents (d)
    dt_x = R / (abs_v_x / top_bot.dx)
    dt_y = R / (abs_v_y / np.abs(top_bot.dy))
    dt_z = R / (abs_v_z / dz_m)

    # overall dt due to advection criterion (d)
    dt = 1.0 / (1.0 / dt_x + 1.0 / dt_y + 1.0 / dt_z)

    dt_xyz = xr.concat(
        (dt_x, dt_y, dt_z), dim=pd.Index(["x", "y", "z"], name="direction")
    )
    return dt, dt_xyz