# -*- coding: utf-8 -*-
"""

"""
import os
import sys
import gc
import xarray as xr
import imod
sys.path.append(os.path.join(os.path.dirname(__file__),"..","1-prepare"))  # add script directory to python path
sys.path.append(r"c:\Svnrepos\nhi-fresh-salt\src\1-prepare")  # add script directory to python path
from utils import intra_cell_boundary_conditions

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

path_layermodel = "data/2-interim/layermodel.nc"
path_ghb = "data/2-interim/ghb.nc"
paths_riv = {
    "riv_h": "data/2-interim/river_h.nc",
#    "riv_p_corr": "data/2-interim/river_p_corr.nc",
    "riv_p": "data/2-interim/river_p.nc",
    "riv_s": "data/2-interim/river_s.nc",
    "riv_t": "data/2-interim/river_t.nc",
}
paths_drn = {
    "drn_b": "data/2-interim/drainage_b.nc",
    "drn_mvgrep": "data/2-interim/drainage_mvgrep.nc",
    "drn_sof": "data/2-interim/drainage_sof.nc",
#    "drn_sof_corr": "data/2-interim/drainage_sof_corr.nc",
    "drn_wadden": "data/2-interim/drainage_wadden.nc",
}

layermodel = xr.open_dataset(path_layermodel)
ghb = xr.open_dataset(path_ghb)
riv = {k:xr.open_dataset(v) for k,v in paths_riv.items()}
drn = {k:xr.open_dataset(v) for k,v in paths_drn.items()}

dt_min, dt_all = intra_cell_boundary_conditions(layermodel, ghb=ghb, riv=riv, drn=drn)
s = dt_all.min(dim=["layer","y","x"]).to_series()
print(s)

small_dt = dt_all.where(dt_all < 25.)
for dim in small_dt.dims:
    small_dt = small_dt.dropna(dim=dim,how="all")
small_dt.name = "small_dt"
small_dt = small_dt.drop(["percentile","dx","dy"])
small_dt = small_dt.to_dataset()
small_dt.to_netcdf("tmp.nc")
small_dt = xr.open_dataset("tmp.nc",chunks={"combination":1,"layer":1})["small_dt"]
gc.collect()
small_dt_df = small_dt.to_dataframe().dropna()

small_dt_df = small_dt_df.reset_index()
indices = imod.select.points_indices(layermodel,x=small_dt_df["x"],y=small_dt_df["y"],layer=small_dt_df["layer"])
small_dt_df["z"] = 0.5*layermodel["top"].isel(**indices) + 0.5*layermodel["bot"].isel(**indices) 

small_dt_df = small_dt_df.reindex(columns=["x","y","z","layer","combination","small_dt"])

for comb in small_dt_df.combination.unique():
    cols = []
    for c in comb.split("-"):
        if "riv" in c:
            small_dt_df[f"{c}_stage"] = riv[c]["stage"].isel(**indices)
            small_dt_df[f"{c}_cond"] = riv[c]["conductance"].isel(**indices)
            cols += [f"{c}_stage",f"{c}_cond"]
        elif "ghb" in c:
            small_dt_df[f"{c}_head"] = ghb[c]["head"].isel(**indices)
            small_dt_df[f"{c}_cond"] = ghb[c]["conductance"].isel(**indices)
            cols += [f"{c}_head",f"{c}_cond"]
        elif "drn" in c:
            small_dt_df[f"{c}_elev"] = drn[c]["elevation"].isel(**indices)
            small_dt_df[f"{c}_cond"] = drn[c]["conductance"].isel(**indices)
            cols += [f"{c}_",f"{c}_cond"]
        small_dt_df[cols] = small_dt_df[cols].round(2)
    df_sel = small_dt_df.loc[small_dt_df.combination == comb].reindex(columns=["x","y","z","layer","combination","small_dt"]+cols)
    imod.ipf.write(f"small_dt_{comb}.ipf",df_sel, nodata=-9999.)

imod.ipf.write("small_dt_boundarycond.ipf",small_dt_df, nodata=-9999.)