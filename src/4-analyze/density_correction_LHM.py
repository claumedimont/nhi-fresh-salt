# -*- coding: utf-8 -*-
"""
Created on Fri May  8 12:54:50 2020

@author: engelen
"""

import imod
import os
import numpy as np
import xarray as xr

#%%Functions
def calc_h_f(h_i, rho_i, z_i, rho_f = 1000.):
    """
    Calculate fresh water heads from point water heads, correcting for density effects
    
    Parameters
    ----------
    h_i : DataArray
        Point water heads

    rho_i : DataArray
        Densities
    
    z_i : DataArray
        Depths

    Returns
    -------
    h_f : DataArray
        Freshwater heads

    References
    ----------
    Equation 6 in:
    Using hydraulic head measurements in variable-density ground water flow analyses. 
    Post, Vincent; Kooi, Henk; Simmons, Craig; (2007)

    """

    h_f = (rho_i/rho_f) * h_i - (rho_i-rho_f)/rho_f * z_i
    return(h_f)
    
#%%Options
write_flag =True #Write .idfs
plot_flag  =False #Make some intermediate analysis plots
calc_hf    =False #Calculate fresh water heads 

#%%Path management
# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
    
path_out = r"data\4-output\LHM\densities"
os.makedirs(path_out, exist_ok=True)

input_path_LHMzz = r"data\3-input"

folders = ["riv_h", "riv_p", "riv_s", "riv_t", "ghb"]

path_active = r"data\1-external"
active_files = [r"riv\hoofdwater\avg_peil_hoofdwater.idf", r"riv\regionaal\PEIL_P1W_250.IDF",
                r"riv\regionaal\PEIL_S1W_250.IDF", r"riv\regionaal\PEIL_T1W_250.IDF",
                r"ghb\ghb_head_l1.idf"]


paths_bcs_LHMzz = dict([(fol, os.path.join(input_path_LHMzz, fol, "*")) for fol in folders])
path_bottom = os.path.join(input_path_LHMzz, "bas", "bottom_l*")
path_top = os.path.join(input_path_LHMzz, "bas", "top.idf")

paths_active = {name: os.path.join(path_active, f) for name, f in zip(folders, active_files)}

#Boils
path_boils_out = os.path.join(path_out, "riv_wel_dens_L1.idf")
boils_LHMzz_f = os.path.join(input_path_LHMzz, "drn_boils", "elevation_l*.idf")
boils_LHM_f   = os.path.join(input_path_LHMzz, "..", "1-external", "riv", "wellen", "peilw_wel.idf")

sal_f  = os.path.join(input_path_LHMzz, "btn", 'starting_concentration_l*.idf')

#%%Read
ds_bc_dic = dict([(key, imod.idf.open_dataset(path, pattern=r"{name}_l{layer}")) for key, path in paths_bcs_LHMzz.items()])

if calc_hf:
    bottoms = imod.idf.open_dataset(path_bottom)["bottom"]
    top = imod.idf.open(path_top)

ds_active = {name: imod.idf.open(path) for name, path in paths_active.items()}
ds_active = {name: np.isfinite(ds) for name, ds in ds_active.items()}

boils_LHMzz = imod.idf.open(boils_LHMzz_f)
boils_LHM = imod.idf.open(boils_LHM_f)

salt = imod.idf.open(sal_f)

#%%Calculate
h_keys = ["stage", "stage", "stage", "stage", "head"] #bcs do not share same name for heads...

ds_dens = {name: ds["density"].mean(dim="layer") for name, ds in ds_bc_dic.items()}

if calc_hf:
    tops = xr.where(bottoms.layer==1, top, bottoms.shift(layer=1))
    z = (bottoms + tops)/2
    ds_hf = {name: calc_h_f(ds[h_key], ds["density"], z) for (name, ds), h_key in zip(ds_bc_dic.items(), h_keys)}
    ds_hf = {name: ds_hf[name].max(dim="layer") for name in ds_hf.keys()}

#Boils
salt_boils = salt.sel(layer = boils_LHMzz.layer).where(np.isfinite(boils_LHMzz))

#Calculate linear slope concentration/density slope for chlorine.
#Cl seawater = 20,376 mg/L (Table 2 in Kohfahl et al., 2015)
#density seawater = 1025 kg/m3 (Seawat manual)

sal_sea = 20.376
dens_sea = 1025.
dens_ref = 1000.

dens_slope = (dens_sea-dens_ref)/sal_sea

density_boils = dens_ref + salt_boils * dens_slope

#Ensure that all cells in LHM data have a value and if not fill with 1000.
density_boils_LHM = density_boils.mean(dim="layer").fillna(1000.).where(np.isfinite(boils_LHM)).rename("density_boils")

#%%Analyze
#Some weird jumps in heads visible between Wadden Islands, this is presumably 
#because the Wadden Sea is assigned to layer 14, and the North Sea to layer 8
layer_ghb = (np.isfinite(ds_bc_dic["ghb"]["head"]) * ds_bc_dic["ghb"]["head"].layer).max(dim="layer")

if plot_flag:
    z_ghb = z.where(np.isfinite(ds_bc_dic["ghb"]["head"])).min(dim="layer")
    layer_ghb.plot(vmax=14)
    z_ghb.plot(vmin=-50)

if plot_flag and calc_hf:
    ds_hf["ghb"].plot(vmax=1)
    #Be careful with zeeland as well:
    ds_hf["riv_h"].sel(x=slice(0, 100000), y=slice(550000, 350000)).plot()

#%%Convert to input for LHM
#Fill empty parts North Sea. Requires bottleneck package
ds_dens["ghb"] = ds_dens["ghb"].bfill("x")
ds_dens["ghb"] = ds_dens["ghb"].ffill("x")

ds_dens_lhm = {name: xr.where(ds_active[name], ds_dens[name], np.nan) for name in ds_dens.keys()}

if calc_hf:
    ds_hf_lhm = {name: xr.where(ds_active[name], ds_hf[name], np.nan) for name in ds_hf.keys()}

#%%Write
if write_flag:
    for name, ds in ds_dens_lhm.items():
        imod.idf.write(os.path.join(path_out, name + "_dens_L1.idf"), ds.squeeze())
    imod.idf.save(path_boils_out, density_boils_LHM)

if write_flag and calc_hf:
    for name, ds in ds_hf_lhm.items():
        imod.idf.write(os.path.join(path_out, name + "_hf_L1.idf"), ds.squeeze())