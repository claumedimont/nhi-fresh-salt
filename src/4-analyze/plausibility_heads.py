import matplotlib.pyplot as plt
import imod
import os
import numpy as np
import xarray as xr

# Input:
os.chdir(r'c:\temp\NHI')
scen = 'NHI-ZZ-2.3.0'
ipfpath = r'meetreeksen\meetreeksen_laag*.ipf'
top_LHM34 = r'data\1-external\layermodel\LHM34\top\TOP_L*.IDF'
bot_LHM34 = r'data\1-external\layermodel\LHM34\bot\BOT_L*.IDF'
bot_NHIzz = r'data\3-input\bas\bottom_l*.idf'
hds_NHIzz = r'data\4-output\head\head_20000101_l*.idf'
#con_NHIzz = r'data\4-output\conc\conc_20000101_l*.idf'
toetsingsregios = r'data\1-external\toetsing\PLAUSIBILITEITSREGIOS.IDF'

# Read IPF, determine mean groundwater head
df = imod.ipf.read(ipfpath)
dfs = df[df['head'] > -999.]    # Remove nodata values
dfm = dfs.groupby(['DINOID','x','y','ilay'])['head'].mean().reset_index()
dfm = dfm.rename(columns={'head': 'obshds'})
dfm = dfm[['x', 'y', 'DINOID', 'ilay', 'obshds']]

# Read TOP's and BOT's of LHM 3.4 (7 watervoerende lagen)
tLHM = imod.idf.open(top_LHM34)
topsel = imod.select.points_values(tLHM, x=dfm.x, y=dfm.y, layer=dfm.ilay)
dfm['top'] = topsel.values
dfm = dfm[~np.isnan(dfm['top'])]      # Removed rows where top = nan
bLHM = imod.idf.open(bot_LHM34)
botsel = imod.select.points_values(bLHM, x=dfm.x, y=dfm.y, layer=dfm.ilay)
dfm['bot'] = botsel.values
dfm['z'] = np.nanmean(np.array([dfm['top'], dfm['bot']]), axis=0)

# Read BOT's of NHI-zz model, determine layer based on z-node LHM3.4
bNHI = imod.idf.open(bot_NHIzz)
getbots = imod.select.points_values(bNHI, x=dfm.x, y=dfm.y)
x = np.repeat(dfm['z'][np.newaxis,:], len(getbots), axis=0)
getlayer = np.sum((getbots.values - x) > 0, axis=0) + 1

# Read heads (IDF) of model run
h = imod.idf.open(hds_NHIzz)
head = h[0].values; head[~np.isnan(head)] = 1; head[np.isnan(head)] = 0
head = xr.DataArray(head, dims=('layer', 'y', 'x'),coords={'x': h.x, 'y': h.y, 'layer': h.layer})
highest_active_layer = imod.select.upper_active_layer(head, is_ibound=True)
gethal = imod.select.points_values(highest_active_layer, x=dfm.x, y=dfm.y)
gethal_int = np.array(gethal.values, dtype='int')

# Extract heads of (highest active) model layer
dfm['nhilay'] = np.max([getlayer,gethal_int], axis=0)
hNHI = imod.select.points_values(h[0], x=dfm.x, y=dfm.y, layer=dfm.nhilay)
dfm['nhihds'] = hNHI.values
dfm['calc_obs'] = dfm['nhihds'] - dfm['obshds']
print('Mean error: %g m' % np.nanmean(dfm['calc_obs']))
print('Mean absolute error: %g m' % np.nanmean(np.abs(dfm['calc_obs'])))

# Export results in table-format (plausbiliteit) grouped per layer
outstat1 = open('toetsing\%s-toetsing.csv' % scen, 'w')
outstat1.write('layer,mediaan,mean,std,voldoet\n')

toetsregios = imod.idf.open(toetsingsregios)
dfm_zones = imod.select.points_values(toetsregios, x=dfm.x, y=dfm.y)
dfm['toetsingsregio'] = dfm_zones.values
dflyrmedian = dfm.groupby(['ilay'])['calc_obs'].median()
dflyrmean = dfm.groupby(['ilay'])['calc_obs'].mean()
dflyrstd = dfm.groupby(['ilay'])['calc_obs'].std()

dfl1 = dfm[dfm['ilay'] == 1]                                    # Toetsing laag 1 (Freatisch)
median_toets = dfl1.groupby(['toetsingsregio'])['calc_obs'].median()
mean_toets = dfl1.groupby(['toetsingsregio'])['calc_obs'].mean()
std_toets = dfl1.groupby(['toetsingsregio'])['calc_obs'].std()
dfl1_zone1 = dfl1[dfl1['toetsingsregio'] == 1]                  # Peilbeheerst = 1
toetsl1_zone1 = np.sum(np.abs(dfl1_zone1.calc_obs) <= 0.3)
voldoetl1_zone1 = (toetsl1_zone1 / len(dfl1_zone1)) * 100
outstat1.write('%s,%.3f,%.3f,%.3f,%i\n' % ('1-peilbeheerst',median_toets[1],mean_toets[1],std_toets[1],voldoetl1_zone1))
dfl1_zone2 = dfl1[dfl1['toetsingsregio'] == 2]                  # Vrij afwaterend = 2
toetsl1_zone2 = np.sum(np.abs(dfl1_zone2.calc_obs) <= 0.5)
voldoetl1_zone2 = (toetsl1_zone2 / len(dfl1_zone2)) * 100
outstat1.write('%s,%.3f,%.3f,%.3f,%i\n' % ('1-vrijafwaterend',median_toets[2],mean_toets[2],std_toets[2],voldoetl1_zone2))
dfl1_zone3 = dfl1[dfl1['toetsingsregio'] == 3]                  # Sterk hellend = 3
toetsl1_zone3 = np.sum(np.abs(dfl1_zone3.calc_obs) <= 1.0)
voldoetl1_zone3 = (toetsl1_zone3 / len(dfl1_zone3)) * 100
outstat1.write('%s,%.3f,%.3f,%.3f,%i\n' % ('1-sterkhellend',median_toets[3],mean_toets[3],std_toets[3],voldoetl1_zone3))
toetsl1_totaal = toetsl1_zone1 + toetsl1_zone2 + toetsl1_zone3
voldoetl1 = (toetsl1_totaal / len(dfl1)) * 100
outstat1.write('%i,%.3f,%.3f,%.3f,%i\n' % (1,dflyrmedian[1],dflyrmean[1],dflyrstd[1],voldoetl1))

dfl2 = dfm[dfm['ilay'] == 2]            # Toetsing laag 2 (WVP1)
toetsl2 = np.sum(np.abs(dfl2.calc_obs) <= 0.5)
voldoetl2 = (toetsl2 / len(dfl2)) * 100
outstat1.write('%i,%.3f,%.3f,%.3f,%i\n' % (2,dflyrmedian[2],dflyrmean[2],dflyrstd[2],voldoetl2))

for i in [3,4,5,6,7]:
    dfl3tot7 = dfm[dfm['ilay'] == i]    # Toetsing laag 3-7 (Diep)
    toetsl3tot7 = np.sum(np.abs(dfl3tot7.calc_obs) <= 1.0)
    voldoet = (toetsl3tot7 / len(dfl3tot7)) * 100
    outstat1.write('%i,%.3f,%.3f,%.3f,%i\n' % (i,dflyrmedian[i],dflyrmean[i],dflyrstd[i],voldoet))
outstat1.close()

# Export results to csv and ipf
dfm.to_csv(r'toetsing\%s.csv' % scen)
dfl1.to_csv(r'toetsing\%s_l1.csv' % scen)
imod.ipf.write(r'toetsing\%s.ipf' % scen, dfm)

# Plot results as scatterplot(s) 
plt.figure(figsize = (10, 10)) 
plt.scatter(dfm['obshds'], dfm['nhihds'], c = dfm['nhilay'])
minobs = np.min(dfm['obshds']); maxobs = np.max(dfm['obshds'])
plt.plot([minobs,maxobs], [minobs,maxobs], color = '#55575A',
         linewidth = 2, linestyle='dashed', marker=None)
plt.xlabel('Gemeten stijghoogte [m NAP]')
plt.ylabel('Berekende Stijghoogte [m NAP]')
plt.tight_layout()
#plt.legend(loc = 'lower right')
plt.grid(color = '#D9D9D9')
pngname1 = 'toetsing\%s.png' % (scen)
plt.savefig(pngname1, dpi = 600)
plt.xlim(-5,40); plt.ylim(-5,40)
pngname2 = 'toetsing\%s_zoom.png' % (scen)
plt.savefig(pngname2, dpi = 600)
plt.close()

print('Succesvol!')