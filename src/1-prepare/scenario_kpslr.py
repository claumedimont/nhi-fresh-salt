"""
This script creates input for the KP SeaLevelRise scenario
- For both LHM-fs and LHM? Yes! (but first LHM-fs)

The scenario:
- Sea level rise in the Sea (GHB) and open connected water bodies:
  - Rhine-Meuse delta, including Haringvliet. SLR according to calculations of De Jong for median river discharge, linearly extrapolated (RIV-H)
  - Ooster- and Westerschelde, level with SLR (RIV-H)
- Transient SLR curve as supplied by RWS. Up until maximum SLR of 3m (~2300 AD)
--> check for flooding? No, assume policy as is

- Soil subsidence:
  - to be decided. 
  - Regional water levels go down with subsidence, as well as the surface elevation. --> Ook kD? check met Joachim.
Uitgangspunten implementatie bodemdaling in Deltascenario's:
"De bodemdaling is in het model geïmplementeerd vertaald naar de peilen van het oppervlaktewater en het maaiveld.
 De oppervlaktewaterpeilen van het regionaal systeem zijn aangepast waarbij is aangenomen dat de ontwateringsdiepte 
 gelijk blijft. Er is aangenomen dat de peilen van het hoofdwatersysteem niet beïnvloed worden door de bodemdaling. 
 Hierom zijn deze peilen niet aangepast als gevolg van de bodemdaling."
 Verder: in Pleistoceen NL bodemdaling (beekdalen) gereduceerd tot max 1 cm/j; voor natte natuur (Landgebruik 13) 
 dalingssnelheid met 50% gereduceerd"
  - ==> is maaiveld echt belangrijk om mee te laten dalen? Of alleen voor ghg plaatjes? Wel richting LHM

- Recharge?
  - remains at present amount

- Changes in salinity of surface water?
  - RMM no? <> Ymkje
  - Ooster/Westerschelde: no


Input:

Output:

"""
import numpy as np
import pandas as pd
import xarray as xr
from pathlib import Path
import cftime
import imod
import gc
from dask.diagnostics import ProgressBar

#### inputs
"""
path_interim = Path("d:/nhi-fresh-salt/data/2-interim")
path_slrcurve = "data/1-external/kpslr/ZSScurve_vos.xlsx"
path_layermodel = path_interim / "layermodel.nc"

# sea and main waterways
path_ghb = path_interim / "ghb.nc"
path_rivh = path_interim / "river_h.nc"
path_rivh_drn = path_interim / "river_h_drn.nc"
path_rivh_slr = path_interim / "rivh_kpslr.nc" # ==> from prepare_kpslr

# soil subsidence
path_subsidence = path_interim / "subsidence.nc"
# regional rivers
path_rivp = path_interim / "river_p.nc"
path_rivp_drn = path_interim / "river_p_drn.nc"
path_rivs = path_interim / "river_s.nc"
path_rivs_drn = path_interim / "river_s_drn.nc"
path_rivt = path_interim / "river_t.nc"
path_drn_b = path_interim / "drainage_b.nc"
path_drn_mvgrep = path_interim / "drainage_mvgrep.nc"
path_drn_sof = path_interim / "drainage_sof.nc"
path_drn_boils = path_interim / "boils.nc"

startyear = 2000
#endyear = 2300
max_slr = 3.
slr_horizons = [0.5, 1., 2., 3.]
freq = "5AS"

path_ghb_out = path_interim / "ghb_kpslr.nc"
path_rivh_out = path_interim /  "river_h_kpslr.nc"
path_rivh_drn_out = path_interim / "river_h_drn_kpslr.nc"
"""
path_slrcurve = snakemake.input.path_slrcurve
path_layermodel = snakemake.input.path_layermodel

# sea and main waterways
path_ghb = Path(snakemake.input.path_ghb)
path_rivh = Path(snakemake.input.path_rivh)
path_rivh_drn = Path(snakemake.input.path_rivh_drn)
path_rivhslr = Path(snakemake.input.path_rivhslr) # ==> from prepare_kpslr
path_concslr = snakemake.input.path_concslr

# soil subsidence
path_subsidence = snakemake.input.path_subsidence
# # regional rivers
path_rivp = Path(snakemake.input.path_rivp)
path_rivp_drn = Path(snakemake.input.path_rivp_drn)
path_rivs = Path(snakemake.input.path_rivs)
path_rivs_drn = Path(snakemake.input.path_rivs_drn)
path_rivt = Path(snakemake.input.path_rivt)
path_drn_b = Path(snakemake.input.path_drn_b)
path_drn_mvgrep = Path(snakemake.input.path_drn_mvgrep)
path_drn_sof = Path(snakemake.input.path_drn_sof)
path_drn_boils = Path(snakemake.input.path_drn_boils)

path_interim = Path(snakemake.params.path_interim)
startyear = snakemake.params.startyear
#endyear = 2300
max_slr =snakemake.params.max_slr
slr_horizons = snakemake.params.slr_horizons
freq = snakemake.params.freq



#### read slr curve
slrcurve = pd.read_excel(path_slrcurve, "Sheet1", skiprows=19, header=0)#, index_col=0, parse_dates=True, date_parser=parser.parse)
slrcurve["time"] = slrcurve["jaar"].map(lambda x:cftime.datetime(x,1,1))
slrcurve = slrcurve.set_index("time", drop=True)
# shift to startyear
slrcurve = slrcurve.shift(startyear - slrcurve["jaar"].iloc[0]).dropna()
slrcurve = slrcurve.rename(columns={"ZSS":"SLR"})
# enforce precise horizons
horizon_times = []
for horizon in slr_horizons:
    diff = np.abs(slrcurve["SLR"] - horizon)
    #print(horizon, slrcurve.loc[diff.idxmin(),"SLR"])
    # replace closest value with horizon
    slrcurve.loc[diff.idxmin(),"SLR"] = horizon
    horizon_times.append(diff.idxmin())

# drop more than max SLR
#slrcurve = slrcurve.loc[slrcurve["SLR"] <= max_slr]
slrcurve = slrcurve["SLR"].to_xarray()

# reindex to freq (5-annual), starttime - endtime, interpolate values
drange = xr.cftime_range(slrcurve.time.values[0],slrcurve.time.values[-1],freq=freq)
# add specific SLR horizons
drange = drange.union(xr.CFTimeIndex(horizon_times))
slrcurve = slrcurve.reindex({"time":drange}).interpolate_na("time")
slrcurve = slrcurve.where(slrcurve <= max_slr, drop=True)
drange = slrcurve.time

# reindex slrconc
conc_slr = xr.open_dataarray(path_concslr, chunks={"SLR":1})
conc_slr = conc_slr.interp(SLR=slrcurve.values) # interpolate to SLR curve
conc_slr = conc_slr.assign_coords({"time":("SLR",drange)})
conc_slr = conc_slr.swap_dims({"SLR":"time"}).chunk({"time":1})
#print(conc_slr)
dens_slr = (conc_slr * 1.316) + 1000.

##### First: GHB
print("Assign SLR to GHB package")
ghb = xr.open_dataset(path_ghb)
# add time dimension and add SLR
ghb["head"] = ghb["head"].expand_dims({"time":drange}, 0)
ghb["concentration"] = ghb["concentration"].expand_dims({"time":drange}, 0)
ghb["density"] = ghb["density"].expand_dims({"time":drange}, 0)
#ghb["save_budget"] = ghb["save_budget"][0]
ghb["head"] = ghb["head"] + slrcurve
ghb["concentration"] = conc_slr.where(ghb["concentration"].notnull()).combine_first(ghb["concentration"])
ghb["density"] = dens_slr.where(ghb["density"].notnull()).combine_first(ghb["density"])
with ProgressBar(), imod.util.ignore_warnings():  # slow, but seems to work...
    ghb.chunk({"time":1}).to_zarr(path_interim / f"{path_ghb.stem}_kpslr.zarr")
del ghb
gc.collect()

##### Then River-h (and riv-drn)
print("Assign SLR to RIVH package")
rivh = xr.open_dataset(path_rivh, chunks={"layer":1})
is_rivh = rivh["stage"].max(dim="layer").notnull()
# Take SLR - level relation from prepare_kpslr.py
rivh_slr = xr.open_dataarray(path_rivhslr, chunks={"SLR":1}).where(is_rivh)
# reindex to slrcurve values (not time)
rivh_slr = rivh_slr.interp(SLR=slrcurve.values).bfill("SLR").ffill("SLR")
rivh_slr = rivh_slr.assign_coords(time=("SLR",drange)).swap_dims({"SLR":"time"})
#check:
# RMM, xy 300,745 -> ~0.99
# VZM, xy 312,873 -> 0
# WS, xy 200,1000 -> 0 -> 3
with imod.util.ignore_warnings():
    assert(np.allclose(rivh_slr.isel(x=300,y=745).load().values, 1., 0.01))
    assert((rivh_slr.isel(x=312,y=873).load().values == 0).all())
    assert((rivh_slr.isel(x=200,y=1000).load().values == 1.).all())

# from factor to absolute wl change. chunk for mem reasons
rivh_slr = (rivh_slr * slrcurve).chunk({"time":1})

# add wl change to stage and elevation of riv_h and riv_h_drn
rivh["stage"] = rivh["stage"].expand_dims({"time":drange})
rivh["concentration"] = rivh["concentration"].expand_dims({"time":drange})
rivh["density"] = rivh["density"].expand_dims({"time":drange})

rivh["stage"] = rivh["stage"] + rivh_slr
rivh["concentration"] = conc_slr.where(rivh["concentration"].notnull()).combine_first(rivh["concentration"])
rivh["density"] = dens_slr.where(rivh["density"].notnull()).combine_first(rivh["density"])
rivh = rivh.chunk({"layer":1,"time":10})
with ProgressBar(), imod.util.ignore_warnings():  # slow, but seems to work...
    #rivh.to_netcdf(path_rivh.parent / f"{path_rivh.stem}_kpslr.nc")
    rivh.to_zarr(path_interim / f"{path_rivh.stem}_kpslr.zarr")
del rivh, conc_slr, dens_slr
gc.collect()

rivh = xr.open_dataset(path_rivh_drn, chunks={"layer":1})
rivh["elevation"] = rivh["elevation"].expand_dims({"time":drange}, 0)
#rivh["save_budget"] = rivh["save_budget"][0]
rivh["elevation"] = rivh["elevation"] + rivh_slr
rivh = rivh.chunk({"layer":1, "time":10})
with ProgressBar(), imod.util.ignore_warnings():
    #rivh.to_netcdf(path_rivh_drn.parent / f"{path_rivh_drn.stem}_kpslr.nc")
    rivh.to_zarr(path_interim / f"{path_rivh_drn.stem}_kpslr.zarr")
del rivh, rivh_slr
gc.collect()

##### Soil subsidence
# subsidence has its own level indexation time line, stored in subsidence.nc
# assume subsidence to apply equally to regional rivers and drains.
# indexation of the surface level would be more gradual, but being pragmatic
# assume no effect on conductance
print("Assign subsidence to rivers and drains")
subsidence = xr.open_dataarray(path_subsidence, chunks={"time":1})
# first: regional rivers (P, S, T)
# handle river-drn with other drainage systems
for pth_riv in [path_rivp, path_rivs, path_rivt]:
    print(pth_riv.stem)
    riv = xr.open_dataset(pth_riv, chunks={"layer":1})
    riv["stage"] = riv["stage"].expand_dims({"time":subsidence.time}, 0)
    riv["bottom_elevation"] = riv["bottom_elevation"].expand_dims({"time":subsidence.time}, 0)
    riv["stage"] = riv["stage"] - subsidence # subsidence in positive number (m)
    riv["bottom_elevation"] = riv["bottom_elevation"] - subsidence
    with ProgressBar(), imod.util.ignore_warnings():
      riv.to_zarr(path_interim / f"{pth_riv.stem}_kpslr.zarr")

    del riv
    gc.collect()

# then: all drainage systems, including regional river drains (P, S)
for pth_drn in [path_rivp_drn, path_rivs_drn, path_drn_b, path_drn_mvgrep, path_drn_sof, path_drn_boils]:
    print(pth_drn.stem)
    drn = xr.open_dataset(pth_drn, chunks={"layer":1})
    drn["elevation"] = drn["elevation"].expand_dims({"time":subsidence.time}, 0)
    drn["elevation"] = drn["elevation"] - subsidence # subsidence in positive number (m)
    with ProgressBar(), imod.util.ignore_warnings():
      drn.to_zarr(path_interim / f"{pth_drn.stem}_kpslr.zarr")

    del drn
    gc.collect()

