# -*- coding: utf-8 -*-
"""
This script creates the recharge input file for the climate change scenario by following
the following steps:

1. Open the input files for recharge calculation and do calculation
2. Do area correction
3. Calulate rch per day
3. Check for high values if recharge > 1 mm/day (original max value is 2.3cm/day than adjust to 1 mm/day)
4. Implement average recharge for wadden islands

Input:
- data/1-external/rch_scenarios_input/S2085BP18/msw_Ebs/msw_Ebs_*.idf
- data/1-external/rch_scenarios_input/S2085BP18/msw_qinf/msw_qinf_*.idf
- data/1-external/rch_scenarios_input/S2085BP18/msw_Tact/msw_Tact_*.idf
- data/2-interim/water_masks.nc
- data/1-external/coastline/wadden_gebied.shp
- data/2-interim/template_2d.nc
- data/2-interim/bnd.nc
- data/1-external/metaswap_area/metaswap_area.idf
- data/1-external/landuse_S2085BP18/lgn250.asc

Output:
- data/2-interim/rch_2085.nc
"""
import geopandas as gpd
import imod
import numpy as np
import os
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

#path recharge for scenario 2085
path_msw_Ebs = r"data/1-external/rch_scenarios_input/S2085BP18/msw_Ebs/msw_Ebs_*.idf"
path_msw_qinf = r"data/1-external/rch_scenarios_input/S2085BP18/msw_qinf/msw_qinf_*.idf"
path_msw_Tact = r"data/1-external/rch_scenarios_input/S2085BP18/msw_Tact/msw_Tact_*.idf"
path_water = r"data/2-interim/water_masks.nc"
path_wadden = r"data/1-external/coastline/wadden_gebied.shp"
path_2d_template = r"data/2-interim/template_2d.nc"
path_bnd = r"data/2-interim/bnd.nc"
metaswap_area = r"data/1-external/metaswap_area/metaswap_area.idf"
landuse = r"data/1-external/landuse_S2085BP18/lgn250.asc"

#open template
like_2d = xr.open_dataset(path_2d_template)['template']
bnd_old_l1 = imod.idf.open("data/1-external/bnd/old/ibound_l1.idf").squeeze('layer',drop=True)
bnd_old_l1 = imod.prepare.Regridder(method="mean").regrid(bnd_old_l1, like_2d)
bnd_new_l1 = imod.idf.open("data/1-external/bnd/ibound_l1.idf").squeeze('layer',drop=True)
bnd_new_l1 = imod.prepare.Regridder(method="mean").regrid(bnd_new_l1, like_2d)
water_masks = xr.open_dataset(path_water)
sea_2d = water_masks['sea_2d']
water = water_masks['water_2d']
AHN_250 = imod.idf.open(r"data/1-external/TOPBOT/AHN_F250.IDF")
GHB_stage_LHM = imod.idf.open(r"data/1-external/ghb/ghb_stage_l01.idf").squeeze('layer', drop=True)
GHB_cond_LHM = (imod.idf.open(r"data/1-external/ghb/ghb_cond_l01.idf")).squeeze('layer', drop=True)

msw_Ebs = imod.idf.open(path_msw_Ebs)
msw_qinf = imod.idf.open(path_msw_qinf)
msw_Tact = imod.idf.open(path_msw_Tact)
landuse_idf = imod.rasterio.open(landuse).load()
metaswap_area_idf = imod.idf.open(metaswap_area)

cellsize = 250

# Recharge calculation
rch_m_m2 = msw_Ebs + msw_qinf + msw_Tact

# Recharge area correction
rch_m = rch_m_m2 * cellsize * cellsize / metaswap_area_idf

# Cummulative recharge
rch_sum = (rch_m.sum(dim='time')).squeeze("layer",drop=True)

# Calculation of days between time period
start = rch_m["time"].min().values
end = rch_m["time"].max().values
days = end - start
days = days.astype('timedelta64[D]')
days = days / np.timedelta64(1, 'D')

# Recharge per day
rch_m_day = rch_sum/days

# Regrid data
regridder = imod.prepare.Regridder(method="mean")
rch_m_day = regridder.regrid(rch_m_day, like_2d)
landuse_idf = regridder.regrid(landuse_idf, like_2d)

# Where the landuse code = 8 (greenhouse), no recharge
rch_m_d_masked=rch_m_day.where(landuse_idf!=8, 0)

#check for high values if recharge > 1 mm/day (original max value is 2.3cm/day than adjust to 1 mm/day)
recharge_meters = rch_m_d_masked.where(rch_m_d_masked <0.001, 0.001)

#check negative values and adjust to 0 mm/day
recharge_meters = recharge_meters.where(rch_m_d_masked > 0.0, 0.0)
rch_mean = recharge_meters.mean().values


wadden = gpd.read_file(path_wadden)
wadden["wadden"] = 1.0
wadden = imod.prepare.rasterize(wadden, like_2d, column="wadden") == 1.0

wadden = (water != 1) & (recharge_meters == 0.00000) & (wadden == 1) 
recharge_meters = recharge_meters.where(wadden == 0.0 , rch_mean)

wadden_sea = (GHB_cond_LHM == 62500.0 ) & (AHN_250 < 9999.0) &(bnd_new_l1 == 1)
bnd = xr.open_dataset(path_bnd)['ibound']

#get 2d first active domain
da = imod.select.layers.upper_active_layer(bnd)
active = bnd.layer == da
active_2d = active.max(dim = 'layer')
BE_DUI = (active_2d == 1) & (water != 1) &(recharge_meters == 0.00000) &(bnd_old_l1 == 0)
BE_DUI = BE_DUI.where(BE_DUI == 0.0, rch_mean)
recharge_meters = recharge_meters + BE_DUI
#recharge_meters = recharge_meters.where( rch_BE_DUI ==False , rch_mean)

# Create package and write to file
rch = imod.wq.RechargeHighestActive(rate=recharge_meters, concentration=0.05)
rch.dataset.to_netcdf("data/2-interim/recharge_2085.nc")