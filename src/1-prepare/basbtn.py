# -*- coding: utf-8 -*-
"""
This script creates the bas and btn package, by following
the following steps:
1. Read ibound from bnd.nc
2. Read top and bot from layermodel.nc
3. Fill bot with bottom of top
4. Read concentration

Input:
- data/2-interim/template.nc
- data/2-interim/layermodel.nc
- data/2-interim/bnd.nc
- data/2-interim/bnd.nc

Output:
- data/2-interim/bas.nc
- data/2-interim/btn.nc
"""
import os
import xarray as xr
import imod
import numba
import numpy as np
# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))


@numba.njit
def _fill_by_layer(layer_id, values, out):
    nlay, nrow, ncol = layer_id.shape
    for k in range(nlay):
        for i in range(nrow):
            for j in range(ncol):
                n = layer_id[k, i, j]
                if np.isnan(n):
                    continue
                elif n%2:
                    v = values[int(n // 2), i, j]
                    out[k, i, j] = v
                else:
                    out[k, i, j] = np.nan

def fill_by_layer(layer_id, values):
    out = xr.full_like(layer_id, np.nan)
    _fill_by_layer(layer_id.values, values.values, out.values)
    return out


# open paths
path_3d_template = snakemake.input.path_template   #"data/2-interim/template.nc"
#path_layermodel = "data/2-interim/layermodel_corr.nc"
path_layermodel = snakemake.input.path_layermodel   #"data/2-interim/layermodel.nc"
path_bnd = snakemake.input.path_bnd   #"data/2-interim/bnd.nc"
path_sconc = snakemake.input.path_conc_corr   #"data/2-interim/concentration_corrected.nc"
path_ibound_lhm = snakemake.input.path_bnd_lhm   # "data/1-external/bnd/ibound_l*.IDF"
path_sh_lhm = snakemake.input.path_shd_lhm   # "data/1-external/shd/HEAD_STEADY-STATE_L*.IDF"
path_bnd_out = snakemake.output.path_bnd_out
path_bas_out = snakemake.output.path_bas_out
path_btn_out = snakemake.output.path_btn_out

# ibound
like = xr.open_dataset(path_3d_template)["template"]
bnd = xr.open_dataset(path_bnd)["ibound"]
# Layermodel
layermodel = xr.open_dataset(path_layermodel)


top = layermodel["top"]
tmp_top = top.sel(layer=slice(2, None))
tmp_top["layer"] = tmp_top["layer"] - 1
bot = layermodel["bot"].combine_first(tmp_top)

sconcnc = xr.open_dataset(path_sconc)["sconc"]

# Limburg area bnd --> -1 
ibound_lhm = imod.idf.open(path_ibound_lhm)
sh_lhm = xr.open_dataarray(path_sh_lhm)#.squeeze('time', drop=True)


fixed_heads = ibound_lhm.where(ibound_lhm == -1)
fixed_heads_2d = fixed_heads.min(dim="layer")
layerid=layermodel['lhmfresh_layer']
fill_fixed_bnd = fill_by_layer(layerid, fixed_heads).fillna(0)

bnd = fill_fixed_bnd.where(~fixed_heads_2d.isnull(), bnd)

sh_lhm = xr.open_dataarray(path_sh_lhm)#.squeeze('time', drop=True)
sh_lhm = sh_lhm.where(sh_lhm > -9999)
sh_lhm = imod.prepare.fill(sh_lhm)
shd = fill_by_layer(layerid, sh_lhm)
shd = imod.prepare.fill(shd)
shd = shd.where(bnd != 0)

remove_cells = xr.full_like(bnd, 1.0)
remove_cells = remove_cells.where((bnd == -1.0) & (shd == 1.0), np.nan)
bnd =  bnd.where(remove_cells != 1.0, 0.0)
shd = shd.where(remove_cells != 1.0, np.nan)
bnd.name = "ibound"
bnd.to_netcdf(path_bnd_out)


assert shd.isnull().equals(bnd == 0)
# Create bas package and write output
bas = imod.wq.BasicFlow(
    ibound=bnd,
    top=top.sel(layer=1),
    bottom=bot,
    starting_head=shd,
    inactive_head=-9999.0,
)
bas.dataset.to_netcdf(path_bas_out)

# Create btn package and write output
btn = imod.wq.BasicTransport(
    icbund=bnd,
    starting_concentration=sconcnc,
    porosity=0.3,
    inactive_concentration=-9999.0,
)
btn.dataset.to_netcdf(path_btn_out)