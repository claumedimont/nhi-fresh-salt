# -*- coding: utf-8 -*-
"""
This script creates nessesary changes to speed up the model,
and to change minor mistakes in the input, 
by following the following steps:
1. Read the conductivity package ( most changes in L10)
2. remove drain cells with very low bottom ( < NAP -7.0 m)
3. remove some cells with high conductance but different stages in (multiple) packages



Input:
- data/1-external/adaptations/K_VERTICAL_L10_CHANGES.IDF
- data/2-interim/conductivity.nc
- data/2-interim/drn

Output:
- data/2-interim/bnd.nc

"""

import scipy.ndimage
import imod
import numpy as np
import os
import xarray as xr
import sys
import gc
from pathlib import Path

sys.path.append(os.path.dirname(__file__))  # add script directory to python path
from utils import assign_layer_3d, inner_join_dataarrays, intra_cell_boundary_conditions

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
# open paths
path_conductivity = snakemake.input.path_conductivity#"data/2-interim/lpf.nc"
path_kv_changes = snakemake.input.path_kv_changes  #"data/1-external/adaptations/lpf/K_VERTICAL_L10_CHANGE.IDF"
path_drn_sof = snakemake.input.path_drn_sof  #"data/2-interim/drainage_sof.nc"
path_drn_mvgrp = snakemake.input.path_drn_mvgrp  # "data/2-interim/drainage_mvgrep.nc"
path_drn_b = snakemake.input.path_drn_b  # "data/2-interim/drainage_b.nc"
path_drn_rivp = snakemake.input.path_drn_rivp  # "data/2-interim/river_p_drn.nc"
path_drn_rivs = snakemake.input.path_drn_rivs  # "data/2-interim/river_s_drn.nc"
path_riv_p = snakemake.input.path_riv_p  #"data/2-interim/river_p.nc"
path_rch = snakemake.input.path_rch  #"data/2-interim/recharge.nc"
path_rch_change = snakemake.input.path_rch_change  #"data/1-external/adaptations/rch/RATE_CHANGE.IDF"
path_drn_sof_cond_l3 = snakemake.input.path_drn_sof_cond_l3  #"data/1-external/adaptations/drn_sof/CONDUCTANCE_L3_CHANGE.IDF"
path_drn_sof_cond_l6 = snakemake.input.path_drn_sof_cond_l6  # "data/1-external/adaptations/drn_sof/CONDUCTANCE_L6_CHANGE.IDF"
path_drn_sof_cond_l9  = snakemake.input.path_drn_sof_cond_l9  #"data/1-external/adaptations/drn_sof/CONDUCTANCE_L9_CHANGE.IDF"
path_drn_mvgrp_cond  = snakemake.input.path_drn_mvgrp_cond  #"data/1-external/adaptations/drn_mvgrp/CONDUCTANCE_L9_CHANGE.IDF"
path_drn_mvgrp_elev = snakemake.input.path_drn_mvgrp_elev  #"data/1-external/adaptations/drn_mvgrp/ELEVATION_L9_CHANGE.IDF"
path_riv_drnp_cond_l3 = snakemake.input.path_riv_drnp_cond_l3  # "data/1-external/adaptations/riv_p_drn/CONDUCTANCE_L3_CHANGE.IDF"
path_riv_drnp_cond_l9 = snakemake.input.path_riv_drnp_cond_l9  #"data/1-external/adaptations/riv_p_drn/CONDUCTANCE_L9_CHANGE.IDF"
path_riv_drns_cond_l9 = snakemake.input.path_riv_drns_cond_l9  #"data/1-external/adaptations/riv_s_drn/CONDUCTANCE_L9_CHANGE.IDF"
path_riv_p_cond_l4 = snakemake.input.path_riv_p_cond_l4  # "data/1-external/adaptations/riv_p/CONDUCTANCE_L4_CHANGE.IDF"
path_riv_p_cond_l9 = snakemake.input.path_riv_p_cond_l9  # "data/1-external/adaptations/riv_p/CONDUCTANCE_L9_CHANGE.IDF"

# output
path_data = Path(snakemake.params.path_data)
path_lpf_adapt = snakemake.output.path_lpf_adapt
path_rch_adapt = snakemake.output.path_rch_adapt
path_drnsof_adapt = snakemake.output.path_drnsof_adapt
path_drnmvgrep_adapt = snakemake.output.path_drnmvgrep_adapt
path_rivpdrn_adapt = snakemake.output.path_rivpdrn_adapt
path_rivsdrn_adapt = snakemake.output.path_rivsdrn_adapt
path_rivp_adapt = snakemake.output.path_rivp_adapt

path_data.mkdir(exist_ok=True, parents=True)

# the majority of adaptations is made in the vertical hydraulic conductivity
#  of the top aquitard ( L10). The changes are minor
# (kv from 3 m/d --> 1 m/d or from 1 m/d --> 0.5 or 0.1 m/d)



kv = xr.open_dataset(path_conductivity)["k_vertical"]
kh = xr.open_dataset(path_conductivity)["k_horizontal"]
kv_changes = imod.idf.open(path_kv_changes)
kv_L10_new = kv.sel(layer=10) - kv_changes.fillna(0.)
kv.loc[{"layer": 10}] = kv_L10_new

conductivity = inner_join_dataarrays(k_horizontal=kh, k_vertical=kv)
lpf = xr.open_dataset(path_conductivity)
lpf['k_horizontal'] = conductivity['k_horizontal']
lpf['k_vertical'] = conductivity['k_vertical']
lpf.to_netcdf(path_lpf_adapt)

recharge = xr.open_dataset(path_rch)
rch_change = imod.idf.open(path_rch_change)
rch = recharge['rate'] 
rch1 = rch - rch_change.fillna(0.)
recharge['rate'] = rch1
recharge.to_netcdf(path_rch_adapt)


drn_sof = xr.open_dataset(path_drn_sof)
drn_sof_cond_l3 = imod.idf.open(path_drn_sof_cond_l3)
drn_sof_cond_l6 = imod.idf.open(path_drn_sof_cond_l6)
drn_sof_cond_l9 = imod.idf.open(path_drn_sof_cond_l9)

drn_sof_cond = drn_sof['conductance']
drn_sof_cond_l3_new = drn_sof_cond.sel(layer=3) - drn_sof_cond_l3.fillna(0.)
drn_sof_cond.loc[{"layer": 3}] = drn_sof_cond_l3_new
drn_sof_cond_l6_new = drn_sof_cond.sel(layer=6) - drn_sof_cond_l6.fillna(0.)
drn_sof_cond.loc[{"layer": 6}] = drn_sof_cond_l6_new
drn_sof_cond_l9_new = drn_sof_cond.sel(layer=9) - drn_sof_cond_l9.fillna(0.)
drn_sof_cond.loc[{"layer": 9}] = drn_sof_cond_l9_new

drn_sof['conductance'] = drn_sof_cond
drn_sof.to_netcdf(path_drnsof_adapt)



drn_mvgrp = xr.open_dataset(path_drn_mvgrp)
drn_mvgrp_cond_l9 = imod.idf.open(path_drn_mvgrp_cond)
drn_mvgrp_elev_l9 = imod.idf.open(path_drn_mvgrp_elev)
drn_mvgrp_cond = drn_mvgrp['conductance']
drn_mvgrp_cond_l9_new = drn_mvgrp_cond.sel(layer=9) - drn_mvgrp_cond_l9.fillna(0.)
drn_mvgrp_cond.loc[{"layer": 9}] = drn_mvgrp_cond_l9_new
drn_mvgrp['conductance'] = drn_mvgrp_cond

drn_mvgrp_elev = drn_mvgrp['elevation']
drn_mvgrp_elev_l9_new = drn_mvgrp_elev.sel(layer=9) - drn_mvgrp_elev_l9.fillna(0.)
drn_mvgrp_elev.loc[{"layer": 9}] = drn_mvgrp_elev_l9_new
drn_mvgrp['elevation'] = drn_mvgrp_elev
drn_mvgrp.to_netcdf(path_drnmvgrep_adapt)


riv_p_drn = xr.open_dataset(path_drn_rivp)
riv_p_drn_cond_l3 = imod.idf.open(path_riv_drnp_cond_l3)
riv_p_drn_cond_l9 = imod.idf.open(path_riv_drnp_cond_l9)
riv_p_drn_cond = riv_p_drn['conductance']
riv_p_drn_cond_l3_new = riv_p_drn_cond.sel(layer=3) - riv_p_drn_cond_l3.fillna(0.)
riv_p_drn_cond.loc[{"layer": 3}] =riv_p_drn_cond_l3_new
riv_p_drn_cond_l9_new = riv_p_drn_cond.sel(layer=9) - riv_p_drn_cond_l9.fillna(0.)
riv_p_drn_cond.loc[{"layer": 9}] = riv_p_drn_cond_l9_new
riv_p_drn['conductance'] = riv_p_drn_cond
riv_p_drn.to_netcdf(path_rivpdrn_adapt)


riv_s_drn = xr.open_dataset(path_drn_rivs)
riv_s_drn_cond_l9 = imod.idf.open(path_riv_drns_cond_l9)
riv_s_drn_cond = riv_s_drn['conductance']
riv_s_drn_cond_l9_new = riv_s_drn_cond.sel(layer=9) - riv_s_drn_cond_l9.fillna(0.)
riv_s_drn_cond.loc[{"layer": 9}] = riv_s_drn_cond_l9_new
riv_s_drn['conductance'] = riv_s_drn_cond
riv_s_drn.to_netcdf(path_rivsdrn_adapt)

riv_p = xr.open_dataset(path_riv_p)
riv_p_cond_l4 = imod.idf.open(path_riv_p_cond_l4)
riv_p_cond_l9 = imod.idf.open(path_riv_p_cond_l9)
riv_p_cond = riv_p['conductance']
riv_p_cond_l4_new = riv_p_cond.sel(layer=4) - riv_p_cond_l4.fillna(0.)
riv_p_cond.loc[{"layer": 4}] =riv_p_cond_l4_new
riv_p_cond_l9_new = riv_p_cond.sel(layer=9) - riv_p_cond_l9.fillna(0.)
riv_p_cond.loc[{"layer": 9}] = riv_p_cond_l9_new
riv_p['conductance'] = riv_p_cond
riv_p.to_netcdf(path_rivp_adapt)
