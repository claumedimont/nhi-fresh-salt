# -*- coding: utf-8 -*-
"""
Creates the drainage input file, by following
the following steps:
1. create separate drainage system for each of LHM's drainage systems
2. add surface drainage system for areas not included in LHM  TODO: still necessary for LHM 4.0?

Input:
- data/1-external/drn/*
- 

Output:
- data/2-interim/drain.nc
- data/2-interim/drainage_b.nc
- data/2-interim/drainage_mvgrep.nc
- data/2-interim/drainage_sof.nc
- data/2-interim/drainage_wadden.nc
"""
import os
import xarray as xr
import imod
import sys

sys.path.append(os.path.dirname(__file__))  # add script directory to python path
from utils import assign_layer_3d, inner_join_dataarrays

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

"""
path_2d_template = "data/2-interim/template_2d.nc"
path_top_bot = "data/2-interim/layermodel.nc"
path_bnd = "data/2-interim/bnd.nc"
path_ahn = "data/1-external/AHN/AHN_F250.IDF"
path_water = "data/2-interim/water_masks.nc"
path_ghb = "data/2-interim/ghb.nc"
path_drn_b = "data/1-external/drn/drn_b_corr.nc"
path_drn_mvgrep = "data/1-external/drn/drn_mvgrep_corr.nc"
path_drn_sof = "data/1-external/drn/drn_sof_corr.nc"
path_script = "src/1-prepare/drainage.py"
"""
# Paths
path_2d_template = snakemake.input.path_template_2d #"data/2-interim/template_2d.nc"
path_top_bot = snakemake.input.path_layermodel  #"data/2-interim/layermodel.nc"
path_bnd = snakemake.input.path_bnd  #"data/2-interim/bnd.nc"
path_ahn = snakemake.input.path_ahn  #"data/1-external/AHN/AHN_F250.IDF"
path_water = snakemake.input.path_water  #"data/2-interim/water_masks.nc"
path_ghb =   snakemake.input.path_ghb  #"data/2-interim/ghb.nc"
path_drn_b = snakemake.input.path_drn_b  #"data/1-external/drn/drn_b_corr.nc"
path_drn_mvgrep = snakemake.input.path_drn_mvgrep  #"data/1-external/drn/drn_mvgrep_corr.nc"
path_drn_sof = snakemake.input.path_drn_sof  #"data/1-external/drn/drn_sof_corr.nc"
path_drn_b_out = snakemake.output.path_drn_b_out
path_drn_mvgrep_out = snakemake.output.path_drn_mvgrep_out
path_drn_sof_out = snakemake.output.path_drn_sof_out

drn_b = xr.open_dataset(path_drn_b)
drn_mvgrep = xr.open_dataset(path_drn_mvgrep)
drn_sof = xr.open_dataset(path_drn_sof)
# open water masks
water_masks = xr.open_dataset(path_water)
sea_2d = water_masks["sea_2d"]
water = water_masks["water_2d"]

drainage_input = {
    "s1": {
        "bot": drn_b["elevation"],
        "cond":  drn_b["conductance"],
        "method": "z_then_upper",
        "within_layers": [1, 9],
        "output": path_drn_b_out,
    },
    "s2": {
        "bot": drn_mvgrep["elevation"],
        "cond": drn_mvgrep["conductance"],
        "method": "z_then_upper",
        "within_layers": [1, 9],
        "output": path_drn_mvgrep_out,
    },
    "s3": {
        "bot": drn_sof["elevation"],
        "cond": drn_sof["conductance"],
        "method": "upper_only",
        "within_layers": [None, None],
        "output": path_drn_sof_out,
    },
}


def create_drainage(
    bot_2d,
    cond_2d,
    like_2d=None,
    ibound=None,
    top_bot=None,
    method="z_then_upper",
    within_layers=[None, None],
):
    """
    Function to create consistent drainage xr.Dataset as input for
    imod.wq.Drainage()
    """
    # conform to like_2d
    if like_2d is not None:
        bot_2d = imod.prepare.reproject(bot_2d, like_2d, method="nearest")
        cond_2d = imod.prepare.reproject(cond_2d, like_2d, method="nearest")

    # inner join bot and cond and make sure both exist
    drain_2d = inner_join_dataarrays(bot=bot_2d, cond=cond_2d)

    # assign to 3d layers using method
    assigned = assign_layer_3d(drain_2d["bot"], ibound, top_bot, method, *within_layers)
    drain_3d = drain_2d.where(assigned)

    # reselect only if in ibound
    drain_3d = drain_3d.where(ibound)
    #drain_3d = drain_3d.where(ghb.isnull())

    # assert all have equal no of nodata cells
    dvars = list(drain_3d.data_vars.keys())
    for k1, k2 in zip(dvars[:-1], dvars[1:]):
        assert drain_3d[k1].isnull().equals(drain_3d[k2].isnull())

    return drain_3d.dropna(dim="layer", how="all")


# open like template
like_2d = xr.open_dataset(path_2d_template)["template"]

# Layermodel
top_bot = xr.open_dataset(path_top_bot)
bnd = xr.open_dataset(path_bnd)["ibound"]
ghb = xr.open_dataset(path_ghb)["conductance"].max(dim="layer")
# create drainage package for each of LHM's drainage systems
for s in drainage_input.keys():
    bot_2d = (drainage_input[s])["bot"].where(ghb.isnull())
    cond_2d =(drainage_input[s])["cond"].where(ghb.isnull())
    ds = create_drainage(
        bot_2d, cond_2d, like_2d, bnd, top_bot, drainage_input[s]["method"], drainage_input[s]["within_layers"]
    )
    #ds["bot"] = ds["bot"].where(xr.ufuncs.isnan(ghb))
    #ds["cond"] = ds["cond"].where(xr.ufuncs.isnan(ghb))
    # create package and write
    drn = imod.wq.Drainage(elevation=ds["bot"], conductance=ds["cond"])
    drn.dataset.to_netcdf(drainage_input[s]["output"])