"""
This script creates input for the KP SeaLevelRise scenario
- For both LHM-fs and LHM? Yes! (but first LHM-fs)

The scenario:
- Sea level rise in the Sea (GHB) and open connected water bodies:
  - Rhine-Meuse delta, including Haringvliet. SLR according to calculations of De Jong for median river discharge, linearly extrapolated (RIV-H)
  - Ooster- and Westerschelde, level with SLR (RIV-H)
- Transient SLR curve as supplied by RWS. Up until maximum SLR of 3m (~2300 AD)
--> check for flooding? No, assume policy as is

- Soil subsidence:
  - to be decided. 
  - Regional water levels go down with subsidence, as well as the surface elevation. --> Ook kD? check met Joachim.
Uitgangspunten implementatie bodemdaling in Deltascenario's:
"De bodemdaling is in het model geïmplementeerd vertaald naar de peilen van het oppervlaktewater en het maaiveld.
 De oppervlaktewaterpeilen van het regionaal systeem zijn aangepast waarbij is aangenomen dat de ontwateringsdiepte 
 gelijk blijft. Er is aangenomen dat de peilen van het hoofdwatersysteem niet beïnvloed worden door de bodemdaling. 
 Hierom zijn deze peilen niet aangepast als gevolg van de bodemdaling."
 Verder: in Pleistoceen NL bodemdaling (beekdalen) gereduceerd tot max 1 cm/j; voor natte natuur (Landgebruik 13) 
 dalingssnelheid met 50% gereduceerd"
  - ==> is maaiveld echt belangrijk om mee te laten dalen? Of alleen voor ghg plaatjes? Wel richting LHM

uitkomsten overleg Henk, Gilles, 22/6/2021

- 2020 is start (bodemdaling = 0)
- na 2100 geen peilindexatie meer, in lijn met internationale verdragen (Parijs, ... al worst-case want veel te laat)
- eerst na vijf, dan elke tien jaar peilindexatie, dus 2025, 2035, 2045, 2055, etc
- interpoleer lineair tussen 2020, 2050 en 2100 langs tijdsas
- daarvoor en daarna: bodemdaling = 0
- dus:
jaar:        2000..2010..2020..2030..2040..2050..2060..2070..2080..2090..2100....2277
bodemdaling:    0     0     0--------------2050--------------------------2100----2100
peilindexatie:                *     *     *     *     *     *     *     *

- Recharge?
  - remains at present amount

- Changes in salinity of surface water?
  - RMM no? <> Ymkje
  - Ooster/Westerschelde: no


Input:

Output:

"""
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr
from scipy.ndimage import binary_dilation
from pathlib import Path
import imod


slr = [1., 2., 3.]
subsyears = [2020,2050,2100]
"""
path_data = Path("data")#Path("d:/nhi-fresh-salt/data")
path_ghb = "d:/nhi-fresh-salt/data/2-interim/ghb.nc"
path_openclose = "data/1-external/kpslr/open_close.shp"
path_rivpoints = "data/1-external/kpslr/ObservationPointsCorr.shp"
paths_rivpointsslr = {k:f"data/1-external/kpslr/Water level mean increase at {int(k)} m SLR.csv" for k in slr}
paths_rmm_csv = path_data / "1-external/kpslr/salinity_RMM/saliniteit_bodemlaag_*.xyz"
paths_subsidence = [f"d:/nhi-fresh-salt/data/1-external/subsidence/bodemdaling_incl_ogzw_2020_{k}.tif" for k in subsyears[1:]]
path_outrivh = "data/2-interim/rivh_kpslr.nc"
path_outconc = "data/2-interim/conc_rmm_kpslr.nc"
path_outsubs = "data/2-interim/subsidence.nc"
startyear = 2000
"""
path_ghb = snakemake.input.path_ghb
path_openclose = snakemake.input.path_openclose
path_rivpoints = snakemake.input.path_rivpoints
paths_rivpointsslr = {k:v for k,v in zip(slr, snakemake.input.paths_rivpointsslr)}
paths_rmm_csv = snakemake.input.paths_rmm_csv
paths_subsidence = snakemake.input.paths_subsidence
path_outrivh = snakemake.output.path_outrivh
path_outconc = snakemake.output.path_outconc
path_outsubs = snakemake.output.path_outsubs
startyear = snakemake.params.startyear

# Sea level rise
ghb = xr.open_dataset(path_ghb)["head"]
ghb = ghb.sel(layer=4)
ghb = ghb.drop_vars("percentile")
shp_openclose = gpd.read_file(path_openclose)
openclose = imod.prepare.rasterize(shp_openclose, like=ghb, column="slr")
shp_rivpoints = gpd.read_file(path_rivpoints)
df_rivpointsslr = {k:pd.read_csv(v, index_col=0) for k,v in paths_rivpointsslr.items()}

# link rivpoints slr to rivpoints shp
for k in slr:
    if k == slr[0]:
        # create common index
        df = df_rivpointsslr[k].reset_index()
        df = df.rename(columns={"index":"node"})
        df["index"] = df.index
        idx = df.set_index("node", drop=False)["index"]

    df_rivpointsslr[k]["index"] = df_rivpointsslr[k].index.map(idx)
    df_rivpointsslr[k]["node"] = df_rivpointsslr[k].index
    df_rivpointsslr[k] = df_rivpointsslr[k].set_index("index", drop=False)

# use idx to assign values
shp_rivpoints["index"] = shp_rivpoints["Name"].map(idx)
# buffer first to not miss points
buffer = shp_rivpoints.buffer(125.)
shp_rivpoints.geometry = buffer

# assign per slr
dal = []
for k in slr:
    riv_slr = df_rivpointsslr[k]["1961"]
    shp_rivpoints["slr"] = shp_rivpoints["index"].map(riv_slr)
    shp_rivpoints["slr"] = shp_rivpoints["slr"].where(shp_rivpoints["slr"] > 0, 0)
    da = imod.prepare.rasterize(shp_rivpoints, like=ghb, column="slr")
    dal.append(da)
rivpoints = xr.concat(dal, dim=pd.Index(slr, name="SLR"))

# Merge: ghb, openclose and rivpoints for different SLRs
# always as factor, so divide by SLR
# combine ghb and rivpoints first and fill
# then force openclose
# first expand dims of ghb and openclose
rivh_slr = ghb.expand_dims(SLR=slr).copy() + 1.
openclose = openclose.expand_dims(SLR=slr).copy()
for k in slr:
    #rivh_slr.loc[{"SLR":k}] += k
    #openclose.loc[{"SLR":k}] *= k
    rivpoints.loc[{"SLR":k}] /= k
# combine ghb and rivpoints and fill (crude approach, but detail in waterlevel seems enough)
rivh_slr = rivh_slr.combine_first(rivpoints)
rivh_slr = imod.prepare.fill(rivh_slr)
#da = imod.prepare.laplace_interpolate(rivh_slr.sel(SLR=1.), ibound=openclose.sel(SLR=1.).isnull(), close=1e-4)
rivh_slr = openclose.combine_first(rivh_slr)
# add a zero slr --> no, backfill in scenario_kpslr, it is a factor, not absolute SLR
#slr0 = xr.zeros_like(rivh_slr.sel(SLR=1.)).assign_coords({"SLR":0})
#rivh_slr = xr.concat((slr0, rivh_slr), dim="SLR")
rivh_slr.to_netcdf(path_outrivh)

#check:
# RMM, xy 300,745 -> ~0.99
# VZM, xy 312,873 -> 0
# WS, xy 200,1000 -> 0 -> 3
assert(np.allclose(rivh_slr.isel(x=300,y=745).load().values, 1., 0.01))
assert((rivh_slr.isel(x=312,y=873).load().values == 0).all())
assert((rivh_slr.isel(x=200,y=1000).load().values == 1.).all())

##################################################################
## Concentration change in Rijn-Maasmonding
salinity_conversion = 1. / 1.80655  # (ignore density)

# First: read csv's in for timesteps and slrs, and combine to calculate average salinity
l_df_0m = []
l_df_4m = []
#for fn in paths_rmm_csv.parent.glob(paths_rmm_csv.name):
for fn in paths_rmm_csv:
    print(fn)
    fn = Path(fn)
    parts = fn.stem.split("_")
    slr = parts[2]
    df = pd.read_csv(fn, delim_whitespace=True, header=0)
    df = df.rename(columns={"x coordinate":"x", "y coordinate":"y", "z coordinate":"z","salinity":f"{fn.stem}"})
    df = df.set_index(["x","y"])[fn.stem]
    if slr == "plus0m":
        l_df_0m.append(df)
    else:
        l_df_4m.append(df)
df_0m = pd.concat(l_df_0m, axis=1)
df_0m["chloride"] = df_0m.mean(axis=1) * salinity_conversion
df_4m = pd.concat(l_df_4m, axis=1)
df_4m["chloride"] = df_4m.mean(axis=1) * salinity_conversion


#x 50_000, 110_000  # evt bijsnijden
#y: 460_000, 420_000
def _get_range(A, step=50, neg=False):
    min_, max_ = A.min(), A.max()
    if neg:
        return np.arange(max_ - max_ % step, min_ - min_ % step - step, -step)
    else:
        return np.arange(min_ - min_ % step, max_ - max_ % step + step, step)

x,y=df_0m.reset_index()[["x","y"]].values.T
rmm_like = xr.DataArray(data=1, dims=["y","x"], coords={"y":_get_range(y, step=50, neg=True), "x":_get_range(x, step=50)})# higher-res like for rasterization
regridder = imod.prepare.Regridder("mean")

conc_slr = []
for slr, df in zip([0, 4],[df_0m, df_4m]):
    df = df.reset_index()
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(x=df.x,y=df.y))
    da_rmm = imod.prepare.rasterize(gdf, rmm_like, "chloride")
    conc_slr.append(regridder.regrid(da_rmm, like=ghb))
conc_slr = xr.concat(conc_slr, dim=pd.Index([0., 4.], name="SLR"))

# fillna within buffer, and reselect to rivh
conc_slr_buf = conc_slr.isel(SLR=0).copy(data=binary_dilation(conc_slr.isel(SLR=0).notnull(), iterations=2))
conc_slr = imod.prepare.fill(conc_slr).where(conc_slr_buf)

# reselect to where SLR influence
conc_slr = conc_slr.where(openclose.isel(SLR=0)!=0)
# interpolate to entire SLR curve => in scenario_kpslr
conc_slr.to_netcdf(path_outconc)

##################################################################
## Soil subsidence - in m cumulative subsidence over given period
##################################################################
da_subs = [imod.rasterio.open(p) for p in paths_subsidence]
# regrid to LHM, prepend a subsidence=0 da, and concatenate to one da
regridder = imod.prepare.Regridder("mean")
da_subs = [regridder.regrid(da, like=ghb) for da in da_subs]
da_subs = 2*[xr.zeros_like(ghb)]+da_subs  # put in 2000 and 2020 - subsidence zero maps
drange_data = pd.DatetimeIndex([f"{y}0101" for y in [startyear]+subsyears], name="time")
da_subs = xr.concat(da_subs, dim=drange_data)
assert(np.allclose(da_subs.isel(x=500,y=500).values,[0., 0., 0.37857, 1.00264], 1e-4))

# interpolate to desired level indexation time axis: 2000 --- 2025 **10y** 2095
drange_interp = pd.date_range("20250101","20950101", freq="10AS")
da_interp = da_subs.interp(time=drange_interp)
assert(np.allclose(da_interp.isel(x=500,y=500).values,[0.06312, 0.18928, 0.31549, 0.44097, 0.56581, 0.69061, 0.81544,0.94024], 1e-4))
# include starttime as a coordinate
da_interp = xr.concat((da_subs.isel(time=0), da_interp), dim="time")
da_interp = da_interp.fillna(0)
da_interp.to_netcdf(path_outsubs)