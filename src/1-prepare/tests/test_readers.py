import pandas as pd
import numpy as np
import pytest
import datetime
from readers import read_VES, read_GEF, read_LAS

def test_read_VES_with_interpretation():
    attrs_test = {
        "VESid":"W41F0010",
        "coord":"RD",
        "x":250333,
        "y":442580,
        "z":43.2,
        "date":datetime.datetime(1965, 7, 2),
        "configuration":"SCHLUMB",
        "L/2":150,
        "quality_measurement":"GOED",
        "quality_interpretation":"REDELIJK",
        "maxdepth_interpretation":70,
        "has_interpretation":True
    }

    meas_data_test = pd.DataFrame(data=[[1.5,40.4],
                                        [2.5,53.6],
                                        [4.0,66.0]], columns=["L/2","R"])

    interp_data_test = pd.DataFrame(data=[[0.,1.,35.],
                                          [1.,None,94.]], columns=["boven","onder","Rs"])

    attrs,meas_data,interp_data = read_VES("tests/testdata/ves_met_interpretatie.txt")

    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert(meas_data.equals(meas_data_test))
    assert(interp_data.equals(interp_data_test))

    
def test_read_VES_without_interpretation():
    attrs,meas_data,interp_data = read_VES("tests/testdata/ves_zonder_interpretatie.txt")
    assert(interp_data is None)

def test_read_GEF_with_conductivity():
    attrs_test = {
        "CPTid":"CPT000000007350",
        "coord":"28992",
        "x":45802.58,
        "y":413607.88,
        "z":-2.04,
        "date":datetime.datetime(2011, 3, 4),
        "is_ecpt":True
    }

    meas_data_test = pd.DataFrame(data=[[0.000,0.000,0.000,0,0.009,-0.016, np.nan, 0.000],
                                        [0.020,0.245,0.000,0,0.013,-0.016,5.5, 0.020],
                                        [0.040,0.405,0.000,0,0.015,-0.017,3.8, 0.040]], 
                                    columns=['sondeertrajectlengte - m (meter)', 'conusweerstand - MPa (megaPascal)',
                                                'elektrische geleidbaarheid - S/m (Siemens/meter)',
                                                'hellingresultante - Â° (graden)',
                                                'plaatselijke wrijving - MPa (megaPascal)',
                                                'waterspanning u2 - MPa (megaPascal)',
                                                'wrijvingsgetal - % (procent; MPa/MPa)', 'diepte - m (meter)'])

    # return full measurement data
    attrs,columninfo,meas_data = read_GEF("tests/testdata/cpt_met_geleidbaarheid.txt")
    meas_data = meas_data.iloc[:3]  # test only first three lines
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert((meas_data.columns == meas_data_test.columns).all())
    assert(meas_data.round(decimals=5).equals(meas_data_test.round(decimals=5)))

    # return only conductivity column
    attrs,columninfo,meas_data = read_GEF("tests/testdata/cpt_met_geleidbaarheid.txt", only_ecpt=True, only_conductivity_column=True)
    meas_data = meas_data.iloc[:3]  # test only first three lines
    meas_data_test = meas_data_test.reindex(columns=['diepte - m (meter)', 'elektrische geleidbaarheid - S/m (Siemens/meter)'])
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert((meas_data.columns == meas_data_test.columns).all())
    assert(meas_data.round(decimals=5).equals(meas_data_test.round(decimals=5)))

def test_read_GEF_without_conductivity():
    attrs_test = {
        "CPTid":"CPT000000000901",
        "coord":"28992",
        "x":126588.000,
        "y":400046.000,
        "z":5.710,
        "date":datetime.datetime(1996, 5, 21),
        "is_ecpt":False
    }

    meas_data_test = pd.DataFrame(data=[[0.000,0.004,0.000,9.0,0,np.nan,np.nan],
                                        [0.020,0.120,0.020,52.0,0,np.nan,np.nan],
                                        [0.040,0.234,0.040,53.0,0,np.nan,np.nan]], 
                                    columns=['sondeertrajectlengte - m (meter)', 'conusweerstand - MPa (megaPascal)',
                                                'diepte - m (meter)', 'verlopen tijd - s (seconde)',
                                                'hellingresultante - Â° (graden)',
                                                'plaatselijke wrijving - MPa (megaPascal)',
                                                'wrijvingsgetal - % (procent; MPa/MPa)', ])

    # default behavior: do not return meas_data, as no ecpt
    attrs,columninfo,meas_data = read_GEF("tests/testdata/cpt_zonder_geleidbaarheid.txt")
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert(meas_data is None)


    attrs,columninfo,meas_data = read_GEF("tests/testdata/cpt_zonder_geleidbaarheid.txt", only_ecpt=False, only_conductivity_column=True)
    meas_data = meas_data.iloc[:3]  # test only first three lines
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert((meas_data.columns == meas_data_test.columns).all())
    assert(meas_data.round(decimals=5).equals(meas_data_test.round(decimals=5)))

def test_read_LAS_with_ln():
    attrs_test = {
        "LASid":"las_met_ln",
        "strt (m)": 0,
        "stop (m)": 72.05,
        "step (m)": 0.05,
        "null": -999999,
        "comp":"wlf",
        "well":"b02g0308",
        "fld": "schiermonnikoog",
        "date":datetime.datetime(1992, 9, 8),
        "has_LN":True,
        "LN_column": "ln160 (ohm-m)"
    }

    meas_data_test = pd.DataFrame(data=[[3.100, 40.110, np.nan, 419.800, np.nan, 8.650],
                                        [3.150, 40.680, .050, 421.600, 60.360, 8.870],
                                        [3.200, 41.080, .049, 424.000, 59.130, 9.080]], 
                                    columns=['depth (m)', 'sn40 (ohm.m)', 'sp (v)', 'cal (mm)', 'ln160 (ohm-m)', 'gamma (cps)'])

    # return full measurement data
    attrs,columninfo,meas_data = read_LAS("tests/testdata/las_met_ln.txt")
    meas_data = meas_data.iloc[:3]  # test only first three lines
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert((meas_data.columns == meas_data_test.columns).all())
    print(meas_data.T)
    print(meas_data_test.T)
    assert(meas_data.round(decimals=5).equals(meas_data_test.round(decimals=5)))

    # return only conductivity column
    attrs,columninfo,meas_data = read_LAS("tests/testdata/las_met_ln.txt", only_ln=True, only_ln_column=True)
    meas_data = meas_data.iloc[:3]  # test only first three lines
    meas_data_test = meas_data_test.reindex(columns=['depth (m)', 'ln160 (ohm-m)'])
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert((meas_data.columns == meas_data_test.columns).all())
    assert(meas_data.round(decimals=5).equals(meas_data_test.round(decimals=5)))

def test_read_LAS_without_ln():
    attrs_test = {
        "LASid":"las_zonder_ln",
        "strt (m)": 0,
        "stop (m)": 72.05,
        "step (m)": 0.05,
        "null": -999999,
        "comp":"wlf",
        "well":"b02g0308",
        "fld": "schiermonnikoog",
        "date":datetime.datetime(1992, 9, 8),
        "has_LN":False,
    }

    meas_data_test = pd.DataFrame(data=[[3.100, 40.110, np.nan, 419.800, np.nan, 8.650],
                                        [3.150, 40.680, .050, 421.600, 60.360, 8.870],
                                        [3.200, 41.080, .049, 424.000, 59.130, 9.080]], 
                                    columns=['depth (m)', 'sn40 (ohm.m)', 'sp (v)', 'cal (mm)', 'ln40 (ohm.m)', 'gamma (cps)'])

    # default option: only return measurement data if has LN
    attrs,columninfo,meas_data = read_LAS("tests/testdata/las_zonder_ln.txt")
    print(attrs)
    print(pd.Series(attrs_test))
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert(meas_data is None)

    # return full measurement data
    attrs,columninfo,meas_data = read_LAS("tests/testdata/las_zonder_ln.txt", only_ln=False)
    meas_data = meas_data.iloc[:3]  # test only first three lines
    assert(np.array([v == attrs[k] for k,v in attrs_test.items()]).all())
    assert((meas_data.columns == meas_data_test.columns).all())
    print(meas_data.T)
    print(meas_data_test.T)
    assert(meas_data.round(decimals=5).equals(meas_data_test.round(decimals=5)))
