"""
Creates the 2d concentration input files, by following
the following steps:
- Read 2d Cl kriging and delft3d interpolation
- Assign Cl in the following areas
    Outside NL: delft3d
    Inside: 2d Kriging  (mg/l !)
    Outside if no model result: 2d kriging

"""
import geopandas as gpd
import xarray as xr
import imod
import os

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__),"..",".."))

# Paths
path_cl_2d = r"data/1-external/2D-oppwater-chloride/v2/Surface_Chloride_no_negatives.tif" # in mg/l!
path_template_2d = r"data/2-interim/template_2d.nc"

# Open  Cl interpolation, template
cl_2d = xr.open_rasterio(path_cl_2d)
cl_2d = cl_2d.squeeze('band', drop=True)
like_2d = xr.open_dataset(path_template_2d)["template"]

# Regrid surface chloride
cl_2d = imod.prepare.Regridder(method="mean").regrid(cl_2d,like_2d)

cl_2d /= 1000 # to g/L

# write
cl_2d.to_netcdf("data/2-interim/chloride_2d.nc")
