"""
This script creates the ghb package, by following
the following steps:
1. read GHB location from layermodel
2. calculate density from chloride
3. assign conc, dens, cond, stage values
4. create 3d ghb

Input:
- data/2-interim/template_2d.nc
- data/1-external/lagenmodel/layermodel.nc
- data/2-interim/bnd.nc

Output:
- data/2-interim/ghb.nc
"""
import os
import imod
import numpy as np
import xarray as xr
import numba
import sys
import pandas as pd

sys.path.append(os.path.dirname(__file__))  # add script directory to python path
from utils import assign_layer_3d, inner_join_dataarrays

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

@numba.njit
def _fill_by_layer(layer_id, values, out):
    nlay, nrow, ncol = layer_id.shape
    for k in range(nlay):
        for i in range(nrow):
            for j in range(ncol):
                n = layer_id[k, i, j]
                if np.isnan(n):
                    continue
                elif n%2:
                    v = values[int(n // 2), i, j]
                    out[k, i, j] = v
                else:
                    out[k, i, j] = np.nan

def fill_by_layer(layer_id, values):
    out = xr.full_like(layer_id, np.nan)
    _fill_by_layer(layer_id.values, values.values, out.values)
    return out


# Paths
path_2d_template = "data/2-interim/template_2d.nc"
path_template = "data/2-interim/template.nc"
path_layermodel = "data/2-interim/layermodel.nc"
path_conc = "data/2-interim/starting_concentration.nc"
path_GHB_cond_LHM = "data/1-external/ghb/ghb_cond_l01.idf"
path_bnd_new_l1 = "data/1-external/bnd/ibound_l1.idf"
path_ahn = "data/1-external/TOPBOT/AHN_F250.IDF"
path_top_bot = "data/2-interim/top_bot.nc"
path_bnd = "data/2-interim/bnd.nc"
path_sh_lhm = "data/1-external/SH_LHM4/sh_L*.IDF"
path_ibound_lhm = "data/1-external/bnd/ibound_l*.IDF"

# open template
like_2d = xr.open_dataset(path_2d_template)["template"]
like = xr.open_dataset(path_template)["template"]
#open ahn
ahn = imod.idf.open(path_ahn)

#open LHM input
ghb_cond_LHM = imod.idf.open(path_GHB_cond_LHM).squeeze("layer", drop=True)
bnd_new_l1 = imod.idf.open(path_bnd_new_l1).squeeze("layer", drop=True)
bnd_new_l1 = imod.prepare.Regridder(method="mean").regrid(bnd_new_l1, like_2d)

# open top_bot, bnd, conc2d
layermodel = xr.open_dataset(path_layermodel)
top_bot = xr.open_dataset(path_top_bot)

bnd = xr.open_dataset(path_bnd)["ibound"]
conc_2d = xr.open_dataset(path_conc)["sconc_2d"]#.squeeze("band", drop=True)

# read GHB
ghb_stage_area = (
    layermodel["offshore_ghb"].where(layermodel["offshore_ghb"] == True) == 1
)
ghb_stage_area = ghb_stage_area.where(ghb_stage_area == 1, np.nan)

wadden_sea = (ghb_cond_LHM == 62500.0) & (ahn < 9999.0) & (bnd_new_l1 == 1)
stage_wadden = xr.full_like(like_2d, 1.0).where(wadden_sea)
ghb_stage_area = ghb_stage_area.combine_first(stage_wadden)

slr_2085 = 0.8

# assign conc, dens, cond, stage values
ghb_conc = xr.full_like(like_2d, conc_2d).where(ghb_stage_area ==1)
ghb_dens = xr.full_like(like_2d, 1000 + conc_2d * 1.316).where(
    ghb_stage_area ==1
)  # Density conversion
ghb_cond = xr.full_like(like_2d, 6250.0).where(ghb_stage_area == 1)
ghb_stage = xr.full_like(like_2d, 1.0).where(ghb_stage_area == 1)

starttime = "2000-01-01"
endtime = "2100-12-31"
frequency = "A"

time = pd.to_datetime([starttime, endtime])
time_multiplication = xr.DataArray(
        data=[0.0,slr_2085],
        coords={"time": time},
        dims=["time"])

# Add first steady-state
timedelta = np.timedelta64(1, "s")  # 1 second duration for initial steady-state
starttime_steady = np.datetime64(starttime) - timedelta
sea_steady = xr.DataArray(data=[0.0], coords={"time": [starttime_steady]}, dims=["time"])

# resample to year resolution
time_multiplication = time_multiplication.resample(time=frequency).interpolate()
time_multiplication = time_multiplication.combine_first(sea_steady)
ghb_stage = (ghb_stage/ghb_stage) * time_multiplication

assert ghb_cond.count() == ghb_conc.count() == ghb_dens.count()

# Save
ghb_sea = xr.Dataset()
ghb_sea["stage"] = ghb_stage
ghb_sea["conc"] = ghb_conc
ghb_sea["cond"] = ghb_cond
ghb_sea["dens"] = ghb_dens


ghb_sea = imod.wq.GeneralHeadBoundary(
    head=ghb_sea["stage"],
    conductance=ghb_sea["cond"],
    concentration=ghb_sea["conc"],
    density=ghb_sea["dens"],
)
ghb_sea.dataset.to_netcdf("data/2-interim/ghb_2085.nc")