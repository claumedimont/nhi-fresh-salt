select lc.locationid, lc.locationcode, lc.x, lc.y, lc.geometrypoint, pc.z, pc.p10, pc.p25, pc.p40, pc.p50, pc.p60, pc.p75, pc.p90
from (
	select ds10.locationid, ds10.z, ds10.value as p10, ds25.value as p25, ds40.value as p40, ds50.value as p50, ds60.value as p60, ds75.value as p75, ds90.value as p90
	from (
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p10'
		) ds10
	join
		(
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p25'
		) ds25 
		on ds25.locationid = ds10.locationid and ds25.z = ds10.z
	join
		(
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p40'
		) ds40 
		on ds40.locationid = ds10.locationid and ds40.z = ds10.z
	join
		(
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p50'
		) ds50 
		on ds50.locationid = ds10.locationid and ds50.z = ds10.z
	join
		(
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p60'
		) ds60 
		on ds60.locationid = ds10.locationid and ds60.z = ds10.z
	join
		(
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p75'
		) ds75 
		on ds75.locationid = ds10.locationid and ds75.z = ds10.z
	join
		(
			select locationid, z, value from
			dataseries ds
			join parameter pr on pr.parameterid = ds.parameterid
			where pr.parametercode = 'Cl_p90'
		) ds90 
		on ds90.locationid = ds10.locationid and ds90.z = ds10.z
) pc
join
	location lc on lc.locationid = pc.locationid
limit 100