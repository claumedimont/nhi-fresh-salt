select lc.locationid, lc.locationcode, t.typecode, lc.geometrypoint, lc.x, lc.y, ds.z, lc.filter_top, lc.filter_bottom, ds.datetime, ds.value, p.parametercode, u.unitabbrev
	from dataseries ds 
		join location lc
			on ds.locationid = lc.locationid
		join type t 
			on lc.typeid = t.typeid
		join compartment c 
			on lc.compartmentid = c.compartmentid
		join parameter p
			on ds.parameterid = p.parameterid
		join unit u
			on ds.unitid = u.unitid
	where t.typecode = 'ANA' and p.parametercode = 'Cl' and c.compartmentcode = 'GW'
	limit 100