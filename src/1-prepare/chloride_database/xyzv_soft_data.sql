select t.typecode, lc.geometrypoint, lc.x, lc.y, ds.z, ds.datetime, ds.value, p.parametercode as parameter, u.unitabbrev as unit
	from dataseries ds 
		join location lc
			on ds.locationid = lc.locationid
		join type t 
			on lc.typeid = t.typeid
		join compartment c 
			on lc.compartmentid = c.compartmentid
		join parameter p
			on ds.parameterid = p.parameterid
		join unit u
			on ds.unitid = u.unitid
	where t.typecode in ('SOFT_SEA','SOFT_DEEP','SOFT_OTHER') and p.parametercode = 'Cl' and c.compartmentcode = 'GW'