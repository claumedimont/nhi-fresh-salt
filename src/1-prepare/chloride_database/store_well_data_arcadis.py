#` -*- coding: utf-8 -*-
# Copyright notice
#   --------------------------------------------------------------------
#   Copyright (C) 2019 Deltares
#       Gerrit Hendriksen
#       gerrit.hendriksen@deltares.nl
#
#   This library is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this library.  If not, see <http://www.gnu.org/licenses/>.
#   --------------------------------------------------------------------
#
# This tool is part of <a href="http://www.OpenEarth.eu">OpenEarthTools</a>.
# OpenEarthTools is an online collaboration to share and manage data and
# programming tools in an open source, version controlled environment.
# Sign up to recieve regular updates of this function, and to contribute
# your own tools.

"""packages"""
import os
import configparser
from pathlib import Path
import pandas as pd
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine,MetaData, func

# local
from orm_zoetzout import Base, Location, Dataseries, Compartment, Unit, Parameter, Type

"""some functions"""
def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser() 
    cf.read(af)
    return cf

# setup connection to database
local = False
if local:
    cf = read_config(r'credentials_template.txt')
else:
    cf = read_config(r'credentials_zoetzout_remote.cfg')
connstr = 'postgres://'+cf.get('Postgis','user')+':'+cf.get('Postgis','pwd')+'@'+cf.get('Postgis','host')+':5432/'+cf.get('Postgis','dbname')
engine = create_engine(connstr,echo=True)

meta=MetaData(engine)

# create session --> all initialisation work has been done in orm_initialize
Session = sessionmaker(bind = engine)
session = Session()


""" ----------------------------------------
    from here it gets dataset specific
    ----------------------------------------
"""
basepath = Path(r"p:\11203718-007-nhi-zoetzout\Chloride_database\chloride_data_discard_bad_data2")

#data_paths = {"well_comp": {"data":"well_comp.csv",
#                "id_col":"NITG-nr",
#                "value_col":"Cl- (mg/l)"},
#        }
#datatypes = {"well_comp":"BL"}

#attrs = pd.read_csv(basepath / paths["attr"], index_col=0)
#data = pd.read_csv(basepath / data_paths["data"])
#id_ = paths["id_col"]
#valuecol = paths["value_col"]

data = pd.read_csv(basepath / "GW_Results_Cl_v2.csv")
id_ = "ID"
valuecol = "Cl (g/l)"

# get relationship ids
type_ = session.query(Type).filter_by(typecode="ANA").first()
compartment_ = session.query(Compartment).filter_by(compartmentcode="GW").first()
parameter_ = session.query(Parameter).filter_by(parametercode="Cl").first()
unit_ = session.query(Unit).filter_by(unitabbrev="g/L").first()

data.dropna(subset=['FilTop'], inplace=True)
#data = data.reset_index(drop=True)

data['Cl (g/l)'] = data['value']/1000

data=data.drop('value', axis=1)

data.loc[:,"Confidential"] = False
data.loc[data['Owner'] == 'Waterleidingbedrijf Noord-Holland PWN', 'Confidential'] = True
#data.loc[data['Owner'] == 'Waterleidingmaatschappij Limburg (WML)', 'Confidential'] = True  # no data from WML in db (excluded zoutwachters and EC data for now)

# to prevent additional locations due to rounding errors on filter depth
data.loc[:,"FilTop"] = data.loc[:,"FilTop"].round(2)
data.loc[:,"FilBot"] = data.loc[:,"FilBot"].round(2)
data_sel = data.groupby([id_, "x", "y", "FilTop", "FilBot"])

for (name, x, y, filttop, filtbot), group in data_sel:
    print(name,x,y,filttop,filtbot)
    #i+=1
    #if i==10:
    #    break
    first = group.iloc[0]
    a = Location(
        locationcode=name,
        typeid=type_.typeid,
        owner=first["Owner"],
        confidential=first["Confidential"],
        compartmentid=compartment_.compartmentid,
        x=float(x),
        y=float(y),
        filter_top=filttop,
        filter_bottom=filtbot,
        mv = first["mv"],
        file_origin = first["File.loc"],
    )
    session.add(a)
    session.flush()
    # get the id of the last inserted data
    locationid = a.locationid
    session.commit()
    
    # define temporary dataframe
    dft = group.copy()
    dft["z"] = 0.5*a.filter_top+0.5*a.filter_bottom
    # check if indeed below surface level, then subtract from surface level
    b = (dft["z"] > 0) & (dft["z"] > a.mv)
    dft.loc[b, "z"] = a.mv - dft.loc[b, "z"]
    dft = dft.rename(columns={"date":"datetime",valuecol:"value"})
    dft = dft.reindex(columns=["z","datetime","value"])
    dft['locationid']  = locationid
    dft['parameterid'] = parameter_.parameterid
    dft['unitid']      = unit_.unitid
    dft = dft.dropna(subset=["value"]).reset_index(drop=True)
    # retrieve max id of dataid
    s=session.query(func.max(Dataseries.seriesid).label('id')).one().id
    if s is None:#str(s) == 'None':
        s = 0
    else:    
        dft.index += s+1
    dft.index.rename('seriesid',inplace=True)
    dft.to_sql('dataseries',con=engine,if_exists='append',schema="public",index=True)
    #break

# finally update the column geometriepunt
strsql = (
    """update public.location set geometrypoint = st_setsrid(st_point(x,y),28992)"""
)
engine.execute(strsql) 

session.close()            
