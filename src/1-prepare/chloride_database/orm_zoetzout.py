"""
Definition ORM model for zoetzout data from various sources.
"""

#  Copyright notice
#   --------------------------------------------------------------------
#   Copyright (C) 2019 Deltares for LHM Zoetzout
#       Gerrit Hendriksen@deltares.nl
#
#   This library is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this library.  If not, see <http://www.gnu.org/licenses/>.
#   --------------------------------------------------------------------
#
# This tool is part of <a href="http://www.OpenEarth.eu">OpenEarthTools</a>.
# OpenEarthTools is an online collaboration to share and manage data and
# programming tools in an open source, version controlled environment.
# Sign up to recieve regular updates of this function, and to contribute
# your own tools.

# $Id: orm_hydrodb.py 945 2016-09-08 14:35:49Z hendrik_gt $
# $Date: 2016-09-08 16:35:49 +0200 (Thu, 08 Sep 2016) $
# $Author: hendrik_gt $
# $Revision: 945 $
# $HeadURL: https://repos.deltares.nl/repos/NHI/trunk/engines/HMDB/orm_hydrodb.py $
# $Keywords: $
import datetime
from geoalchemy2 import Geometry
from sqlalchemy import Integer, Float, DateTime, String, Boolean
from sqlalchemy import Sequence, ForeignKey, Column, ForeignKeyConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Location(Base):
    __tablename__ = 'location'
    locationid    = Column(Integer, Sequence('location_id_seq'), unique=True,primary_key=True)
    geometrypoint = Column(Geometry('POINT', srid=28992))
    locationcode  = Column(String,doc='location code')
    nitgcode      = Column(String,doc='nitg code')
    typeid        = Column(Integer,
                          ForeignKey('type.typeid', ondelete='CASCADE'),
                          nullable=False)
    owner         = Column(String,doc='owner')
    confidential  = Column(Boolean, default=False)
    compartmentid = Column(Integer,
                          ForeignKey('compartment.compartmentid', ondelete='CASCADE'),
                          nullable=False)
    x             = Column(Float)
    y             = Column(Float)
    mv            = Column(Float)
    filternr      = Column(Integer)
    filter_top    = Column(Float)
    filter_bottom = Column(Float)
    file_origin   = Column(String,doc='File location origin of data')
    date_added    = Column(DateTime, default=datetime.datetime.utcnow)

class Type(Base):
    __tablename__  = 'type'
    typeid       = Column(Integer, Sequence('type_id_seq'), unique=True,primary_key=True)
    typecode     = Column(String, nullable=False,doc='Measurement type code')    
    typename    = Column(String, nullable=False,doc='Measurement type description')    
    def __init__(self, typecode, typename):
        """"""
        self.typecode = typecode
        self.typename= typename

class Parameter(Base):
    __tablename__     = 'parameter'
    parameterid       = Column(Integer, Sequence('parameter_id_seq'), unique=True,primary_key=True)
    parametercode     = Column(String, nullable=False)
    parametername     = Column(String,doc='parameter name')
    def __init__(self, parametercode, parametername):
        """"""
        self.parametercode = parametercode
        self.parametername = parametername

class Compartment(Base):
    __tablename__     = 'compartment'
    compartmentid     = Column(Integer, Sequence('compartment_id_seq'), unique=True,primary_key=True)
    compartmentcode   = Column(String,doc='compartiment code')
    compartmentname   = Column(String, nullable=False)
    def __init__(self, compartmentcode, compartmentname):
        """"""
        self.compartmentcode = compartmentcode
        self.compartmentname = compartmentname

class Unit(Base):
    __tablename__ = "unit"
    unitid        = Column(Integer, Sequence('unit_id_seq'), unique=True,primary_key=True)
    unit          = Column(String, nullable=False,doc='indien mogelijk aquo unit code')
    unitabbrev    = Column(String, nullable=False,doc='abbreviation of unit (e.g. m/L)')
    conversiefactor = Column(Float,doc='in queries dit wordt factor van vermenigvuldiging',default=1.)
    def __init__(self, unit, unitabbrev, conversiefactor=1.):
        """"""
        self.unit = unit
        self.unitabbrev = unitabbrev
        self.conversiefactor = conversiefactor

class Dataseries(Base):
    __tablename__ = 'dataseries'
    seriesid      = Column(Integer, Sequence('series_id_seq'), unique=True,primary_key=True)
    locationid    = Column(Integer,
                          ForeignKey('location.locationid', ondelete='CASCADE'),
                          nullable=False)
    parameterid   = Column(Integer,
                          ForeignKey('parameter.parameterid', ondelete='CASCADE'),
                          nullable=False)
    filternr      = Column(Integer)
    datetime      = Column(DateTime, nullable=False)
    z             = Column(Float)
    value         = Column(Float, nullable=False)
    unitid        = Column(Integer,
                          ForeignKey('unit.unitid', ondelete='CASCADE'),
                          nullable=False)
    outlier       = Column(Boolean)
