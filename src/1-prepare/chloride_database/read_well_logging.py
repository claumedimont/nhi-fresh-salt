# -*- coding: utf-8 -*-
"""
Read borelog data from Dinoloket

TODO: store in database
"""
from pathlib import Path
from readers import read_LAS
import pandas as pd
import datetime
import matplotlib.pyplot as plt

#LASpath = Path("p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/Well log(s)_Geological borehole research")
#LASpath = Path(r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\LHM zoetzout\Dinoloket\Well log(s)_Geological borehole research")
#result_path = Path("../../data/2-interim")
#fig_path = Path("../../reports/figures")
LASpath = Path(r"C:\projects\nhi-fresh-salt\data\1-external\Gegevens_Dinoloket\Well log(s)_Geological borehole research")
coord_csv = pd.read_csv(r"c:\projects\nhi-fresh-salt\data\1-external\coordinates.csv")
result_path = Path("data/2-interim")
fig_path = Path("reports/figures")

attrs_l = []
meas_data_l = []
i = 0
for fn in LASpath.glob("*.*"):  # .las, .LAS. .log, .$$$, 
    i += 1
    #attrs,colinfo,meas_data = read_LAS(fn)
    attrs,colinfo,meas_data = read_LAS(fn, only_ln=True, only_ln_column=True)
    if meas_data is not None:
        #print(f"Well log: {fn.name}")
        attrs_l += [attrs]
        meas_data.loc[:,"LASid"] = attrs["LASid"] #meas_data.loc[:,"CPTid"] = attrs["CPTid"]
        meas_data = meas_data.rename(columns={attrs["LN_column"]:"LN (ohm-m)"})
        meas_data_l += [meas_data]
    
    if i == 100:
        break

attrs = pd.concat(attrs_l,axis=1).T

attrs.insert(1, "x", '', True) #new column for x coord
attrs.insert(2, "y", '', True) #new column for y coord

# Correlate the coordinates from csv to the LAS attrs
for key, value in attrs.iterrows():
    id_val = value['LASid'][:-3] #without the extra text in the id
    for i, value in coord_csv.iterrows():
        if id_val == value['nitgNr']:
            result1 = value[' x']
            result2 = value[' y']
    attrs['x'][key] = float(result1)
    attrs['y'][key] = float(result2)

attrs.to_csv(result_path / "LAS-attributes.csv")
attrs.to_pickle(result_path / "LAS-attributes.pickle")
print(attrs.info())

if len(meas_data_l):
    meas_data = pd.concat(meas_data_l,axis=0,ignore_index=True,sort=False)
    meas_data.to_pickle(result_path / "LAS-measurement-data.pickle")
print(meas_data.info())


"""
# get unique column names
colinfo_l = []
i=0
for fn in LASpath.glob("*.*"):
    try:
        attrs,colinfo,meas_data = read_LAS(fn)
    except:
        print(fn)
        raise
    colinfo_l += [colinfo]
    i += 1
    if i % 1000 == 0:
        print(i)
    #if i == 100:
    #    break
    
colinfo = pd.concat(colinfo_l,axis=1).T
colinfo.to_pickle("LAS_colinfo.pickle")
colinfo.to_csv("LAS_colinfo.csv")

unique_cols = []
for idx in colinfo.columns:
    unique_cols += colinfo[idx].unique().tolist()

unique_cols = pd.Series(unique_cols).dropna().unique()
unique_cols.sort()
print(unique_cols)
"""