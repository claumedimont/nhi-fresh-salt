# ` -*- coding: utf-8 -*-
# Copyright notice
#   --------------------------------------------------------------------
#   Copyright (C) 2019 Deltares
#       Gerrit Hendriksen
#       gerrit.hendriksen@deltares.nl
#
#   This library is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this library.  If not, see <http://www.gnu.org/licenses/>.
#   --------------------------------------------------------------------
#
# This tool is part of <a href="http://www.OpenEarth.eu">OpenEarthTools</a>.
# OpenEarthTools is an online collaboration to share and manage data and
# programming tools in an open source, version controlled environment.
# Sign up to recieve regular updates of this function, and to contribute
# your own tools.

"""packages"""
import os
import configparser
from pathlib import Path
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, func

# local
from orm_zoetzout import Base, Location, Dataseries, Compartment, Unit, Parameter, Type

"""some functions"""


def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser()
    cf.read(af)
    return cf


# setup connection to database
local = True
if local:
    cf = read_config(r"credentials_zoetzout_local.cfg")
else:
    cf = read_config(r"credentials_zoetzout_remote.cfg")
connstr = (
    "postgres://"
    + cf.get("Postgis", "user")
    + ":"
    + cf.get("Postgis", "pwd")
    + "@"
    + cf.get("Postgis", "host")
    + ":5432/"
    + cf.get("Postgis", "dbname")
)
engine = create_engine(connstr, echo=True)

meta = MetaData(engine)

# create session --> all initialisation work has been done in orm_initialize
Session = sessionmaker(bind=engine)
session = Session()


""" ----------------------------------------
    from here it gets dataset specific
    ----------------------------------------
"""

############ SOFT POINTS ############ 
basepath = Path(r"p:\11203718-007-nhi-zoetzout\3D-chloride\soft_points")

data_params = {
    "sea": {
        "path": basepath / "soft_points_sea_salinity_coarsened.csv",
        "valuecol":"sea_cl",
        "datatype":"SOFT_SEA"
        },
    "deep": {
        "path": basepath / "soft_points_deep_salinity_coarsened.csv",
        "valuecol":"deep_cl",
        "datatypecol":"type"
        }
    }

for datatype, params in data_params.items():
    data = pd.read_csv(params["path"])
    grouped = data.groupby(["x","y"])
    
    date_modified = pd.Timestamp.fromtimestamp(params["path"].stat().st_mtime)
    
    # get relationship ids
    if "datatype" in params:
        typecode = params["datatype"]
        type_ = session.query(Type).filter_by(typecode=typecode).first()
    compartment_ = session.query(Compartment).filter_by(compartmentcode="GW").first()
    parameter_ = session.query(Parameter).filter_by(parametercode="Cl").first()
    unit_ = session.query(Unit).filter_by(unitabbrev="g/L").first()

    # loop through groups and store in database
    idx = {}
    for (x,y), xydata in grouped:
        if "datatypecol" in params:
            typecode = xydata[params["datatypecol"]].iloc[0]
            type_ = session.query(Type).filter_by(typecode=typecode).first()
        
        if typecode not in idx:
            idx[typecode] = 1
        else:
            idx[typecode] += 1
        
        #print (idx, x,y, len(xydata))
        #continue
        a = Location(
            locationcode=f"{typecode}_{idx[typecode]:05d}",
            typeid=type_.typeid,
            #nitgcode=nitgcode,
            owner="Deltares",
            confidential=False,
            compartmentid=compartment_.compartmentid,
            x=x,
            y=y,
            file_origin=str(params["path"]),
        )
        session.add(a)
        session.flush()
        # get the id of the last inserted data
        locationid = a.locationid
        session.commit()

        # define temporary dataframe
        dft = xydata.copy()
        dft = dft.reindex(columns=["z", params["valuecol"]])
        dft = dft.rename(columns={params["valuecol"]: "value"})
        dft["locationid"] = locationid
        dft["parameterid"] = parameter_.parameterid
        dft["datetime"] = date_modified
        dft["unitid"] = unit_.unitid
        dft = dft.dropna(subset=["value"]).reset_index(drop=True)
        # retrieve max id of dataid
        s = session.query(func.max(Dataseries.seriesid).label("id")).one().id
        if not s:  # str(s) == 'None':
            s = 0
        else:
            dft.index += s + 1
        dft.index.rename("seriesid", inplace=True)
        dft.to_sql(
            "dataseries",
            con=engine,
            if_exists="append",
            schema="public",
            index=True,
        )

# finally update the column geometrypoint
strsql = (
    """update public.location set geometrypoint = st_setsrid(st_point(x,y),28992)"""
)
engine.execute(strsql)


session.close()
