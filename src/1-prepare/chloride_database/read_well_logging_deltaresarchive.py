# -*- coding: utf-8 -*-
"""
Read borelog data from Deltares archives

TODO: find out why originally more locations
"""
from pathlib import Path
import os, sys
sys.path.append(os.path.dirname(__file__))  # add script directory to python path
from readers import read_LAS, read_ASC
import pandas as pd

#LASpath = Path("p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/Well log(s)_Geological borehole research")
#LASpath = Path(r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\LHM zoetzout\Dinoloket\Well log(s)_Geological borehole research")
result_path = Path("data/2-interim")
#fig_path = Path("../../reports/figures")
Archivepath = Path(r"p:\11203718-007-nhi-zoetzout\Grondmetingen_Deltares\Boorgatmetingen_archief")
"""
i=0
cols_l = []
for fn in Archivepath.glob('**/*'):
    if fn.is_file():
        if fn.suffix.lower() == ".asc":
            try:
                a,m = read_ASC(fn)
                cols_l += list(m.columns)
                i+=1
            except UnicodeDecodeError:
                print("UnicodeDecodeError: ",fn)
            except ValueError:
                print(fn)

            if i % 1000 == 0:
                print(i)

cols = pd.Series(cols_l).drop_duplicates()
cols.to_csv("uniquecols.csv")
print("done")
"""

attrs_l = []
meas_data_l = []
for dir_ in Archivepath.iterdir():
    print(dir_.name)
    fns = list(dir_.glob("las_asc_files*.txt"))
    if len(fns) > 1:
        fn = [f for f in fns if "corr" in str(f)][0]
    elif len(fns) == 1:
        fn = fns[0]
    else:
        continue

    with open(fn,"r") as f:
        for line in f:
            line=line.strip()
            line = line.replace(r"p:\430-tgg-data\land_data\meetdata\boorgatmetingen",str(Archivepath))
            fn_asclas = Path(line)
            m = None
            try:
                if fn_asclas.suffix.lower() == ".las":
                    #continue
                    a,c,m = read_LAS(fn_asclas, only_ln=True, only_ln_column=True)
                    a["ASCid"] = None
                    #print(a)
                elif fn_asclas.suffix.lower() == ".asc":
                    a,m = read_ASC(fn_asclas, only_ln=True, only_ln_column=True)
                    a["LASid"] = None
                    #print(a)
                if m is not None:
                    m["LASid"] = a["LASid"]
                    m["ASCid"] = a["ASCid"]
                    attrs_l += [a]
                    meas_data_l += [m]
            except (UnicodeDecodeError, ValueError):
                pass
            #break
        #break
    #break

attrs = pd.concat(attrs_l,axis=1).T
attrs.to_csv(result_path / "all_attributes.csv")
attrs.to_pickle(result_path / "all_attributes.pickle")
print(attrs.info())

if len(meas_data_l):
    meas_data = pd.concat(meas_data_l,axis=0,ignore_index=True,sort=False)
    meas_data.to_pickle(result_path / "all_measurement-data.pickle")
print(meas_data.info())

meas_data = pd.read_pickle(result_path / "all_measurement-data.pickle")

columns = ["depth (m)","LASid","ASCid","cond (mmho/m)","res(64n) (ohm-m)","res(64n) (ohm-m)_001",
                "res(64n) (ohm-m)_002","res(64n) (ohm-m)_003","ln200 (ohm-m)","ln160 (ohm-m)","res(64n) (ohm.m)",
                "lono.multi (ohmm)","lono (ohmm)","ln100 (ohm-m)","ln20 (ohm-m)"]

meas_data = meas_data.reindex(columns=columns).dropna(axis=0,how="all")
meas_data["id"] = meas_data["ASCid"].combine_first(meas_data["LASid"])
ln = meas_data["res(64n) (ohm-m)"].loc[meas_data["res(64n) (ohm-m)"]>0]
for c in columns[5:]:
    ln = ln.combine_first(meas_data[c].loc[meas_data[c]>0])
meas_data["LN (ohmm)"] = ln

meas_data = meas_data.reindex(columns=["id","depth (m)","cond (mmho/m)","LN (ohmm)"])
meas_data = meas_data.dropna(subset=["cond (mmho/m)","LN (ohmm)"],axis=0,how="all")
meas_data.to_pickle(result_path / "selected_measurement-data.pickle")

# also select attrs
attrs = pd.read_pickle(result_path / "all_attributes.pickle")
attrs["id"] = attrs["ASCid"].combine_first(attrs["LASid"])
attrs = attrs.loc[attrs["id"].isin(meas_data["id"].unique())]

#,ASCid,LASid,LN_column,api,cnty,comp,company,country,county,ctry,date,depth driller,depth increment,depth reference,elevation above datum,elevation above sea level,engineer,field,fld,fluid density,fluid type,has_LN,job number,lat,lati,loc,location x,location y,lon,long,null,prov,rem2,remarks,run,srvc,stat,step (m),stop (m),strt (m),uwi,well,witness
columns = ['ASCid', 'LASid', 'LN_column', 'date', 'lat', 'lati', 'loc', 'locx', 'locy', 'location x', 'location y', 'lon', 'long', 'well', 'id']
attrs = attrs.reindex(columns=columns)

# get as much coordinates as possible
coordinates = pd.read_csv(r"p:\11203718-007-nhi-zoetzout\Gegevens_Dinoloket\coordinates.csv")
coordinates = coordinates.set_index("nitgNr")
def well_corr(w):
    try:
        w = w.upper()
        if w[0] != "B":
            w = "B"+w
        return w
    except:
        return w

attrs["well_corr"] = attrs["well"].map(well_corr)
attrs["x"] = attrs["well_corr"].map(coordinates[" x"])
attrs["y"] = attrs["well_corr"].map(coordinates[" y"])

attrs["x"] = attrs["x"].combine_first(attrs["locx"]).combine_first(attrs["location x"]).combine_first(attrs["lat"]).combine_first(attrs["lati"])
attrs["y"] = attrs["y"].combine_first(attrs["locy"]).combine_first(attrs["location y"]).combine_first(attrs["lon"]).combine_first(attrs["long"])

# reselect where x and y are floats, and in RD coords
x_is_float = attrs["x"].map(lambda x:type(x)==float)
y_is_float = attrs["y"].map(lambda x:type(x)==float)
attrs = attrs.loc[x_is_float & y_is_float]
attrs = attrs.loc[attrs["y"] > 300000]


attrs = attrs.reindex(columns=["id",'ASCid', 'LASid', 'LN_column', 'date',"x","y","well","well_corr"])
attrs.to_pickle(result_path / "selected_attributes.pickle")
attrs.to_csv(result_path / "selected_attributes.csv")

attrs = pd.read_pickle(result_path / "selected_attributes.pickle")
attrs["fn_orig"] = None

for dir_ in Archivepath.iterdir():
    print(dir_.name)
    fns = list(dir_.glob("las_asc_files*.txt"))
    if len(fns) > 1:
        fn = [f for f in fns if "corr" in str(f)][0]
    elif len(fns) == 1:
        fn = fns[0]
    else:
        continue

    with open(fn,"r") as f:
        for line in f:
            line=line.strip()
            fn_asclas = Path(line)
            id_ = fn_asclas.stem
            attrs.loc[attrs["id"]==id_, "fn_orig"] = line
            #break
    #break

attrs.to_pickle(result_path / "selected_attributes_fnorig.pickle")
attrs.to_csv(result_path / "selected_attributes_fnorig.csv")