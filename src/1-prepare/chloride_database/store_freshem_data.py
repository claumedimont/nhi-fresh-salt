# ` -*- coding: utf-8 -*-
# Copyright notice
#   --------------------------------------------------------------------
#   Copyright (C) 2019 Deltares
#       Gerrit Hendriksen
#       gerrit.hendriksen@deltares.nl
#
#   This library is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this library.  If not, see <http://www.gnu.org/licenses/>.
#   --------------------------------------------------------------------
#
# This tool is part of <a href="http://www.OpenEarth.eu">OpenEarthTools</a>.
# OpenEarthTools is an online collaboration to share and manage data and
# programming tools in an open source, version controlled environment.
# Sign up to recieve regular updates of this function, and to contribute
# your own tools.

"""packages"""
import os
import configparser
from pathlib import Path
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, func

# local
from orm_zoetzout import Base, Location, Dataseries, Compartment, Unit, Parameter, Type

"""some functions"""


def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser()
    cf.read(af)
    return cf


# setup connection to database
local = True
if local:
    cf = read_config(r"credentials_zoetzout_local.cfg")
else:
    cf = read_config(r"credentials_zoetzout_remote.cfg")
connstr = (
    "postgres://"
    + cf.get("Postgis", "user")
    + ":"
    + cf.get("Postgis", "pwd")
    + "@"
    + cf.get("Postgis", "host")
    + ":5432/"
    + cf.get("Postgis", "dbname")
)
engine = create_engine(connstr, echo=True)

meta = MetaData(engine)

# create session --> all initialisation work has been done in orm_initialize
Session = sessionmaker(bind=engine)
session = Session()


""" ----------------------------------------
    from here it gets dataset specific
    ----------------------------------------
"""


############ GROUND GEOPHYSICS ############ 
basepath = Path(r"c:\Svnrepos\nhi-fresh-salt\data\1-external\chloride_data")

data_params = {
    "freshem-zl": {
        "attr":r"c:\Svnrepos\nhi-fresh-salt\data\2-interim\zeeland_freshem-attributes.pickle",
        "data":r"c:\Svnrepos\nhi-fresh-salt\data\2-interim\zeeland_freshem-measurement-data.pickle",
        "id_col": "locid",
        "percentiles": [10,25,40,50,60,75,90],
        "owner": "Prov. Zeeland",
        "orig_file_path": r"n:\Projects\1209000\1209220\C. Report - advise\Zeeland\3D\csv_absoluut\chloride.csv",
        "confidential":False,
        "date":pd.Timestamp(2015,6,1),
    },
    "freshem-dunea": {
        "attr":r"c:\Svnrepos\nhi-fresh-salt\data\2-interim\dunea_freshem-attributes.pickle",
        "data":r"c:\Svnrepos\nhi-fresh-salt\data\2-interim\dunea_freshem-measurement-data.pickle",
        "id_col": "locid",
        "percentiles": [10,25,40,50,60,75,90],
        "owner": "Dunea",
        "orig_file_path": r"p:\11203718-007-nhi-zoetzout\Grondmetingen_Deltares\Dunea\dunea_freshem.csv",
        "confidential":True,
        "date":pd.Timestamp(2011,6,1),
    },
}
datatypes = {
    "freshem-zl": "FHEM",
    "freshem-dunea": "FHEM",
}

run_datatypes = ["freshem-zl","freshem-dunea"]  # "LAS","GEF","Deltares_LAS","VES"
for datatype in run_datatypes:
    params = data_params[datatype]
    attrs = pd.read_pickle(params["attr"])
    data = pd.read_pickle(params["data"])
    id_ = params["id_col"]
    owner = params["owner"]
    confidential = params["confidential"]

    # get relationship ids
    type_ = session.query(Type).filter_by(typecode=datatypes[datatype]).first()
    compartment_ = session.query(Compartment).filter_by(compartmentcode="GW").first()
    parameter_ = {p:session.query(Parameter).filter_by(parametercode=f"Cl_p{p}").first() for p in params["percentiles"]}
    unit_ = session.query(Unit).filter_by(unitabbrev="g/L").first()

    # loop through attrs and store in database
    for i, attr in attrs.iterrows():

        data_sel = data.loc[data[id_] == attr[id_]]
        if len(data_sel):  # only if there are data for this attr
            fn_orig = params["orig_file_path"].format(id=attr[id_])

            a = Location(
                locationcode=f"{datatype}_{int(attr[id_]):05d}",
                typeid=type_.typeid,
                #nitgcode=nitgcode,
                owner=owner,
                confidential=confidential,
                compartmentid=compartment_.compartmentid,
                x=attr["x"],
                y=attr["y"],
                file_origin=fn_orig,
            )
            session.add(a)
            session.flush()
            # get the id of the last inserted data
            locationid = a.locationid
            session.commit()

            
            # define temporary dataframe for all percentiles at once
            dft = []
            for p in params["percentiles"]:
                df_pct = data_sel[["z",f"p{p}"]]
                df_pct = df_pct.rename(columns={f"p{p}": "value"})
                df_pct["value"] /= 1000.
                df_pct.loc[df_pct["value"] > 18.,"value"] = 18.  # limit to 18 g/L
                df_pct["locationid"] = locationid
                df_pct["parameterid"] = parameter_[p].parameterid
                df_pct["datetime"] = params["date"]
                df_pct["unitid"] = unit_.unitid
                dft.append( df_pct.dropna(subset=["value"]) )
            dft = pd.concat(dft, axis=0, ignore_index=True)    
            dft = dft.reset_index(drop=True)
                
            # retrieve max id of dataid
            s = session.query(func.max(Dataseries.seriesid).label("id")).one().id
            if not s:  # str(s) == 'None':
                s = 0
            else:
                dft.index += s + 1
            dft.index.rename("seriesid", inplace=True)
            dft.to_sql(
                "dataseries",
                con=engine,
                if_exists="append",
                schema="public",
                index=True,
            )
            #break
    # finally update the column geometrypoint
    strsql = (
        """update public.location set geometrypoint = st_setsrid(st_point(x,y),28992)"""
    )
    engine.execute(strsql)

session.close()

