# First part script
import os, glob, pandas

# Defining directory
os.chdir(r"c:\projects\nhi-fresh-salt\data\1-external\Gegevens_Dinoloket\Groundwater compositions, Well\1")

# Headers
## Defining headings
#headers = ['NITG-nr','X-coord','Y-coord','Monster datum','Analyse datum']
#head_dic = ['Cl','EC','HCO3','T','Chl']      # More parameters can be added/edited

## Identifying the headers
#for file in glob.glob('*.txt'):
#    with open(file, 'r') as f:
#        for i, line in enumerate(f):
#            if 'KWALITEIT gegevens VLOEIBAAR' in line:
#                break
#        f.seek(0)
#        if i<3:
#            pass
#        else:
#            csv = pandas.read_csv(f, delimiter='\t', skiprows=i+1)
#            csv_cols = list(csv.columns)
#            for j in range (len(head_dic)):
#                for i, value in enumerate(csv_cols):
#                    if csv_cols[i].startswith(head_dic[j]):
#                        headers.append(csv_cols[i])
#headers = pandas.Series(headers)
#headers = headers.drop_duplicates().reset_index(drop=True)
#headers.to_csv(r'c:\projects\nhi-fresh-salt\data\2-interim\headers.csv')

# Second part script
## Creating initial dataframe
headers=pandas.read_csv(r'c:\projects\nhi-fresh-salt\data\2-interim\headers.csv', header=None) #file contains only the selected headers
headers = headers[0]
df = pandas.DataFrame([], columns=list(headers))

# Extracting values from csv files
ind = 0
for file in glob.glob('*.txt'):
    ind += 1
    with open(file, 'r') as coord:
        lines = coord.readlines()
        x = lines[2].split("\t")[1]
        y = lines[2].split("\t")[2]
        surflevel = lines[2].split("\t")[6]
        
    with open(file, 'r') as f:
        for i, line in enumerate(f):
            if 'KWALITEIT gegevens VLOEIBAAR' in line:
                break
        f.seek(0)
        if i<3:
            pass
        else:
            csv = pandas.read_csv(f, delimiter='\t', skiprows=i+1)
            cols_csv = list(csv.columns)
            for key, value in csv.iterrows():
                df = df.append(pandas.Series(), ignore_index=True)
                df.iloc[-1,1] = x
                df.iloc[-1,2] = y
                df.iloc[-1,3] = surflevel
                for header in range (len(headers)):
                    for k in range (len(cols_csv)):
                        if headers[header] == cols_csv[k]:
                            df.iloc[-1,header] = csv.iloc[key,k]
    print(str(ind) + ". " + file)
    #if ind == 100:
    #    break
#df

df['Chloride (Cl) (mg/l)'] = df['Chloride (Cl) (ug/l)']/1000 #changing units

#Joining all columns together by filling the empty values of Cl- (mg/l)
df.loc[df['Cl- (mg/l)'].isnull(),'Cl- (mg/l)' ] = df['Chloride (mg/l)'] 
df.loc[df['Cl- (mg/l)'].isnull(),'Cl- (mg/l)' ] = df['Cl- (g/m3)']
df.loc[df['Cl- (mg/l)'].isnull(),'Cl- (mg/l)' ] = df['Chloride (Cl) (mg/l)']

df.drop(columns=['Chloride (Cl) (ug/l)', 'Chloride (mg/l)', 'Cl- (g/m3)', 'Chloride (Cl) (mg/l)'], axis=1, inplace=True)

#Delete if concentration is over 18000 mg/l and any left empty values
for key, value in df.iterrows():
    try:
        float(value['Cl- (mg/l)'])>18000
    except:
        pass
    else:
        if float(value['Cl- (mg/l)'])>18000:
            df=df.drop(key)
df.dropna(subset=['Cl- (mg/l)'], inplace=True)
df = df.reset_index(drop=True)

df.to_csv('c:/projects/nhi-fresh-salt/data/2-interim/well_comp.csv')