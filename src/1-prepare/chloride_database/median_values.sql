select 
	locationid, locationcode, geometrypoint, x, y, z, filter_top, filter_bottom,
	round(percentile_cont(0.25) within group (order by value)::numeric,2) as p25_value,
	round(percentile_cont(0.5) within group (order by value)::numeric,2) as median_value,
	round(percentile_cont(0.75) within group (order by value)::numeric,2) as p75_value,
	round(stddev_samp(value)::numeric,2) as stddev_value,
	parametercode, unitabbrev,
	count(*), 
	percentile_disc(0.5) within group (order by datetime) as median_time,
	min(datetime) as start_time,
	max(datetime) as end_time
	
from xyzv_analyses_gw 
group by (locationid, locationcode, geometrypoint, x, y, z, filter_top, filter_bottom, parametercode, unitabbrev) 
order by count DESC
limit 100
