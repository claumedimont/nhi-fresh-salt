create or replace view open_gw_measurement_values as

select 
	vals.typecode, 
	loc.locationcode, 
	loc.owner, 
	loc.x, 
	loc.y, 
	vals.z, 
	vals.value, 
	vals.unit, 
	vals.filter_top,
	vals.filter_bottom,
	vals.start_time,
	vals.end_time,
	loc.geometrypoint from
(
select 
	locationid,
	z,
	'ANA' as typecode,
	filter_top,
	filter_bottom,
	start_time,
	end_time,
	latest_value as value,
	'g/L' as unit
	from xyzv_analyses_gw_aggregate
union
	(
		select
		locationid,
		z,
		typecode,
		NULL as filter_top,
		NULL as filter_bottom,
		datetime as start_time,
		NULL as end_time,
		value as value,
		'g/L' as unit
		from xyzv_geophysics
	)
union
	(
		select
		locationid,
		z,
		'FHEM' as typecode,
		NULL as filter_top,
		NULL as filter_bottom,
		datetime as start_time,
		NULL as end_time,
		cl_p50 as value,
		'g/L' as unit
		from xyzvt_freshem_p50
	)
) as vals
join location loc on loc.locationid = vals.locationid
where loc.confidential = False and loc.compartmentid = 2