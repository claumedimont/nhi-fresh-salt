# -*- coding: utf-8 -*-
"""
Transform geophysics data into chloride measurements for input in database

STEPS:
1) read geophysics data
2) transform to EC25 (conductivity at temp of 25deg) in units mS/cm
3) resample to 1m depth intervals (if not done already)
4) depth from surface level to NAP
5) combine datalocations (x,y,z) with GeoTOP and/or REGIS
6) determine which data is mostly sand, discard other data
6b) discard data outside plausible range of sand resistivities (0.6 - 250 Ohmm, from Marios Karaoulis)
7) calculate EC25 of water, by EC25_water = EC25_bulk * 4.  (4 being formation factor for sand)
8) calculate chloride by formula perry: Cl (mg/l) = 360 * EC25_water - 450  (with minimum of 0 mg/l)
9) Store data

TODO: store in database
"""
from pathlib import Path
import pandas as pd
import xarray as xr
import numpy as np
import imod
import re
pd.set_option('mode.chained_assignment', None)

def get_columns(df):
    is_ec = None
    z = None
    ec = None
    df = df.dropna(axis=1, how="all")  # drop empty columns
    for c in df.columns:
        c_low = c.lower()
        if "depth" in c_low or "diepte" in c_low or "z" in c_low:
            z = c
        elif "boven" in c_low or "onder" in c_low or "top" in c_low or "bot" in c_low:
            if z is None:
                z = []
            z.append(c)
        elif (
            "geleidbaarheid" in c_low or "cond" in c_low or "ecb" in c_low
        ):  # CPT, sometimes also cond in LAS...
            ec = c
            is_ec = True
        elif ec is None and (
            "ln" in c_low
            or "res" in c_low
            or "n100" in c_low
            or "rs" in c_low
            or "rho" in c_low
        ):
            ec = c
            is_ec = False
    return z, ec, is_ec


def z_from_topbot(df, interval=1.0, top="top", bot="bot"):
    # sort
    df = df.sort_values(by=top, ascending=False)

    # conform top values to interval, so they don't get lost in reindexing
    # first value: must ceil to include original value, others to closest
    idx = interval * (df[top] / interval).round(0)
    idx.iloc[0] = interval * (df[top].iloc[0] // interval) + interval
    df.index = idx - 0.5 * interval
    df = df[~df.index.duplicated()]

    # ztop, zbot
    ztop = df.index[0]
    zbot = (
        interval * (df[bot].iloc[-1] // interval) + 0.5 * interval
    )  # to include orig bot

    idx = pd.Index(
        np.linspace(ztop, zbot, int((ztop - zbot) // interval + 1)), name="z_interval"
    )
    return df.reindex(index=idx, method="ffill")


def appelboor_regis(ds_regis, x, y, top=None, bot=None):
    """"
    Functie om uit de netcdf REGIS dataset een appelboor-steekproef te nemen.
    
    Input: fn_regis: str or Path() naar REGIS-netcdf
    x: x-locatie RDnew
    y: y-locatie RDnew
    diepte_min: bovenkant van appelboor in m NAP
    diepte_max: onderkant van appelboor in m NAP
    
    Output: xr DataArray with numerical codes ready for regridder, and translator to go back to formation names

    (from Simon Buijs)
    """
    df = imod.select.points_values(ds_regis, x=x, y=y).to_dataframe()
    # df = ds_regis.sel(x=x,y=y, method='nearest', tolerance=100.).drop(['x','y']).to_dataframe()
    df = df.dropna(axis=0, subset=["top", "bot"])
    df = df.sort_values(by="top", ascending=False).reset_index()
    df = df.drop(columns="index")
    if top is not None and bot is not None:
        sel = (df["bot"] <= top) & (df["bot"] >= bot) | (df["top"] <= top) & (
            df["top"] >= bot
        )
        df = df.loc[sel]

    # to dataset
    df["dz"] = df["top"] - df["bot"]
    df["z"] = 0.5 * df["top"] + 0.5 * df["bot"]
    # make sure z is monotonic increasing
    df = df.loc[df["dz"] > 0.0]

    translator = df["formation"]
    df = df.reset_index()
    ds = df.set_index("z").to_xarray()
    ds = ds.set_coords("dz")
    return ds["index"], translator


regis_p = re.compile(
    r"([A-Z]+)([kzcqsbv])[0-9]*"
)  # (formation) (clay / sand / complex)


def test_regis_sand(formation):
    m = regis_p.match(formation)
    return m[2] == "z" and m[1] not in [
        "OO",
        "BR",
    ]  # must be sand, and not glauconiet-containing Breda or Oosterhout formation


def interval_data_to_like(df, id_):
    """Transform interval data to a like object for regridder"""
    df.index.name = "z"
    df.loc[:,"dz"] = df["dz"].astype(float).values
    ds = df.to_xarray()
    ds = ds.set_coords("dz")
    return ds[id_]


def unit_factor_deltares_las(col):
    if col == "cond (mmho/m)":
        return 0.001
    elif col == "cond (ms/m)":
        return 0.001
    else:
        return 1.0


#base_path = Path(r"c:\Users\delsman\Documents\nhi-fresh-salt")
base_path = Path(r"C:\Svnrepos\nhi-fresh-salt")
data_paths = {
    "dino_las": {
        "attrs": base_path / "data/2-interim/LAS-attributes.pickle",
        "data": base_path / "data/2-interim/LAS-measurement-data.pickle",
        "result": base_path / "data/2-interim/LAS-interval-data.csv",
        "id": "LASid",
    },
    "dino_ecpt": {
        "attrs": base_path / "data/2-interim/GEF-attributes.pickle",
        "data": base_path / "data/2-interim/GEF-measurement-data.pickle",
        "result": base_path / "data/2-interim/GEF-interval-data.csv",
        "id": "CPTid",
    },
    "dino_ves": {
        "attrs": base_path / "data/2-interim/VES-attributes_clean.pickle",
        "data": base_path / "data/2-interim/VES-interpreted-data_clean.pickle",
        "result": base_path / "data/2-interim/VES-interval-data.csv",
        "id": "VESid",
    },
    "deltares_las": {
        "attrs": base_path / "data/2-interim/deltares-LAS-attributes_fnorig.csv",
        "data": base_path / "data/2-interim/selected_measurement-data.pickle",
        "result": base_path / "data/2-interim/deltares-LAS-interval-data.csv",
        "id": "id",
        "unit_factor": unit_factor_deltares_las,
    },
    "zeeland": {
        "attrs": base_path / "data/2-interim/zeeland-attributes.pickle",
        "data": base_path / "data/2-interim/zeeland-measurement-data.pickle",
        "result": base_path / "data/2-interim/zeeland-interval-data.csv",
        "id": "ID",
    },
    "skytem_PWN": {
        "attrs": base_path / "data/2-interim/skytem_PWN-attributes.pickle",
        "data": base_path / "data/2-interim/skytem_PWN-measurement-data.pickle",
        "result": base_path / "data/2-interim/skytem_PWN-interval-data.csv",
        "id": "RECORD",
    },
    "PWN_ecpt": {
        "attrs": base_path / "data/2-interim/PWN-GEF-attributes.csv",
        "data": base_path / "data/2-interim/PWN-GEF-measurement-data.pickle",
        "result": base_path / "data/2-interim/PWN-GEF-interval-data.csv",
        "id": "CPTid",
    },
    "skytem_CliwatFryslan": {
        "attrs": base_path
        / "data/2-interim/skytem_CliwatFryslan-attributes_250m.pickle",
        "data": base_path / "data/2-interim/skytem_CliwatFryslan-data_250m.pickle",
        "result": base_path / "data/2-interim/skytem_CliwatFryslan-interval-data.csv",
        "id": "RECORD",
    },
    "HEM_CliwatFryslan": {
        "attrs": base_path / "data/2-interim/HEM_CliwatFryslan-attributes_250m.pickle",
        "data": base_path / "data/2-interim/HEM_CliwatFryslan-data_250m.pickle",
        "result": base_path / "data/2-interim/HEM_CliwatFryslan-interval-data.csv",
        "id": "RECORD",
    },
}


# regis_path = "p:/11203718-007-nhi-zoetzout/REGIS_v2_2/REGIS_v2_2.nc"
# geotop_path = "p:/11203718-007-nhi-zoetzout/GeoTOP/GeoTOP.nc"
# ahn_path = "p:/11203718-007-nhi-zoetzout/NHI_zz_basic/data/1-external/LHM34_lagenmodel/AHN_F250.IDF"
#regis_path = base_path / "data/1-external/REGIS_v2_2/REGIS_v2_2_filled.nc"
regis_path = base_path / "data/1-external/REGIS_v2_2/REGIS_v2_2.nc"
geotop_path = base_path / "data/1-external/GeoTOP/GeoTOP.nc"
ahn_path = base_path / "data/1-external/LHM34_lagenmodel/AHN_F250.IDF"

regis = xr.open_dataset(regis_path)
geotop = xr.open_dataset(geotop_path)["lithology"]
ahn = imod.idf.open(ahn_path, pattern="{name}")
z_interval = 1.0  # resample to z_interval of ...

mode_regridder = imod.prepare.Regridder("mode", ndim_regrid=1)
geotop_is_sand = {
    -1: False,
    0: False,
    1: False,
    2: False,
    3: False,
    4: False,
    5: True,
    6: True,
    7: True,
    8: False,
    9: False,
    np.nan: np.nan,
}


########### MAIN LOOP #############
datatypes = list(data_paths.keys())
datatypes = ['skytem_PWN', 'skytem_CliwatFryslan', 'HEM_CliwatFryslan']#['skytem_CliwatFryslan', 'HEM_CliwatFryslan']#["deltares_las"]['dino_las','dino_ecpt', 'dino_ves',  'deltares_las', 'zeeland', 'skytem_PWN', 'PWN_ecpt', ]# 
for datatype in datatypes:
    paths = data_paths[datatype]
    # load data
    if Path(paths["attrs"]).suffix.lower() == ".csv":
        attrs = pd.read_csv(paths["attrs"], index_col=0)
    else:
        attrs = pd.read_pickle(paths["attrs"])
    data = pd.read_pickle(paths["data"])
    id_ = paths["id"]
    if "unit_factor" in paths:
        unit_factor = paths["unit_factor"]
    else:
        unit_factor = lambda x: 1.0

    prev_point = (0, 0, -1)

    # load intermediate results
    try:
        results = pd.read_csv(paths["result"])
        done = results[id_].unique().tolist()
    except IOError:
        results = None
        done = []
    # loop though measurements
    for i, attr in attrs.iterrows():
        try:
            # not already in results?
            if attr[id_] in done:
                continue

            if datatype == "skytem_PWN":
                # calc dist to prev point,
                prev_x, prev_y, prev_fl = prev_point
                if prev_fl == attr["LINE_NO"]:
                    dist = (
                        (attr["x"] - prev_x) ** 2 + (attr["y"] - prev_y) ** 2
                    ) ** 0.5
                    # print(dist)
                    if dist < 500:
                        print(f"{datatype} - Skipping {attr[id_]}")
                        continue
                    else:
                        prev_point = (attr["x"], attr["y"], attr["LINE_NO"])
                else:
                    prev_point = (0, 0, attr["LINE_NO"])

            print(f"{datatype} - {attr[id_]}")

            # Step 1) select measurement data
            point_data = data.loc[data[id_] == attr[id_]].copy()
            if not len(point_data):
                print(f"Error {datatype}, {attr[id_]}, no point data for this id")
                with open("error_ids.debug", "a") as f:
                    f.write(f"{datatype}, {attr[id_]}, no point data for this id\n")
                continue

            # Step 2) transform to EC25  ==> if the value column is equal for entire dataframe (all measurements, steps 2 and 3 could be done outside loop: faster)
            z_col, value_col, is_ec = get_columns(point_data)
            if value_col is None or z_col is None:
                print(
                    f"Error {datatype}, {attr[id_]}, problem interpreting measurement"
                )
                with open("error_ids.debug", "a") as f:
                    f.write(
                        f"{datatype}, {attr[id_]}, problem interpreting measurement\n"
                    )
                continue

            # throw out unnecessary data
            if isinstance(z_col, list):
                columns = z_col + [id_, value_col]
            else:
                columns = [z_col, id_, value_col]
            point_data = point_data.reindex(columns=columns)
            if not is_ec:
                point_data["ECb"] = (
                    10.0 / point_data[value_col]
                )  # assuming if ln, unit always ohmm -> mS/cm
                value_col = "ECb"
            else:
                point_data["ECb"] = (
                    10.0 * point_data[value_col]
                )  # assuming S/m, or mho/m
            point_data["ECb"] *= unit_factor(value_col)

            point_data["EC25b"] = (
                point_data["ECb"] * 1.39
            )  # assuming 11 degrees groundwater temp   EC  =  [ 1 + a (t – 25) ]  EC25	with   a = 0.020

            # Step 3) depth from surface level to NAP
            try:
                mv = imod.select.points_values(ahn.fillna(0), x=float(attr["x"]), y=float(attr["y"]))
            except:
                print(
                    f"Error {datatype}, {attr[id_]} (x={attr['x']}, y={attr['y']} not in AHN, skipping"
                )
                with open("error_ids.debug", "a") as f:
                    f.write(f"{datatype}, {attr[id_]} not in AHN\n")
                continue
            point_data.loc[:, z_col] = float(mv) - point_data.loc[:, z_col]
            point_data = point_data.sort_values(by=z_col, ascending=False)

            # Step 4) resample to 1m depth intervals or mid of layer for VES (if not done already)
            if not isinstance(z_col, list):
                # resample to meter-intervals, for data with 1 z coordinate
                point_data["z"] = (
                    z_interval * (point_data[z_col] // z_interval) + 0.5 * z_interval
                )  # to midpoints
                interval_data = point_data.groupby(
                    "z", sort=False
                ).mean()  # mean over interval
                interval_data["dz"] = z_interval
                interval_data[id_] = attr[id_]  # got lost in mean()

            else:  # if datatype in ["dino_ves","skytem_PWN"]:
                # for data with top and bot z coordinate
                point_data = point_data.sort_values(by=z_col[0], ascending=False)
                point_data["dz"] = point_data[z_col[0]] - point_data[z_col[1]]
                if datatype == "dino_ves":
                    # VES has no lowest value, set to half the length of the previous layer
                    point_data[z_col[1]] = point_data[z_col[1]].fillna(
                        point_data[z_col[0]].min() - point_data["dz"].max()
                    )
                    point_data["dz"] = point_data[z_col[0]] - point_data[z_col[1]]
                point_data.index = pd.Index(
                    0.5 * point_data[z_col[0]] + 0.5 * point_data[z_col[1]], name="z"
                )
                # interval_data = z_from_topbot(point_data, interval=z_interval, top=z_col[0], bot=z_col[1])
                interval_data = point_data

                # drop intervals with zero thickness
                interval_data = interval_data.loc[interval_data["dz"]>0]

            # Step 5) combine datalocations (x,y,z) with GeoTOP and/or REGIS
            interval_data["is_sand"] = None
            like = interval_data_to_like(interval_data, id_)
            try:
                # base on Geotop, throws KeyError if no Geotop at this location
                # geotop_point = geotop.sel(x=attr["x"],y=attr["y"], method="nearest",tolerance=100.)  #z=interval_data.index[interval_data.index>=-50],
                geotop_point = imod.select.points_values(
                    geotop, x=float(attr["x"]), y=float(attr["y"])
                ).squeeze("index", drop=True)

                # throw out nodata, sort descending and regrid
                geotop_point = geotop_point.where(
                    ~geotop_point.isin(geotop_point.nodatavals)
                )
                geotop_point = geotop_point.sortby("z", ascending=False)
                geotop_regrid = mode_regridder.regrid(geotop_point, like)

                geotop_regrid = geotop_regrid.to_dataframe(name="litho")
                interval_data["litho"] = geotop_regrid["litho"]
                interval_data["is_sand"] = interval_data["litho"].map(geotop_is_sand)
            except:
                pass

            # always base on REGIS as well
            regis_point, translator = appelboor_regis(
                regis, x=float(attr["x"]), y=float(attr["y"])
            )  # , top=interval_data.index[0], bot=interval_data.index[-1])

            # regrid to depth intervals and translate back to formations  #W26C0015
            try:
                regis_regrid = mode_regridder.regrid(regis_point, like)
            except:
                print(f"Error {datatype}, {attr[id_]}, regridder problem")
                with open("error_ids.debug", "a") as f:
                    f.write(f"{datatype}, {attr[id_]}, regridder problem\n")
                continue
            regis_regrid = regis_regrid.to_series().map(translator)


            # regis_point = z_from_topbot(regis_point,interval=z_interval)
            interval_data["formation"] = regis_regrid
            interval_data["is_sand"] = (
                interval_data["is_sand"].combine_first(
                    interval_data["formation"]
                    .dropna()
                    .map(lambda x: regis_p.match(x)[2] == "z")
                )
            ).fillna(False)
            interval_data["is_glauc"] = (
                interval_data["formation"]
                .dropna()
                .map(lambda x: regis_p.match(x)[1] in ["OO", "BR"])
            )
            interval_data["is_sand"] = interval_data["is_sand"] & ~interval_data[
                "is_glauc"
            ].fillna(False)

            # Step 6) determine which data is mostly sand, discard other data
            interval_data = interval_data.loc[
                interval_data["is_sand"]
            ].copy()  # copy to skip settingwithcopy warnings

            # Step 6b) throw out data outside probable range (instead of clipping to range)
            rho_sand_bnds = np.array([0.6, 250.0])  # ohmm
            ec25b_sand_bnds = 10.0 / rho_sand_bnds * 1.39
            within_bnds = (interval_data["EC25b"] > ec25b_sand_bnds.min()) & (
                interval_data["EC25b"] < ec25b_sand_bnds.max()
            )
            interval_data = interval_data.loc[within_bnds]

            # Step 7) calculate EC25 of water, by EC25_water = EC25_bulk * 4.  (4 being formation factor for sand)
            interval_data["EC25w"] = 4.0 * interval_data["EC25b"]

            # Step 8) calculate chloride by formula perry: Cl (g/l) = 0.36 * EC25_water - 0.45  (with minimum of 0 mg/l)
            interval_data["Cl (g/l)"] = 0.36 * interval_data["EC25w"] - 0.45
            interval_data.loc[
                interval_data["Cl (g/l)"] < 0.05, "Cl (g/l)"
            ] = 0.05  # set negatives to 50 mg/l
            interval_data.loc[
                interval_data["Cl (g/l)"] > 18.0, "Cl (g/l)"
            ] = 18.0  # set maximum to 18000 mg/l

            # throw out unnecessary columns and round
            interval_data = (
                interval_data.reindex(columns=[id_, "Cl (g/l)"]).reset_index().round(3)
            )

            if len(interval_data):
                # Step 9) Store data
                # if results is None:
                #    results = interval_data
                # else:
                #    results.append(interval_data)
                #    results.to_pickle(paths["result"])  # keep overwriting... rather crude
                if len(done):
                    interval_data.to_csv(
                        paths["result"], index=False, mode="a", header=False
                    )  # write to csv in append mode
                else:
                    interval_data.to_csv(
                        paths["result"], index=False, mode="a"
                    )  # write to csv in append mode

                done.append(attr[id_])
            else:
                print(f"Error {datatype}, {attr[id_]}, empty result")
                with open("error_ids.debug", "a") as f:
                    f.write(f"{datatype}, {attr[id_]}, empty result\n")
        except:
            print(f"Error {datatype}, {attr[id_]}, unidentified error")
            with open("error_ids.debug", "a") as f:
                f.write(f"{datatype}, {attr[id_]}, unidentified error\n")
            continue
            #raise
    # df = pd.concat(results, axis=0, ignore_index=True)
    # df.to_pickle(paths["result"])
    # TODO: store in database, or save
