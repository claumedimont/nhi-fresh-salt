# split dataframe in attributes and data

import pandas as pd
from pathlib import Path

fn_orig = r"c:\Svnrepos\nhi-fresh-salt\data\1-external\overzichtmetingen.csv"
result_path = Path(r"c:\Svnrepos\nhi-fresh-salt\data\2-interim")

df = pd.read_csv(fn_orig, index_col=0)

attrs = df.groupby("ID").first()
attrs = attrs.reindex(columns=["x","y","mNAP","IDstring","Type","date"]).reset_index()

data = df.reindex(columns=["ID","mmv","ECbulk"])
data = data.rename(columns={"mmv":"diepte", "ECbulk":"ECb (S/m)"})
#data["diepte"] *= -1.
data["ECb (S/m)"] *= 0.1
data = data.loc[data["ECb (S/m)"] >= 0.]

attrs.to_pickle(result_path / "zeeland-attributes.pickle")
data.to_pickle(result_path / "zeeland-measurement-data.pickle")