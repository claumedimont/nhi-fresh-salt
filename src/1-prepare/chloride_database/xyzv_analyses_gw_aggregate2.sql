SELECT 
	i.locationid, 
	i.locationcode, 
	i.geometrypoint, 
	i.x, 
	i.y, 
	i.z, 
	i.filter_top, 
	i.filter_bottom, 
	i.p25_value, 
	i.median_value, 
	i.p75_value, 
	d.latest_value, 
	i.stddev_value,
    i.parametercode,
    i.unitabbrev,
	i.count,
	i.median_time,
	i.start_time,
	i.end_time
FROM (
SELECT DISTINCT ON (g.locationid, g.z) 
	g.locationid,
    g.z,
	round(last_value(g.value) OVER (PARTITION BY g.locationid, g.z 
							  ORDER BY g.datetime desc)::numeric,2) latest_value
   FROM xyzv_analyses_gw g
     JOIN gw_analyses_bounds b ON g.locationid = b.locationid AND g.z = b.z
  WHERE g.value >= b.lower_bound AND g.value <= b.upper_bound
	) AS d
join (SELECT h.locationid,
    h.locationcode,
    h.geometrypoint,
    h.x,
    h.y,
    h.z,
    h.filter_top,
    h.filter_bottom,
    round(percentile_cont(0.25::double precision) WITHIN GROUP (ORDER BY h.value)::numeric, 2) AS p25_value,
    round(percentile_cont(0.5::double precision) WITHIN GROUP (ORDER BY h.value)::numeric, 2) AS median_value,
    round(percentile_cont(0.75::double precision) WITHIN GROUP (ORDER BY h.value)::numeric, 2) AS p75_value,
    round(stddev_samp(h.value)::numeric, 2) AS stddev_value,
    h.parametercode,
    h.unitabbrev,
    count(*) AS count,
    percentile_disc(0.5::double precision) WITHIN GROUP (ORDER BY h.datetime) AS median_time,
    min(h.datetime) AS start_time,
    max(h.datetime) AS end_time
   FROM xyzv_analyses_gw h
     JOIN gw_analyses_bounds b ON h.locationid = b.locationid AND h.z = b.z
  WHERE h.value >= b.lower_bound AND h.value <= b.upper_bound
  GROUP BY h.locationid, h.locationcode, h.geometrypoint, h.x, h.y, h.z, h.filter_top, h.filter_bottom, h.parametercode, h.unitabbrev
	) AS i on i.locationid = d.locationid and i.z = d.z
  limit 100