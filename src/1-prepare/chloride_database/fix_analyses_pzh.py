"""
PrZuid-Holland_Cl_db_v2.csv lacks the base data category
'CL-analyses'.

"""
import pandas as pd

path_basedata = "data/2-interim/fresh_salt_PZH_final_20190822_noAI.csv"
path_result = "data/2-interim/GW_Results_Cl_owner_pzhfix.csv"

#,ID,x,y,mv,FilTop,FilBot,date,parameter,value,unit,File.loc,Provincie,Owner
# 31-1-1990 00:00
df = pd.read_csv(path_basedata,parse_dates=["datum"],infer_datetime_format=True,)#, index_col=0)
df = df.loc[df.type.isin(["cl_analyse","CL_analyse","ec"])]
df.loc[df.datum.isnull(),"datum"] = pd.datetime(2000,1,1)

df = df.rename(columns={"id":"ID","maaiveld_AHN2":"mv","depth_nap":"FilTop","datum":"date","clmgl":"value"})
df["FilBot"] = df["FilTop"]
df["parameter"] = "chloride"
df["unit"] = "mg/l"
df["File.loc"] = r"p:\11203718-007-nhi-zoetzout\Grondmetingen_Deltares\PZH\fresh_salt_PZH_final_20190822_noAI.csv"
df["Provincie"] = "Provincie Zuid-Holland"
df["Owner"] = "Deltares"
df.loc[df.bron=="P_Stuyfzand","Owner"] = "P_Stuyfzand"
df = df.reindex(columns=["ID","x","y","mv","FilTop","FilBot","date","parameter","value","unit","File.loc","Provincie","Owner"])
df.to_csv(path_result)