basepath = r'C:\Oppervlaktewater'
resultfile = 'Cl_final'
parameter_id = 'l_db.csv'     # Chloride = 'l_db.csv', EC = 'EGV.csv'
max_value = 18000     # Units: mg/l

import os, glob, pandas, numpy

df = pandas.DataFrame([])
dir_list = []
for root, dirs, files in os.walk(basepath):
    for f in files:
        dir_list.append(os.path.join(root,f))

for key, value in enumerate(dir_list):
    if parameter_id in value:
        if 'Rijkswaterstaat' in value: csv = pandas.read_csv(value, sep=",", quotechar='"')
        else: csv = pandas.read_csv(value,sep='\t')
        if numpy.shape(csv)[1] == 1 : csv = pandas.read_csv(value)
        
        if not 'Portaal' in value: csv['owner'] = value.split('\\')[-2]
        else: csv['owner'] = csv['Beheer']
        
        try: csv['date'] = pandas.to_datetime(csv['date'], dayfirst=True)
        except pandas.errors.OutOfBoundsDatetime:
            for i,val in csv.iterrows():
                try: pandas.to_datetime(val['date'])
                except pandas.errors.OutOfBoundsDatetime: csv.drop([i],inplace=True)
            csv['date'] = pandas.to_datetime(csv['date'], dayfirst=True)
        try:
            csv = csv[csv.value <= max_value]
            csv = csv.groupby(['ID','x','y','parameter','unit','File.loc','owner', pandas.Grouper(key='date',freq='M')]).agg({'value':numpy.mean})
        except:
            csv['value'] = csv['value'].apply(lambda x: float(str(x).replace(',','.')))
            csv = csv[csv.value <= max_Cl_value]
            csv = csv.groupby(['ID','x','y','parameter','unit','File.loc','owner', pandas.Grouper(key='date',freq='M')]).agg({'value':numpy.mean})
        csv['value'] = csv['value'] / 1000   # Change conversions according to case
        csv = csv.reset_index()
        csv['unit'] = ['g/l' for i,val in csv.iterrows()]    # Change units according to case
        csv = csv[['ID','x','y','date','parameter','value','unit','File.loc','owner']]
        df = pandas.concat([df,csv], axis=0, ignore_index=True, sort=False)

df.to_csv(os.path.join(basepath,str(resultfile)+'.csv'))
df.to_pickle(os.path.join(basepath,str(resultfile)+'.pickle'))