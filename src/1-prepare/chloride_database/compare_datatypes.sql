SELECT distinct on (l1.datetime) l1.locationcode, l1.typeid, l1.x, l1.y, l1.z, l1.value, l2.locationcode as locationcode2, l2.typeid as typeid2, l2.x as x2, l2.y as y2, l2.z as z2, l2.value as value2
FROM 
(
	select l.locationid, l.locationcode, l.geometrypoint, l.compartmentid, l.typeid, l.x, l.y, d.z, d.datetime, d.value
	from dataseries d
	join location l on l.locationid = d.locationid
	where l.typeid < 9
	) l1,
(
	select l.locationid, l.locationcode, l.geometrypoint, l.compartmentid, l.typeid, l.x, l.y, d.z, d.datetime, d.value
	from dataseries d
	join location l on l.locationid = d.locationid
	where l.typeid < 9	
	order by d.seriesid desc
	) l2
-- within 50 m xy, and 5 percent on z
WHERE ST_DWithin(l1.geometrypoint, l2.geometrypoint, 50) 
	and abs((l1.z - l2.z) / l1.z) < 0.05
	and l1.typeid < l2.typeid 
	and l1.compartmentid = l2.compartmentid
ORDER BY l1.datetime desc
