# ` -*- coding: utf-8 -*-
# Copyright notice
#   --------------------------------------------------------------------
#   Copyright (C) 2019 Deltares
#       Gerrit Hendriksen
#       gerrit.hendriksen@deltares.nl
#
#   This library is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this library.  If not, see <http://www.gnu.org/licenses/>.
#   --------------------------------------------------------------------
#
# This tool is part of <a href="http://www.OpenEarth.eu">OpenEarthTools</a>.
# OpenEarthTools is an online collaboration to share and manage data and
# programming tools in an open source, version controlled environment.
# Sign up to recieve regular updates of this function, and to contribute
# your own tools.

"""packages"""
import os
import configparser
from pathlib import Path
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, func

# local
from orm_zoetzout import Base, Location, Dataseries, Compartment, Unit, Parameter, Type

"""some functions"""


def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser()
    cf.read(af)
    return cf


# setup connection to database
local = True
if local:
    cf = read_config(r"credentials_zoetzout_local.cfg")
else:
    cf = read_config(r"credentials_zoetzout_remote.cfg")
connstr = (
    "postgres://"
    + cf.get("Postgis", "user")
    + ":"
    + cf.get("Postgis", "pwd")
    + "@"
    + cf.get("Postgis", "host")
    + ":5432/"
    + cf.get("Postgis", "dbname")
)
engine = create_engine(connstr, echo=True)

meta = MetaData(engine)

# create session --> all initialisation work has been done in orm_initialize
Session = sessionmaker(bind=engine)
session = Session()


""" ----------------------------------------
    from here it gets dataset specific
    ----------------------------------------
"""


############ GROUND GEOPHYSICS ############ 
basepath = Path(r"c:\Svnrepos\nhi-fresh-salt\data\1-external\chloride_data")

data_params = {
    "LAS": {
        "attr": "LAS-attributes.csv",
        "data": "LAS-interval-data.csv",
        "id_col": "LASid",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "DinoLoket",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/Well log(s)_Geological borehole research/{id}.las",
        "confidential":False,
        "date":pd.datetime(1970,1,1),
    },
    "GEF": {
        "attr": "GEF-attributes.csv",
        "data": "GEF-interval-data.csv",
        "id_col": "CPTid",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "DinoLoket",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/cpt/{id}.gef",
        "confidential":False,
        "date":pd.datetime(1970,1,1),
    },
    "VES": {
        "attr": "VES-attributes_clean.csv",
        "data": "VES-interval-data.csv",
        "id_col": "VESid",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "DinoLoket",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/Vertical electrical sounding(s) (VES)/{id}.txt",
        "confidential":False,
        "date":pd.datetime(1970,1,1),
    },
    "Deltares_LAS": {
        "attr": "deltares-LAS-attributes_fnorig.csv",
        "data": "deltares-LAS-interval-data.csv",
        "id_col": "id",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "Deltares",
        "orig_file_path": "fn_orig",
        "confidential":True,
        "date":pd.datetime(1970,1,1),
    },
    "Zeeland": {
        "attr": "zeeland-attributes.csv",
        "data": "zeeland-interval-data.csv",
        "id_col": "ID",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "Deltares",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Grondmetingen_Deltares/Freshem/overzichtmetingen.csv",
        "confidential":False,
        "date":pd.datetime(2011,1,1),
    },
    "Skytem-PWN": {
        "attr": "skytem_PWN-attributes.csv",
        "data": "skytem_PWN-interval-data.csv",
        "id_col": "RECORD",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "PWN",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Gegevensuitvraag/Aangeleverd/Grondwater/Waterleidingbedrijf Noord-Holland (PWN)/SkyTEM2011/29L_6B_MOD_inv.xyz",
        "confidential":True,
        "date":pd.datetime(2011,1,1),        
    },
    "PWN-ECPT": {
        "attr": "PWN-GEF-attributes.csv",
        "data": "PWN-GEF-interval-data.csv",
        "id_col": "CPTid",
        "loccode_pattern": "{}",
        "value_col": "Cl (g/l)",
        "owner": "PWN",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Gegevensuitvraag/Aangeleverd/Grondwater/Waterleidingbedrijf Noord-Holland (PWN)/ECPT/{id}.gef",
        "confidential":True,
        "date":pd.datetime(2011,1,1),    
    },
    "Skytem-Cliwat": {
        "attr": "skytem_CliwatFryslan-attributes_250m.csv",
        "data": "skytem_CliwatFryslan-interval-data.csv",
        "id_col": "RECORD",
        "loccode_pattern": "cliwat-skytem-{}",
        "value_col": "Cl (g/l)",
        "owner": "Prov. Fryslan",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Grondmetingen_Deltares/Cliwat_Friesland/S25_2b_april2011_25laags_incl_kopregels_DOI/S25_2b_april2011_25laags_incl_kopregels_DOI.csv",
        "confidential":False,
        "date":pd.datetime(2009,9,14),        
    },
    "HEM-Cliwat": {
        "attr": "HEM_CliwatFryslan-attributes_250m.csv",
        "data": "HEM_CliwatFryslan-interval-data.csv",
        "id_col": "RECORD",
        "loccode_pattern": "cliwat-hem-{}",
        "value_col": "Cl (g/l)",
        "owner": "Prov. Fryslan",
        "orig_file_path": "p:/11203718-007-nhi-zoetzout/Grondmetingen_Deltares/Cliwat_Friesland/HEM137_INV_RD_juni2011/HEM137_INV_RD_juni2011.csv",
        "confidential":False,
        "date":pd.datetime(2009,8,1),        
    },
}
datatypes = {
    "LAS": "BL",
    "GEF": "ECPT",
    "VES": "VES",
    "Deltares_LAS": "BL",
    "CPT": "ECPT",
    "EM-Slimflex": "SF",
    "Open hole borehole logs": "BL",
    "TEC Probe": "TEC",
    "Skytem-PWN":"AEM",
    "PWN-ECPT":"ECPT",
    "Skytem-Cliwat":"AEM",
    "HEM-Cliwat":"AEM",
}

run_datatypes = ["Skytem-Cliwat","HEM-Cliwat"]  # "LAS","GEF","Deltares_LAS","VES","Zeeland","Skytem-PWN","PWN-ECPT"]
for datatype in run_datatypes:
    params = data_params[datatype]
    attrs = pd.read_csv(basepath / params["attr"], index_col=0)
    if datatype == "Deltares_LAS":
        attrs["well"] = attrs["well_corr"]
    data = pd.read_csv(basepath / params["data"])
    id_ = params["id_col"]
    valuecol = params["value_col"]
    owner = params["owner"]
    confidential = params["confidential"]

    # get relationship ids
    if datatype != "Zeeland":
        type_ = session.query(Type).filter_by(typecode=datatypes[datatype]).first()
    compartment_ = session.query(Compartment).filter_by(compartmentcode="GW").first()
    parameter_ = session.query(Parameter).filter_by(parametercode="Cl").first()
    unit_ = session.query(Unit).filter_by(unitabbrev="g/L").first()

    # loop through attrs and store in database
    for i, attr in attrs.iterrows():

        data_sel = data.loc[data[id_] == attr[id_]]
        if len(data_sel):  # only if there are data for this attr
            #if "LAS" in datatype:
            #    nitgcode = attr["well"]
            #else:
            #    nitgcode = None
            if datatype == "Zeeland":
                # skip borehole logs from Zeeland, duplicates with Dinoloket
                if datatypes[attr["Type"]] == "BL":
                    continue

                type_ = (
                    session.query(Type)
                    .filter_by(typecode=datatypes[attr["Type"]])
                    .first()
                )
            if datatype == "Deltares_LAS":
                fn_orig = attr[params["orig_file_path"]]
            else:
                fn_orig = params["orig_file_path"].format(id=attr[id_])

            a = Location(
                locationcode=params["loccode_pattern"].format(attr[id_]),
                typeid=type_.typeid,
                #nitgcode=nitgcode,
                owner=owner,
                confidential=confidential,
                compartmentid=compartment_.compartmentid,
                x=attr["x"],
                y=attr["y"],
                file_origin=fn_orig,
            )
            session.add(a)
            session.flush()
            # get the id of the last inserted data
            locationid = a.locationid
            session.commit()

            if "date" not in attr or pd.isnull(attr["date"]):
                attr["date"] = params["date"]
            if pd.to_datetime(attr["date"]) > pd.Timestamp.now():
                # most likely YY converted to 20YY i/o 19YY
                attr["date"] = pd.to_datetime(attr["date"]) - pd.Timedelta(seconds=100*31556952)  # subtract 100 years worth of seconds

            # define temporary dataframe
            dft = data_sel.copy()
            dft = dft.reindex(columns=["z", valuecol])
            dft = dft.rename(columns={valuecol: "value"})
            dft["locationid"] = locationid
            dft["parameterid"] = parameter_.parameterid
            dft["datetime"] = attr["date"]
            dft["unitid"] = unit_.unitid
            dft = dft.dropna(subset=["value"]).reset_index(drop=True)
            # retrieve max id of dataid
            s = session.query(func.max(Dataseries.seriesid).label("id")).one().id
            if not s:  # str(s) == 'None':
                s = 0
            else:
                dft.index += s + 1
            dft.index.rename("seriesid", inplace=True)
            dft.to_sql(
                "dataseries",
                con=engine,
                if_exists="append",
                schema="public",
                index=True,
            )

            # break

# finally update the column geometrypoint
strsql = (
    """update public.location set geometrypoint = st_setsrid(st_point(x,y),28992)"""
)
engine.execute(strsql)


session.close()
