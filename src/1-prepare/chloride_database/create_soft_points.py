# -*- coding: utf-8 -*-
"""
Script to create "soft points", as a representation of qualitative system knowledge, 
to aid in interpolation of groundwater chloride concentration

Steps:
- Get the interpolation domain
- Define soft points to represent:
  - seaward boundary DONE
    - from sea concentration
  - saline groundwater at depth  DONE
    - diffusion profile from uppermost connate aquitard below GHB
    - maps of TNO
  - infiltrating surface water  TODO
    - map of infiltrating surface water (LHM)
    - surface water concentration
  - rainwater? or by 'inspeel' model run?  TODO
  - wells?   TODO
- Store in database with clear denomination of soft point and large variance

Regarding saline groundwater at depth:
Procedure is to go from base of NHI zz downward. The first data source that is encountered is used.
- map TNO fresh-brackish interface (150 mg/l). Set soft point at top of depth class, with concentration of 150 mg/l. Top class (< 100m -NAP) is discarded, always above base
- map TNO brackish-saline interface (1000 mg/l). Set soft point at top of depth class, with concentration of 1000 mg/l. Use nearest to fill map.
- Holocene extent of maximum transgression: set soft point at bottom of NHI zz layermodel
- REGIS II.2. Set soft point at uppermost marine clay layer, with concentration of 18000 mg/l 
  - Exception for Twenthe?: while there are marine formations present at shallow depths, containing connate marine water,
    continuous infiltration of fresh water will have prevented a diffusion profile.
    KEEP THEM FOR NOW

Regarding surface water salinity:
- Define boundary with shapefile
- Use surface water salinity within this shapefile (from Delft3D-FM model run)
- Copy boundary condition down every 50m to 400m -NAP
"""
from pathlib import Path
import pandas as pd
import geopandas as gpd
import xarray as xr
import numpy as np
import imod
import re
#from pyproj import CRS, transform

"""REGIS Mariene lagen:
Formatie van Naaldwijk
Eem Formatie
Formatie van Maassluis
Formatie van Oosterhout
Formatie van Breda
Formatie van Veldhoven
Rupel Formatie
Formatie van Tongeren
Formatie van Dongen
Formatie van Landen
['AK', 'AP', 'BERO', 'BE', 'BR', 'BXLM', 'BXSC', 'BX', 'DN', 'DOAS',
       'DOIE', 'DO', 'DRGI', 'DRUI', 'DR', 'DT', 'EE', 'GU', 'HL', 'HO',
       'HT', 'IE', 'KI', 'KL', 'KRTW', 'KRWY', 'KRZU', 'KR', 'KW', 'LA',
       'MS', 'MT', 'OO', 'PE', 'PZWA', 'PZ', 'RUBO', 'RU', 'ST', 'SY',
       'TOGO', 'TOZEWA', 'TO', 'UR', 'VA', 'VEVO', 'VI', 'WA', 'WB']
"""
regis_marine = ["OO","BR","VEVO","RU","RUBO","TOGO", "TOZEWA", "TO","DOAS","DOIE","DO","LA"] #"NA","EE","MS",
geotop_holocenetransgression = [1020,1030,1040,1050,1080,1100,1110,1120]

top_bot_path = "p:/11203718-007-nhi-zoetzout/NHI_zz_basic/data/1-external/top_bot.nc"
regis_path = "c:/Svnrepos/nhi-fresh-salt/data/1-external/REGIS_v2_2/REGIS_v2_2.nc"
geotop_path = "c:/Svnrepos/nhi-fresh-salt/data/1-external/GeoTOP/GeoTop.nc"
tno_freshbrackish_path = "p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/zoetzout-kaarten/zoet_brak/zoetbrak_grensvlak.shp"
tno_brackishsalt_path = "p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/zoetzout-kaarten/brak_zout/Brakzout_mnap1.img"
maxtransgr_path = "c:/Svnrepos/nhi-fresh-salt/data/1-external/transgression/transgression_extent.shp"
like_2d_path = "c:/Svnrepos/nhi-fresh-salt/data/2-interim/template_2d.nc"
surfw_boundary_path = "p:/11203718-007-nhi-zoetzout/Oppervlaktewater/surfwater_boundary.shp"
surfw_boundary_cl_path = "p:/11203718-007-nhi-zoetzout/Oppervlaktewater/chloride_gL_mean_2012.idf"
result_path = Path("c:/Svnrepos/nhi-fresh-salt/data/2-interim")

regis = xr.open_dataset(regis_path)
regis_top = regis["top"]
#geotop = xr.open_dataset(geotop_path)["lithostrat"]
top_bot = xr.open_dataset(top_bot_path)
z_interval = 1. # resample to z_interval of ...
regis_p = re.compile(r"([A-Z]+)([kzcqsbv])[0-9]*")  # (formation) (clay / sand / complex)

mode_regridder = imod.prepare.Regridder("mode",ndim_regrid=1)
#geotop_is_sand = {-1:False,0:False,1:False,2:False,3:False,4:False,5:True,6:True,7:True,8:False,9:False,np.nan:np.nan}
coarsen_kernel = {"x":20,"y":20}  # 5km x 5km
z_step = 50

def get_columns(df):
    is_ec = None
    z = None
    ec = None
    df = df.dropna(axis=1,how="all") # drop empty columns
    for c in df.columns:
        if "depth" in c.lower() or "diepte" in c.lower():
            z = c
        elif "boven" in c.lower() or "onder" in c.lower():
            if z is None:
                z = []
            z.append(c)
        elif "geleidbaarheid" in c.lower() or "cond" in c.lower():  # CPT, sometimes also cond in LAS...
            ec = c
            is_ec = True
        elif ec is None and ("ln" in c.lower() or "res" in c.lower() or "n100" in c.lower() or "rs" in c.lower()):
            ec = c
            is_ec = False
    return z, ec, is_ec

def z_from_topbot(df,interval=1.,top="top",bot="bot"):
    # sort
    df = df.sort_values(by=top,ascending=False)

    # conform top values to interval, so they don't get lost in reindexing
    # first value: must ceil to include original value, others to closest
    idx = interval * (df[top] / interval).round(0)
    idx.iloc[0] = interval * (df[top].iloc[0] // interval) + interval
    df.index = idx - 0.5 * interval
    df = df[~df.index.duplicated()]

    # ztop, zbot
    ztop = df.index[0]
    zbot = interval * (df[bot].iloc[-1] // interval) + 0.5 * interval # to include orig bot
    
    idx = pd.Index(np.linspace(ztop, zbot, int((ztop-zbot)//interval+1)), name="z_interval")
    return df.reindex(index=idx, method="ffill")

def appelboor_regis(ds_regis, x, y, top=None, bot=None):
    """"
    Functie om uit de netcdf REGIS dataset een appelboor-steekproef te nemen.
    
    Input: fn_regis: str or Path() naar REGIS-netcdf
    x: x-locatie RDnew
    y: y-locatie RDnew
    diepte_min: bovenkant van appelboor in m NAP
    diepte_max: onderkant van appelboor in m NAP
    
    Output: xr DataArray with numerical codes ready for regridder, and translator to go back to formation names

    (from Simon Buijs)
    """
    
    df = ds_regis.sel(x=x,y=y, method='nearest', tolerance=100.).drop(['x','y']).to_dataframe()
    df = df.dropna(axis=0, how='all')
    df = df.sort_values(by='top',ascending=False).reset_index()
    if top is not None and bot is not None:
        sel = (df['bot']<= top) & (df['bot'] >= bot) | (df['top']<= top) & (df['top'] >= bot)
        df = df[sel]
    
    # to dataset
    df["dz"] = df["top"] - df["bot"]
    df["z"] = 0.5*df["top"] + 0.5 * df["bot"]
    translator = df["formation"]
    df = df.reset_index()
    ds = df.set_index("z").to_xarray()
    ds = ds.set_coords("dz")
    return ds["index"], translator

like_2d = xr.open_dataset(like_2d_path)["template"]
like_2d["dx"] = 250.
like_2d["dy"] = -250.
base_lhmzz = top_bot.sel(layer=40)["bot"].drop(["dx","dy","layer"])

# TNO FRESH-BRACKISH INTERFACE
gdf = gpd.read_file(tno_freshbrackish_path)
gdf["interface_depth"] = gdf["Text"].map({"<100":1e6,"100-200":-100,"200-300":-200,"300-400":-300,"400-500":-400,">500":-500})
freshbrack = imod.prepare.rasterize(gdf,like_2d,column="interface_depth")
freshbrack = imod.prepare.fill(freshbrack)
imod.idf.write(result_path/"freshbrack.idf",freshbrack)

# TNO BRACKISH-SALINE INTERFACE
bracksalt = imod.rasterio.open(tno_brackishsalt_path)
bracksalt = imod.prepare.reproject(bracksalt, like_2d)
bracksalt["dx"] = like_2d["dx"]
bracksalt["dy"] = like_2d["dy"]
bracksalt = imod.prepare.fill(bracksalt)
imod.idf.write(result_path/"bracksalt.idf",bracksalt)

# MAXIMUM TRANSGRESSION
# Get maximum transgression from shapefile based on paleomaps
gdf = gpd.read_file(maxtransgr_path)
gdf["TRANSGR"] = 1.
maxtransgr = imod.prepare.rasterize(gdf,like_2d,column="TRANSGR")
maxtransgr = base_lhmzz.where(maxtransgr==1)
imod.idf.write(result_path/"maxtransgression.idf",maxtransgr)

## UPPERMOST REGIS MARINE LAYER
def is_regis_marine(fm):
    m = regis_p.match(fm)
    return m[1] in regis_marine and m[2] in ["k","c"]

is_m = [is_regis_marine(fm) for fm in regis_top.formation.values]
top_marine = regis_top.sel(formation=is_m)

# reproject to REGIS and then back
base_lhmzz_regis = imod.prepare.reproject(base_lhmzz, top_marine.isel(formation=0))
top_marine = top_marine.where(top_marine < base_lhmzz_regis)
upper_top_marine = top_marine.max(dim="formation")
upper_top_marine = imod.prepare.reproject(upper_top_marine, like_2d)
imod.idf.write(result_path/"upper_top_marine_claycomplex_ghb.idf",upper_top_marine)

# MERGE THE FOUR DATASETS
soft_freshbrack = freshbrack.where(freshbrack < base_lhmzz)
soft_bracksalt = bracksalt.where(bracksalt < base_lhmzz)
soft_maxtransgr = base_lhmzz.where(maxtransgr)
soft_regis = upper_top_marine
# combine z values
soft_z = soft_freshbrack.combine_first(soft_bracksalt)
soft_z = soft_z.combine_first(soft_maxtransgr)
soft_z = soft_z.combine_first(soft_regis)
soft_z = soft_z.fillna(-1000.)
imod.idf.write(result_path/"soft_z.idf",soft_z)

soft_freshbrack_cl = (xr.ones_like(like_2d)*0.15).where(~soft_freshbrack.isnull())
soft_bracksalt_cl = (xr.ones_like(like_2d)*1.0).where(~soft_bracksalt.isnull())
soft_maxtransgr_cl = (xr.ones_like(like_2d)*16.8).where(~maxtransgr.isnull())
soft_regis_cl = (xr.ones_like(like_2d)*16.8).where(~soft_regis.isnull())
# combine cl values
soft_cl = soft_freshbrack_cl.combine_first(soft_bracksalt_cl)
soft_cl = soft_cl.combine_first(soft_maxtransgr_cl)
soft_cl = soft_cl.combine_first(soft_regis_cl)
soft_cl = soft_cl.fillna(16.8)
imod.idf.write(result_path/"soft_cl.idf",soft_cl)

softtypes = {1:"SOFT_FRESHBRACK",2:"SOFT_BRACKSALT",3:"SOFT_TRANSGR",4:"SOFT_CONNATE",5:"SOFT_DEEP"}
soft_freshbrack_ty = (xr.ones_like(like_2d)).where(~soft_freshbrack.isnull())
soft_bracksalt_ty = (xr.ones_like(like_2d)*2).where(~soft_bracksalt.isnull())
soft_maxtransgr_ty = (xr.ones_like(like_2d)*3).where(~maxtransgr.isnull())
soft_regis_ty = (xr.ones_like(like_2d)*4).where(~soft_regis.isnull())
# combine cl values
soft_type = soft_freshbrack_ty.combine_first(soft_bracksalt_ty)
soft_type = soft_type.combine_first(soft_maxtransgr_ty)
soft_type = soft_type.combine_first(soft_regis_ty)
soft_type = soft_type.fillna(5)
imod.idf.write(result_path/"soft_type.idf",soft_cl)

# EXPORT TO POINT DATA SET
soft_deep_salinity = soft_cl.assign_coords({"z":soft_z, "typeid":soft_type})
soft_deep_salinity = soft_deep_salinity.where(~base_lhmzz.isnull())
#soft_deep_salinity = soft_deep_salinity.coarsen(coarsen_kernel).mean()
soft_deep_salinity.name = "deep_cl"
soft_deep_salinity = soft_deep_salinity.drop(["dx","dy"])
df_soft_deep_salinity = soft_deep_salinity.to_dataframe().dropna().reset_index().reindex(columns=["x","y","z","typeid","deep_cl"])
df_soft_deep_salinity["type"] = df_soft_deep_salinity["typeid"].map(softtypes)
df_soft_deep_salinity.to_csv(result_path / "soft_points_deep_salinity.csv",index=False)
imod.ipf.write(result_path / "soft_points_deep_salinity.ipf",df_soft_deep_salinity)

# EXPORT TO COARSENED POINT DATA SET
soft_deep_salinity_crsnd = soft_deep_salinity.coarsen(coarsen_kernel, boundary="trim", coord_func="median").min()
soft_deep_salinity_crsnd["typeid"] = soft_deep_salinity_crsnd["typeid"].round(0)
soft_deep_salinity_crsnd.name = "deep_cl"
df_soft_deep_salinity_crsnd = soft_deep_salinity_crsnd.to_dataframe().dropna().reset_index().reindex(columns=["x","y","z","typeid","deep_cl"])
df_soft_deep_salinity_crsnd["type"] = df_soft_deep_salinity_crsnd["typeid"].map(softtypes)
df_soft_deep_salinity_crsnd.to_csv(result_path / "soft_points_deep_salinity_coarsened.csv",index=False)
imod.ipf.write(result_path / "soft_points_deep_salinity_coarsened.ipf",df_soft_deep_salinity_crsnd)

# SURFACE WATER SALINITY TO SOFT POINTS
# FOR NOW: ONLY NORTH SEA / WADDEN SEA / SCHELDT ESTUARY
gdf_sw = gpd.read_file(surfw_boundary_path)
surfw_boundary = imod.prepare.rasterize(gdf_sw,like_2d)

surfw_boundary_cl = imod.idf.open(surfw_boundary_cl_path)
surfw_boundary_cl = (xr.ones_like(like_2d) * 18.).where(surfw_boundary_cl > 18, surfw_boundary_cl)
surfw_boundary_cl = surfw_boundary_cl.where(~surfw_boundary.isnull())
surfw_boundary_cl_crsnd = surfw_boundary_cl.coarsen(coarsen_kernel).mean()
imod.idf.write(result_path / "surfw_boundary_cl.idf",surfw_boundary_cl)
surfw_boundary_cl_crsnd.name = "sea_cl"
surfw_boundary_cl_crsnd = surfw_boundary_cl_crsnd.drop(["dx","dy"])
z_idx = pd.Index(range(0,-400 + z_step,-z_step),name="z")
surfw_boundary_cl_crsnd = xr.concat([surfw_boundary_cl_crsnd]*len(z_idx),z_idx)
df_surfw_boundary_cl_crsnd = surfw_boundary_cl_crsnd.to_dataframe().dropna().reset_index().reindex(columns=["x","y","z","sea_cl"])
df_surfw_boundary_cl_crsnd.to_csv(result_path / "soft_points_sea_salinity_coarsened.csv",index=False)
imod.ipf.write(result_path / "soft_points_sea_salinity_coarsened.ipf",df_surfw_boundary_cl_crsnd)
