"""
Script to combine kriging result and delft-FM North Sea result, resample to 250m resolution
"""
import geopandas as gpd
import xarray as xr
import imod
from pathlib import Path

basepath = Path("c:/Svnrepos/nhi-fresh-salt")
data_paths = {"sw_kriging": basepath / "data/1-external/2D-oppwater-chloride/Chloride_2D_interpolation.tif",
              "dfm_result": basepath / "data/1-external/2D-oppwater-chloride/chloride_gL_mean_2012.idf",
              "water_systems": basepath / "data/1-external/2D-oppwater-chloride/Watersystems_chloride_interpolation_dissolve.shp",
              "like":basepath / "data/2-interim/template_2d.nc",
              "result": basepath / "data/2-interim/chloride_2d_gL_250m",
                }

like = xr.open_dataset(data_paths["like"])["template"]
sw_kriging = imod.rasterio.open(data_paths["sw_kriging"])
dfm_result = imod.idf.open(data_paths["dfm_result"])
water_systems = gpd.read_file(data_paths["water_systems"])

# regrid sw_kriging naar 250m (en g/L)
regridder = imod.prepare.Regridder("median")
sw_kriging_250m = regridder.regrid(sw_kriging, like) / 1000.

# North Sea: rasterize water_systems and use for combining sw_kriging and dfm_result
water_systems = water_systems.loc[water_systems.FIRST_NAAM=="Noordzee"]
northsea = imod.prepare.rasterize(water_systems, like=like)

# combine
swconc = dfm_result.where(~northsea.isnull(),sw_kriging_250m)

# fill and cut to within 0 - 18
swconc = imod.prepare.fill(swconc)
swconc = swconc.where(swconc > 0., 0.)
swconc = swconc.where(swconc < 18., 18.)

imod.idf.write(data_paths["result"], swconc)
imod.rasterio.write(f"{data_paths['result']}.tif", swconc, driver="GTiff")