SELECT l1.locationid, l1.locationcode, l1.owner, l1.x, l1.y, l2.locationid, l2.locationcode, l2.owner, l2.x, l2.y
FROM location l1, location l2
WHERE ST_DWithin(l1.geometrypoint, l2.geometrypoint, 1.0) 
	and l1.typeid = l2.typeid 
	and l1.compartmentid = l2.compartmentid
	and l1.filter_top = l2.filter_top
	and (l1.owner <> l2.owner or l1.locationcode = l2.locationcode)
	and l1.locationid < l2.locationid 
