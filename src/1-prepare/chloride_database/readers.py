# -*- coding: utf-8 -*-
"""
Different reader functions for fresh-salt data

TODO: formalize testing
"""
import pandas as pd
import re
from pathlib import Path


def read_VES(fn):
    """
    Read <VES>.txt textfile from DINO database. Sometimes multiple interpretations exist,
    it now only reads the first one it encounters. 

    Parameters:
    fn : Filename

    Output:
    attrs : dictionary of attributes
    meas_data : measurement data VES (pd.DataFrame)
    interp_data : interpreted data (pd.DataFrame)

    Expected format of textfile:
    <location id>;
    <geogr>;  (RD)
    <x>,<y>;
    <z>;

    Kop VES data;  --> 16 lines
    <owner?>;
    ;
    <date:dd-mm-YYYY>;
    ;
    <?>74;
    ;
    <configuration>;  SCHLUMB
    <L/2>150.0;
    ;
    ;
    <quality_measurements>REDELIJK/GOED;
    ;
    ;
    ;
    ;
    ;

    VES data;
    L/2;a;V;I;R;
    <<DATABLOCK>>

    Kop VES interpretatie; --> 12 lines
    ;
    ;
    ;
    ;
    ;
    ;
    ;
    <quality_interpretation>SLECHT;
    <maxdepth_interpretation>; ??
    ;
    ;
    ;

    VES interpretatie;
    boven;onder;Rs;lithologie;grondwater;geologie;
    <<DATABLOCK>>
    """

    def _float(s):
        if s == "":
            return None
        try:
            return float(s)
        except ValueError:
            return s

    def _process_line(f, split=";"):
        l = f.readline()
        if l == "":
            raise EOFError()
        l = l.replace(";\n", "", 1)
        l = l.strip()
        ls = l.split(split)
        ls = [_float(v) for v in ls]
        if len(ls) > 1:
            return ls
        else:
            return ls[0]

    def _skip_line(f, n=1):
        for i in range(n):
            l = f.readline()
            if l == "":
                raise EOFError()

    def _search_line(f, s):
        while 1:
            l = _process_line(f)  # counting on _process_line to throw error at EOF
            if l == s:
                return

    attrs = {}
    with open(fn, "r") as f:
        attrs["VESid"] = _process_line(f)
        attrs["coord"] = _process_line(f)
        attrs["x"], attrs["y"] = _process_line(f, split=",")
        attrs["z"] = _process_line(f, split=",")

        # Kop VES data
        _search_line(f, "Kop VES data")
        _skip_line(f, 2)
        attrs["date"] = pd.to_datetime(_process_line(f), dayfirst=True)
        _skip_line(f, 3)
        attrs["configuration"] = _process_line(f)
        attrs["L/2"] = _process_line(f)
        _skip_line(f, 2)
        attrs["quality_measurement"] = _process_line(f)

        # VES data
        _search_line(f, "VES data")
        header = _process_line(f)
        data = []
        while 1:
            datal = _process_line(f)
            if datal is None:
                break
            data += [datal]
        meas_data = pd.DataFrame(data=data, columns=header).dropna(axis=1, how="all")

        # Kop VES interpretatie - empty if no interpretation
        _search_line(f, "Kop VES interpretatie")
        try:
            _skip_line(f, 7)
        except EOFError:
            attrs["has_interpretation"] = False
            return pd.Series(attrs), meas_data, None
        attrs["quality_interpretation"] = _process_line(f)
        attrs["maxdepth_interpretation"] = _process_line(f)  # not sure of this one
        _skip_line(f, 4)

        # VES interpretatie;
        _search_line(f, "VES interpretatie")
        header = _process_line(f)
        data = []
        while 1:
            try:
                datal = _process_line(f)
            except EOFError:
                break
            if datal is None:
                break
            data += [datal]

        interp_data = pd.DataFrame(data=data, columns=header).dropna(axis=1, how="all")
        attrs["has_interpretation"] = True

    return pd.Series(attrs), meas_data, interp_data


def read_GEF(fn, only_ecpt=True, only_conductivity_column=False):
    """
    Read <CPT>.gef textfile from DINO database. 

    Parameters:
    fn : Filename
    only_ecpt : Return only CPTs that measured conductivity (bool, defaults to True)
    only_conductivity_column : Return only conductivity column of ECPT (bool, defaults to False)

    Output:
    attrs : dictionary of attributes
    meas_data : measurement data CPT (pd.DataFrame)

    Expected format of textfile:
    header with keywords, denoted by #<keyword>=<value>
    datablock

    Possible data / columns:
    ['sondeertrajectlengte' 'verlopen tijd' 'conusweerstand' 'diepte'
    'plaatselijke wrijving' 'waterspanning u1' 'hellingresultante'
    'waterspanning u2' 'elektrische geleidbaarheid' 'helling oost-west'
    'helling x' 'wrijvingsgetal' 'gecorrigeerde conusweerstand'
    'helling noord-zuid' 'magnetische veldsterkte x' 'netto conusweerstand'
    'waterspanning u3' 'helling y' 'magnetische veldsterkte y' 'porienratio'
    'magnetische veldsterkte z' 'totale magnetische veldsterkte'
    'temperatuur' 'magnetische inclinatie' 'magnetische declinatie']
        """

    pattern = re.compile(r"#(?P<key>\w+) *= *(?P<value>.*)[ \n]*")

    def _process_line(line):
        m = pattern.match(line)
        if not m:
            print(f"//{line}//")
        return m["key"], m["value"]

    attrs = {}
    header = {}
    units = {}
    nanvals = {}
    attrs["is_ecpt"] = False
    attrs["ec_col"] = None
    meas_data = None
    sep = None
    with open(fn, "r") as f:
        for line in f:
            key, value = _process_line(line)
            if key == "COLUMNINFO":
                vs = value.split(",")
                header[int(vs[0])] = vs[2].strip()
                units[int(vs[0])] = vs[1].strip()

                if "geleidbaarheid" in vs[2]:
                    attrs["is_ecpt"] = True
            elif key == "COLUMNSEPARATOR":
                sep = value
            elif key == "COLUMN":
                ncols = int(value)
            elif key == "COLUMNVOID":
                vs = value.split(",")
                nanvals[int(vs[0])] = vs[1].strip()
            elif key == "STARTDATE":
                attrs["date"] = pd.to_datetime(value, yearfirst=True)
            elif key == "TESTID":
                attrs["CPTid"] = value
            elif key == "XYID":
                vs = value.split(",")
                attrs["coord"] = vs[0]
                if attrs["coord"] == "4326":
                    attrs["lat"] = float(vs[1])
                    attrs["lon"] = float(vs[2])
                else:
                    attrs["x"] = float(vs[1])
                    attrs["y"] = float(vs[2])
            elif key == "ZID":
                vs = value.split(",")
                attrs["z"] = float(vs[1])
            elif key == "EOH":  # End of header
                break

        names = [f"{header[i+1]} - {units[i+1]}" for i in range(ncols)]
        na_values = {names[i]: nanvals[i + 1] for i in range(ncols)}
        if attrs["is_ecpt"]:
            attrs["ec_col"] = [c for c in names if "geleidbaarheid" in c.lower()][0]
        if attrs["is_ecpt"] or not only_ecpt:
            # read data
            meas_data = pd.read_csv(
                f,
                sep=sep,
                header=None,
                usecols=list(range(ncols)),
                names=names,
                na_values=na_values,
            )

            # if no depth column: recalculate depth  TODO: adjust for hellingshoek
            if "diepte" not in header:
                c = [c for c in names if "sondeer" in c.lower()][0]
                meas_data["diepte - m (meter)"] = meas_data[c]

            if only_ecpt and only_conductivity_column:
                depth_col = [c for c in names if "diepte" in c.lower()][0]
                meas_data = meas_data.reindex(
                    columns=[
                        "diepte - m (meter)",
                        "elektrische geleidbaarheid - S/m (Siemens/meter)",
                    ]
                )

        columninfo = pd.Series(header, name=attrs["CPTid"])
        # attrs["colums"] = names
        # attrs["units"] = [units[i + 1] for i in range(ncols)]

        return pd.Series(attrs), columninfo, meas_data


def read_LAS(fn, only_ln=True, only_ln_column=False):
    """
    Read <borelog>.LAS textfile from DINO database. 

    Parameters:
    fn : Filename
    only_ln : Return only borelogs that measured long normal resistivity (bool, defaults to True)
    only_ln_column : Return only depth and the column with long normal data (bool, defaults to False)

    Output:
    attrs : dictionary of attributes
    meas_data : measurement data borelog (pd.DataFrame), None if only_ln and not a long-normal resistivity log

    Expected format of textfile:
    header with keywords, denoted by:
    <keyword>.<unit> <value>:<description>

    datablock

    --> no unique code in LAS files, use well and make sure no duplicates
    --> no x-y coordinates...
    """

    def _float(s):
        if not s:
            return pd.np.nan
        try:
            return float(s)
        except ValueError:
            return s

    pattern = re.compile(
        r" *(?P<key>[^.]+?)[ \t]*\.(?P<unit>[^ \t]*)[ \t]+(?P<value>[^:]*?)[ \t]*:[ \t]*(?P<desc>[^\\\n]*)[ \\par\n]*"
    )

    def _process_line(line):
        m = pattern.match(line)
        if not m:
            raise ValueError(f"Regex could not match //{line}//")
        return m["key"], m["unit"], m["value"], m["desc"]

    blockpattern = re.compile(r"~(?P<block>[^\t ]+?)[ \t]+.*")

    def _get_block(line):
        m = blockpattern.match(line)
        if not m:
            raise ValueError(f"Block Regex could not match //{line}//")
        return m["block"].lower()

    def _rename_duplicates(l):
        if len(l) < 2:
            return l
        dups = {il: 0 for il in l}
        l2 = l.copy()
        for i in range(1, len(l)):
            if l[i] in l[:i]:
                dups[l[i]] += 1
                l2[i] = f"{l[i]}_{dups[l[i]]:03d}"
        return l2

    ln_pattern = re.compile(r".*?([0-9]+).*?")

    def _choose_ln_col(ln_cols):
        """choose ln column with highest electrode distance"""
        if len(ln_cols) > 1:
            ln_nr = [
                float(ln_pattern.match(lc)[1]) if ln_pattern.match(lc) else -1
                for lc in ln_cols
            ]
            idx = ln_nr.index(max(ln_nr))
            return ln_cols[idx]
        else:
            return ln_cols[0]

    ln_keys = [
        "ln",
        "ln10",
        "ln100",
        "ln16",
        "ln160",
        "ln2",
        "ln20",
        "ln200",
        "ln2000",
        "ln64",
        "res(64)",
        "res(64N)",
        "n100",
    ]
    cond_keys = ["cond"]
    attrs = {
        "LASid": Path(fn).stem
    }  # as there is no clear unique id for the well log, use the filename of the LAS file
    block = None
    header = []
    has_LN = False
    ln_col = []
    with open(fn, "r") as f:
        for line in f:
            if line[0] in ["#", "{", "\n"]:
                # comment
                continue
            elif line[0] == "~":
                # block signifier
                block = _get_block(line)
                if block == "a":
                    # start of data block, exit loop
                    break
            else:
                if block in ["version", "other"]:
                    continue
                else:
                    key, unit, value, desc = _process_line(line.lower())
                    if block == "curve":
                        if key in ln_keys:
                            has_LN = True
                            header += [f"{key} (ohm-m)"]
                            ln_col += [header[-1]]
                        elif key in cond_keys:
                            has_LN = True
                            header += [f"{key} ({unit})"]
                            ln_col += [header[-1]]
                        else:
                            header += [f"{key} ({unit})"]
                    elif block == "well" or (
                        block == "parameter"
                        and key
                        in ["lat", "lon", "locationx", "locationy", "locx", "locy"]
                    ):
                        if unit:
                            attrs[f"{key} ({unit})"] = _float(value)
                        else:
                            attrs[key] = _float(value)
                    else:
                        continue
        attrs = pd.Series(attrs)
        attrs["has_LN"] = has_LN
        if has_LN:
            attrs["LN_column"] = _choose_ln_col(ln_col)
        if "date" in attrs:
            try:
                attrs["date"] = pd.to_datetime(attrs["date"], dayfirst=True)
            except ValueError:
                attrs["date"] = pd.NaT
        if "null" not in attrs:
            attrs["null"] = None

        # Read data block
        columninfo = pd.Series(header, name=attrs["LASid"])
        meas_data = None
        if has_LN or not only_ln:
            meas_data = pd.read_csv(
                f,
                delim_whitespace=True,
                header=None,
                names=_rename_duplicates(header),
                na_values=str(attrs["null"]),
            )
            meas_data = meas_data.rename(columns={"dept (m)": "depth (m)"})

            if only_ln and only_ln_column:
                # drop all columns except depth and ln column
                meas_data = meas_data.reindex(columns=["depth (m)", attrs["LN_column"]])

    return attrs.dropna(), columninfo, meas_data


def read_ASC(fn, only_ln=True, only_ln_column=False):
    """
    Read <borelog>.ASC textfile from ANTARES output

    Parameters:
    fn : Filename
    only_ln : Return only borelogs that measured long normal resistivity (bool, defaults to True)
    only_ln_column : Return only depth and the column with long normal data (bool, defaults to False)

    Output:
    attrs : dictionary of attributes
    meas_data : measurement data borelog (pd.DataFrame), None if only_ln and not a long-normal resistivity log

    Expected format of textfile:
    header with keywords, denoted by:
    <keyword>.<unit> <value>:<description>

    datablock
    """

    def _float(s):
        if not s:
            return pd.np.nan
        try:
            return float(s)
        except ValueError:
            return s

    pattern = re.compile(r"#\s+(?P<key>[^:]+)\s*:\s*(?P<value>\S*)\s*")
    comment_pattern = re.compile(r"#\s+$")  # comments are only "# "

    def _process_line(line):
        m = pattern.match(line)
        if not m:
            raise ValueError(f"Regex could not match //{line}//")
        return m["key"], m["value"]

    ln_keys = ["lono.multi", "lono"]
    attrs = {
        "ASCid": Path(fn).stem
    }  # as there is no clear unique id for the well log, use the filename of the ASC file
    block = None
    header = []
    with open(fn, "r") as f:
        for line in f:
            if comment_pattern.match(line):
                # comment
                continue
            elif line[:5] == "#####":
                # block signifier
                if block is None:
                    block = "header"
                else:
                    # data block, break out of loop
                    break
            else:
                key, value = _process_line(line.lower())
                attrs[key] = _float(value)

        attrs = pd.Series(attrs)
        if "date" in attrs:
            try:
                attrs["date"] = pd.to_datetime(attrs["date"], dayfirst=True)
            except ValueError:
                attrs["date"] = pd.NaT

        # Read data block
        meas_data = pd.read_csv(f, delim_whitespace=True, header=[0, 1])

        # merge multiindex header column to parameter (unit)
        colnames = meas_data.columns
        attrs["has_LN"] = False
        for i, k in enumerate(colnames.get_level_values(0)):
            if k.lower() in ln_keys:
                attrs["has_LN"] = True
                attrs["LN_column"] = i
        colnames = [
            f"{p.lower()} ({u.lower()})"
            for p, u in zip(colnames.get_level_values(0), colnames.get_level_values(1))
        ]
        if attrs["has_LN"]:
            attrs["LN_column"] = colnames[attrs["LN_column"]]
        meas_data.columns = colnames

        meas_data = meas_data.rename(
            columns={
                "depth (meter)": "depth (m)",
                "depth (meter)": "depth (m)",
                "depth.enc (m)": "depth (m)",
                "dept (meter)": "depth (m)",
            }
        )

        if only_ln:
            if not attrs["has_LN"]:
                meas_data = None
            elif only_ln_column:
                # drop all columns except depth and ln column
                meas_data = meas_data.reindex(columns=["depth (m)", attrs["LN_column"]])

    return attrs.dropna(), meas_data


if __name__ == "__main__":
    fn_gef = r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\LHM zoetzout\Dinoloket\CPT000000111540_IMBRO.gef"
    fn_las = r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\LHM zoetzout\Dinoloket\Well log(s)_Geological borehole research\B42H1083_01.las"
    fn_asc = r"p:/11203718-007-nhi-zoetzout/Grondmetingen_Deltares/Boorgatmetingen_archief/--- afgerond 2010/Dordrecht/2010-43-Dordrecht-Haitjema-/dordrecht_haitjema/WELL_1_ANT_MULTI_2.asc"

    """
    attrs, colinfo, meas_data = read_GEF(fn_gef, False)
    print(attrs)
    print(colinfo)
    print(meas_data.info())
    
    attrs, colinfo, meas_data = read_LAS(fn_las, False)
    print(attrs)
    print(colinfo)
    print(meas_data.info())
    """
    a, m = read_ASC(fn_asc, False)
    print(a)
