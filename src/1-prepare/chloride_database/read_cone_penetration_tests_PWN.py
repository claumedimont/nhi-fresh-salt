# -*- coding: utf-8 -*-
"""
Read CPT data from Dinoloket

TODO: store in database
"""
from pathlib import Path
from readers import read_GEF
import pandas as pd
import datetime


GEFpath = Path("p:/11203718-007-nhi-zoetzout/Gegevensuitvraag/Aangeleverd/Grondwater/Waterleidingbedrijf Noord-Holland (PWN)/ECPT")
result_path = Path("../../data/2-interim")


attrs_l = []
meas_data_l = []
i = 0
for fn in GEFpath.glob("*.gef"):
    i += 1
    attrs,colinfo,meas_data = read_GEF(fn, only_ecpt=True)
    if meas_data is not None:
        print(f"ECPT: {fn.name}")
        attrs_l += [attrs]
        meas_data.loc[:,"CPTid"] = attrs["CPTid"]
        meas_data_l += [meas_data]
    
    #if i == 1000:
    #    break

attrs = pd.concat(attrs_l,axis=1).T
attrs.to_csv(result_path / "PWN-GEF-attributes.csv")
attrs.to_pickle(result_path / "PWN-GEF-attributes.pickle")
print(attrs.info())

if len(meas_data_l):
    meas_data = pd.concat(meas_data_l,axis=0,ignore_index=True)
    meas_data.to_pickle(result_path / "PWN-GEF-measurement-data.pickle")
print(meas_data.info())


"""
# get unique column names
colinfo_l = []
i=0
for fn in GEFpath.glob("*.gef"):
    attrs,colinfo,meas_data = read_GEF(fn)
    colinfo_l += [colinfo]
    i += 1
    if i % 1000 == 0:
        print(i)
    #if i == 10:
    #    break
    
colinfo = pd.concat(colinfo_l,axis=1).T
colinfo.to_pickle("test_colinfo.pickle")

unique_cols = []
for idx in colinfo.columns:
    unique_cols += colinfo[idx].unique().tolist()

unique_cols = pd.Series(unique_cols).dropna().unique()
print(unique_cols)
"""