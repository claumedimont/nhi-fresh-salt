# -*- coding: utf-8 -*-
"""
Creates the boils drainage input file, by following
the following steps:
1. read boil stage and conductance from LHM river systems
2. for each boil location:
    - place a DRN in each LHMzz layer in first LHM aquifer
        --> aquifer 1 are layers 11 - 16 TODO: have mapping in layermodel
    - divide conductance over layers
    TODO: check if omitted density correction causes significant flux difference
3. write out wq.Drainage

Input:
- data/2-interim/template_2d.nc
- data/2-interim/bnd.nc
- data/1-external/riv/peilw_wel.idf
- data/1-external/riv/peilz_wel.idf
- data/1-external/riv/cond_wel.idf

Output:
- data/2-interim/boils.nc
"""
import os
import xarray as xr
import imod
import sys

sys.path.append(os.path.dirname(__file__))  # add script directory to python path
from utils import inner_join_dataarrays

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# Paths
path_2d_template = snakemake.input.path_template_2d   #"data/2-interim/template_2d.nc"
path_bnd = snakemake.input.path_bnd   #"data/2-interim/bnd.nc"
path_boil_stage_w = snakemake.input.path_wellen_w   #"data/1-external/riv/wellen/peilw_wel.idf"
path_boil_stage_s = snakemake.input.path_wellen_z   #"data/1-external/riv/wellen/peilz_wel.idf"
path_boil_cond = snakemake.input.path_wellen_cond   #"data/1-external/riv/wellen/cond_wel.idf"
path_boils_out = snakemake.output.path_boils_out

layers = [11, 12, 13, 14, 15]

def create_boils(bot_2d, cond_2d, layers, like_2d, ibound):
    """
    Function to create consistent drainage xr.Dataset as input for
    imod.wq.Drainage()
    """
    # conform to like_2d
    if like_2d is not None:
        bot_2d = imod.prepare.reproject(bot_2d, like_2d, method="nearest")
        cond_2d = imod.prepare.reproject(cond_2d, like_2d, method="nearest")

    # divide conductance over layers
    cond_2d /= len(layers)

    # inner join bot and cond and make sure both exist
    drain_2d = inner_join_dataarrays(bot=bot_2d, cond=cond_2d)

    # assign to each layer
    drain_3d = drain_2d.where(ibound.layer.isin(layers))

    # reselect only if in ibound
    drain_3d = drain_3d.where(ibound)

    # assert all have equal no of nodata cells
    dvars = list(drain_3d.data_vars.keys())
    for k1, k2 in zip(dvars[:-1], dvars[1:]):
        assert drain_3d[k1].isnull().equals(drain_3d[k2].isnull())

    return drain_3d.dropna(dim="layer", how="all")


# open like template
like_2d = xr.open_dataset(path_2d_template)["template"]

# open ibound, to DataArray
bnd = xr.open_dataset(path_bnd)["ibound"]

# create drainage package for boil locations
bot_2d = 0.5 * imod.idf.open(path_boil_stage_w) + 0.5 * imod.idf.open(path_boil_stage_s)
cond_2d = imod.idf.open(path_boil_cond)
ds_boils = create_boils(bot_2d, cond_2d, layers, like_2d, bnd)

# Write output
ds_boils = imod.wq.Drainage(elevation=ds_boils["bot"], conductance=ds_boils["cond"])
ds_boils.dataset.to_netcdf(path_boils_out)

