"""
This script serves to transform the geohydrological layers of the fresh LHM to
a more finely vertically discretized version for variable density modelling.
What follows is an overview of the steps taken.

We start by converting the seven layers, in which each layer consists of an
aquifer on top of an aquitard (except for the last), to thirteen layers, so
that aquifers and aquitards are fully separate layers. This is effectively
adapting the Block Centered Flow (BCF) input to Layer Property Flow (LPF) input.

The next step is selecting the lower ten layers (of thirteen). These layers are
grouped into aquitard - aquifer (in that order) combinations. The reason for
this is the relatively horizontal course of aquitards, and the relatively equal
thickness of aquitard - aquitard combo's; aquifers and aquitards alone fluctuate
much more so in vertical thickness.

These combined layers are divided into five sublayers; the top three layers are
not divided into a fixed number of sublayers, as they can be flexibly set
active or inactive in the IBOUND. This cannot be done (easily) for the middle
layers: shifts in the number of layers propagate throughout the original layer,
thereby possibly creating continuity issues -- such aquifers that are no longer
connected, aquitards that become riddled with holes.

The sublayers consist of a combination of aquitard and aquifer. The next step
is therefore figuring out which sublayer belongs to an aquifer, and which one
belongs to an aquitard. To this end, the relative thickness of the aquitard
within the combined layer is computed. This fraction is used to assign zero to
five of the sublayers to a specific aquifer or aquitard. The assignment is not
exactly proportional: in some cases, an aquifer might only have a relatively
thickness of 0.02 (two percent). Assigning proportionally, this results in zero
sublayers as aquifer. This clearly affects continuity as originally, there was
(some) continuity via the aquifer. So, when relative thicknesses are greater
than zero, at least one sublayer is assigned (for both aquifer and aquitard).

The next step consists of creating the sublayers for the upper three layers
(phreatic aquifer - aquitard - aquifer). The number of sublayers is computed by
dividing layer thickness by a maximum thickness. It's important to note for a
fairly large part of the country, the thickness of the phreatic aquifer is zero
in the layer model, even though there is still a phreatic transmissivity. One
option is to merge phreatic aquifer and the underlying aquitard, and consider
it as a single layer. However, this means the effective transmissivity for a
river cell isn't right, unless extendeded to span the entire combined
thickness! The resistance is divided equally over the sublayers, resulting in
large resistance between the sublayers of the phreatic LHM layer, effectively
isolating the surface water. Instead, the thickness has been set to atleast one
meter in this case, without significant vertical resistance within the layer.
River cells should therefore not be assigned purely on bottom elevation, but
should be assigned to the phreatic layer for commensurability with LHM-fresh.

Then, we address layers that are extremely thin. If these layers are located
at the bottom, are very thin, are not separated by aquitards; they are merged
into a single layer. All remaining very thin layers are assigned a small
minimum thickness.

Finally, using the computed thicknesses, hydraulic conductivities are assigned.
These are based on the hydraulic transmissivities and resistances of the LHM.
The idea is to preserve these (lumped) characteristics. Conductivities are
therefore computed by dividing by the computed thicknesses (and seawat will
compute the original transmissivities and resistances). One detail is that due
to the use of LPF rather than BCF, aquifers and aquitards must both have a
horizontal and vertical conductivity. These are currently given arbitrarily
high and low values (1.0e6 for aquifer vertical conductivity, 1.0e-6 for
aquitard horizontal conductivity), to mimick BCF input.

In summary:
    * Use tops and bottoms to create thirteen layers
    * Divide lower ten layers into five sublayers each
    * Compute required number of layers for the upper three layers
    * Merge thin bottom layers not separated by aquitards
    * Expand very thin layers to a mimimum thickness
    * Compute conductivities from transmissivities and resistances

For offshore:
    * create top layers as follows:
      1: nan, 2: nan, 3: nan, 4:ghb (1m), 5: sea (thick to bath?), 6-10: c1
"""

import pathlib
import geopandas as gpd
import imod
import matplotlib.pyplot as plt
import numba
import numpy as np
import xarray as xr
import pandas as pd
import os
import scipy.ndimage.morphology

def mask_offshore(is_land, buffer_distance):
    dims = ("y", "x")
    coords = {
    "x":is_land.x,
    "y":is_land.y,
    }
    distance = scipy.ndimage.morphology.distance_transform_edt( ~is_land.values, sampling=250.0)
    distance = xr.DataArray((distance),coords,dims)
    mask = distance > buffer_distance
    mask = xr.DataArray((mask), coords, dims).where(~is_land, 0.0)
    return mask


def smooth_nlayer(nlayer, kernelsize, percentile):
    """
    Smooth the number of layers spatially.

    Parameters
    ----------
    nlayer : 2D xr.DataArray with dtype float
        The number of layers
        nodata is assumed np.nan
    kernelsize : int
        Size of the kernel, for both x and y.
    percentile : int
        Which percentile to return for the kernel.

    Returns
    -------
    nlayer : 2D xr.DataArray with dtype float
        The smoothed result.
    """
    # First, define the filtering function
    if not (kernelsize % 2) == 1:
        raise ValueError("kernelsize must be an uneven number")

    low = -1 * int(np.floor(kernelsize / 2))
    high = int(np.ceil(kernelsize / 2))

    @numba.stencil(neighborhood=((low, high), (low, high)), cval=2.0)
    def percentile_kernel(a):
        tmp = np.empty(kernelsize * kernelsize)
        k = 0
        for i in range(low, high):
            for j in range(low, high):
                tmp[k] = a[i, j]
                k += 1
        return np.nanpercentile(tmp, percentile)

    # Avoid side-effects
    nlayer = nlayer.copy()
    is_data = nlayer.notnull()
    # Apply the filter
    nlayer.values = percentile_kernel(nlayer.values)
    nlayer = nlayer.where(is_data)
    return nlayer


def inactivate_isolated_cells(to_fill, cutoff=1e-2):
    """ Function that inactivates ibound areas that are 
    smaller than the cutoff (in fraction of number of cells)
    """
    active_cells = to_fill == 1
    label, _ = scipy.ndimage.label(active_cells)
    unique, counts = np.unique(label, return_counts=True)
    counts = counts / counts.sum()
    if (counts < cutoff).any():  # less than cutoff: unconnected region
        isolated = unique[counts < cutoff]
        isolated = np.isin(label, isolated)
        to_fill = to_fill.where(~isolated, 0)  # set bnd to zero for isolated regions
    return to_fill


def dilate(da, iterations):
    dims = ("y", "x")
    coords = {
    "x":da.x,
    "y":da.y,
    }
    dilation = scipy.ndimage.binary_dilation(da.values, iterations=iterations)
    return xr.DataArray((dilation), coords, dims)


def create_offshore_data(da, to_fill):
    return imod.prepare.fill(da, by="layer").where(to_fill)

def extend_ibound_boundary_flux(ibound, boundary_flux):
    # extend ibound for all layers by cells that have a boundary flux
    bf_all_layers = boundary_flux.groupby(["xcoord", "ycoord"]).first().reset_index()
    bf_all_layers["layer"] = ibound.layer.data[0]
    tmp = bf_all_layers.copy()
    for layer in ibound.layer.data[1:]:
        tmp["layer"] = layer
        bf_all_layers = pd.concat((bf_all_layers, tmp), axis=0, ignore_index=True)

    imod.select.points_set_values(
        ibound,
        values=1,
        x=bf_all_layers["xcoord"],
        y=bf_all_layers["ycoord"],
        layer=bf_all_layers["layer"],
    )
    return ibound


def bcf_to_lpf(aquifer_top, aquifer_bot):
    """
    Convert the Block Centered Flow (BCF) input to Layer Property Flow (LPF) input.
    BCF defines only aquifers, with aquitards implicitly in between.

    LPF layer are all explicit.

    Parameters
    ----------
    aquifer_top : xr.DataArray
        Aquifer tops
    aquifer_bot : xr.DataArray
        Aquifer bots

    Returns
    -------
    top : xr.DataArray
    bot : xr.DataArray
    thickness : xr.DataArray
    """
    # Avoid side-effects
    aquifer_top = aquifer_top.copy()
    aquifer_bot = aquifer_bot.copy()
    # Create separate layers for aquifers and aquitards
    aquitard_top = aquifer_bot.sel(layer=[1, 2, 3, 4, 5, 6, 7]).copy()
    aquitard_bot = aquifer_top.sel(layer=[2, 3, 4, 5, 6, 7, 8]).copy()
    aquitard_top.name = "top"
    aquitard_bot.name = "bot"
    aquitard_bot = aquitard_bot.copy()  # Avoid side-effects
    aquitard_bot["layer"] = aquitard_top["layer"]

    # Create single set tops and bottoms
    aquifer_top["layer"] = aquifer_top["layer"].values * 2 - 1
    aquitard_top["layer"] = aquitard_top["layer"].values * 2
    aquifer_bot["layer"] = aquifer_bot["layer"].values * 2 - 1
    aquitard_bot["layer"] = aquitard_bot["layer"].values * 2

    top = xr.concat([aquifer_top, aquitard_top], dim="layer").sortby("layer")
    bot = xr.concat([aquifer_bot, aquitard_bot], dim="layer").sortby("layer")
    thickness = top - bot
    return top.astype(np.float64), bot.astype(np.float64), thickness.astype(np.float64)


def subdivide_aquitard_aquifer(top, bot, thickness, nsublayer):
    def aquitard_sublayers(da_fraction, nsublayer):
        """
        Compute how many sublayers are required for an aquifer, given a total
        of nsublayer per single layer.

        Inner function, not available outside of subdivide_middle_layers()
        """
        if nsublayer < 2:
            raise ValueError("Too few nsublayer")
        fraction = da_fraction.values  # Get numpy array from DataArray
        nlayer_aquitard = np.round(fraction * nsublayer)
        # Make sure to preserve layers even if they are tiny
        nlayer_aquitard[(nlayer_aquitard == 0) & (fraction > 0.0)] = 1
        nlayer_aquitard[(nlayer_aquitard == nsublayer) & (fraction < 1.0)] = (
            nsublayer - 1
        )
        return nlayer_aquitard

    @numba.njit
    def _assign_layer_id(layer_id, id_aquitard, id_aquifer, n_aquitard, n_aquifer):
        # mutates layer_id
        _, nrow, ncol = layer_id.shape
        for i in range(nrow):
            for j in range(ncol):
                n_t = int(n_aquitard[i, j])
                n_f = int(n_aquifer[i, j])
                for k in range(n_t):
                    layer_id[k, i, j] = id_aquitard
                for k in range(n_t, n_t + n_f):
                    layer_id[k, i, j] = id_aquifer

    aquitard_thickness = top.isel(layer=0) - bot.isel(layer=0)
    aquifer_thickness = top.isel(layer=1) - bot.isel(layer=1)
    fraction_aquitard = (
        (aquitard_thickness / thickness.sum("layer")).fillna(0.0).compute()
    )

    nsublayers_aquitard = aquitard_sublayers(fraction_aquitard, nsublayer)
    new_aquitard_thickness = (aquitard_thickness / nsublayers_aquitard).compute()
    nsublayers_aquifer = nsublayer - nsublayers_aquitard
    new_aquifer_thickness = (aquifer_thickness / nsublayers_aquifer).compute()

    dims = tuple(top.dims)
    coords = dict(top.coords)
    coords["layer"] = np.arange(1, nsublayer + 1)
    _, nrow, ncol = top.shape
    layer_id = xr.DataArray(np.full((nsublayer, nrow, ncol), np.nan), coords, dims)
    id_aquitard = float(top["layer"].isel(layer=0))
    id_aquifer = float(top["layer"].isel(layer=1))
    _assign_layer_id(
        layer_id.values,
        id_aquitard,
        id_aquifer,
        nsublayers_aquitard,
        nsublayers_aquifer,
    )
    is_aquifer = layer_id == id_aquifer

    new_thickness = (
        new_aquifer_thickness
        .where(is_aquifer)
        .combine_first(new_aquitard_thickness)
    )
    assert np.allclose(new_thickness.sum("layer"), thickness.sum("layer"))
    return new_thickness, layer_id


def subdivide_layers(top, bot, thickness, subdivision, minimum_thickness):
    # Phreatic aquifer, first aquitard
    (aquifer, aquitard), nsublayer = subdivision[0]

    dims = tuple(top.dims)
    coords = dict(top.coords)
    coords["layer"] = np.arange(1, nsublayer + 1)
    _, nrow, ncol = top.shape

    #t = xr.DataArray(np.full((nsublayer, nrow, ncol), np.nan), coords, dims)
    t = xr.DataArray(np.full((nsublayer, nrow, ncol), 1.0), coords, dims)
    topthickness = top.sel(layer=aquifer) - bot.sel(layer=aquitard)
    nsublayertop = np.floor(topthickness / minimum_thickness)
    nsublayertop = nsublayertop.where(~(nsublayertop > nsublayer), other=nsublayer)  # max nsublayer
    nsublayertop = nsublayertop.where(~(nsublayertop < 2), other=2) #minimally two layers
    nsublayertop = smooth_nlayer(nsublayertop, kernelsize=3, percentile=25)

    sublayerthickness = topthickness / nsublayertop
    sublayerthickness = sublayerthickness.where(
        ~(sublayerthickness < minimum_thickness), other=minimum_thickness
    )
    t = xr.ones_like(t) * sublayerthickness
    t = t.where(t["layer"] > (nsublayer - nsublayertop))
    i = xr.ones_like(t).where(t.notnull()) * aquifer
    # Set last layer as aquitard, all resistance goes here.
    i[-1, ...] = aquitard
    newthickness = [t]
    newlayerid = [i]

    # First aquifer
    aquifer, nsublayer = subdivision[1]
    data = np.stack([thickness.sel(layer=aquifer).values / nsublayer] * nsublayer)
    dims = tuple(top.dims)
    coords = dict(top.coords)
    coords["layer"] = np.arange(1, nsublayer + 1)
    t = xr.DataArray(data, coords, dims)
    i = xr.full_like(t, aquifer).where(t.notnull())
    newthickness.append(t)
    newlayerid.append(i)

    for (aquitard, aquifer), nsublayer in subdivision[2:]:
        t, i = subdivide_aquitard_aquifer(
            top.sel(layer=slice(aquitard, aquifer)),
            bot.sel(layer=slice(aquitard, aquifer)),
            thickness.sel(layer=slice(aquitard, aquifer)),
            nsublayer,
        )
        newthickness.append(t)
        newlayerid.append(i)

    new_thickness = xr.concat(newthickness, dim="layer")
#    new_thickness = new_thickness.where(
#        (new_thickness > minimum_thickness), other=minimum_thickness
#    )
    layer_id = xr.concat(newlayerid, dim="layer")

    nlayer, _, _ = new_thickness.shape
    new_thickness["layer"] = np.arange(1, nlayer + 1)
    layer_id["layer"] = np.arange(1, nlayer + 1)
    return new_thickness, layer_id


def create_offshore_thickness(
    upper_thickness,
    new_upper_thickness,
    top,
    to_fill,
    bathymetry,
    sea_level,
    ghb_thickness,
    nlayer_sea,
    nlayer_resistance,
    minimum_thickness,
):
    """
    Places three layers of cells offshore:
    * GHB
    * Sea
    * Seabed (with resistance)
    """
    if nlayer_resistance < 1:
        raise ValueError("nlayer_resistance must be >= 1")

    # Remove "above land" parts
    bathymetry = bathymetry.where(bathymetry < 0.0, other=0.0)
    # Only use offshore part
    bathymetry = bathymetry.where(to_fill).compute()
    # Add four layers for offshore: aquifer (wvp1), aquitard (sdl1), the sea, ghb cells
    # Add seven layers: five aquitard (sdl1), sea, ghb cells

    # Extrapolate offshore thickness, smoothly
    offshore_aquitard_thickness = upper_thickness.sum("layer").where(to_fill)  # sum orig layers 1&2 (aq1+at1)
    offshore_aquitard_thickness = offshore_aquitard_thickness.where(
        upper_thickness.isel(layer=1).notnull()
    )
    offshore_aquitard_thickness = (
        imod.prepare.laplace_interpolate(
            offshore_aquitard_thickness, close=0.1, mxiter=10, iter1=100
        ).where(to_fill)
        / nlayer_resistance
    )
    nlay_hol = int(new_upper_thickness["layer"].max())

    # Resistance thickness for nlayer_resistance sublayers
    thickness_resistance = new_upper_thickness.isel(layer=slice(-nlayer_resistance, None))
    thickness_resistance[:,:] = offshore_aquitard_thickness.where(to_fill)


    ghb_layer = -(nlayer_resistance + nlayer_sea + 1)
    sea_layer = -(nlayer_resistance + nlayer_sea)
    ghb_top = xr.full_like(new_upper_thickness.isel(layer=ghb_layer), sea_level).where(
        to_fill
    )
    ghb_bot = xr.full_like(
        new_upper_thickness.isel(layer=ghb_layer), sea_level - ghb_thickness,
    ).where(to_fill)
    sea_top = xr.full_like(
        new_upper_thickness.isel(layer=sea_layer), sea_level - ghb_thickness
    ).where(to_fill)
    sea_bot = new_upper_thickness.isel(layer=-(nlayer_resistance + 1))
    sea_bot[:,:] = bathymetry.where(to_fill)

    thickness_ghb = ghb_top - ghb_bot
    thickness_sea = (sea_top - sea_bot) / nlayer_sea
    thickness_sea = xr.concat(nlayer_sea*[thickness_sea], dim=pd.Index(range(nlay_hol-nlayer_resistance-nlayer_sea+1,nlay_hol-nlayer_resistance+1), name="layer"))
    offshore_thickness = xr.concat(
        [thickness_ghb, thickness_sea, thickness_resistance], dim="layer"
    )
    #print(offshore_thickness)
    # Remove negative thickness: effectively, push down layers
    offshore_thickness = offshore_thickness.where(
        (offshore_thickness > minimum_thickness) | ~to_fill, other=minimum_thickness
    )
    new_upper_thickness = new_upper_thickness.where(~to_fill)
    new_upper_thickness = new_upper_thickness.combine_first(offshore_thickness)

    # set top of the layermodel to sea_level at the offshore part
    first_top = top.isel(layer=0)
    first_top = first_top.where(to_fill == 0, other=sea_level)

    # set layer_id
    new_layer_id = xr.zeros_like(new_upper_thickness)
    new_layer_id.loc[{"layer":slice(None,nlay_hol-nlayer_resistance-nlayer_sea)}] = np.nan
    new_layer_id.loc[{"layer":slice(nlay_hol-nlayer_resistance-nlayer_sea, nlay_hol-nlayer_resistance)}] = 1
    new_layer_id.loc[{"layer":slice(nlay_hol-nlayer_resistance+1, None)}] = 2
    new_layer_id = new_layer_id.where(to_fill)

    return new_upper_thickness, first_top, new_layer_id


def increased_thickness(thickness, dt, min_dt=10.0, new_min_thick=3.0, step_thick=1.0):
    # Increase thickness to new_thick in areas where the simulated velocities are too high.
    # Where thickness > new_min_thick: thick = thick + step_thick
    new_thick = (thickness + step_thick).where(
        thickness > (new_min_thick - step_thick), new_min_thick
    )
    thickness = thickness.where((dt > min_dt) | thickness.isnull(), new_thick)
    return thickness.compute()


def compute_conductivity(
    new_thickness, layer_id, kd, c, thickness, vertical_anisotropy
):
    new_thickness = new_thickness.load()
    layer_id = layer_id.load()
    kd = kd.load()
    c = c.load()

    # Sum new thicknesses per layer, to conserve lump kd and c
    @numba.njit
    def _lumped_thickness(thickness, layer_id, out):
        nlay, nrow, ncol = thickness.shape
        for k in range(nlay):
            for i in range(nrow):
                for j in range(ncol):
                    dz = thickness[k, i, j]
                    layernum = int(layer_id[k, i, j])
                    if np.isnan(layernum):
                        continue
                    else:
                        layernum = int(layernum)

                    if np.isnan(dz):
                        continue
                    else:
                        out[layernum - 1, i, j] += dz

    def lumped_thickness(new_thickness, layer_id):
        _, nrow, ncol = new_thickness.shape
        nlay = int(layer_id.max())

        data = np.zeros((nlay, nrow, ncol))
        coords = dict(new_thickness.coords)
        coords["layer"] = np.arange(1, nlay + 1)
        dims = tuple(new_thickness.dims)
        out = xr.DataArray(data, coords, dims)

        _lumped_thickness(new_thickness.values, layer_id.values, out.values)
        return out

    @numba.njit
    def _fill_by_layer(layer_id, values, out):
        nlay, nrow, ncol = layer_id.shape
        for k in range(nlay):
            for i in range(nrow):
                for j in range(ncol):
                    n = layer_id[k, i, j]
                    if np.isnan(n):
                        continue
                    else:
                        v = values[int(n - 1), i, j]
                        out[k, i, j] = v

    def fill_by_layer(layer_id, values):
        out = xr.full_like(layer_id, np.nan)
        _fill_by_layer(layer_id.values, values.values, out.values)
        return out

    layer_id = layer_id.where(new_thickness.notnull())
    lumped = lumped_thickness(new_thickness, layer_id)
    lumped = lumped.where(lumped > 0.0)

    kh = xr.full_like(lumped, np.nan)
    kv = xr.full_like(lumped, np.nan)
    # Aquitards are given a negligible kh of 1.0e-6
    kh = kh.combine_first(kd / lumped).fillna(1.0e-6)
    # Cutoff kh values higher than 50 m/d
    kh = kh.where(~(kh > 50.0), 50.0)
    kv = kv.combine_first(lumped / c).fillna(kh * vertical_anisotropy).compute()
    # Set kv of phreatic layer to 100 to not interfere with riv/drn conductances
    kv.loc[dict(layer=1)] = 10.0

    kh_out = fill_by_layer(layer_id, kh)
    kv_out = fill_by_layer(layer_id, kv)

    return kh_out, kv_out

"""
path_coastline_vector = "data/1-external/coastline/nl_imergis_kustlijn_2018.shp"
path_bathymetry = "data/1-external/offshore/bathymetry.tif"
path_aquifer_bot = "data/1-external/layermodel/bot/MDL_BOT_L*.IDF"
path_aquifer_top = "data/1-external/layermodel/top/MDL_TOP_L*.IDF"
path_kd = "data/1-external/kd/MDL_KD_l*.IDF"
path_c = "data/1-external/c/MDL_C_l*.IDF"
path_ibound = "data/1-external/bnd/ibound_l*.IDF"
path_ghb = "data/1-external/ghb/ghb_head_l1.idf"
path_aq_top_lhm34 = "data/1-external/LHM3.4-zoet/top/top_l*.IDF"
#path_aq_bot_lhm34 = "data/1-external/LHM3.4-zoet/bot/bot_l*.IDF"
path_boundary_flow = "data/1-external/ghb/Randflux_L*.ipf"
#paths_dt = "data/2-interim/{dt_model}.nc", dt_model=layermodel_dt_models)
increase_thickness=False
min_dt = 10. # days
minimum_thickness = 1.0 # m
min_thick_dt = 3.0 # m
step_thick_dt = 1.0 # m


"""

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
#paths
path_coastline_vector  = snakemake.input.path_coastline_vector
path_bathymetry = snakemake.input.path_bathymetry
path_aquifer_top = snakemake.input.path_aquifer_top
path_aquifer_bot = snakemake.input.path_aquifer_bot
#path_aq_top_lhm34 = snakemake.input.path_aq_top_lhm34
#path_aq_bot_lhm34 = snakemake.input.path_aq_bot_lhm34
path_kd = snakemake.input.path_kd
path_c = snakemake.input.path_c
path_ibound  = snakemake.input.path_ibound
path_ghb = snakemake.input.path_ghb
path_boundary_flow = snakemake.input.path_randflux
path_topbot_orig_out = snakemake.output.path_topbot_orig_out
path_conductivity_out = snakemake.output.path_conductivity_out
path_layermodel_out = snakemake.output.path_layermodel_out
path_topbot_out = snakemake.output.path_topbot_out



coastline_vector = gpd.read_file(path_coastline_vector) #"data/1-external/coastline/nl_imergis_kustlijn_2018.shp"
bathymetry = imod.rasterio.open(path_bathymetry)#"data/1-external/offshore/bathymetry.tif"
aquifer_top = imod.idf.open(path_aquifer_top)#.load()#"data/1-external/layermodel/top/*.idf"
aquifer_bot = imod.idf.open(path_aquifer_bot)#.load()#"data/1-external/layermodel/bot/*.idf"
#aq_top_lhm34 = imod.idf.open(path_aq_top_lhm34)#"data/LHM3.4-zoet/top/*"
#aq_bot_lhm34 = imod.idf.open(path_aq_bot_lhm34).load()#"data/LHM3.4-zoet/bot/*"
kd = imod.idf.open(path_kd)#.load()#"data/1-external/kd/*.idf"
c = imod.idf.open(path_c)#.load()#"data/1-external/c/*.idf"
ghb = imod.idf.open(path_ghb)
ibound = imod.idf.open(path_ibound)#.load()#"data/1-external/bnd/ibound_l*.idf"

# Set thickness parameters
sea_level = 0.0
ghb_thickness = 1.0
nlayer_sea = 5
nlayer_resistance = 1
minimum_thickness = snakemake.params.min_thick

# Check if all boundary flows are included
#boundary_flow = imod.ipf.read(path_boundary_flow)
boundary_flow = imod.ipf.read("data/1-external/ghb/randflux_l*.ipf")
sel_top = imod.select.points_values(
    aquifer_top,
    x=boundary_flow["xcoord"],
    y=boundary_flow["ycoord"],
    layer=boundary_flow["layer"],
)
assert not sel_top.isnull().any()

# Write out original LHM-fresh layer model
top_bot_orig = xr.Dataset()
top_bot_orig["top"] = aquifer_top
top_bot_orig["bot"] = aquifer_bot
top_bot_orig.to_netcdf(path_topbot_orig_out)

# Masking land and water
#coastline = imod.prepare.rasterize(
#    coastline_vector, like=aquifer_bot.isel(layer=-1).drop("layer")
#)
# Coastline doesn't match exactly with the aquifer_top and aquifer_bot
# Dilate it by four cells so no missing data appears
#is_water = coastline.notnull()
#is_water = dilate(is_water, iterations=4)
#is_offshore = ~(aq_top_lhm34.notnull().any("layer")) & is_water
#is_land = coastline.isnull()
#masked = mask_offshore(is_land, buffer_distance=30_000.0)
#is_offshore = is_offshore.where((masked ==0) & (is_offshore == 1), 0.0)
#is_offshore = inactivate_isolated_cells(is_offshore).astype(bool)
is_offshore = ghb.max("layer").notnull().load()

# Fill up holes (nearest neighbour) and cut to ibound

# Fill layer model into the sea => not necessary anymore
# Now (4.1 onwards): only replace top layer 1 with bathymetry for North sea
###top_sea = bathymetry.where(is_offshore)
###aquifer_top.loc[{"layer":1}] = top_sea.combine_first(aquifer_top.sel(layer=1))

#bot = aquifer_bot.where(to_fill != 1.0)
#off_fill_bot = create_offshore_data(bot[1:8,:,:], to_fill)
#bot[0,:,:] = bot[0,:,:].combine_first((bathymetry.where(to_fill == 1.0)) + ((off_fill_bot[0,:,:]).where(to_fill == 1.0)) / 2 )
#aquifer_bot = off_fill_bot.combine_first(bot)

# adjust offshore thickness to at least min_thick (is further on in script again applied per freshsalt layer...)
###thickness = aquifer_top - aquifer_bot
###thickness = thickness.where((thickness >= minimum_thickness), other=minimum_thickness) #~is_offshore | 
###aquifer_bot = aquifer_top - thickness

aquifer_top = imod.prepare.fill(aquifer_top).where(ibound)
aquifer_bot = imod.prepare.fill(aquifer_bot).where(ibound)

kd = imod.prepare.fill(kd).where(ibound)
c = imod.prepare.fill(c).where(ibound)

# Define number of sublayers per aquifer
subdivision = [
    ((1, 2), 10),  # phreatic and cover aquitard
    (3, 5),  # aquifer 1
    ((4, 5), 5),
    ((6, 7), 5),
    ((8, 9), 5),
    ((10, 11), 5),
    ((12, 13), 2),
    ((14, 15), 2),
]

# Create input
top, bot, thickness = bcf_to_lpf(aquifer_top, aquifer_bot)
top = top.transpose("layer", "y", "x").load()
bot = bot.transpose("layer", "y", "x").load()
thickness = thickness.transpose("layer", "y", "x")
with imod.util.ignore_warnings():
    new_thickness, layer_id = subdivide_layers(
        top, bot, thickness, subdivision, minimum_thickness
    )
#new_thickness = new_thickness.where(new_thickness >= minimum_thickness, other = minimum_thickness).load()
new_thickness = new_thickness.where(new_thickness.isnull() | (new_thickness >= minimum_thickness), other = minimum_thickness).load()

upper_thickness = thickness.sel(layer=slice(1, 2)).load()
new_upper_thickness = new_thickness.sel(layer=slice(None, subdivision[0][1])).load().copy()
new_upper_thickness, first_top, new_layer_id = create_offshore_thickness(
    upper_thickness,
    new_upper_thickness,
    top,
    is_offshore.load(),
    bathymetry.load(),
    sea_level,
    ghb_thickness,
    nlayer_sea,
    nlayer_resistance,
    minimum_thickness,
)
new_thickness = new_thickness.where(~is_offshore | (new_thickness["layer"] > subdivision[0][1]))
new_thickness = new_upper_thickness.combine_first(new_thickness)

sea_end = subdivision[0][1] - nlayer_resistance
sea_start = sea_end - 2
sea_cells = (
    new_thickness.where(is_offshore)
    .where((new_thickness["layer"] <= sea_end) & (new_thickness["layer"] > sea_start))
    .notnull()
)

#surface_lvl = top.sel(layer=1).drop("layer")

#new_bot = (surface_lvl -  new_thickness.cumsum("layer"))
#new_top = new_bot + new_thickness
#new_top = new_top.where(new_thickness.notnull())
#new_bot = new_bot.where(new_thickness.notnull())

## For broadcasting rules, new_thickness goes first to preserve dimension order
end = subdivision[0][1]
layer_id = layer_id.where(~((layer_id["layer"] < end) & is_offshore))
# Set resistance in the sea to layer number 2 for the aquitard
#layers_to_2 = layer_id.sel(layer=slice(start, end)).where(layer_id.sel(layer=slice(start, end)) == np.nan, 2)
layer_id = layer_id.combine_first(new_layer_id.where(is_offshore))


avg_thick_org = new_thickness.mean().compute()
# increase thickness in cells which cause a small stepsize
increase_thickness = snakemake.params.increase_thickness
if increase_thickness:
    paths_dt = snakemake.input.paths_dt
    # combine to one dt da by taking minimum
    #dts = [xr.open_dataset(p) for p in paths_dt]
    dts = xr.open_dataarray(paths_dt).squeeze('time', drop=True)
    #dts = xr.concat(dts, dim=pd.Index(range(len(paths_dt), name="dts")))
    #dt = dts.min(dim="dts")
    dt = imod.prepare.Regridder(method="mean").regrid(dts, new_thickness)
    new_thickness = increased_thickness(
        new_thickness, dt, min_dt=snakemake.params.min_dt, new_min_thick=snakemake.params.min_thick_dt, step_thick=snakemake.params.step_thick_dt
    )
    avg_thick_dt = new_thickness.mean().compute()
    print(f"Dt check resulted in change of average thickness from {float(avg_thick_org):.4f} m to {float(avg_thick_dt):.4f} m")

new_bot = -new_thickness.cumsum("layer") + first_top.drop("layer")
new_top = new_bot + new_thickness
new_top = new_top.where(new_thickness.notnull())
new_bot = new_bot.where(new_thickness.notnull())

# nearest neighbour fill offshore
#kd = imod.prepare.fill(kd)#.where(~to_fill_3d)
#c = imod.prepare.fill(c)#.where(~to_fill_3d)
# Interleave kd's and c's
#c_offshore = create_offshore_resistance(to_fill, holoceen, bergen, velsen, zeeland)
#c_offshore = c_offshore.assign_coords(layer=1)

#kd_offshore = c_offshore.where(xr.ufuncs.isnan(c_offshore), 50.0)

# Merge c and offshore c
#c = c.combine_first(c_offshore)
#kd = kd.combine_first(kd_offshore)
kd["layer"] = kd["layer"] * 2 - 1
c["layer"] = c["layer"] * 2

vertical_anisotropy = 0.3
kh, kv = compute_conductivity(
    new_thickness, layer_id, kd, c, thickness, vertical_anisotropy
)
# Correct sea conductivity, set to 50 m/d
#kh = xr.full_like(kh, 50.0).where(sea_cells).combine_first(kh)
#kv = xr.full_like(kv, 50.0).where(sea_cells).combine_first(kv)


# Make sure all data exists everywhere
# convert bnd to 9 layers  #TODO: not entirely correct
bnd_39 = []
nlaytot = 0
for il, (_, nlay) in enumerate(subdivision):
    for i in range(nlay):
        bnd_39.append(ibound.sel(layer=il+1, drop=True))
    nlaytot += nlay
bnd = xr.concat(bnd_39, dim=pd.Index(range(1,40), name="layer"))

ibound_check = (kh > 0) & (bnd != 0)
kh = kh.where(ibound_check == True)
kv = kv.where(ibound_check == True)
new_top = new_top.where(ibound_check == True)
new_bot = new_bot.where(ibound_check == True)
bnd = bnd.where(kh > 0.0)
bnd = bnd.where(bnd == 1, 0)
layer_id = layer_id.where(ibound_check == True)

# assert three layer levels are approximately equal (inland):
# top_l2 (LHMfresh) == bot_l10 (LHMfreshsalt)
# bot_l2 (LHMfresh) == bot_l15 (LHMfreshsalt)
diff1 = top_bot_orig["top"].sel(layer=2) - new_bot.sel(layer=10)
diff2 = top_bot_orig["bot"].sel(layer=2) - new_bot.sel(layer=15)
diff3 = top_bot_orig["bot"].sel(layer=8) - new_bot.isel(layer=-1)
landslice = {"x":slice(150_000,200_000), "y":slice(550_000, 500_000)}
print(f"Avg deviation bottom Holocene: {abs(float(diff1.sel(landslice).mean().compute())):.2f}")# < 0.5)#1e-3)
print(f"Avg deviation bottom WVP1: {abs(float(diff2.sel(landslice).mean().compute())):.2f}")# < 1.5)#1e-3)
print(f"Avg deviation geohydrological basis: {abs(float(diff3.sel(landslice).mean().compute())):.2f}")# < 1.5)#1e-3)

# Output
# -----
layermodel = xr.Dataset()
layermodel["top"] = new_top.transpose("layer", "y", "x")
layermodel["bot"] = new_bot.transpose("layer", "y", "x")
layermodel["dz"] = new_thickness
layermodel["ibound"] = bnd
layermodel["lhmfresh_layer"] = layer_id
layermodel["offshore_ghb"] = is_offshore.assign_coords(
    layer=subdivision[0][1] - nlayer_resistance - nlayer_sea - 1
)
layermodel.to_netcdf(path_layermodel_out)

conductivity = xr.Dataset()
conductivity["kh"] = kh
conductivity["kv"] = kv
conductivity.to_netcdf(path_conductivity_out)

top_bot = xr.Dataset()
top_bot["top"] = new_top.transpose("layer", "y", "x")
top_bot["bot"] = new_bot.transpose("layer", "y", "x")
top_bot.to_netcdf(path_topbot_out)


# Visualize
# ---------
visualize = False

if visualize:
    # Assign top and bottom coords for cross-section visualization
    is_aquifer = layer_id % 2 == 0
    is_aquifer = is_aquifer.assign_coords(top=new_top.compute())
    is_aquifer = is_aquifer.assign_coords(bottom=new_bot.compute())

    #    new_cs = is_aquifer.isel(y=500).compute()
    new_cs = is_aquifer.sel(y=447_000.0, method="nearest").compute()
    new_cs = new_cs.astype(int)
    newfigs = pathlib.Path("reports/figures/latest")
    newfigs.mkdir(exist_ok=True, parents=True)

    fig, ax = plt.subplots(figsize=(20, 8))
    imod.visualize.cross_section(
        new_cs, layers=True, levels= [0,1], colors=["#ef8a62","#f7f7f7","#67a9cf"]
    )
    fig.savefig(newfigs / "whole.png", dpi=600)
    plt.close()

    edges = [0.0, 5.0e4, 1.0e5, 1.5e5, 2.0e5, 2.5e5]
    for i, (xs, xe) in enumerate(zip(edges[:-1], edges[1:])):
        fig, ax = plt.subplots(figsize=(20, 8))
        imod.visualize.cross_section(
            new_cs.sel(x=slice(xs, xe)),
            layers=True,
            levels=[0, 1],
            colors=["#41ab5d", "#238443", "#005a32"],
        )
        fig.savefig(newfigs / f"{i}.png", dpi=600)
        plt.close()
