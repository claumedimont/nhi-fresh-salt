# -*- coding: utf-8 -*-
"""
Keep all functions relevant to multiple package scripts
Should be pushed to imod-python
"""
import numpy as np
import os
import xarray as xr
import pandas as pd
import imod


def get_upper_active_layer(da, is_ibound=True, include_constant_head=False):
    """
    Function to get the upper active layer from ibound xr.DataArray

    Parameters:
    da : 3d xr.DataArray
    is_ibound: da is interpreted as ibound, with values 0 inactive, 1, active, -1 chd
                if False: upper_active_layer is interpreted as upper layer that has data
    include_constant_head : also incluse chd cells? bool, default False

    Returns:
    2d xr.DataArray of layernumber of upper active model layer
    """
    if is_ibound:
        # check if indeed ibound: convertible to int
        if not da.astype(int).equals(da):
            raise ValueError(
                "Passed DataArray is no ibound, while is_bound was set to True"
            )
        # include constant head cells (?)
        if include_constant_head:
            is_active = da.fillna(0) != 0  # must be filled for argmax
        else:
            is_active = da.fillna(0) > 0
    else:
        is_active = ~da.isnull()

    # get layer of upper active cell
    da = is_active.layer.isel(layer=is_active.argmax(dim="layer"))
    da = da.drop("layer")

    # skip where no active cells
    return da.where(is_active.sum(dim="layer") > 0)


def assign_layer_3d(
    z_2d, ibound, top_bot, method="z_then_upper", laymin=None, laymax=None
):
    """
    Function to assign boundary conditions to appropriate 3d cell,
    based on ibound and top and bot of layers.

    Parameters:
    z_2d : 2d xr.DataArray of z value of boundary condition
    ibound : 3d xr.DataArray of ibound
    top_bot : 3d xr.Dataset containing "top" and "bot" of layers
    method : string selecting which method to use to assign layer
    laymin : minimum (upper) layer number to assign to, optional
    laymax : maximum (lower) layer number to assign to, optional

    Possible methods: 
        "z_only": assigns boundary condition only based on z value of bot_2d and top_bot.
                  if z is above all cells, it is not assigned.
        "upper_only": assigns boundary condition to upper active cell.
        "z_then_upper": assigns boundary condition firstly based on z value of bot_2d and top_bot.
                  if z is above all cells, boundary condition is assigned to upper active cell. (default)
    
    Returns:
    3d boolean xr.DataArray with assigned cells set to True
    """
    if method not in ["z_only", "upper_only", "z_then_upper"]:
        raise ValueError("Method can only be one of (z_only, upper_only, z_then_upper)")

    if method in ["z_only", "z_then_upper"]:
        # Get layer of bot. First: conform lagenmodel to bnd
        top_bot = top_bot.where(ibound)
        # if laymin or laymax:
        if laymin is not None:
            z_2d = z_2d.where(
                (z_2d < top_bot["top"].sel(layer=laymin))
                | top_bot["top"].sel(layer=laymin).isnull(),
                top_bot["bot"].sel(layer=laymin) + 1e-5,
            )
        if laymax is not None:
            z_2d = z_2d.where(
                (z_2d >= top_bot["bot"].sel(layer=laymax))
                | top_bot["bot"].sel(layer=laymax).isnull(),
                top_bot["bot"].sel(layer=laymax) + 1e-5,
            )

        z_layer_3d = (top_bot["top"] > z_2d) & (top_bot["bot"] <= z_2d)
        if method == "z_only":
            return z_layer_3d

    if method in ["upper_only", "z_then_upper"]:
        # Set to top active if not in lagenmodel
        upper_active_2d = get_upper_active_layer(ibound)
        # if laymin or laymax:
        # if laymin is not None:
        #    upper_active_2d = upper_active_2d.where(upper_active_2d >= laymin, laymin)
        # if laymax is not None:
        #    upper_active_2d = upper_active_2d.where(upper_active_2d <= laymax, laymax)
        upper_active_3d = ibound.layer == upper_active_2d
        if method == "upper_only":
            return upper_active_3d

    return upper_active_3d.where(z_layer_3d.sum(dim="layer") == 0, z_layer_3d)


def inner_join_dataarrays(**kwargs):
    """Merges dataarrays into a returned dataset,
    inner joins data: only data is returned where
    all dataarrays have data
    """
    # set names of dataarrays
    for key, value in kwargs.items():
        value.name = key

    # make sure all data is aligned
    ds = xr.merge(kwargs.values(), join="inner")

    # only where all data_vars have data
    for key in kwargs.keys():
        ds = ds.where(~ds[key].isnull())

    return ds


def assign_to_dataset(ds_from, ds_to, suffix):
    for k, v in ds_from.data_vars.items():
        ds_to[f"{k}_{suffix}"] = v
    return ds_to


def stability_criterion_wel(wel, top_bot, porosity=0.3):
    # assumes x, y and layer present in WEL

    R = 1.0  # no sorption: retardation factor is 1

    b = imod.select.points_in_bounds(
        top_bot, x=wel["x"], y=wel["y"], layer=wel["layer"]
    )
    indices = imod.select.points_indices(
        top_bot, x=wel.loc[b, "x"], y=wel.loc[b, "y"], layer=wel.loc[b, "layer"]
    )

    if "dz" not in top_bot:
        top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    top_bot["volume"] = top_bot["dz"] * top_bot.dx * -top_bot.dy

    wel.loc[b, "qs"] = wel.loc[b, "Q"].abs() / top_bot["volume"].isel(indices).values
    wel.loc[b, "dt"] = R * porosity / wel.loc[b, "qs"]

    return wel


def stability_criterion_advection(bdgfrf, bdgfff, bdgflf, top_bot, porosity=0.3):
    R = 1.0  # no sorption: retardation factor is 1

    # top_bot reselect to bdg bounds
    top_bot = top_bot.sel(x=bdgfrf.x, y=bdgfrf.y, layer=bdgfrf.layer)

    if "dz" not in top_bot:
        top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    # dz between layers is 0.5*dz_up + 0.5*dz_down
    dz_m = top_bot.dz.rolling(layer=2, min_periods=2).mean()
    dz_m = dz_m.shift(layer=-1)

    # cell side area (m2)
    A_x = top_bot.dz * bdgfrf.dy
    A_y = top_bot.dz * bdgfff.dx
    A_z = abs(bdgflf.dx * bdgflf.dy)

    # specific discharge (m/d)
    qs_x = bdgfrf / A_x
    qs_y = bdgfff / A_y
    qs_z = bdgflf / A_z

    # absolute velocities (m/d)
    abs_v_x = np.abs(qs_x / porosity)
    abs_v_y = np.abs(qs_y / porosity)
    abs_v_z = np.abs(qs_z / porosity)

    # dt of constituents (d)
    dt_x = R / (abs_v_x / top_bot.dx)
    dt_y = R / (abs_v_y / abs(top_bot.dy))
    dt_z = R / (abs_v_z / dz_m)

    # overall dt due to advection criterion (d)
    dt = 1.0 / (1.0 / dt_x + 1.0 / dt_y + 1.0 / dt_z)

    dt_xyz = xr.concat(
        (dt_x, dt_y, dt_z), dim=pd.Index(["x", "y", "z"], name="direction")
    )
    return dt, dt_xyz


def stability_criterion_advection2(bdgfrf, bdgfff, bdgflf, top_bot, porosity=0.3):
    # R = 1.0  # no sorption: retardation factor is 1

    # top_bot reselect to bdg bounds
    top_bot = top_bot.sel(x=bdgfrf.x, y=bdgfrf.y, layer=bdgfrf.layer)

    if "dz" not in top_bot:
        top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    top_bot["volume"] = top_bot["dz"] * top_bot.dx * -top_bot.dy

    # average to cell centre
    bdgfrf = bdgfrf.rolling(x=2, min_periods=2).mean().shift(x=-1)
    bdgfff = bdgfff.rolling(y=2, min_periods=2).mean().shift(y=-1)
    bdgflf = bdgflf.rolling(layer=2, min_periods=2).mean().shift(layer=-1)

    volume = top_bot["volume"] * porosity

    # dt of constituents (d), make absolute
    dt_x = np.abs(volume / bdgfrf)
    dt_y = np.abs(volume / bdgfff)
    dt_z = np.abs(volume / bdgflf)

    dt_xyz = xr.concat(
        (dt_x, dt_y, dt_z), dim=pd.Index(["x", "y", "z"], name="direction")
    )

    # overall dt due to advection criterion (d)
    dt = dt_xyz.min(dim="direction")

    return dt, dt_xyz


def unravel_index(idx, da):
    """Utility function that performs like np.unravel_index,
    but returns a dim-index dict for use in DataArray isel.
    Works only if idx is a single value (eg. from da.argmin(dim=xr.ALL_DIMS))

    Parameters
    ----------
    idx : raveled index into array, as returned by argmin() or argmax()
    da : xr.DataArray

    Returns
    -------
    indices : dict of {dim: index} for use in da.isel()
    
    Examples
    --------

    To get indices of minimum value:

    >>> indices = unravel_index(da.argmin(xr.ALL_DIMS),da)
    """
    # check if not an array of indices
    if da.shape:
        raise ValueError("Function expects a single index value")

    indices = np.unravel_index(idx, da.shape)
    return {k: v for k, v in zip(da.dims, indices)}


def _calculate_intra_cell_dt(
    source_stage, source_cond, sink_stage, sink_cond, eff_volume
):
    """Calculate intra-cell dt by assuming a flux from a higher source_stage to a lower sink_stage,
    ignoring other head influences. Use limiting (lowest) conductance. eff_volume is the effective
    volume per cell (cell volume * effective porosity)"""
    cond = xr.ufuncs.minimum(source_cond, sink_cond)
    Q = cond * (source_stage - sink_stage)
    Q = Q.where(source_stage > sink_stage)

    return eff_volume / Q


def intra_cell_boundary_conditions(
    top_bot, porosity=0.3, riv=None, ghb=None, drn=None, drop_allnan=True
):
    """Function to pre-check boundary-conditions against one another for large intra-cell fluxes.
    ghb and river can function as source and sink, drn only as sink.
    
    Parameters
    ----------
    top_bot: xr.Dataset of floats
    'top_bot' should at least contain `top` and `bot` data_vars
    porosity: float or xr.DataArray of floats, optional
    Effective porosity of model cells
    riv: (dict or list of) imod.RiverPackage, optional
    ghb: (dict or list of) imod.GeneralHeadBoundaryPackage, optional
    drn: (dict or list of) imod.DrainagePackage, optional
    drop_allnan: boolean, optional
    Whether source-sink combinations without overlap should be dropped from result (default True)

    Returns
    -------
    dt_min: xr.DataArray of floats
    `dt_min` is the minimum calculated timestep over all combinations of boundary conditions
    dt_all: xr.DataArray of floats
    `dt_all` is the calculated timestep for all combinations of boundary conditions
    """
    if riv is None and ghb is None:
        raise ValueError(
            "At least one source boundary condition must be supplied through riv or ghb."
        )

    # convert all inputs to dictionaries of packages
    if riv is None:
        riv = {}
    elif isinstance(riv, dict):
        pass
    elif isinstance(riv, (list, tuple)):
        riv = {f"riv_{i}": l for i, l in enumerate(riv)}
    else:
        riv = {"riv": riv}

    if ghb is None:
        ghb = {}
    elif isinstance(ghb, dict):
        pass
    elif isinstance(ghb, (list, tuple)):
        ghb = {f"ghb_{i}": l for i, l in enumerate(ghb)}
    else:
        ghb = {"ghb": ghb}

    if drn is None:
        drn = {}
    elif isinstance(drn, dict):
        pass
    elif isinstance(drn, (list, tuple)):
        drn = {f"drn_{i}": l for i, l in enumerate(drn)}
    else:
        drn = {"drn": drn}

    # get sources and sinks:
    sources = {}
    sources.update(ghb)
    sources.update(riv)
    sinks = {}
    sinks.update(ghb)
    sinks.update(riv)
    sinks.update(drn)

    # determine effective volume
    if "dz" not in top_bot:
        top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    eff_volume = top_bot["dz"] * top_bot.dx * -top_bot.dy * porosity

    def _get_stage_name(sid):
        if sid in riv:
            return "stage"
        elif sid in ghb:
            return "head"
        elif sid in drn:
            return "elevation"

    # for all possible combinations: determine dt
    resultids = []
    results = []
    for sourceid, source in sources.items():
        for sinkid, sink in sinks.items():
            if sourceid == sinkid:
                continue
            comb = f"{sourceid}-{sinkid}"
            print(comb)

            if comb not in resultids:
                # source in riv: only where stage > bottom elev
                if sourceid in riv:
                    source = source.where(source["stage"] > source["bottom_elevation"])

                dt = _calculate_intra_cell_dt(
                    source_stage=source[_get_stage_name(sourceid)],
                    source_cond=source["conductance"],
                    sink_stage=sink[_get_stage_name(sinkid)],
                    sink_cond=sink["conductance"],
                    eff_volume=eff_volume,
                )

                if not drop_allnan or not dt.isnull().all():
                    results.append(dt)
                    resultids.append(comb)
    dt_all = xr.concat(
        results, pd.Index(resultids, name="combination"), coords="minimal"
    )

    # overall dt
    dt_min = dt_all.min(dim="combination")

    return dt_min, dt_all

