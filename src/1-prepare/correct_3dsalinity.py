
"""
Script to correct 3d salinity distribution, if salinity distribution 
contains interpolation artefacts.

If:
- salinity inversion of > ? g/L
- that is not resting on an aquitard of at least ? d resistance
  ('resting on' defined as aquitard within ? m vertical, and/or ? m horizontal)
- then correct fresh water below inversion to salinity of 
  overlying water

TODO: influence of resistance in meters i/o layers...
"""
import numba
import numpy as np
import xarray as xr
import pandas as pd
from scipy.ndimage import maximum_filter

# from dask.diagnostics import ProgressBar


@numba.njit
def _fill_inversions(cl, c, threshold_cl, threshold_c, out, inversion):
    nlay, nrow, ncol = cl.shape
    for i in range(nrow):
        for j in range(ncol):
            """
            # TODO:finish
            # from bottom upward:
            # look upward until treshold_c is crossed, and while cl increases
            # mark entire range as inversion and apply upper cl concentration
            k = nlay
            while k >= 1:
                li = k
                cli = cl[k, i, j]
                while c[k-1, i, j] < threshold_c and cl[k-1, i, j] > cli:
                    k = k - 1
            """
            out[0, i, j] = cl[0, i, j]
            inversion[0, i, j] = False
            for k in range(1, nlay):
                out[k, i, j] = cl[k, i, j]
                inversion[k, i, j] = False
                if not (np.isnan(cl[k - 1, i, j]) or np.isnan(cl[k, i, j])):
                    if (
                        (out[k - 1, i, j] - out[k, i, j]) > threshold_cl
                    ) and (  # chloride inversion with overlying layer at least thresh_cl
                        (c[k - 1, i, j] < threshold_c)
                        and (
                            c[k, i, j] < threshold_c
                        )  # resistance in overlying or current layer less than tresh_c
                    ):
                        # TODO: apply threshold only at start of inversion, treat all values lower than the starting value as part of inversion
                        # TODO: maybe? inversions that gradually go from saline to fresh now do not pass the threshold...
                        out[k, i, j] = out[k - 1, i, j]
                        inversion[k, i, j] = True
            

@numba.njit
def _nearby_resistance(c, z, threshold_z, out):
    nlay, nrow, ncol = c.shape
    for i in range(nrow):
        for j in range(ncol):
            # from top downward:
            # Note: only extend resistance downward, as we want to prevent salinity 'interpolated' to below an aquitard to be
            # filled downward. Other way around is not a problem
            out[0, i, j] = c[0, i, j]
            for k in range(1, nlay):
                out[k, i, j] = c[k, i, j]
                if not np.isnan(c[k, i, j]):
                    for l in range(0, k):
                        if not (np.isnan(c[l, i, j]) or np.isnan(z[l, i, j])):
                            # what overlying layers are within max distance, and c > c of current layer?
                            if (z[l, i, j] - z[k, i, j] < threshold_z) and (
                                c[l, i, j] > c[k, i, j]
                            ):
                                out[k, i, j] = c[l, i, j]


def fill_inversions(
    cl, c, threshold_cl=1.0, threshold_c=5000.0, window_c=None, test=False
):
    # TODO: proper docstring
    # maybe: include uncertainty?
    """
    Function that finds and fills non-realistic chloride inversions.

    Parameters
    ----------
    cl : 3D xr.DataArray
        The datarray containing chloride concentration. Dimensions ``layer``, ``y``, ``x``
    c : 1D or 3D xr.DataArray
        Datarray containing resistance of layers (d).
    threshold_cl : float
        Chloride threshold (g/L) to consider a lower chloride concentration an inversion. Default: 1.0 g/L.
    threshold_c : float
        Resistance threshold above which an aquitard is considered resistive enough to allow inversions.
    window_c : kernel in (horizontal, vertical), in m!!

    Returns
    -------
    xr.DataArray
        Chloride concentration with non-realistic inversions filled. Dimensions ``layer``, ``y``, ``x``
    xr.DataArray
        Boolean array: is a cell flagged as an non-realistic inversion. Dimensions ``layer``, ``y``, ``x``
    """

    cl = cl.load()
    c = c.load()
    if window_c is not None:
        # c.values = maximum_filter(c.values, )  # TODO: use scipys ndimage filter to better steer filter?
        window_hor, window_vert = window_c
        window_hor = int((window_hor // int(c.dx)) * 2 + 1)
        if window_hor > 0:
            # have large c values extend over larger area
            c = c.rolling(x=window_hor, y=window_hor, center=True, min_periods=1).max()
        if window_vert > 0:
            # have large c values extend over layer below and above
            # c = c.rolling(layer=window_c[1], center=True, min_periods=1).max()
            cvert = xr.full_like(c, np.nan)
            _nearby_resistance(c.values, c.z.values, window_vert, cvert.values)
            c = cvert

    out = xr.full_like(cl, np.nan)
    inv = xr.full_like(cl, np.nan, dtype=bool)
    _fill_inversions(
        np.atleast_3d(cl.values),
        c.values,
        threshold_cl,
        threshold_c,
        out.values,
        inv.values,
    )

    if test:
        return out, inv, cvert
    else:
        return out


# starting easy with model resolution, already aligned TODO switch to native resolution
path_cl = snakemake.input.path_cl  #"data/2-interim/starting_concentration.nc"
path_conductivity = snakemake.input.path_conductivity  #"data/2-interim/conductivity.nc"
path_layermodel = snakemake.input.path_layermodel  #"data/2-interim/layermodel.nc"
path_clcorrected = snakemake.output.path_sconc_out
threshold_cl = 0.5  # chloride concentration in g/L
threshold_c = 2000  # resistance in days
window_c = (1000.0, 25.0)  # ncells horizontal window, nlayers vertical window

# load l,y,x dataarrays, are already aligned
cl = xr.open_dataset(path_cl)["sconc"]
kv = xr.open_dataset(path_conductivity)["kv"]
lay = xr.open_dataset(path_layermodel)
thick = lay["top"] - lay["bot"]
c = thick / kv
c = c.assign_coords(z=(0.5 * lay["top"] + 0.5 * lay["bot"]))
c.name = "C"

"""
# for testing, area in Markermeer, just W of Oostvaardersplassen
x, y = 150_000, 500_000
clsel = cl.sel(
    x=slice(x - 200, x + 200), y=slice(y + 200, y - 200)
)  # , method="nearest")
csel = c.sel(
    x=slice(x - 200, x + 200), y=slice(y + 200, y - 200)
)  # , method="nearest")

test, inv, cnew = fill_inversions(clsel, csel, threshold_cl, threshold_c, window_c, test=True)
df = pd.concat(
    (
        cnew.z.isel(x=0, y=0).to_series(),
        clsel.isel(x=0, y=0).to_series(),
        csel.isel(x=0, y=0).to_series(),
        cnew.isel(x=0, y=0).to_series(),
        test.isel(x=0, y=0).to_series(),
        inv.isel(x=0, y=0).to_series(),
    ),
    axis=1,
)
print(df)
"""
# with ProgressBar:
clcorr = fill_inversions(
    cl, c, threshold_cl=threshold_cl, threshold_c=threshold_c, window_c=window_c
)
clcorr.to_netcdf(path_clcorrected)
