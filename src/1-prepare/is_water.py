"""
This script creates the surface water as 2d input (sea and inland surface water bodies), by following
the following steps:

1. open the geotop surface water file
2. open the shapefile of the Dutch coastline
3. combine the offshore water with inland surface water bodies

Input:
- data/1-external/water/GEOTOP_watervlak.tif
- data/1-external/water/nl_imergis_kustlijn_2018/nl_imergis_kustlijn_2018.shp
- data/2-interim/template.nc

Output:
- data/2-interim/water_masks.nc

"""

import geopandas as gpd
import numpy as np
import xarray as xr
import os
import imod
try:
    from skimage.morphology import binary_dilation
except:
    from scipy.ndimage.morphology import binary_dilation

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
# path water files
path_geotop_water = snakemake.input.path_geotop_water# r"data/1-external/water/GEOTOP_watervlak.tif"
path_sea = snakemake.input.path_coastline#r"data/1-external/water/nl_imergis_kustlijn_2018/nl_imergis_kustlijn_2018.shp"
#
path_3d_template = snakemake.input.path_template#r"data/2-interim/template.nc"
path_2d_template = snakemake.input.path_template_2d #r"data/2-interim/template_2d.nc"
path_water_out = snakemake.output.path_water_out

like = xr.open_dataset(path_3d_template)
like_2d = xr.open_dataset(path_2d_template)

#open template
like = xr.open_dataset(path_3d_template)['template']
like_2d = xr.open_dataset(path_2d_template)['template']

# Water surfaces
geotop_water = (
        xr.open_rasterio(path_geotop_water).squeeze("band", drop=True)
)
geotop_water = geotop_water.where(geotop_water != -2147483647)
geotop_water = imod.prepare.Regridder(method="mean").regrid(geotop_water,like_2d)

is_water = geotop_water == 2.0

# Sea
coastline = gpd.read_file(path_sea)
coastline["stage"] = 0.0
sea_2d = imod.prepare.rasterize(coastline, like=like_2d, column="stage") == 0.0
sea_2d.values = binary_dilation(sea_2d.values)
sea = like.copy(deep=True)
for i in range(len(like.layer)):
        sea[i,:,:] = sea_2d

#is_sea = (sea.where(sea["z"] < 0.0)) == 1

is_water = is_water.where(is_water ==1)
sea_2d = sea_2d.where(sea_2d ==1,np.nan) 
water = is_water.combine_first(sea_2d)  

water_template = xr.Dataset()
water_template['water_2d'] = water
water_template['sea_2d'] = sea_2d
water_template.to_netcdf(path_water_out)