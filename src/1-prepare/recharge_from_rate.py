# -*- coding: utf-8 -*-
"""
This script creates the recharge input file by taking a pre-calculated
idf of mean recharge rate

Input:
- data/1-external/rch/RECHARGE_mean_98_07.IDF
- data/1-external/coastline/wadden_gebied.shp
- data/2-interim/template.nc
- data/2-interim/water_masks.nc

Output:
- data/2-interim/rch.nc
"""
import imod
import numpy as np
import os
from pathlib import Path
import pandas as pd
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

#Paths  
path_bnd = snakemake.input.path_bnd
path_rchlhm_md = snakemake.input.path_rchlhm_md
path_rch_out = snakemake.output.path_rch_out
rch_rate_max = snakemake.params.rch_rate_max

# Now compute m/d
rchlhm_md = imod.idf.open(path_rchlhm_md, pattern="{name}")
rch_mean = rchlhm_md #/ 1000. # imod-wq rate in m/d

# check for high values if recharge > 1 mm/day (original max value is 2.3cm/day than adjust to 1 mm/day)
rch_mean = rch_mean.where((rch_mean < rch_rate_max) | rch_mean.isnull(), rch_rate_max)

bnd = xr.open_dataset(path_bnd)['ibound']
#get 2d first active domain
da = imod.select.layers.upper_active_layer(bnd)
active = bnd.layer==da
active_2d = active.max(dim='layer')
rch_mean = rch_mean.where(active_2d.notnull())

# Create package and write to file
rch = imod.wq.RechargeHighestActive(rate=rch_mean, concentration=0.05)
rch.dataset.to_netcdf(path_rch_out)
