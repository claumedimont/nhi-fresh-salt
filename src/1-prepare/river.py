
# -*- coding: utf-8 -*-
"""
Creates the river input files, by following
the following steps:
1. Create separate river system for each of LHM's river systems
2. devide river input over the modellayers based on the river bottoms
3. assign corresponding density/cl from initial salt concentration input for H river system
4. if infiltrating river: assign LHM conductance * inffactor instead of conductance

Input:
- data/2-interim/template.nc
- data/2-interim/layermodel.nc
- data/2-interim/bnd.nc
- LHM idf river input

Otput:
- data/2-interim/river.nc
"""
import imod
import numpy as np
import xarray as xr
from utils import assign_layer_3d, assign_to_dataset, inner_join_dataarrays
import os
import glob
import gc
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# Paths
path_3d_template = snakemake.input.path_template  #path_interim / "template.nc"
path_2d_template = snakemake.input.path_template_2d #path_interim / "template_2d.nc"
path_layermodel = snakemake.input.path_layermodel #path_interim / "layermodel.nc"
path_bnd = snakemake.input.path_bnd #path_interim / "bnd.nc"
path_conc_2d = snakemake.input.path_conc_2d #path_interim / "starting_concentration.nc"
path_ghb = snakemake.input.path_ghb
path_GHB_dens_LHM = snakemake.input.path_GHB_dens_LHM  #"data/1-external/ghb/ghb_dens_l1.idf"
path_riv_h = snakemake.input.path_riv_h #"data/1-external/riv/hoofdwater/steady-state/riv_h_ss_orig_corr.nc"
path_riv_p = snakemake.input.path_riv_p #"data/1-external/riv/regionaal/steady-state/riv_p_ss_orig_corr.nc"
path_riv_s = snakemake.input.path_riv_s #"data/1-external/riv/regionaal/steady-state/riv_s_ss_orig.nc"
path_riv_t = snakemake.input.path_riv_t #"data/1-external/riv/regionaal/steady-state/riv_t_ss_orig.nc"
path_riv_h1_inff = snakemake.input.path_riv_h1_inff
path_riv_h2_inff = snakemake.input.path_riv_h2_inff
path_riv_p_inff = snakemake.input.path_riv_p_inff
path_riv_s_inff = snakemake.input.path_riv_s_inff
path_riv_t_inff = snakemake.input.path_riv_t_inff

riv_h = xr.open_dataset(path_riv_h)
riv_p = xr.open_dataset(path_riv_p)
riv_s = xr.open_dataset(path_riv_s)
riv_t = xr.open_dataset(path_riv_t)
#path_heads = "data/4-output/V0.1.16-output/heads/head"
do_inffactor = True
rivdrn_as_river = snakemake.params.rivdrn_as_river
path_interim = Path(snakemake.params.path_interim)

river_input = {
    "h1": {
        "stage": riv_h['stage'],
        "bot": riv_h['bottom_elevation'].sel(layer=1),
        "cond": riv_h['conductance'].sel(layer=1),
        "inffact": path_riv_h1_inff,
        "method": "z_then_upper",
        "within_layers": [1, 9],
        "output": path_interim / "river_h.nc",
        "output_drn": path_interim / "river_h_drn.nc",
    },
    "h2": {
        "stage": riv_h['stage'],
        "bot": riv_h['bottom_elevation'].sel(layer=2),
        "cond": riv_h['conductance'].sel(layer=2),
        "inffact": path_riv_h2_inff,
        "within_layers": [11, 15],
    },
    "p": {
        "stage":riv_p['stage'],
        "bot": riv_p['bottom_elevation'],
        "cond": riv_p['conductance'],
        "inffact": path_riv_p_inff,
        "method": "z_then_upper",
        "within_layers": [1, 9],
        "output": path_interim / "river_p.nc",
        "output_drn": path_interim / "river_p_drn.nc",
    },
    "s": {
        "stage":riv_s['stage'],
        "bot": riv_s['bottom_elevation'],
        "cond": riv_s['conductance'],
        "inffact": path_riv_s_inff,
        "method": "z_then_upper",
        "within_layers": [1, 9],
        "output": path_interim / "river_s.nc",
        "output_drn": path_interim / "river_s_drn.nc",
    },
    "t": {
        "stage":riv_t['stage'],
        "bot": riv_t['stage'],  # just LHM fresh: set bot to stage, to prevent infiltration
        "cond": riv_t['conductance'],
        "inffact": path_riv_t_inff,
        "method": "z_then_upper",
        "within_layers": [1, 9],
        "output": path_interim / "river_t.nc",
        "output_drn": path_interim / "river_t_drn.nc",  # unnecessary: system t does not infiltrate
    },
}


def create_river(
    stage_2d,
    bot_2d,
    cond_2d,
    conc_2d,
    dens_2d,
    inff_2d,
    like_2d=None,
    ibound=None,
    top_bot=None,
    method="z_then_upper",
    within_layers=[None, None],
    inff_through_drain=True,
):
    """
    Function to create consistent river xr.Dataset as input for
    imod.wq.River()
    """
    # conform to like_2d
    if like_2d is not None:
        stage_2d = imod.prepare.reproject(stage_2d, like_2d, method="nearest")
        bot_2d = imod.prepare.reproject(bot_2d, like_2d, method="nearest")
        cond_2d = imod.prepare.reproject(cond_2d, like_2d, method="nearest")
        if conc_2d is not None:
            conc_2d = imod.prepare.reproject(conc_2d, like_2d, method="nearest")
        dens_2d = imod.prepare.reproject(dens_2d, like_2d, method="nearest")
        inff_2d = imod.prepare.reproject(inff_2d, like_2d, method="nearest")

    # inner join stage, bot, cond and dens, makes sure all exist everywhere
    stage_2d.name = "stage"
    bot_2d.name = "bot"
    cond_2d.name = "cond"
    dens_2d.name = "dens"
    inff_2d.name = "inff"
    join = dict(stage=stage_2d,
        bot=bot_2d,
        cond=cond_2d,
        dens=dens_2d,
        inff=inff_2d)
    if conc_2d is not None:
        conc_2d.name = "conc"
        join["conc"] = conc_2d

    riv_2d = inner_join_dataarrays(**join)

    # assert bottom <= stage
    riv_2d["bot"] = riv_2d["bot"].where(
        riv_2d["bot"] <= riv_2d["stage"], riv_2d["stage"]
    )

    # assign to 3d layers using method
    assigned = assign_layer_3d(riv_2d["bot"], ibound, top_bot, method, *within_layers)
    riv_3d = riv_2d.where(assigned)

    # reselect only if in ibound and not in GHB
    riv_3d = riv_3d.where(ibound==1)

    # assert all have equal no of nodata cells
    dvars = list(riv_3d.data_vars.keys())
    for k1, k2 in zip(dvars[:-1], dvars[1:]):
        assert riv_3d[k1].isnull().equals(riv_3d[k2].isnull())
    
    return riv_3d.dropna(dim="layer", how="all")

def adjust_conductance_inffactor(river, heads):
    """
    If steady-state heads are below the stage (infiltration),
    adjust conductance of river by multiplying with inffact.
    Expects inffact to be part of river dataset.
    """
    # heads below stage? and stage > bot?
    is_infiltration = (heads < river["stage"]) & (river["stage"] > river["bot"])

    river["cond"] = river["cond"].where(
        ~is_infiltration, river["cond"] * river["inff"]
    )
    return river


# open template, layer model, ibound
like = xr.open_dataset(path_3d_template)["template"]
like_2d = xr.open_dataset(path_2d_template)["template"]
bnd = xr.open_dataset(path_bnd)["ibound"]
conc_2d = xr.open_dataset(path_conc_2d)["sconc_2d"]
ghb = xr.open_dataset(path_ghb)["conductance"]
ghb_2d = ghb.max(dim="layer")
# Layermodel
top_bot = xr.open_dataset(path_layermodel)

# density
dens_2d = (conc_2d *1.316) + 1000

# Different systems: hoofdwater (h1 & h2), primair (p), secundair (s), tertiair (t)

# First: hoofdwater
# Merge the two layers, assign mean level, sum conductance (was divided over two layers)
stage_2d = river_input["h1"]["stage"]
stage_2d = stage_2d.where(ghb_2d.isnull())
cond_1 = river_input["h1"]["cond"]
cond_2 = river_input["h2"]["cond"]
#cond_2d = (
#    (cond_1 + cond_2).combine_first(cond_2).combine_first(cond_1)
#)  # first: sum the two, then add where only 1 exists
bot_1 = river_input["h1"]["bot"]
bot_2 = river_input["h2"]["bot"]
#bot_2d = bot_2.combine_first(bot_1) # combine, take deepest first
inff1_2d = imod.idf.open(river_input["h1"]["inffact"], pattern="{name}")
inff2_2d = imod.idf.open(river_input["h2"]["inffact"], pattern="{name}")
#inff_2d = inff1_2d.combine_first(inff2_2d)
riv_h1 = create_river(
    stage_2d,
    bot_1,
    cond_1,
    conc_2d,
    dens_2d,
    inff1_2d,
    like_2d,
    bnd,
    top_bot,
    river_input["h1"]["method"],
    river_input["h1"]["within_layers"],
)
riv_h2 = create_river(
    stage_2d,
    bot_2,
    cond_2,
    conc_2d,
    dens_2d,
    inff2_2d,
    like_2d,
    bnd,
    top_bot,
    river_input["h1"]["method"],
    river_input["h2"]["within_layers"],
)
riv_h = riv_h1.reindex_like(like).combine_first(riv_h2).dropna(dim="layer", how="all")

if do_inffactor:
    # Create wq Package and write to file
    riv = imod.wq.River(
        stage=riv_h["stage"],
        conductance=(riv_h["cond"] * riv_h["inff"]),
        bottom_elevation=riv_h["bot"],
        concentration=riv_h["conc"],
        density=riv_h["dens"],
    )
    if rivdrn_as_river:
        # Create 'drain' for 'rest' of conductance. Make as river with bot=stage, so flux ends up in bdgriv
        rivdrn = imod.wq.River(
            stage=riv_h["stage"],
            conductance=(riv_h["cond"] * (1.-riv_h["inff"])),
            bottom_elevation=riv_h["stage"],
            density=riv_h["dens"],
        )
    else:
        rivdrn = imod.wq.Drainage(
            elevation=riv_h["stage"],
            conductance=(riv_h["cond"] * (1.-riv_h["inff"])),
        )
    riv.dataset.to_netcdf(river_input["h1"]["output"])
    rivdrn.dataset.to_netcdf(river_input["h1"]["output_drn"])
else:
    # Create wq Package and write to file
    riv = imod.wq.River(
        stage=riv_h["stage"],
        conductance=riv_h["cond"],
        bottom_elevation=riv_h["bot"],
        concentration=riv_h["conc"],
        density=riv_h["dens"],
    )
    riv.dataset.to_netcdf(river_input["h1"]["output"])

del (
    river_input["h1"],
    river_input["h2"],
)  # drop from dict, so it doesn't interfere with following loop
del riv, rivdrn
gc.collect()

# Then: other river systems
for sys in river_input.keys():
    # stage: average of summer and winter
    stage_2d = (river_input[sys])["stage"]
#    stage_s = imod.idf.open(river_input[sys]["stage_summer"], pattern="{name}")
#    stage_w = imod.idf.open(river_input[sys]["stage_winter"], pattern="{name}")
#    stage_2d = (stage_s + stage_w) / 2.0
    stage_2d = stage_2d.where(ghb_2d.isnull())
    # bot: average of summer and winter
    #bot_s = imod.idf.open(river_input[sys]["bot_summer"], pattern="{name}")
    #bot_w = imod.idf.open(river_input[sys]["bot_winter"], pattern="{name}")
    #bot_2d = (bot_s + bot_w) / 2.0
    bot_2d = (river_input[sys])["bot"]
    cond_2d = (river_input[sys])["cond"]
    if 'layer' in cond_2d.dims:
        cond_2d = cond_2d.squeeze('layer',drop=True)
    inff_2d = imod.idf.open(river_input[sys]["inffact"], pattern="{name}")

    rs = create_river(
        stage_2d,
        bot_2d,
        cond_2d,
        None,
        dens_2d,
        inff_2d,
        like_2d,
        bnd,
        top_bot,
        river_input[sys]["method"],
        river_input[sys]["within_layers"],
    )
    
    if do_inffactor and sys != "t":
        # Create wq Package and write to file
        # These systems without concentration, only one system with concentrations is allowed
        riv = imod.wq.River(
            stage=rs["stage"],
            conductance=(rs["cond"] * rs["inff"]),
            bottom_elevation=rs["bot"],
            #        concentration=rs["conc"],
            density=rs["dens"],
        )
        if rivdrn_as_river:
            rivdrn = imod.wq.River(
                stage=rs["stage"],
                conductance=(rs["cond"] * (1.-rs["inff"])),
                bottom_elevation=rs["stage"],  # set bot to stage, so not allowed to infiltrate (but flux ends up in bdgriv)
                density=rs["dens"],
            )
        else:
            rivdrn = imod.wq.Drainage(
                elevation=rs["stage"],
                conductance=(rs["cond"] * (1.-rs["inff"])),
            )
        riv.dataset.to_netcdf(river_input[sys]["output"])
        rivdrn.dataset.to_netcdf(river_input[sys]["output_drn"])
        del riv, rivdrn
    else:
        # Create wq Package and write to file
        # These systems without concentration, only one system with concentrations is allowed
        riv = imod.wq.River(
            stage=rs["stage"],
            conductance=rs["cond"],
            bottom_elevation=rs["bot"],
            #        concentration=rs["conc"],
            density=rs["dens"],
        )
        riv.dataset.to_netcdf(river_input[sys]["output"])
        del riv
    gc.collect()
