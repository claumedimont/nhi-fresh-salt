# -*- coding: utf-8 -*-
"""
Creates the Wel input files, by following
the following steps:
1. Read LHM fresh wel package input files
2. Assign to LHM freshsalt layers based on screen information
3. If no screen information: assign based on LHM fresh layer,
    assuming a screen between 5 and 20 m, placed in the upper
    part of the aquifer
4. Divide discharge over layers based on kh. Set Q to zero when 
    kD < kD_min. If necessary extend assigned layers downward to
    accomodate Q
5. Calculate stability constraint. Divide well Q over horizontally
    adjacent cells

Input:
- data/2-interim/top_bot_orig.nc
- data/2-interim/layermodel.nc
- data/2-interim/conductivity.nc
- LHM ipf wel input

Otput:
- data/2-interim/wel.nc
"""
import os

import imod
import numba
import numpy as np
import pandas as pd
import xarray as xr
import re
from pathlib import Path


# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# input
"""
path_topbot_lhmfresh = "data/2-interim/top_bot_orig.nc" #
path_layermodel = "data/2-interim/layermodel.nc" #
path_conductivity = "data/2-interim/conductivity.nc" #
wel_path = Path("data/1-external/wel") #
path_interim = Path("data/2-interim" ) #
"""
path_topbot_lhmfresh = snakemake.input.path_topbot_lhmfresh
path_layermodel = snakemake.input.path_layermodel
path_conductivity = snakemake.input.path_conductivity
wel_path = Path(snakemake.input.path_wel)
path_interim = Path(snakemake.params.path_interim)
well_screen_minimum = 1.0  # m
well_screen_maximum = 20.0  # m
cellsize = 250.0
kh_min = 1.0  # m/d do not place wells in layers with a kh < khmin
kd_min = 10.0  # m2/d extend filter to meet minimum kd
tmin = 20  # adjust wells if stability constraint dt falls below tmin


@numba.njit
def _overlap(a, b):
    return max(0, min(a[1], b[1]) - max(a[0], b[0]))


@numba.njit
def Q_per_layer(
    filtertopi, filterboti, layertopi, layerboti, Qi, khi, kd_min=10.0, kh_min=1.1
):
    nlayer = layertopi.size
    layer = np.arange(1, nlayer + 1)
    overlaps = np.zeros(nlayer)
    missing = (
        np.isnan(khi) | np.isnan(layertopi) | np.isnan(layerboti)
    )  # positve and negative wells
    khi[missing] = 0.0
    thick = layertopi - layerboti
    kdi = khi * thick
    kdi[missing] = 0.0
    for i in range(nlayer):
        if missing[i]:
            continue
        overlaps[i] = _overlap((filterboti, filtertopi), (layerboti[i], layertopi[i]))

    ovlapscp = overlaps.copy()
    # only aquifers
    overlaps[khi < kh_min] = 0.0

    # if total overlap is now 0: place screen in next available aquifer
    if np.all(overlaps == 0):
        # start from first overlap, find first non-overlap layer with sufficient kh
        for i in range(np.argmax(ovlapscp), nlayer):
            if ovlapscp[i] == 0 and (khi[i] > kh_min or kdi[i] > kd_min):
                overlaps[i] = ovlapscp.sum()
                break

        # if overlaps is still all zeros: just revert to calculated overlaps
        if np.all(overlaps == 0):
            overlaps = ovlapscp.copy()

    # throw out small parts (< 5%) of screen
    overlap_pct = overlaps / overlaps.sum()
    overlaps[overlap_pct < 0.05] = 0.0

    # divide Q over layers, weighted by kD
    kd_effective = overlaps * khi
    proportion = kd_effective / kd_effective.sum()
    Qlayer = Qi * proportion
    Qlayer = Qlayer[~missing]
    layer = layer[~missing]
    return Qlayer, layer, layertopi[~missing], layerboti[~missing]


@numba.njit
def _place_wells(x, y, filtertop, filterbot, Q, tops, bots, khs, kd_min, kh_min):
    npoints, nlayer = tops.shape
    n_allocate = nlayer * npoints
    xs = np.zeros(n_allocate)
    ys = np.zeros(n_allocate)
    Qlayers = np.zeros(n_allocate)
    layers = np.zeros(n_allocate, dtype=np.int32)
    laytop = np.zeros(n_allocate, dtype=np.float32)
    laybot = np.zeros(n_allocate, dtype=np.float32)
    valid = np.zeros(n_allocate, dtype=np.bool_)

    count = 0
    for i in range(npoints):
        # print(i)
        t = tops[i]
        b = bots[i]
        if np.isnan(t).all():
            continue
        if np.isnan(b).all():
            continue
        Qlayer, layer, layt, layb = Q_per_layer(
            filtertopi=filtertop[i],
            filterboti=filterbot[i],
            layertopi=t,
            layerboti=b,
            Qi=Q[i],
            khi=khs[i],
            kd_min=kd_min,
            kh_min=kh_min,
        )
        layersize = layer.size
        xs[count : count + layersize] = x[i]
        ys[count : count + layersize] = y[i]
        Qlayers[count : count + layersize] += Qlayer
        layers[count : count + layersize] = layer
        laytop[count : count + layersize] = layt
        laybot[count : count + layersize] = layb
        valid[count : count + layersize] = True
        count += nlayer

    return xs, ys, Qlayers, layers, laytop, laybot, valid


def place_wells(
    idcode, x, y, filtertop, filterbot, Q, layertop, layerbot, kh, kd_min, kh_min
):
    nlayer, _, _ = layertop.shape
    indices = imod.select.points_indices(layertop, x=x, y=y)
    tops = layertop.values[:, indices["y"], indices["x"]].T
    bots = layerbot.values[:, indices["y"], indices["x"]].T
    khs = kh.values[:, indices["y"], indices["x"]].T

    new_x, new_y, new_Q, new_layer, new_top, new_bot, valid = _place_wells(
        x=x.values,
        y=y.values,
        filtertop=filtertop.values,
        filterbot=filterbot.values,
        Q=Q.values,
        tops=tops,  # .values,
        bots=bots,  # .values,
        khs=khs,  # .values,
        kd_min=kd_min,
        kh_min=kh_min,
    )

    df = pd.DataFrame()
    df["id"] = np.repeat(idcode.values, nlayer)[valid]
    df["x"] = new_x[valid]
    df["y"] = new_y[valid]
    df["Q"] = new_Q[valid]
    df["filtertop"] = np.repeat(filtertop.values, nlayer)[valid]
    df["filterbot"] = np.repeat(filterbot.values, nlayer)[valid]
    df["Q_org"] = np.repeat(Q.values, nlayer)[valid]
    df["layer"] = new_layer[valid]
    df["layertop"] = new_top[valid]
    df["layerbot"] = new_bot[valid]
    return df


# read all wel files
# do not use glob function of imod.ipf.read to store original filenames
# either:
# - ipf has filter information: use as top and bot, let imod-seawat assign layer, or:
# - ipf only has LHM layer information, set top and bot to top and bot of original LHM layer
dfs = []
pattern = re.compile(r".+_[lL]([0-9]+).*")
for fn in wel_path.glob("*.ipf"):
    df = imod.ipf.read(fn)
    df["fn_orig"] = fn.stem
    m = pattern.match(fn.stem)
    if m:
        df["fn_layer"] = int(m[1])
    else:
        df["fn_layer"] = None
    # fix first five column names, as they are used by imod as X,Y,Q,Z1,Z2
    df.columns = ["LHMfs_X", "LHMfs_Y", "LHMfs_Q", "LHMfs_Z1", "LHMfs_Z2"] + list(
        df.columns[5:]
    )  # proposition to make sure no overlap with other column names
    dfs.append(df)
df = pd.concat(dfs, axis=0, ignore_index=True, sort=False)

total_volume_check1 = df["LHMfs_Q"].sum()
print(total_volume_check1)

# conform order to imod-seawat
df = df.reindex(
    columns=[
        "LHMfs_X",
        "LHMfs_Y",
        "LHMfs_Q",
        "LHMfs_Z1",
        "LHMfs_Z2",
        "fn_layer",
        "fn_orig",
    ]
)

# conform names to imod-seawat
df = df.rename(
    columns={
        "LHMfs_X": "X",
        "LHMfs_Y": "Y",
        "LHMfs_Q": "Q",
        "LHMfs_Z1": "TOP",
        "LHMfs_Z2": "BOT",
        "fn_layer": "LHMfresh_layer",
        "fn_orig": "LHMfresh_fn",
    }
)
total_volume_check2 = df["Q"].sum()
print(total_volume_check2)


# select top and bot of top_bot_orig where layer code is present
top_bot_lhmfresh = xr.open_dataset(path_topbot_lhmfresh)
df_sel = df.loc[~df["LHMfresh_layer"].isnull()]
idx = imod.select.points_indices(
    top_bot_lhmfresh["top"],
    x=df_sel["X"],
    y=df_sel["Y"],
    layer=df_sel["LHMfresh_layer"],
)
laytop = top_bot_lhmfresh["top"].isel(**idx)
laybot = top_bot_lhmfresh["bot"].isel(**idx)
laytop = laytop.drop(("dx", "dy", "x", "y", "layer")).to_series()
laybot = laybot.drop(("dx", "dy", "x", "y", "layer")).to_series()
df.loc[df_sel.index, "laytop"] = laytop.values  # .values b/c index is not aligned
df.loc[df_sel.index, "laybot"] = laybot.values

# apply minimal and maximum well screen length, from laytop
# (assume filt_top/bot in original files are correct)
screenlength = df["laytop"] - df["laybot"]
# screenlength = screenlength.where(
#    screenlength > well_screen_minimum, well_screen_minimum
# )
screenlength = screenlength.where(
    screenlength < well_screen_maximum, well_screen_maximum
)
df["laybot"] = df["laytop"] - screenlength

# OVERRIDE! TOP and BOT column if layer is hard set in filename
df["topbot_from_layer"] = 0
df.loc[df_sel.index, "topbot_from_layer"] = 1
df.loc[df_sel.index, "TOP"] = df.loc[df_sel.index, "laytop"]
df.loc[df_sel.index, "BOT"] = df.loc[df_sel.index, "laybot"]
df["idcode"] = df.index.map(str)
df["filtertop"] = df["TOP"]
df["filterbot"] = df["BOT"]

# apply minimum screenlength
screenlength = df["TOP"] - df["BOT"]
screenlength = screenlength.where(
    screenlength > well_screen_minimum, well_screen_minimum
)
df["filterbot"] = df["TOP"] - screenlength

# throw out zero rate wells
df = df.loc[df["Q"] != 0.0]

total_volume_check3 = df["Q"].sum()
print(total_volume_check3)

# df2 = df.copy()

# df = df2  # .iloc[:1000, :]
df.to_csv(path_interim / "wells_before_placement.csv")

# top_bot of LHM fresh-salt
top_bot = xr.open_dataset(path_layermodel)
layertop = top_bot["top"].compute()
layerbot = top_bot["bot"].compute()
conductivity = xr.open_dataset(path_conductivity)
kh = conductivity["kh"]

out_df = place_wells(
    idcode=df["idcode"],
    x=df["X"],
    y=df["Y"],
    filtertop=df["filtertop"],
    filterbot=df["filterbot"],
    Q=df["Q"],
    layertop=layertop,
    layerbot=layerbot,
    kh=kh,
    kd_min=kd_min,
    kh_min=kh_min,
)
total_volume_check4 = out_df["Q"].sum()
print(total_volume_check4)

out_df = out_df[out_df["Q"] != 0]
out_df = out_df[~out_df["Q"].isnull()]
wel = out_df[["id", "x", "y", "Q", "layer"]]

out_df.round(2).to_csv(path_interim / "wells_not_corrected_extradata.csv")
wel.to_csv(path_interim / "wells_not_corrected.csv")

# check which wells lost Q or have fallen off
out_df["id"] = out_df["id"].astype(int)
out_dfsum = out_df.groupby("id").sum()
compare = df.join(out_dfsum, rsuffix="_out")
diffs = compare.loc[compare["Q"].round(2) != compare["Q_out"].round(2)]
diffs.to_csv(path_interim / "wells_differences.csv")

# Check stability constraint for wells and adjust where timestep is too small
porosity = 0.35
test_stability = imod.evaluate.stability_constraint_wel(wel, top_bot, porosity)
print(
    test_stability.qs.min(),
    test_stability.qs.max(),
    test_stability.dt.min(),
    test_stability.dt.max(),
)

need_corr = wel[wel.dt < tmin]

dx_corr = [
    cellsize,
    cellsize,
    0.0,
    -cellsize,
    -cellsize,
    -cellsize,
    0.0,
    cellsize,
] * len(need_corr)
dx_corr = np.asarray(dx_corr)
dy_corr = [
    0.0,
    -cellsize,
    -cellsize,
    -cellsize,
    0.0,
    cellsize,
    cellsize,
    cellsize,
] * len(need_corr)
dy_corr = np.asarray(dy_corr)

need_corr = pd.concat([need_corr] * 8, ignore_index=True).sort_values(by=["Q"])
# need_corr = need_corr.drop(['Unnamed: 0', 'id'], axis=1).reset_index(drop=True)
need_corr.loc[:, "Q"] = need_corr["Q"] * (1 / 9)
need_corr["dx"] = dx_corr
need_corr["dy"] = dy_corr

need_corr["x"] = need_corr["x"] + need_corr["dx"]
need_corr["y"] = need_corr["y"] + need_corr["dy"]
need_corr = need_corr.drop(["dx", "dy"], axis=1).reset_index(drop=True)

wel.loc[:, "Q"] = wel["Q"].where(wel["dt"] > tmin, other=wel["Q"] * (1 / 9))


wells_corrected = wel.append(need_corr, sort=False).reset_index(drop=True)
wells_corrected["id"] = wells_corrected.index

test_stability = imod.evaluate.stability_constraint_wel(wel, top_bot, porosity)
if test_stability.dt.min() < tmin:
    print(f"Smallest dt ({test_stability.dt.min():.2f}) still below tmin ({tmin})")

total_volume_check5 = wells_corrected["Q"].sum()
print(total_volume_check5)

# imod.ipf.save("data/3-input/wel/well", out_df)
wells_corrected.to_csv(path_interim / "wells_corrected.csv")

# Create wq package and write
wel = imod.wq.Well(
    id_name=wells_corrected["id"],
    x=wells_corrected["x"],
    y=wells_corrected["y"],
    layer=wells_corrected["layer"],
    rate=wells_corrected["Q"],
    save_budget=True,
)
wel.dataset.to_netcdf(path_interim / "wel.nc")
wel.save(path_interim / "wel_input")

# Make sure total volume equals input data total volume
total_volume_welcorr = float(wel["rate"].sum(dim=["index"]).compute())
volume_deviation_pct = (
    (total_volume_welcorr - total_volume_check1) / total_volume_check1 * 100.0
)
print("--------------------------------")
print("Resultaten aanmaken WEL package:")
print(
    f"Totaal volume orig: {total_volume_check1:.1e}\nTotaal volume wel:  {total_volume_welcorr:.1e}\nAfwijking: {volume_deviation_pct:.3f}%"
)
assert abs(volume_deviation_pct) < 0.5  # 0.5% afwijking
