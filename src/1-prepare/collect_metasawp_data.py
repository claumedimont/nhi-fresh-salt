from dask.diagnostics import ProgressBar
import geopandas as gpd
import imod
import numpy as np
import os
import pathlib
import pandas as pd
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

dirs = [
    r"data/1-external/metaswap/run_NHI4.1.1_2009_2018"
]

params = [
"msw_Ebs",
"msw_qinf",
"msw_Tact",
"bdgqmodf"
]

for p in params:
    print(p)
    das = []
    for d in dirs:
        print(d)
        da = imod.idf.open(pathlib.Path(d, p, f"{p}*L1.idf"))
        #da = da.sel(x=slice(70_000, 100_000), y=slice(475_000, 445_000))
        das.append(da)
    outda = xr.concat(das, dim="time")
    outda.name = p
    outda.to_netcdf(f"data/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_{p}.nc")
