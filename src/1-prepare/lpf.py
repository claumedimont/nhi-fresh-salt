# -*- coding: utf-8 -*-
"""
This script creates the lpf package, by following
the following steps:
1. Reading Kh and Kv from conductivity.nc
2. Create specific storage (.15 / dz in top active layer, 1e-5 in deeper layers)

Input:
- data/2-interim/conductivity.nc
- data/2-interim/layermodel.nc
- data/2-interim/template.nc
- data/2-interim/bnd_corr.nc

Output:
- lpf.nc
"""
import os
import xarray as xr
import imod

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# open paths
#path_layermodel = "data/2-interim/layermodel_corr.nc"
#path_conductivity = "data/2-interim/conductivity_corr.nc"
path_layermodel = snakemake.input.path_layermodel    #"data/2-interim/layermodel.nc"
path_conductivity = snakemake.input.path_conductivity    #"data/2-interim/conductivity.nc"
path_3d_template = snakemake.input.path_template    # "data/2-interim/template.nc"
path_bnd = snakemake.input.path_bnd_corr    #"data/2-interim/bnd_corr.nc"
path_lpf_out = snakemake.output.path_lpf_out
phreatic_layers = list(range(1,11))

# conductivity
conductivity = xr.open_dataset(path_conductivity)
kh = conductivity["kh"]
kv = conductivity["kv"]

# open template, layermodel and ibound
like = xr.open_dataset(path_3d_template)["template"]
dz = xr.open_dataset(path_layermodel)["dz"]
bnd = xr.open_dataset(path_bnd)["ibound"]

# add specific storage for transient run
# for phreatic: divide specific yield by layer thickness
is_phreatic = like.layer.isin(phreatic_layers)
is_active = (bnd!=0)
specific_storage = is_active.astype(int) * 1.0e-5
specific_storage = specific_storage.where(~is_phreatic, 0.15 / dz).where(is_active)
specific_storage = specific_storage.where(is_active)

lpf = imod.wq.LayerPropertyFlow(
    k_horizontal=kh.where(is_active),
    k_vertical=kv.where(is_active),
    specific_storage=specific_storage,
    head_dry= -9999.0,
)
lpf.dataset.to_netcdf(path_lpf_out)
