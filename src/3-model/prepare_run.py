# -*- coding: utf-8 -*-
"""
This script prepares the iMOD-WQ model for easy running, 
by following the following steps:
0. Do a git status check
1. Check if modelname is present as a git tag
2. Checkout -b <modelname>   <- are filedates different, causing a snakemake recalc? then maybe commit and tag?
3. dvc? TODO
4. get commit id or tag and store as file in rundir
5. copy runfile to rundir
6. zip data/3-input and data/4-rundir into zip 
7. copy to p-drive or S3

Output:
- zipped iMOD-WQ model input + runfile
"""
import os
import shutil
from zipfile import ZipFile
import datetime
from pathlib import Path
from git import Repo, GitCommandError
import boto3
import sys
import threading


# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

repo = Repo(".")
assert not repo.bare  # fail if not in git repo


modelname = snakemake.params[0]
upload = snakemake.params[1]
# modelname = "V0.0.12"


def check_tag(tag, repo):
    # check existence of git tag
    for t in repo.tags:
        if t.name.lower() == tag.lower():
            return False
    return True


def stage_changes(repo):
    # assert not on master
    assert (
        not repo.active_branch.name == "master"
    ), f"Not allowed to commit to master..."

    # assert no untracked files in repo?
    assert not len(
        repo.untracked_files
    ), f"You have untracked files in you working area. Clean working area first"

    gitstage = repo.index
    # check for files to-be-committed:
    diff = gitstage.diff(None)
    files = []
    if len(diff):
        for d in diff:
            if d.change_type in ["M"]:  # stage only modified files
                fn = d.b_rawpath.decode("utf-8")
                print(f"Staging {fn}")
                files.append(fn)
            else:
                print(f"Not staging {d.b_rawpath}")
        gitstage.add(files)
    return files


def commit_and_tag(tag, repo):
    # commit and tag current commit
    # return commit code
    # pull first
    try:
        pullinfo = repo.remotes.origin.pull()
        for pi in pullinfo:
            assert pi.flags != pi.ERROR and pi.flags != pi.REJECTED
    except (AssertionError, GitCommandError):
        raise AssertionError("Git pull failed, use commandline git pull and re-run script")
    
    gitstage = repo.index
    gitstage.commit(f"Model version {tag}, tagging as {tag.lower()}")

    # tag the last commit
    repo.create_tag(tag.lower(), message=f"Model version {tag}")

    # return the last commit
    return repo.head.commit


def push_remote(repo):
    repo.remotes.origin.push(tags=True)


def files_in_dir(path):
    """iterator returns all files and folders from path as absolute path string
    """
    for child in Path(path).iterdir():
        yield child
        if child.is_dir():
            for grand_child in files_in_dir(child):
                yield grand_child


class ProgressPercentage(object):
    """from https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-uploading-files.html"""

    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify, assume this is hooked up to a single filename
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s  %s / %s  (%.2f%%)"
                % (self._filename, self._seen_so_far, self._size, percentage)
            )
            sys.stdout.flush()


# assert modelname not already present as a tag
assert check_tag(modelname, repo), f"Proposed tag {modelname.lower()} already exists..."

# stage changes, commit
staged = stage_changes(repo)
commit = commit_and_tag(modelname, repo)

with open("data/4-output/metadata.txt", "w") as f:
    f.write(f"Modelname: {modelname}\n")
    f.write(f"Created on: {datetime.datetime.utcnow()}\n")
    f.write(f"Created by: {os.getlogin()}\n")
    f.write(f"Git commit: {commit.name_rev}\n")
    f.write(f"Git tag: {modelname}\n")

# copy runfile to rundir
shutil.copyfile(f"data/3-input/{modelname}.run", f"data/4-output/{modelname}.run")

print(f"Zipping modelinput to {modelname}_modelinput.zip")
# zip data/3-input and data/4-output
with ZipFile(f"{modelname}_modelinput.zip", "w") as z:
    # iterate through data/3-input and data/4-output and add to zip
    # alternatively: query model for filenames?
    z.write("data/3-input")
    for fn in files_in_dir("data/3-input"):
        z.write(fn)
    z.write("data/4-output")
    z.write(f"data/4-output/{modelname}.run")
    z.write("data/4-output/metadata.txt")

if upload:
    print(f"Uploading {modelname}_modelinput.zip to s3://nhi-fresh-salt/")
    # copy to aws cli
    s3 = boto3.resource("s3")
    bucket = s3.Bucket("nhi-fresh-salt")
    bucket.upload_file(
        Filename=f"{modelname}_modelinput.zip",
        Key=f"{modelname}_modelinput.zip",
        Callback=ProgressPercentage(f"{modelname}_modelinput.zip"),
    )

# push to remote
push_remote(repo)

