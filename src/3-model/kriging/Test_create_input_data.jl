using CSV
using LinearAlgebra
using DataFramesMeta

# Setting threads
num_cores = Int(Sys.CPU_THREADS)
BLAS.set_num_threads(num_cores)

include("Test_functions.jl")

# Indicator thresholds
indicators = [150, 500, 1000, 3000, 5000, 10000, 15000]

# Read credentials PostgreSQL
user = "zitman"
password = open(joinpath(@__DIR__, "credentials_zoetzout.txt")) do file
    read(file, String)
end

# Read data from POSTGIS
@time postgis_Data = read_postgis(user, password)
@time freshem_Data = load_freshem(user, password)

# Subset to test area with 20km buffer: xmn=87250, ymn=464000, xmx=154000, ymx=501000
postgis_Data = @linq postgis_Data |> where(
    :x .<= 174250,
    :x .>= 67250,
    :y .<= 521000,
    :y .>= 444000,
) |> select(:)

# Calculate indicator values for the data
indicators_values = set_indicators(
    postgis_Data.value,
    indicators,
    postgis_Data.variance,
)

df_ind = hcat(postgis_Data[:, [:x, :y, :z]], indicators_values)

# Calculate indicator values for freshem
@time df_fresh_ind = indicators_freshem(freshem_Data, indicators)

Data = vcat(df_ind, df_fresh_ind)

# Write data
CSV.write("Input_Data.csv", Data)
