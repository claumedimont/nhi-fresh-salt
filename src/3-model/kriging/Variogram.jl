using CSV
using Plots
using Distributed
using StatsFuns
using LinearAlgebra
using TableView
using DataFramesMeta

# Setting threads
num_cores = Int(Sys.CPU_THREADS)
BLAS.set_num_threads(num_cores)

include("functions.jl")

# Indicator thresholds
indicators = [150, 500, 1000, 3000, 5000, 10000, 15000]

# Read credentials PostgreSQL
user = "zitman"
password = open(joinpath(@__DIR__, "credentials_zoetzout.txt")) do file
    read(file, String)
end

# Read data from POSTGIS
@time postgis_Data = read_postgis(user, password)

# Remove softdata from the dataset
postgis_Data = postgis_Data[postgis_Data[:, :type].!=3, :]

# Calculate indicator values for the data
indicators_values = set_indicators(
    postgis_Data.value,
    indicators,
    postgis_Data.variance,
    false,
)

Data = hcat(postgis_Data[:, [:x, :y, :z]], indicators_values)

path = joinpath(@__DIR__, "variograms")

function variograms(Data, indicators, path)
    df = DataFrame()
    for i = 1:length(indicators)
        varname = "I_$(indicators[i])"
        println("running now indicator $varname")
        append!(df, R_variogram(Data, varname, true, path))
    end
    return df
end

variogram = variograms(Data, indicators, path)

# Write data
CSV.write("vgm_values_nofreshem.csv", variogram)
