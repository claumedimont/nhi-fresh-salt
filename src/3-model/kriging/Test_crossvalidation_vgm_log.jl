using CSV
using LinearAlgebra
using DataFrames
using LibPQ, Tables
using DataFramesMeta
using StatsBase
using Plots

# Setting threads
num_cores = Int(Sys.CPU_THREADS)
BLAS.set_num_threads(num_cores)

include("Test_functions.jl")

Test_data = CSV.read(
   joinpath(@__DIR__, "Data/Input/Validatie_Data_Lognorm_10.csv");
   delim = ',',
)

filename = joinpath(@__DIR__, "Data\\Output\\Chloride_lognl_25_50_75.nc")
Chloride_lognl = ncread(filename, "Chloride")
filename = joinpath(@__DIR__, "Data\\Output\\Chloride_logvgm1_25_50_75.nc")
Chloride_logvgm1 = ncread(filename, "Chloride")
filename = joinpath(@__DIR__, "Data\\Output\\Chloride_logvgm7_25_50_75.nc")
Chloride_logvgm7 = ncread(filename, "Chloride")

# Calculate MAE, RMSE, COR & ME
function stats(Data, testdata)
   df = DataFrame()
   for i = 1:3
      x = find_nearest_predicted_value(Data[i, :, :, :], testdata)
      append!(df, crossstats(x.value, x.pred_value))
   end
   df[:percentage] = [25, 50, 75]
   return df
end

# Crossvalidation results
log_nl = stats(Chloride_vgm_nl, Test_data)
log_nl[:name] = "vgm_nl"
log_vgm1 = stats(Chloride_vgm1, Test_data)
log_vgm1[:name] = "vgm1"
log_vgm7 = stats(Chloride_vgm7, Test_data)
log_vgm7[:name] = "vgm7"
diff_nl = convert(Matrix, log_nl[:, 1:end-2]) - convert(Matrix, log_vgm7[:, 1:end-2])
diff_nl = DataFrame(
   MAE = diff_nl[:, 1],
   RMSE = diff_nl[:, 2],
   COR = diff_nl[:, 3],
   ME = diff_nl[:, 4],
   percentage = [25, 50, 75],
   name = "diff_log_nl_vgm7",
)
diff = convert(Matrix, log_vgm1[:, 1:end-2]) - convert(Matrix, log_vgm7[:, 1:end-2])
diff = DataFrame(
   MAE = diff[:, 1],
   RMSE = diff[:, 2],
   COR = diff[:, 3],
   ME = diff[:, 4],
   percentage = [25, 50, 75],
   name = "diff_log_vgm1_vgm7",
)
df = vcat(log_nl,log_vgm1, log_vgm7, diff_nl, diff)

# Write results
CSV.write(joinpath(@__DIR__, "Data\\CV\\Crossvalidation_results_log.csv"), df)
