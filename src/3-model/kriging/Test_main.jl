using CSV
using Plots
using Distributed
using StatsFuns
using LinearAlgebra
using DataFramesMeta

# Setting threads
num_cores = Int(Sys.CPU_THREADS)
BLAS.set_num_threads(num_cores)

include("Test_functions.jl")

# Input Data filenames
filenames = ["Input_Data_Lognorm_90","Input_data_90"]
i_names = ["log","norm"]
j_names = ["vgm_nl","vgm1","vgm7"]

# Variogram values
vgm_nl = DataFrame(
    nugget = fill(0.00635, 7),
    sill = fill(0.04781, 7),
    range = fill(5900, 7),
    anis = fill(120.2, 7),
)
vgm7 = CSV.read(
    joinpath(@__DIR__, "Variograms\\vgm_7_indicators_log\\vgm_values.csv");
    delim = ',',
)
select!(vgm7, Not([:varname]))
vgm1 = DataFrame(
    nugget = fill(vgm7[4, 1], 7),
    sill = fill(vgm7[4, 2], 7),
    range = fill(vgm7[4, 3], 7),
    anis = fill(vgm7[4, 4], 7),
)

# MIK
function run_kriging(Data, indicators, domain, vgm)
    Kriging_Data = zeros(length(indicators), domain...)
    for i = 1:length(indicators)
        varname = "I_$(indicators[i])"
        nugget, sill, rang, anis = vgm[i,:]
        cores = 36
        println("running now indicator $varname")
        @time Kriging_Data[i, :, :, :] = Kriging_gstat(
            Data,
            nugget,
            sill,
            rang,
            anis,
            cores,
            varname,
        )
    end
    return Kriging_Data
end

function main()
    indicators = [150, 500, 1000, 3000, 5000, 10000, 15000]
    domain = [46, 148, 267]

    for i = 1:length(filenames), j = 1:3
        # Load data
        Data = CSV.read(
            joinpath(@__DIR__, "Data\\Input\\$(filenames[i]).csv");
            delim = ',',
        )

        # Select variogram values
        if j == 1
            vgm = vgm_nl
        elseif j == 2
            vgm = vgm1
        elseif j == 3
            vgm = vgm7
        end

        # Perform indicator kriging with the loaded data and set indicators
        @time Kriging_Data = run_kriging(Data, indicators, domain, vgm)

        # Write the 4-dims indicator kriging result to a NetCDF
        write_netcdf_4dims(
            Kriging_Data,
            indicators,
            "indicators_$(i_names[i])_$(j_names[j])",
        )

        # Calculate the chloride concentrations for each percentile
        perc = [25, 50, 75]
        @time Chloride = ordrel(Kriging_Data, indicators, domain, perc)

        # Write the chloride concentrations results as a NetCDF
        write_netcdf_4dims(
            Chloride,
            perc,
            "Chloride_$(i_names[i])_$(j_names[j])",
        )
    end
end

main()
