using CSV
using LinearAlgebra
using DataFrames
using LibPQ, Tables
using DataFramesMeta
using StatsBase
using Random

# Setting threads
num_cores = Int(Sys.CPU_THREADS)
BLAS.set_num_threads(num_cores)

include("Test_functions.jl")

# Read credentials PostgreSQL
user = "zitman"
password = open(joinpath(@__DIR__, "credentials_zoetzout.txt")) do file
    read(file, String)
end

# Read data from POSTGIS
@time postgis_Data = read_postgis(user, password)
@time freshem_Data = load_freshem(user, password)

# Subset to test area with 20km buffer: xmn=87250, ymn=464000, xmx=154000, ymx=501000
postgis_Data = @linq postgis_Data |> where(
    :x .<= 174000,
    :x .>= 67250,
    :y .<= 521000,
    :y .>= 444000,
) |> select(:)

postgis_Data[:,:index] .= 1:size(postgis_Data,1)

data_analyses_gw_subset = @linq postgis_Data |> where(
    :x .<= 154000,
    :x .>= 87250,
    :y .<= 501000,
    :y .>= 464000,
    :type .== 1,
) |> select(:)

# Leave out 10% for cross-validation
subset_index = sample(
    MersenneTwister(1234),
    axes(data_analyses_gw_subset, 1),
    trunc(Int,size(data_analyses_gw_subset,1)*0.10);
    replace = false,
    ordered = true,
)

Test_data = data_analyses_gw_subset[subset_index, :]

Data = @where(postgis_Data, :index .∉[Test_data.index])

# Indicator thresholds
indicators = [150, 500, 1000, 3000, 5000, 10000, 15000]

# Calculate indicator values for the data
indicators_values = set_indicators(Data.value, indicators, Data.variance)
df_ind = hcat(Data[:, [:x, :y, :z]], indicators_values)

# Calculate indicator values for freshem
@time df_fresh_ind = indicators_freshem(freshem_Data, indicators)
Data = vcat(df_ind, df_fresh_ind)

# Write data
CSV.write(joinpath(@__DIR__,"Data\\Input\\Crossvalidation_train_data_90.csv"), Data)
CSV.write(joinpath(@__DIR__,"Data\\Input\\Crossvalidation_test_data_10.csv"), Test_data)
